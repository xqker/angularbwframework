var myApp = {};

myApp.angModule = angular.module('myApp', [
    'ui.router',
    'angControllers',
    'angDirectives',
    'angServices',
    'angFilters',

    /** bw-components */
    'bw.utilities',
    'bw.filters',
    'bw.chart',
    'bw.grid',
    'bw.tooltip',
    'bw.calendar',
    'bw.timeline',
    'bw.map'
]);

myApp.angServices = angular.module('angServices', []);
myApp.angControllers = angular.module('angControllers', []);
myApp.angDirectives = angular.module('angDirectives', []);
myApp.angFilters = angular.module('angFilters', []);


myApp.angModule.config(function($stateProvider, $urlRouterProvider) {

    $urlRouterProvider.otherwise('/grid_demo');

    $stateProvider
        .state('chart_demo', {
            url: '/chart_demo',
            templateUrl: '/public/tpl/pages/chart-demo-page.tpl.html',
            controller: 'ChartDemoPageCtrl'
        })
        .state('grid_demo', {
            url: '/grid_demo',
            templateUrl: '/public/tpl/pages/grid-demo-page.tpl.html',
            controller: 'GridDemoPageCtrl'
        })
        .state('export_demo', {
            url: '/export_demo',
            templateUrl: '/public/tpl/pages/export-demo-page.tpl.html',
            controller: 'ExportDemoPageCtrl'
        })
        .state('annotation_demo', {
            url: '/annotation_demo',
            templateUrl: '/public/tpl/pages/annotation-demo-page.tpl.html',
            controller: 'AnnotationDemoPageCtrl'
        })
        .state('collection_demo', {
            url: '/collection_demo',
            templateUrl: '/public/tpl/pages/collection-demo-page.tpl.html',
            controller: 'CollectionDemoPageCtrl'
        })
        .state('tooltip_demo', {
            url: '/tooltip_demo',
            templateUrl: '/public/tpl/pages/tooltip-demo-page.tpl.html',
            controller: 'TooltipDemoPageCtrl'
        })
        .state('progress_bar_demo', {
            url: '/progress_bar_demo',
            templateUrl: '/public/tpl/pages/progress-bar-demo-page.tpl.html',
            controller: 'ProgressBarDemoPageCtrl'
        })
        .state('calendar_demo', {
            url: '/calendar_demo',
            templateUrl: '/public/tpl/pages/calendar-demo-page.tpl.html',
            controller: 'CalendarDemoPageCtrl'
        })
        .state('timeline_demo', {
            url: '/timeline_demo',
            templateUrl: '/public/tpl/pages/timeline-demo-page.tpl.html',
            controller: 'TimelineDemoPageCtrl'
        })
        .state('map_demo', {
            url: '/map_demo',
            templateUrl: '/public/tpl/pages/map-demo-page.tpl.html',
            controller: 'MapDemoPageCtrl'
        })
        .state('report_demo', {
            url: '/report_demo',
            templateUrl: '/public/tpl/pages/report-demo-page.tpl.html',
            controller: 'ReportDemoPageCtrl'
        })
        .state('endless_list_demo', {
            url: '/endless_list',
            templateUrl: '/public/tpl/pages/endless-list-demo-page.tpl.html',
            controller: 'EndlessListDemoPageCtrl'
        })
        .state('bugs', {
            url: '/bugs',
            templateUrl: '/public/tpl/pages/bugs-fixing-page.tpl.html',
            controller: 'BugsFixingPageCtrl'
        });
});
