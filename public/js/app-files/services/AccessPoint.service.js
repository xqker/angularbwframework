(function(module){

    'use strict';

    module.service('AccessPointService', [
        '$http', '$q', '$location',
        function($http, $q, $location){
            var accessPointService = this;

            Object.defineProperties(accessPointService, {
                absUrl: {
                    value: $location.$$absUrl
                }
            });

            function loadFile (fileName){
                var defer = $q.defer();

                $http.get(fileName)
                    .then(function(res){
                        defer.resolve(res.data);
                    })
                    .catch(function(err){
                        defer.reject(err.data);
                    });

                return defer.promise;
            }

            function sendRequest (data, params){
                var defer = $q.defer();

                $http.post(accessPointService.absUrl, {data: data})
                    .then(function(res){
                        defer.resolve(res.data);
                    })
                    .catch(function(err){
                        defer.reject(err.data);
                    });

                return defer.promise;
            }

            Object.defineProperties(accessPointService, {
                absUrl: {
                    value: $location.$$absUrl
                },

                saveAsPDF: {
                    value: function(data, params){
                        return sendRequest(data, params);
                    }
                },

                getScatterplotData: {
                    value: function(){
                        var defer = $q.defer();

                        $http.get('test.json')
                            .then(function(res){
                                defer.resolve(res.data);
                            })
                            .catch(function(err){
                                defer.reject(err.data);
                            });

                        return defer.promise;
                    }
                },

                getJSONMap: {
                    value: function(territory){
                        return loadFile('maps/' + territory + '-map.json');
                    }
                },
                getCountries: {
                    value: function(){
                        return loadFile('maps/countries.json');
                    }
                },
                getStates: {
                    value: function(){
                        return loadFile('maps/states.json');
                    }
                },
                getCounties: {
                    value: function(){
                        return loadFile('maps/counties.json');
                    }
                },
                generateReport: {
                    value: function(data){
                        return sendRequest(data);
                    }
                }
            });
        }
    ]);


}(myApp.angServices));

