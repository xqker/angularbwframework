(function(module){

    'use strict';

    module.directive('bondSelect', [
        function(){
            return {
                restrict: 'A',
                scope: {
                    collection: '=bondSelect',
                    model: '=bondSelectModel',
                    onSelect: '=onItemSelect'
                },
                transclude: true,
                templateUrl: '/tpl/mocks/bondSelect.tpl.html',
                controller: BondSelectDirectiveCtrl
            };
        }
    ]);

    BondSelectDirectiveCtrl.$inject = ['$scope', '$element', '$attrs', '$timeout', 'd3'];
    function BondSelectDirectiveCtrl ($scope, $element, $attrs, $timeout, d3){
        var self = this;

        this.wrapper = d3.select($element.get(0)).classed('bond-select-wrapper', true);
        this.container = this.wrapper.select('.bond-select-container');

        $scope.isOpened = false;

        $scope.changeSelectedItem = function(item){
            if(!item.isSelected){
                item.select();
                $scope.isOpened = false;
            }
        };
    }

}(myApp.angDirectives));
