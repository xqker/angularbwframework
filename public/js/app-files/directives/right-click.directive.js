(function(module){

    'use strict';

    module.directive('ngRightclick', [
        '$parse',
        function($parse){
            return function($scope, $element, attrs){
                var fn = $parse(attrs['ngRightclick']);
                $element.on('contextmenu', function(e){
                    e.preventDefault();
                    e.stopPropagation();
                    fn(e);

                    console.log(e, fn);
                });
            };
        }
    ]);

}(myApp.angDirectives));
