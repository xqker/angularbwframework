(function(module){

    'use strict';

    module.directive('bondAnnotation', [
        function(){
            return {
                restrict: 'A',
                scope: {
                    model: '=bondAnnotation'
                },
                templateUrl: '/tpl/mocks/bondAnnotation.tpl.html',
                controller: BondAnnotationDirectiveCtrl

            };
        }
    ]);

    BondAnnotationDirectiveCtrl.$inject = ['$scope', '$element', '$attrs', 'BondChartTooltipService', 'd3'];
    function BondAnnotationDirectiveCtrl ($scope, $element, $attrs, BondChartTooltipService, d3){
        var self = this,
            bodyTag = d3.select('body');

        $scope.BondAnnotationTooltip = BondChartTooltipService.get({});
        $scope.BondAnnotationTooltipSettings = {};



        setEventListeners();

        function onTextSelect (e){
            var selection;

            if (window.getSelection) {
                selection = window.getSelection();
            } else if (document.selection) {
                selection = document.selection.createRange();
            }

            if (selection.toString()) {
                console.log(selection.toString());
            }

            $scope.BondAnnotationTooltipSettings.coords = {x: e.clientX,y: e.clientY};
            $scope.BondAnnotationTooltip.refreshView();
        }




        function setEventListeners (){
            bodyTag.on('mousedown', onMouseDown);
            bodyTag.on('mouseup', null);

            function onMouseDown (e){
                bodyTag.on('mouseup', onMouseUp);
                $scope.BondAnnotationTooltip.hide();
            }

            function onMouseUp (e){
                bodyTag.on('mouseup', null);

                var event = d3.event,
                    selection;


                if (window.getSelection) {
                    selection = window.getSelection();
                } else if (document.selection) {
                    selection = document.selection.createRange();
                }

                if (selection.toString()) {
                    $scope.BondAnnotationTooltipSettings.coords = {x: event.clientX,y: event.clientY};
                    $scope.BondAnnotationTooltip.refreshView();
                } else {
                    console.log(2);

                    setEventListeners();
                }
            }
        }


    }


}(myApp.angDirectives));
