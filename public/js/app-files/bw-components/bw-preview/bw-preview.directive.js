(function(module){

    'use strict';

    module.directive('bwPreview', [
        function(){
            return {
                restrict: 'A',
                scope: {
                    model: '=bwPreview',
                    userSettings: '=bwPreviewSettings'
                },
                template:'' +
                    '<div class="bw-preview-container" ng-class="{opened : settings.isOpened}">' +
                        '<div class="bw-preview-canvas"></div>' +
                        '<div class="bw-preview-overlay"></div>' +
                        '<div class="bw-preview-controllers">' +
                            '<div class="bw-preview-buttons">' +
                                '<i class="fa fa-file-pdf-o" aria-hidden="true"></i>' +
                                '<i class="fa fa-th-large"></i>' +
                                '<i class="fa fa-times" ng-click="closePreview()"></i>' +
                            '</div>' +
                            '<div class="bw-preview-pagination">' +
                                '<span>Page {{model.selected.nestedCollection.getSelectedIndex() + 1}} of {{model.selected.nestedCollection.getItems().length}}</span>' +
                            '</div>' +
                            '<div class="bw-preview-dropdown">' +
                                '<div bw-dropdown="model" bw-dropdown-settings="dropdownSettings"></div>' +
                            '</div>' +
                        '</div>' +
                        '<i class="fa fa-angle-left bw-preview-prev-page" ng-click="changePage(model.selected.nestedCollection.getSelectedIndex() - 1)"></i>' +
                        '<i class="fa fa-angle-right bw-preview-next-page" ng-click="changePage(model.selected.nestedCollection.getSelectedIndex() + 1)"></i>' +
                    '</div>' +
                '',
                controller: BWPreviewDirectiveCtrl
            };
        }
    ]);

    BWPreviewDirectiveCtrl.$inject = ['$scope', '$element', 'd3'];

    function BWPreviewDirectiveCtrl($scope, $element, d3) {
        var self = this,
            defaultSettings = {
                opened: false
            };

        self.wrapper = d3.select($element.get(0)).classed('bw-preview-wrapper', true);
        self.container = self.wrapper.select('.bw-preview-container');
        self.canvas = self.container.select('.bw-preview-canvas');
        self.overlay = self.container.select('.bw-preview-overlay');

        Object.defineProperties($scope, {
            settings: {
                get: function(){
                    return angular.merge(defaultSettings, ($scope.userSettings || {}));
                }
            },
            closePreview: {
                value: function(){
                    self.canvas.html('');
                    $scope.userSettings.isOpened = false;
                }
            },
            changePage: {
                value: function(nextIndex){
                    var selectedDocumentCollection = $scope.model.selected.nestedCollection;

                    if(nextIndex >= 0 && nextIndex < selectedDocumentCollection.getItems().length) {
                        selectedDocumentCollection.getItemsByProperty('index', nextIndex)[0].select();
                        self.canvas.html(selectedDocumentCollection.selected.content.html);
                    }
                }
            },
            dropdownSettings: {
                value: {
                    keys: {
                        label: 'documentName'
                    },
                    onSelect: function(item){
                        item.select();
                        $scope.model.selected.nestedCollection.getItems()[0].select();
                        showPreview();
                    }
                }
            }
        });

        $scope.$watch(
            function(){
                return $scope.settings.isOpened;
            },
            function(curr, prev){
                if(curr && (curr !== prev)) {
                    $scope.model.selected.nestedCollection.getItems()[0].select();
                    showPreview();
                }
            }
        );

        function showPreview (){
            var currentScale = 1,
                currentTranslate = [0, 0],
                scaleLimit = [0.5, 2],
                touchStartPosition = [0, 0];

            self.canvas.html($scope.model.selected.nestedCollection.selected.content.html);

            self.container.on('mousewheel', onMouseWheel);

            self.canvas.on('mousedown', onMouseDown);

            updatePreview(currentTranslate, currentScale);

            function onMouseWheel (){
                var event = d3.event,
                    delta = event.wheelDelta < 0 ? -0.1 : 0.1,
                    newScale = currentScale + delta;

                event.preventDefault();

                if(scaleLimit[0] > newScale) {
                    newScale = scaleLimit[0];
                } else if(scaleLimit[1] < newScale){
                    newScale = scaleLimit[1];
                }

                currentScale = newScale;

                updatePreview(currentTranslate, currentScale);
            }

            function onMouseDown (){
                var event = d3.event;

                event.stopPropagation();

                touchStartPosition[0] = event.clientX;
                touchStartPosition[1] = event.clientY;

                self.overlay
                    .style({ display: 'block' })
                    .on('mousemove', onMouseMove)
                    .on('mouseup', onMouseUp);
            }

            function onMouseMove (){
                var event = d3.event,
                    touchMovePosition = [event.clientX, event.clientY];

                currentTranslate[0] -= (touchStartPosition[0] - touchMovePosition[0]);
                currentTranslate[1] -= (touchStartPosition[1] - touchMovePosition[1]);

                touchStartPosition[0] = touchMovePosition[0];
                touchStartPosition[1] = touchMovePosition[1];

                updatePreview(currentTranslate, currentScale);
            }

            function onMouseUp (){
                self.overlay
                    .style({ display: 'none' })
                    .on('mousemove', null)
                    .on('mouseup', null);
            }
        }

        function updatePreview (translate, scale){
            self.canvas.style({
                transform: 'translate(' + [translate[0] + 'px', translate[1] + 'px'] + ')scale(' + scale + ')'
            });
        }
    }

}(angular.module('bw.utilities')));