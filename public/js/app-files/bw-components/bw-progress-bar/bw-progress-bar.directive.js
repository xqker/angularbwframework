(function(module){

    'use strict';

    module.directive('bwProgressBar', [
        function(){
            return {
                restrict: 'A',
                scope: {
                    model: '=bwProgressBar',
                    userSettings: '=bwProgressBarSettings'
                },
                template: '' +
                    '<div class="bw-progress-bar-container"></div>' +
                '',
                controller: BWProgressBarCtrl
            };
        }
    ]);

    BWProgressBarCtrl.$inject = ['$scope', '$element', '$attrs', '$timeout', '$filter', 'd3', 'BWSharedService'];
    function BWProgressBarCtrl ($scope, $element, $attrs, $timeout, $filter, d3, BWSharedService){
        var self = this,
            defaultSettings = {
                rowWidth: 150,
                autoresize: false,
                barHeight: 7,
                railHeight: 3,
                labelFontSize: 12,
                descriptionFontSize: 10,
                flagFontSize: 10,
                labelWidth: 40,
                range: 'userSelect',
                barColor: '#ed7c76',
                railColor: '#eef1f4',
                bgColor: '#ffffff',
                animationSpeed: 100,
                singlePoint: true,
                showDescription: false,
                showFlags: false,
                showLabel: true,
                showHelpers: false,
                watchers: [],

                /** Callbacks */
                onClick: angular.noop
            },
            watchers = [];

        /** DOM-elements */
        var wrapper = d3.select($element.get(0)).classed('bw-progress-bar-wrapper', true),
            container = wrapper.select('.bw-progress-bar-container');

        /** Data & sizes */
        var _data_ = null;

        Object.defineProperties($scope, {
            settings: {
                get: function(){
                    return angular.merge(defaultSettings, ($scope.userSettings || {}));
                }
            }
        });

        Object.defineProperties($scope.model._api_, {
            refreshData: {
                value: function(data){
                    _data_ = data;
                    $scope.model._api_.refreshView();
                }
            },
            refreshView: {
                value: function() {
                    var rows = container.selectAll('.bw-progress-bar-item').data(_data_.getItems());

                    rows.enter().append('div').classed('bw-progress-bar-item', true);

                    rows.exit().remove();

                    rows
                        .attr({
                            class: function(d){
                                refreshRowInner(this);
                                return 'bw-progress-bar-item ' + (d.isSelected ? 'selected' : '');
                            }
                        })
                        .on('click', function(d){
                            $scope.settings.onClick(d);
                        });
                }
            }
        });

        /** Watchers */
        $scope.settings.watchers.forEach(function(propertyToWatch){
            watchers.push(
                $scope.$watch(
                    function(){
                        return BWSharedService.getProperty(propertyToWatch);
                    },
                    function(curr, prev){
                        if(curr !== prev) {
                            $scope.model._api_.refreshView();
                        }
                    }
                )
            );
        });


        function refreshRowInner(row) {
            var elem = d3.select(row),
                datum = row.__data__,
                content = datum.content,
                settings = $scope.settings,
                padding = {
                    top: 10,
                    right: 10,
                    bottom: 10,
                    left: 10
                },
                flagHeight = 20,
                labelWidth = settings.labelWidth,
                fontSize = settings.fontSize,
                rowWidth = settings.autoresize ? row.clientWidth : settings.rowWidth,
                barHeight = settings.barHeight,
                railHeight = settings.railHeight,
                singlePoint = settings.singlePoint,
                svgHeight = d3.max([barHeight, railHeight]) + (settings.showFlags ? (flagHeight * 2) : 0),
                svgWidth = rowWidth,
                railWidth = (svgWidth - (padding.left + padding.right) - (settings.showLabel ? labelWidth : 0)),
                scale = d3.scale.linear()
                    .domain(settings.range === 'auto' ? _data_.getRange(settings.singlePoint) : content.range)
                    .range([0, railWidth]);

            if(!elem.select('.bw-pb-canvas').node()) {
                refreshStructure(elem);
            }

            /** DOM-Elements */
            var canvas = elem.select('.bw-pb-canvas'),
                mainLayer = canvas.select('.bw-pb-main-layer'),
                flagsLayer = canvas.select('.bw-pb-flags-layer'),
                helpersLayer = canvas.select('.bw-pb-helpers-layer'),
                rail = mainLayer.select('.bw-pb-rail'),
                caret = mainLayer.select('.bw-pb-caret');

            canvas.attr({
                width: svgWidth,
                height: svgHeight,
                fill: settings.bgColor
            });

            [mainLayer, flagsLayer, helpersLayer].forEach(function(layer){
                layer.attr({ transform: 'translate(' + [padding.right, svgHeight / 2] + ')' });
            });

            rail.attr({
                width: railWidth,
                height: settings.railHeight,
                transform: 'translate(' + [0, -settings.railHeight / 2] + ')',
                fill: settings.railColor
            });

            caret.attr({
                height: settings.barHeight,
                transform: 'translate(' + [0, -settings.barHeight / 2] + ')',
                fill: settings.barColor
            });

            elem.select('.bw-pb-description')
                .style({
                    'font-size': settings.descriptionFontSize + 'px',
                    display: settings.showDescription ? 'block' : 'none',
                    padding: '0 ' + (padding.right + (settings.showLabel ? labelWidth : 0)) + 'px 0 ' + padding.left + 'px'
                })
                .html('<span>' + content.description + '</span>');

            elem.select('.bw-pb-label')
                .style({
                    'font-size': settings.labelFontSize + 'px',
                    color: '#4b4a4a',
                    bottom: -(settings.labelFontSize * 0.3) + 'px',
                    display: settings.showLabel ? 'inline' : 'none'
                })
                .html(refreshLabel);

            /** Flags */
            var flags = flagsLayer.selectAll('.bw-pg-flag').data(settings.showFlags ? content.flags : []);

            flags.enter().append('g').classed('bw-pg-flag', true);

            flags.exit().remove();

            flags
                .transition()
                .attr({
                    transform: function(d){
                        var thisFlag = d3.select(this),
                            flagPosition = scale(d.value),
                            flagTag = d.shape === 'point' ? 'circle' : 'polyline',
                            arrowFlagOffsetTop = d3.max([settings.barHeight, settings.railHeight]) / 2,
                            arrowHeight = 7,
                            arrowWidth = 6,
                            arrowPoints = [
                                [-(arrowWidth / 2), -(arrowFlagOffsetTop + arrowHeight)],
                                [(arrowWidth / 2), -(arrowFlagOffsetTop + arrowHeight)],
                                [0, -arrowFlagOffsetTop]
                            ].join(' ');

                        if(!this.childNodes.length) {
                            thisFlag.html('' +
                                '<' + flagTag + ' class="bw-pb-flag-shape"></' + flagTag + '>' +
                                '<text class="bw-pb-flag-label"></text>' +
                            '');
                        }

                        thisFlag.select('.bw-pb-flag-shape').attr(
                            d.shape === 'point' ? {
                                cy: 0,
                                r: d.radius,
                                fill: d.color,
                                stroke: '#FFF',
                                'stroke-width': 2
                            } : {
                                points: arrowPoints,
                                stroke: d.color,
                                fill: d.color
                            });

                        thisFlag.select('.bw-pb-flag-label').attr(
                            d.shape === 'point' ?
                            {
                                x: 0,
                                y: -(d.radius + 3),
                                fill: d.color,
                                'font-size': settings.flagFontSize,
                                'text-anchor': 'middle'
                            } : {
                                x: 0,
                                y: -(arrowFlagOffsetTop + arrowHeight + 3),
                                fill: d.color,
                                'font-size': settings.flagFontSize,
                                'text-anchor': 'middle'
                            }
                        ).text(d.label);
                        return 'translate(' + [flagPosition, 0] + ')';
                    }
                })
                .duration(settings.animationSpeed);


            refreshCaretPosition();

            function refreshCaretPosition (){
                var fromPoint = singlePoint ? 0 : scale(content.values[0]),
                    toPoint = singlePoint ? scale(content.value) : scale(content.values[1]),
                    width = (toPoint - fromPoint) || 1;

                caret
                    .transition()
                    .attr({ x: fromPoint, width: width })
                    .duration(settings.animationSpeed);

                /** Helpers */
                helpersLayer.select('.bw-pb-from-point')
                    .attr({
                        y: settings.flagFontSize * 1.5,
                        fill: '#4b4a4a',
                        'font-size': settings.flagFontSize,
                        'text-anchor': 'middle'
                    })
                    .text(_data_.getFormattedValue((singlePoint ? 0 : content.values[0]), content.format))
                    .transition()
                    .attr({ x: fromPoint })
                    .duration(settings.animationSpeed);


                helpersLayer.select('.bw-pb-to-point')
                    .attr({
                        y: settings.flagFontSize * 1.5,
                        fill: '#4b4a4a',
                        'font-size': settings.flagFontSize,
                        'text-anchor': 'middle'
                    })
                    .text(_data_.getFormattedValue((singlePoint ? content.value : content.values[1]), content.format))
                    .transition()
                    .attr({ x: toPoint })
                    .duration(settings.animationSpeed);
            }

            function refreshLabel (){
                return _data_.getFormattedValue((singlePoint ? content.value : content.values[1]), content.format, content.before, content.after);
            }
        }

        function refreshStructure(row) {
            row.html('' +
                '<div class="bw-pb-description"></div>' +
                '<div class="bw-pb-container">' +
                    '<svg class="bw-pb-canvas"></svg>' +
                    '<span class="bw-pb-label"></span>' +
                '</div>' +
            '');

            var rowCanvas = row.select('.bw-pb-container').select('.bw-pb-canvas');

            rowCanvas.append('g').classed('bw-pb-main-layer', true);
            rowCanvas.append('g').classed('bw-pb-flags-layer', true);
            rowCanvas.append('g').classed('bw-pb-helpers-layer', true);

            rowCanvas.select('.bw-pb-main-layer').append('rect').classed('bw-pb-rail', true);
            rowCanvas.select('.bw-pb-main-layer').append('rect').classed('bw-pb-caret', true).attr('width', 0);

            rowCanvas.select('.bw-pb-helpers-layer').append('text').classed('bw-pb-from-point', true).attr('x', 0);
            rowCanvas.select('.bw-pb-helpers-layer').append('text').classed('bw-pb-to-point', true).attr('x', 0);
        }
    }

}(angular.module('bw.utilities')));

