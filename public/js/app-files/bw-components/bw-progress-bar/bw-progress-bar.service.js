(function(module){

    'use strict';

    module.service('BWProgressBarService', [
        '$filter', 'AsyncAPI', 'BWCollectionService', 'd3', 'moment',
        function($filter, AsyncAPI, BWCollectionService, d3, moment){
            var bwProgressBarService = this,
                progressBarCounter = 0;

            Object.defineProperties(bwProgressBarService, {
                createNew: {
                    value: function(){
                        return new BWProgressBarEntity(progressBarCounter++);
                    }
                }
            });

            function BWProgressBarEntity (counter){
                var bwGridEntity = this;
                Object.defineProperties(bwGridEntity, {
                    id: {
                        value: '#bw-progress-bar-' + counter
                    },
                    _api_: {
                        value: AsyncAPI.get({})
                    }
                });
            }

            BWProgressBarEntity.prototype.refreshData = function(data){
                this._api_.applyFn('refreshData', data);
            };

            BWProgressBarEntity.prototype.refreshView = function(){
                this._api_.applyFn('refreshView');
            };

            BWProgressBarEntity.prototype.getNewProgressBarCollection = function(data, parentItem){
                var newProgressBarCollection = (
                    parentItem ? parentItem.createNestedCollection : BWCollectionService.createNew
                )(data);

                newProgressBarCollection.getFormattedValue = function(val, format, before, after){
                    format = format || 'string';

                    var floatRegExp = /^float#(\d+)$/,
                        resp = '';

                    if(floatRegExp.test(format)) {
                        var fractionLen = parseInt(floatRegExp.exec(format)[1]);
                        resp = parseFloat(val.toFixed(fractionLen));
                    } else if(format === 'bigNumber'){
                        resp = $filter('bigNumber')(val);
                    } else {
                        resp = parseInt(val);
                    }

                    return (before ? before : '') + resp + (after ? after : '');
                };

                newProgressBarCollection.getRange = function(singlePoint){
                    var minVal = 0,
                        maxVal = d3.max(newProgressBarCollection.getItems().map(function(item){
                            return singlePoint ? item.content.value : item.content.values[1];
                        }));
                    return [minVal, maxVal * 1.1];
                };

                return newProgressBarCollection;
            };

            /** Random data */
            BWProgressBarEntity.prototype.getRandomProgressBarCollectionData = function( userRange, userFormat ) {
                var bars = ['PERCENTAGE', 'THOUSANDS', 'BILLIONS', 'MONEY', 'SOME SMALL VALUES'],
                    ranges = [ [0, 100], [0, 999999], [0, 999999999999], [0, 999999999], [0, 1] ],
                    befores = [null, null, null, '$', null],
                    afters = ['%', null, null, null, 'pts'],
                    formats = ['string', 'bigNumber', 'bigNumber', 'bigNumber', 'float#2'];

                function getRandomFlags (range, format){
                    var flags = [],
                        rail = range[1] - range[0],
                        valA = range[0] + (Math.random() * rail),
                        valB = Math.random() * range[0];

                    flags.push({
                        name: 'flagA',
                        value: valA,
                        label: format === 'float#2' ? parseFloat(valA).toFixed(2) : $filter('bigNumber')(valA),
                        color: '#5fc08e',
                        shape: 'point',
                        radius: 6,
                        labelPosition: 'top'
                    });

                    flags.push({
                        name: 'flagB',
                        value: valB,
                        label: format === 'float#2' ? parseFloat(valB).toFixed(2) : $filter('bigNumber')(valB),
                        color: ['#eb655f', '#2686c8'][parseInt(Math.random() * 2)],
                        shape: 'arrow',
                        labelPosition: 'top'
                    });

                    return flags;
                }

                return bars.map(function(barName, i){
                    var range = userRange ? userRange : ranges[i],
                        before = userRange ? null : befores[i],
                        after =  userRange ? null : afters[i],
                        format =  userFormat ? userFormat : formats[i],
                        randomB = Math.random() * range[1],
                        randomA = Math.random() * randomB,
                        floatRegExp = /^float#(\d+)$/,
                        value = null,
                        values = [];

                    if(floatRegExp.test(format)) {
                        var fractionLen = parseInt(floatRegExp.exec(format)[1]);
                        value = parseFloat(randomB.toFixed(fractionLen));
                        values = [parseFloat(randomA.toFixed(fractionLen)) , value];
                    } else {
                        value = parseInt(randomB);
                        values = [parseInt(randomA), value];
                    }

                    return {
                        range: range,
                        description: barName,
                        value: value,
                        values: values,
                        before: before,
                        after: after,
                        format: format,
                        flags: getRandomFlags(values, format)
                    }
                });
            };
        }
    ]);


}(angular.module('bw.utilities')));

