(function(module){

    'use strict';

    module.directive('bwTimeline', [
        function(){
            return {
                restrict: 'A',
                scope: {
                    model: '=bwTimeline',
                    userSettings: '=bwTimelineSettings'
                },
                template: '' +
                '<div class="bw-timeline-container">' +
                    '<div class="bw-timeline-events">' +
                        '<div class="bw-timeline-mobile-list">' +
                            '<div class="bw-timeline-flags"></div>' +
                            '<svg class="bw-timeline-scale">' +
                                '<g class="date-axis"></g>' +
                            '</svg>' +
                        '</div>' +
                    '</div>' +
                    '<div class="bw-timeline-slider">' +
                        '<div class="bw-timeline-slider-caret"></div>' +
                    '</div>' +
                    '<div class="bw-timeline-range-selector">' +
                        '<div class="rs-left-panel"></div>' +
                        '<div class="rs-right-panel"></div>' +
                        '<svg class="rs-points-canvas"></svg>' +
                        '<div class="rs-main-panel">' +
                            '<div class="rs-main-handler">' +
                                '<div class="rs-left-handler">' +
                                    '<div class="rs-handler-decoration"></div>' +
                                    '<div class="rs-left-handler-helper"></div>' +
                                '</div>' +
                                '<div class="rs-right-handler">' +
                                    '<div class="rs-handler-decoration"></div>' +
                                    '<div class="rs-right-handler-helper"></div>' +
                                '</div>' +
                            '</div>' +
                        '</div>' +
                    '</div>' +
                    '<svg class="rs-date-scale">' +
                        '<g class="date-axis"></g>' +
                    '</svg>' +
                    '<div class="bw-page-overlay"></div>' +
                '</div>' +
                '',
                controller: BWTimelineDirectiveCtrl
            };
        }
    ]);

    BWTimelineDirectiveCtrl.$inject = ['$scope', '$element', 'd3', 'moment', 'BWSharedService'];
    function BWTimelineDirectiveCtrl ($scope, $element, d3, moment, BWSharedService){
        var self = this,
            watchers = [],
            defaultSettings = {
                step: 100,
                flagHeight: 45,
                watchers: [],
                range: [],

                /** Callbacks */
                onRangeChanged: angular.noop,
                onReady: angular.noop
            };

        self._data_ = null;

        self.wrapper = d3.select($element.get(0)).classed('bw-timeline-wrapper', true);
        self.container = self.wrapper.select('.bw-timeline-container');
        self.eventsContainer = self.container.select('.bw-timeline-events');
        self.eventsMobileContainer = self.eventsContainer.select('.bw-timeline-mobile-list');
        self.sliderContainer = self.container.select('.bw-timeline-slider');
        self.rangeSelector = self.container.select('.bw-timeline-range-selector');
        self.pageOverlay = self.container.select('.bw-page-overlay');

        $scope.userSettings = $scope.userSettings || {};

        Object.defineProperties($scope, {
            settings: {
                get: function(){
                    return angular.merge(defaultSettings, $scope.userSettings);
                }
            }
        });

        Object.defineProperties($scope.model._api_, {
            refreshData: {
                value: function(data){
                    self._data_ = data;
                    $scope.model._api_.refreshView();
                }
            },
            refreshView: {
                value:  function(){
                    var dateRanges = getRanges(),
                        overallMonths = Math.floor(moment.duration(dateRanges[1]).asMonths() - moment.duration(dateRanges[0]).asMonths()),
                        containerWidth = self.eventsContainer.node().clientWidth,
                        innerWidth = overallMonths * $scope.settings.step,
                        innerHeight = 220,
                        renderingData = getRenderingData(),
                        reversedScaleA = d3.scale.linear()
                            .domain([0, containerWidth])
                            .range([0, 1]),
                        reversedScaleB = d3.scale.linear()
                            .domain([0, 1])
                            .range(dateRanges),
                        handlers = self.rangeSelector
                            .select('.rs-main-panel')
                            .select('.rs-main-handler'),
                        leftHelper = handlers.select('.rs-left-handler').select('.rs-left-handler-helper'),
                        rightHelper = handlers.select('.rs-right-handler').select('.rs-right-handler-helper');


                    self.eventsMobileContainer.style({ width: innerWidth + 'px' });

                    self.eventsMobileContainer.select('.bw-timeline-scale').attr({ width: innerWidth });

                    refreshFlags();

                    refreshAxis();

                    refreshSlider();

                    refreshRangeSelector();

                    refreshRangeSelectorPoints();

                    refreshRangeSelectorHelpers();

                    function getRanges (){
                        var items = self._data_.getItems(),
                            min = d3.min(items, function(d){ return d.content.timestamp }),
                            max = d3.max(items, function(d){ return d.content.timestamp }),
                            range = max - min,
                            offset = range * 0.03;

                        return [min - offset, max + offset];
                    }

                    function getRenderingData (){
                        var scale = d3.scale.linear()
                                .domain(dateRanges)
                                .range([0, innerWidth]),
                            pointsScale = d3.scale.linear()
                                .domain(dateRanges)
                                .range([0, containerWidth]),
                            slotHeight = innerHeight / 4;

                        return self._data_.getItems().map(function(item, i){
                            var content = item.content,
                                x = scale(content.timestamp),
                                xx = pointsScale(content.timestamp),
                                y = slotHeight * (i % 4),
                                h = innerHeight - y;

                            return {
                                entity: item,
                                x: x,
                                xx: xx,
                                y: y,
                                h: h,
                                t: content.timestamp,
                                color: content.color,
                                text1: content.event,
                                text2: content.description
                            }
                        });
                    }

                    function refreshFlags (){
                        var flags = self.eventsMobileContainer.select('.bw-timeline-flags').selectAll('.bw-timeline-flag').data(renderingData);

                        flags.enter().append('div').classed('bw-timeline-flag', true);

                        flags.exit().remove();

                        flags.style({
                            left: function(d){
                                refreshFlag(d, d3.select(this));
                                return d.x + 'px';
                            }
                        });

                    }

                    function refreshFlag (d, elem){
                        elem.html('' +
                        '<div class="bw-timeline-flag-inner">' +
                            '<div class="flagpole"></div>' +
                                '<div class="flag-canvas">' +
                                    '<span class="flag-label-1">' + d.text1 + '</span>&nbsp;&nbsp;&nbsp;<i class="fa fa-info-circle"></i>' +
                                    (d.text2 ? '<br><span class="flag-label-2">' + d.text2 + '</span>' : '') +
                                '</div>' +
                            '<div class="color-point"></div>' +
                        '</div>' +
                        '');

                        var inner = elem.select('.bw-timeline-flag-inner');

                        inner.select('.color-point').style({ background: d.color });

                        inner.select('.flagpole').style({
                            height: d.h + 'px'
                        });

                        inner.select('.flag-canvas').style({
                            bottom: (d.h - 45) + 'px',
                            height: 45 + 'px',
                            'border-left': '5px solid ' + d.color
                        });
                    }

                    function refreshAxis (){
                        var dateAxis = self.eventsMobileContainer.select('.bw-timeline-scale').select('.date-axis'),
                            timeScale = d3.time.scale()
                                .domain([new Date(dateRanges[0]), new Date(dateRanges[1])])
                                .rangeRound([0, innerWidth]),
                            axis = d3.svg.axis()
                                .scale(timeScale)
                                .orient('bottom')
                                .ticks(d3.time.months);

                        dateAxis.call(axis);

                        dateAxis.select('path').attr({
                            fill : 'rgba(0,0,0,0)',
                            stroke: '#e3e3e3',
                            'stroke-width': 0.5
                        });

                        dateAxis.selectAll('.tick')
                            .select('text')
                            .attr({
                                fill: '#9a9a9a',
                                'font-size': 10
                            })
                            .text(function(d){
                                return moment(d).format('MMM YY');
                            });

                        dateAxis.selectAll('.tick')
                            .select('line')
                            .attr({
                                stroke: '#e3e3e3',
                                x1: 0,
                                x2: 0,
                                y1: 0,
                                y2: 5
                            });
                    }

                    function refreshSlider (){
                        var caret = self.sliderContainer.select('.bw-timeline-slider-caret'),
                            staticWidth = self.eventsContainer.node().clientWidth,
                            mobileWidth = self.eventsMobileContainer.node().clientWidth - staticWidth,
                            caretWidth = (staticWidth / mobileWidth) * staticWidth,
                            railWidth = staticWidth - caretWidth,
                            caretPosition = (self.eventsContainer.node().scrollLeft / mobileWidth) * railWidth,
                            touchStartPosition = 0;

                        caret.style({
                            width: caretWidth + 'px',
                            'margin-left': caretPosition + 'px'
                        });

                        caret.on('mousedown', onMouseDown);

                        function onMouseDown(){
                            var event = d3.event;
                            touchStartPosition = event.clientX;
                            caretWidth = (staticWidth / mobileWidth) * staticWidth;
                            railWidth = staticWidth - caretWidth;
                            caretPosition = (self.eventsContainer.node().scrollLeft / mobileWidth) * railWidth;

                            self.pageOverlay.style({display: 'block', cursor: 'ew-resize'});

                            self.pageOverlay
                                .on('mousemove', onMouseMove)
                                .on('mouseup', onMouseUp);
                        }

                        function onMouseMove(){
                            var event = d3.event,
                                offset = event.clientX - touchStartPosition,
                                newPosition = caretPosition + offset,
                                scrollPosition;

                            event.preventDefault();

                            if(newPosition > railWidth) {
                                newPosition = railWidth;
                            }
                            if(newPosition < 0) {
                                newPosition = 0;
                            }

                            caret.style({ 'margin-left': newPosition + 'px' }).classed('hover', true);

                            scrollPosition = (newPosition / railWidth) * mobileWidth;

                            self.eventsContainer.node().scrollLeft = scrollPosition;

                            caretPosition = newPosition;
                            touchStartPosition = event.clientX;
                        }

                        function onMouseUp(){
                            self.pageOverlay.style({display: 'none'});

                            self.pageOverlay.on('mousemove', null);
                            self.pageOverlay.on('mouseup', null);

                            caret.classed('hover', false);
                        }
                    }

                    function refreshRangeSelector (){
                        var rsLeftPanel = self.rangeSelector.select('.rs-left-panel'),
                            rsRightPanel = self.rangeSelector.select('.rs-right-panel'),
                            mainPanel = self.rangeSelector.select('.rs-main-panel'),
                            mainHandler = mainPanel.select('.rs-main-handler'),
                            leftHandler = mainHandler.select('.rs-left-handler'),
                            rightHandler = mainHandler.select('.rs-right-handler'),
                            leftHandlerPosition, rightHandlerPosition, selectorWidth, touchStartPosition, limits;

                        if(!$scope.userSettings.range) {
                            $scope.userSettings.range = [0.9, 1];
                        }

                        leftHandlerPosition = containerWidth * $scope.userSettings.range[0];
                        rightHandlerPosition = containerWidth * $scope.userSettings.range[1];
                        selectorWidth = rightHandlerPosition - leftHandlerPosition;

                        rsLeftPanel.style({
                            width: leftHandlerPosition + 'px'
                        });

                        rsRightPanel.style({
                            left: rightHandlerPosition  + 'px',
                            width: (containerWidth - rightHandlerPosition) + 'px'
                        });

                        mainPanel.style({
                            left: leftHandlerPosition + 'px',
                            width: selectorWidth + 'px'
                        });

                        mainHandler.on('mousedown', onMainHandlerGrab);
                        leftHandler.on('mousedown', onLeftHandlerGrab);
                        rightHandler.on('mousedown', onRightHandlerGrab);

                        function onMainHandlerGrab (){
                            var event = d3.event;

                            touchStartPosition = event.clientX;

                            limits = [0, containerWidth - selectorWidth];

                            self.pageOverlay.style({display: 'block', cursor: 'ew-resize'});

                            self.pageOverlay
                                .on('mousemove', onMainHandlerDrag)
                                .on('mouseup', onHandlerRelease);
                        }

                        function onLeftHandlerGrab (){
                            var event = d3.event;

                            event.stopPropagation();

                            touchStartPosition = event.clientX;

                            limits = [0, rightHandlerPosition - (containerWidth * 0.1)];

                            self.pageOverlay.style({display: 'block', cursor: 'ew-resize'});

                            self.pageOverlay
                                .on('mousemove', onLeftHandlerDrag)
                                .on('mouseup', onHandlerRelease);
                        }

                        function onRightHandlerGrab (){
                            var event = d3.event;

                            event.stopPropagation();

                            touchStartPosition = event.clientX;

                            limits = [leftHandlerPosition + (containerWidth * 0.1), containerWidth];

                            self.pageOverlay.style({display: 'block', cursor: 'ew-resize'});

                            self.pageOverlay
                                .on('mousemove', onRightHandlerDrag)
                                .on('mouseup', onHandlerRelease);
                        }

                        function onMainHandlerDrag (){
                            var event = d3.event,
                                offset = event.clientX - touchStartPosition,
                                newPosition = leftHandlerPosition + offset;

                            if(newPosition < limits[0]) {
                                newPosition = limits[0];
                            }
                            if(newPosition > limits[1]) {
                                newPosition = limits[1];
                            }

                            leftHandlerPosition = newPosition;
                            rightHandlerPosition = leftHandlerPosition + selectorWidth;
                            touchStartPosition = event.clientX;

                            updateRanges();
                            refreshRangeSelectorHelpers();

                            rsLeftPanel.style({
                                width: leftHandlerPosition + 'px'
                            });

                            rsRightPanel.style({
                                left: rightHandlerPosition  + 'px',
                                width: (containerWidth - rightHandlerPosition) + 'px'
                            });

                            mainPanel.style({
                                left: leftHandlerPosition + 'px',
                                width: selectorWidth + 'px'
                            });
                        }

                        function onLeftHandlerDrag (){
                            var event = d3.event,
                                offset = event.clientX - touchStartPosition,
                                newPosition = leftHandlerPosition + offset;

                            if(newPosition < limits[0]) {
                                newPosition = limits[0];
                            }
                            if(newPosition > limits[1]) {
                                newPosition = limits[1];
                            }

                            leftHandlerPosition = newPosition;
                            selectorWidth = rightHandlerPosition - leftHandlerPosition;
                            touchStartPosition = event.clientX;

                            updateRanges();
                            refreshRangeSelectorHelpers();

                            rsLeftPanel.style({
                                width: leftHandlerPosition + 'px'
                            });

                            mainPanel.style({
                                left: leftHandlerPosition + 'px',
                                width: selectorWidth + 'px'
                            });
                        }

                        function onRightHandlerDrag (){
                            var event = d3.event,
                                offset = event.clientX - touchStartPosition,
                                newPosition = rightHandlerPosition + offset;

                            if(newPosition < limits[0]) {
                                newPosition = limits[0];
                            }
                            if(newPosition > limits[1]) {
                                newPosition = limits[1];
                            }

                            rightHandlerPosition = newPosition;
                            selectorWidth = rightHandlerPosition - leftHandlerPosition;
                            touchStartPosition = event.clientX;

                            updateRanges();
                            refreshRangeSelectorHelpers();

                            rsRightPanel.style({
                                left: rightHandlerPosition  + 'px',
                                width: (containerWidth - rightHandlerPosition) + 'px'
                            });

                            mainPanel.style({
                                width: selectorWidth + 'px'
                            });
                        }

                        function onHandlerRelease (){
                            self.pageOverlay.style({display: 'none'});

                            self.pageOverlay
                                .on('mousemove', null)
                                .on('mouseup', null);
                        }

                        function updateRanges (){
                            $scope.userSettings.range[0] = reversedScaleA(leftHandlerPosition);
                            $scope.userSettings.range[1] = reversedScaleA(rightHandlerPosition);

                            $scope.settings.onRangeChanged(
                                [reversedScaleB($scope.userSettings.range[0]), reversedScaleB($scope.userSettings.range[1])],
                                refreshSelections

                                //function(range){
                                //    var highlightedEvents = [];
                                //
                                //    self.eventsMobileContainer
                                //        .select('.bw-timeline-flags')
                                //        .selectAll('.bw-timeline-flag')
                                //        .classed('highlighted', function(d){
                                //            var isInRange = d.t >= range[0] && d.t <= range[1];
                                //
                                //            if(isInRange) {
                                //                highlightedEvents.push(d.entity);
                                //            }
                                //
                                //            return isInRange;
                                //        });
                                //
                                //    return highlightedEvents;
                                //}
                            );
                        }
                    }

                    function refreshRangeSelectorPoints (){
                        var pointsCanvas = self.rangeSelector.select('.rs-points-canvas'),
                            points = pointsCanvas.selectAll('circle').data(renderingData);

                        points.enter().append('circle');

                        points.exit().remove();

                        points.attr({
                            cx: function(d){
                                return d.xx;
                            },
                            cy: 3,
                            r: 2,
                            fill: function(d){
                                return d.color;
                            }
                        });

                        var dateAxis = self.container.select('.rs-date-scale').select('.date-axis'),
                            timeScale = d3.time.scale()
                                .domain([new Date(dateRanges[0]), new Date(dateRanges[1])])
                                .rangeRound([0, containerWidth]),
                            axis = d3.svg.axis()
                                .scale(timeScale)
                                .orient('bottom')
                                .ticks(d3.time.years);

                        dateAxis.call(axis);

                        dateAxis.select('path').attr({
                            fill : 'rgba(0,0,0,0)',
                            stroke: '#e3e3e3',
                            'stroke-width': 0.5
                        });

                        dateAxis.selectAll('.tick')
                            .select('text')
                            .attr({
                                fill: '#9a9a9a',
                                'font-size': 10,
                                transform: 'translate(' + [ 15, -3 ] + ')'
                            })
                            .text(function(d){
                                return moment(d).format('YYYY');
                            });

                        dateAxis.selectAll('.tick')
                            .select('line')
                            .attr({
                                stroke: '#e3e3e3',
                                x1: 0,
                                x2: 0,
                                y1: 0,
                                y2: 5
                            });
                    }

                    function refreshRangeSelectorHelpers (){
                        leftHelper
                            .html(moment(reversedScaleB($scope.settings.range[0])).format('MMM YYYY'))
                            .style({
                                'margin-left': function(){
                                    return -(this.clientWidth * 0.5) + 'px';
                                }
                            });
                        rightHelper
                            .html(moment(reversedScaleB($scope.settings.range[1])).format('MMM YYYY'))
                            .style({
                                'margin-left': function(){
                                    return -(this.clientWidth * 0.5) + 'px';
                                }
                            });
                    }

                    function refreshSelections (range){
                        var highlightedEvents = [];

                        self.eventsMobileContainer
                            .select('.bw-timeline-flags')
                            .selectAll('.bw-timeline-flag')
                            .classed('highlighted', function(d){
                                var isInRange = d.t >= range[0] && d.t <= range[1];

                                if(isInRange) {
                                    highlightedEvents.push(d.entity);
                                }

                                return isInRange;
                            });

                        return highlightedEvents;
                    }

                    $scope.settings.onReady(
                        [reversedScaleB($scope.userSettings.range[0]), reversedScaleB($scope.userSettings.range[1])],
                        refreshSelections
                    );
                }
            }
        });

        $scope.settings.watchers.forEach(function(propertyToWatch){
            watchers.push(
                $scope.$watch(
                    function(){
                        return BWSharedService.getProperty(propertyToWatch);
                    },
                    function(curr, prev){
                        if(curr !== prev) {
                            $scope.model._api_.refreshView();
                        }
                    }
                )
            );
        });
    }

}(angular.module('bw.timeline')));

