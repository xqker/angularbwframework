(function(module){

    'use strict';

    module.service('BWTimelineService', [
        'AsyncAPI', 'BWCollectionService', 'BWColorsService', 'd3', 'moment',
        function(AsyncAPI, BWCollectionService, BWColorsService, d3, moment){
            var timelineService = this,
                timelineCounter = 0;

            Object.defineProperties(timelineService, {
                createNew: {
                    value: function(){
                        return new TimelineEntity(timelineCounter++);
                    }
                }
            });

            function TimelineEntity (id){
                var bwTimelineEntity = this;
                Object.defineProperties(bwTimelineEntity, {
                    id: {
                        value: '#bw-timeline-' + id
                    },
                    _api_: {
                        value: AsyncAPI.get({})
                    }
                });
            }

            TimelineEntity.prototype.refreshData = function(data){
                this._api_.applyFn('refreshData', data);
            };

            TimelineEntity.prototype.refreshView = function(){
                this._api_.applyFn('refreshView');
            };

            TimelineEntity.prototype.getRandomData = function(){
                var self = this,
                    numberOfEvents = 150,
                    events = [],
                    firstDate = moment(new Date()).startOf('day').subtract(10, 'years').unix() * 1000,
                    lastDate = moment(new Date()).startOf('day').unix() * 1000,
                    timeRange = lastDate - firstDate,
                    _events = 'Ask for money_Kill somebody_Put on poncho and sombrero_Steal expensive red car_Bake a cake_Catch Roadrunner_I need your clothes, your boots and your motorcycle'.split('_'),
                    _descriptions = 'Do it quickly!!!_O rly?__Arrrrgghhhh!__Luke, I am your father_ACAB'.split('_'),
                    _eventTypes = 'Negative,Neutral,Positive'.split(','),
                    _colors = {
                        Negative: BWColorsService.brandPalette.bwRedLight,
                        Neutral: BWColorsService.brandPalette.bwGrayLight,
                        Positive: BWColorsService.brandPalette.bwGreenLight
                    };

                for(var i = 0 ; i < numberOfEvents ; i++){
                    var randomTimestamp = (parseInt(Math.random() * timeRange) + firstDate),
                        randomEvent = _events[parseInt(Math.random() * _events.length)],
                        randomDescription = _descriptions[parseInt(Math.random() * _descriptions.length)],
                        randomEventType = _eventTypes[parseInt(Math.random() * _eventTypes.length)];

                    events.push({
                        id: i,
                        timestamp: randomTimestamp,
                        event: randomEvent,
                        description: randomDescription,
                        eventType: randomEventType,
                        color: _colors[randomEventType]
                    });
                }
                return events.sort(function(a, b){
                    var timestampA = a.timestamp,
                        timestampB = b.timestamp;

                    if(timestampA < timestampB){
                        return 1;
                    }
                    if(timestampA > timestampB){
                        return -1;
                    }
                    return 0;
                });
            };
        }
    ]);

}(angular.module('bw.timeline')));

