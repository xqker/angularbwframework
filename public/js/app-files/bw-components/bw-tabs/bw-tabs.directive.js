(function(module){

    'use strict';

    module.directive('bwTabs', [
        function(){
            return {
                restrict: 'A',
                scope: {
                    model: '=bwTabs'
                },
                template: '' +
                    '<div class="bw-tabs-container">' +
                        '<div class="bw-tabs-list-wrapper">' +
                            '<ul class="bw-tabs-list">' +
                                '<li class="bw-tabs-item" ' +
                                    'ng-repeat="tab in model.getItems()" ' +
                                    'ng-class="{selected: tab.isSelected}" ' +
                                    'ng-click="tab.select()">' +
                                    '<i class="bw-tabs-item-icon fa {{tab.content.icon}}"></i>' +
                                    '<span class="bw-tabs-item-label">{{tab.content.label}}</span>' +
                                '</li>' +
                            '</ul>' +
                        '</div>' +
                        '<div class="bw-tabs-slider">' +
                            '<div class="bw-tabs-slider-caret"></div>' +
                        '</div>' +
                        '<div class="bw-tabs-overlay"></div>' +
                    '</div>' +
                '',
                controller: BWTabsDirectiveCtrl
            };
        }
    ]);

    BWTabsDirectiveCtrl.$inject = ['$scope', '$element', '$timeout', 'd3'];
    function BWTabsDirectiveCtrl($scope, $element, $timeout, d3){
        var wrapper = d3.select($element.get(0)).classed('bw-tabs-wrapper', true),
            container = wrapper.select('.bw-tabs-container'),
            staticContainer = container.select('.bw-tabs-list-wrapper'),
            slider = container.select('.bw-tabs-slider').select('.bw-tabs-slider-caret'),
            overlay = container.select('.bw-tabs-overlay'),
            itemWidth = 153,
            touchStartPosition = 0,
            sliderPosition = 0,
            railWidth = 0,
            totalWidth = 0,
            sliderWidth = 0,
            containerWidth = 0;

        function refreshSlider() {
            containerWidth = staticContainer.node().clientWidth;
            totalWidth = ((itemWidth * $scope.model.getItems().length)) - 3;
            sliderWidth = (containerWidth / totalWidth) * containerWidth;
            railWidth = containerWidth - sliderWidth;
            sliderPosition = (container.node().scrollLeft / (totalWidth - containerWidth)) * railWidth;

            slider.style({
                width: sliderWidth + 'px',
                'margin-left': sliderPosition + 'px',
                display: totalWidth > containerWidth ? 'block' : 'none'
            });

            slider.on('mousedown', onMouseDown);
        }

        function onMouseDown (){
            touchStartPosition = d3.event.clientX;

            slider.classed('hover', true);

            overlay
                .style({ display: 'block', cursor: 'ew-resize' })
                .on('mousemove', onMouseMove)
                .on('mouseup', onMouseUp);
        }

        function onMouseMove (){
            var event = d3.event,
                offset = event.clientX - touchStartPosition,
                newPosition = sliderPosition + offset,
                scrollPosition;

            event.preventDefault();

            if(newPosition > railWidth) {
                newPosition = railWidth;
            }
            if(newPosition < 0) {
                newPosition = 0;
            }

            slider.style({ 'margin-left': newPosition + 'px' });

            scrollPosition = (newPosition / railWidth) * (totalWidth - containerWidth);

            staticContainer.node().scrollLeft = scrollPosition;

            sliderPosition = newPosition;
            touchStartPosition = event.clientX;
        }

        function onMouseUp (){
            slider.classed('hover', false);

            overlay
                .style({ display: 'none' })
                .on('mousemove', null)
                .on('mouseup', null);
        }

        $timeout(refreshSlider, 500);

        d3.select(window).on('resize', refreshSlider);
    }


}(angular.module('bw.utilities')));