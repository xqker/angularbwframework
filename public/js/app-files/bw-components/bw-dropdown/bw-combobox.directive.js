(function(module){

    'use strict';

    module.directive('bwCombobox', [
        function(){
            return {
                restrict: 'A',
                scope: {
                    model: '=bwCombobox',
                    userSettings: '=bwComboboxSettings'
                },
                template:'' +
                '<div class="bw-combobox-container" ng-class="{' + 'opened' + ': isOpened}">' +
                    '<div class="bw-combobox-input">' +
                        '<input type="text" ng-model="model.content[settings.indexingProperty]" ng-click="$event.stopPropagation()" ng-focus="$event.stopPropagation(); openDropdown()" ng-keydown="onKeyPress($event)" />' +
                    '</div>' +
                    '<ul class="bw-combobox-list" ng-class="{shadows: settings.withShadows, reversed: settings.openUpstairs }" ng-show="isOpened && filteredList.length">' +
                        '<li class="bw-combobox-item"' +
                            'ng-repeat="item in filteredList track by $index"' +
                            'ng-class="{highlighted : item.highlighted}"' +
                            'ng-mouseover="highlightItem(item.index, true)"' +
                            'ng-click="$event.stopPropagation(); onSelect(item)">' +
                            '<span class="bw-combobox-item-label">{{item.entity.content.label}}</span>' +
                        '</li>' +
                    '</ul>' +
                    '<i class="bw-combobox-btn fa fa-caret-down" ng-show="!isOpened" ng-click="$event.stopPropagation(); openDropdown()"></i>' +
                    '<i class="bw-combobox-btn icon-cross" ng-show="isOpened" ng-click="$event.stopPropagation(); closeDropdown()"></i>' +
                '</div>' +
                '',
                controller: BWComboboxDirectiveCtrl
            };
        }
    ]);

    BWComboboxDirectiveCtrl.$inject = ['$scope', '$element', '$timeout', 'd3'];

    function BWComboboxDirectiveCtrl($scope, $element, $timeout, d3) {
        var self = this,
            defaultSettings = {
                indexingProperty: 'label',
                withShadows: true,
                openUpstairs: false,
                onSelect: function(item){
                    item.select();
                },
                onFail: function(str){
                    $scope.model.deselectAll();
                }
            },
            onModelChangeWatcher = null,
            keys = {
                enter: 13,
                up: 38,
                down: 40
            };

        self.wrapper = d3.select($element.get(0)).classed('bw-combobox-wrapper', true);
        self.container = self.wrapper.select('.bw-combobox-container');
        self.mainInput = self.container.select('.bw-combobox-input').select('input');
        self.escapeString = self.container.select('.bw-combobox-escape-string');

        Object.defineProperties($scope, {
            isOpened: {
                value: false,
                writable: true
            },
            hoveringItemId: {
                value: null,
                writable: true
            },
            preventFiltering: {
                value: false,
                writable: true
            },
            filteredList: {
                value: [],
                writable: true
            },
            highlightItem: {
                value: function(index, changeLabel){
                    $scope.filteredList.forEach(function(item){
                        item.highlighted = item.index === index;
                    });

                    if(changeLabel) {
                        var highlightedItem = getHighlightedItem();

                        if(highlightedItem) {
                            $scope.preventFiltering = true;
                            $scope.model.content[$scope.settings.indexingProperty] = highlightedItem.entity.content[$scope.settings.indexingProperty];

                            $timeout(function(){
                                $scope.preventFiltering = false;
                            });
                        }
                    }
                }
            },
            openDropdown: {
                value: function(){
                    filterDropdownItems();
                    setOnModelChangeWatcher();

                    $scope.isOpened = true;
                    $scope.highlightItem(0);
                    self.mainInput.node().focus();

                    $timeout(function(){
                        window.addEventListener('click', onClickSomewhereOutOfCombobox, false);
                    }, 100);
                }
            },
            closeDropdown: {
                value: function(){
                    clearOnModelChangeWatcher();
                    $scope.isOpened = false;
                    self.mainInput.node().blur();
                    window.removeEventListener('click', onClickSomewhereOutOfCombobox, false);
                }
            },
            onKeyPress: {
                value: function(e){
                    var itemsLength = $scope.filteredList.length,
                        highlightedItem = getHighlightedItem(),
                        keyPressed = e.keyCode,
                        nextIndex;

                    if(itemsLength && !highlightedItem) {
                        $scope.highlightItem(0);
                        highlightedItem = $scope.filteredList[0];
                    }

                    if(keyPressed === keys.enter) {
                        $scope.onSelect(highlightedItem);
                    } else if(itemsLength && keyPressed === keys.up) {
                        e.preventDefault();
                        nextIndex = highlightedItem.index - 1;
                        if(nextIndex < 0) {
                            nextIndex = 0;
                        }

                        $scope.highlightItem(nextIndex, true);
                    } else if(itemsLength && keyPressed === keys.down) {
                        e.preventDefault();
                        nextIndex = highlightedItem.index + 1;
                        if(nextIndex > itemsLength - 1) {
                            nextIndex = itemsLength - 1;
                        }

                        $scope.highlightItem(nextIndex, true);
                    }
                }
            },
            onSelect: {
                value: function(item){
                    clearOnModelChangeWatcher();
                    $scope.closeDropdown();

                    if(item) {
                        $scope.model.content[$scope.settings.indexingProperty] = item.entity.content[$scope.settings.indexingProperty];
                        $scope.settings.onSelect(item.entity);
                    } else {
                        $scope.settings.onFail($scope.model.content[$scope.settings.indexingProperty]);
                    }
                }
            },
            settings: {
                get: function(){
                    return angular.merge(defaultSettings, $scope.userSettings || {});
                }
            }
        });

        $scope.model.content[$scope.settings.indexingProperty] = $scope.model.content[$scope.settings.indexingProperty] || '';

        function onClickSomewhereOutOfCombobox (){
            $scope.closeDropdown();
            $scope.$digest();
            window.removeEventListener('click', onClickSomewhereOutOfCombobox, false);
        }

        function filterDropdownItems (){
            var safeString = getEscapedString($scope.model.content[$scope.settings.indexingProperty]),
                regexp = new RegExp(safeString, 'i');

            $scope.filteredList = $scope.model.getItems().filter(function(item){
                return regexp.test(item.content[$scope.settings.indexingProperty]);
            }).map(function(item, i){
                return {
                    index: i,
                    highlighted: !i,
                    entity: item
                };
            });
        }

        function setOnModelChangeWatcher (){
            clearOnModelChangeWatcher();

            onModelChangeWatcher = $scope.$watch(
                function(){
                    return $scope.model.content[$scope.settings.indexingProperty];
                },
                function(curr, prev){
                    if(!$scope.preventFiltering && (curr !== prev)) {
                        filterDropdownItems();
                    }
                }
            );
        }

        function clearOnModelChangeWatcher (){
            if(onModelChangeWatcher) {
                onModelChangeWatcher();
                onModelChangeWatcher = null;
            }
        }

        function getHighlightedItem (){
            var highlightedItems = $scope.filteredList.filter(function(item){
                return item.highlighted;
            });
            return highlightedItems.length ? highlightedItems[0] : null;
        }

        function getEscapedString (str){
            var specials = ["-", "[", "]", "/", "{", "}", "(", ")", "*", "+", "?", ".", "\\", "^", "$", "|"],
                regex = new RegExp('[' + specials.join('\\') + ']', 'g');

            return str.toString().replace(regex, '\\$&');
        }
    }


}(angular.module('bw.utilities')));

