(function(module){

    'use strict';

    module.directive('bwDropdown', [
        function(){
            return {
                restrict: 'A',
                scope: {
                    model: '=bwDropdown',
                    userSettings: '=bwDropdownSettings'
                },
                template:'' +
                    '<div class="bw-dropdown-container" ng-class="{' + 'opened' + ': isOpened}">' +
                        '<div class="bw-dropdown-selected-item" ng-click="toggleDropdown()">' +
                            '<i class="bw-dropdown-item-icon fa {{model.selected.content.icon}}"></i>' +
                            '<span class="bw-dropdown-item-label">  {{model.selected.content[settings.keys.label]}}</span>' +
                        '</div>' +
                        '<ul class="bw-dropdown-list" ng-class="{shadows: settings.withShadows, reversed: settings.openUpstairs }" ng-show="isOpened">' +
                            '<li class="bw-dropdown-item"' +
                                'ng-repeat="item in model.getItems() track by $index"' +
                                'ng-show="!item.isSelected"' +
                                'ng-click="onSelect(item)">' +
                                '<i class="bw-dropdown-item-icon fa {{item.content.icon}}"></i>' +
                                '<span class="bw-dropdown-item-label">  {{item.content[settings.keys.label]}}</span>' +
                            '</li>' +
                        '</ul>' +
                        '<i class="bw-dropdown-btn fa fa-caret-down" ng-show="!isOpened" ng-click="openDropdown()"></i>' +
                        '<i class="bw-dropdown-btn fa fa-caret-up" ng-show="isOpened" ng-click="closeDropdown()"></i>' +
                    '</div>' +
                '',
                controller: BWDropdownDirectiveCtrl
            }
        }
    ]);

    BWDropdownDirectiveCtrl.$inject = ['$scope', '$element', '$timeout', 'd3'];
    function BWDropdownDirectiveCtrl ($scope, $element, $timeout, d3) {
        var self = this,
            defaultSettings = {
                keys: {
                    label: 'label'
                },
                withShadows: true,
                openUpstairs: false,
                onSelect: function(item){
                    item.select();
                }
            };

        self.wrapper = d3.select($element.get(0)).classed('bw-dropdown-wrapper', true);

        Object.defineProperties($scope, {
            isOpened: {
                value: false,
                writable: true
            },
            toggleDropdown: {
                value: function(){
                    $scope[$scope.isOpened ? 'closeDropdown' : 'openDropdown']();
                }
            },
            openDropdown: {
                value: function(){
                    $scope.isOpened = true;
                    $timeout(function(){
                        window.addEventListener('click', onClickSomewhereOutOfDropdown, false);
                    }, 100);
                }
            },
            closeDropdown: {
                value: function(){
                    $scope.isOpened = false;
                    window.removeEventListener('click', onClickSomewhereOutOfDropdown, false);
                }
            },
            settings: {
                get: function(){
                    return angular.merge(defaultSettings, $scope.userSettings || {});
                }
            },
            onSelect: {
                value: function(item){
                    console.log();

                    if(!item.isSelected) {
                        $scope.closeDropdown();
                        $scope.settings.onSelect(item);
                    }
                }
            }
        });

        function onClickSomewhereOutOfDropdown (){
            $scope.closeDropdown();
            $scope.$digest();
            window.removeEventListener('click', onClickSomewhereOutOfDropdown, false);
        }
    }


}(angular.module('bw.utilities')));

