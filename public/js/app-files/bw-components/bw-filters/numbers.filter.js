(function(module){

    'use strict';

    module
        .filter('bigNumber', [
            function(){
                var symbols = ['', 'K', 'M', 'B', 'T', 'Qd', 'Qn'],
                    values = [],
                    i = 0,
                    len = symbols.length;

                for(; i < len ; i++){
                    values.push(Math.pow(10, (i * 3)));
                }

                function getAdequateLetter (numb){
                    var _numb = Math.abs(numb);
                    return symbols.filter(function(s, i){
                        return _numb >= values[i] && _numb < values[i + 1];
                    })[0] || '';
                }

                function getAdequateDischarge (numb){
                    var _numb = Math.abs(numb);
                    return values.filter(function(s, i){
                        return _numb >= values[i] && _numb < values[i + 1];
                    })[0] || 1;
                }

                return function(numb){
                    var letter = getAdequateLetter(numb),
                        discharge = getAdequateDischarge(numb);
                    return parseInt(numb / discharge) + letter;
                };
            }
        ])
        .filter('comaSeparated', [
            function(){
                return function(numb, showDecimals){
                    var isNegative = numb < 0,
                        separator = ',',
                        _numb = Math.abs(Math.floor(numb)).toFixed(0),
                        len = _numb.length,
                        head = _numb.substr(0, len % 3),
                        headLen = head.length,
                        discharges = [],
                        i = headLen ? headLen : 0;

                    if(headLen) {
                        discharges.push(head);
                    }

                    for(; i < len; i += 3){
                        discharges.push(_numb.substr(i, 3));
                    }

                    return discharges.join(separator);
                }
            }
        ]);


}(angular.module('bw.filters')));

