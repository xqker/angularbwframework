(function(module){

    'use strict';

    module.directive('bwSwitch', [
        function(){
            return {
                restrict: 'A',
                scope: {
                    model: '=bwSwitch',
                    userSettings: '=bwSwitchSettings'
                },
                template: '' +
                    '<div class="bw-switch-container" ng-style="containerStyle">' +
                        '<div class="bw-switch-item" ng-repeat="item in model.getItems()"' +
                            'ng-mouseover="onMouseOver(item)"' +
                            'ng-click="onSelect(item)"' +
                            'ng-class="{selected : item.isSelected}"' +
                            'ng-style="itemStyle">' +
                            '<div class="bw-switch-item-arrow"></div>' +
                            '<div class="bw-switch-item-icon"><i class="fa {{item.content.icon}}"></i></div>' +
                            '<div class="bw-switch-item-label">' +
                                '<span>{{item.content.label}}</span>' +
                            '</div>' +
                        '</div>' +
                    '</div>' +
                '',
                controller: BWSwitchDirectiveCtrl
            };
        }
    ]);

    BWSwitchDirectiveCtrl.$inject = ['$scope', '$element', '$attrs', 'd3'];
    function BWSwitchDirectiveCtrl ($scope, $element, $attrs, d3){
        var self = this,
            singleButtonWidth = 40,
            multiButtonWidth = 45,
            defaultSettings = {
                onSelect: function(item){
                    item.select();
                }
            };

        self.wrapper = d3.select($element.get(0)).classed('bw-switch-wrapper', true);
        self.container = self.wrapper.select('.bw-switch-container');

        Object.defineProperties($scope, {
            settings: {
                get: function(){
                    return angular.merge(defaultSettings, $scope.userSettings || {});
                }
            },
            onSelect: {
                value: function(item){
                    if(!item.isSelected) {
                        $scope.settings.onSelect(item);
                    }
                }
            },

            singleButton: {
                get: function(){
                    return $scope.model.numberOfItems === 1;
                }
            },
            itemStyle: {
                get: function(){
                    return {
                        width: ($scope.singleButton ? singleButtonWidth : multiButtonWidth) + 'px'
                    };
                }
            },
            containerStyle: {
                get: function(){
                    return {
                        width: (($scope.singleButton ? singleButtonWidth : multiButtonWidth) * $scope.model.numberOfItems) + 'px'
                    }
                }
            }
        });
    }


}(angular.module('bw.utilities')));
