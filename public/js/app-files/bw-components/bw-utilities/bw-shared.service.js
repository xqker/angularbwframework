(function(module){

    'use strict';

    module.service('BWSharedService',  [
        'd3',
        function(d3){
            var self = this,
                props = {},
                scrollWatchers = [],
                resizeWatchers = [];

            self.setProperty = function(prop, value){
                props[prop] = value;
            };

            self.getProperty = function(prop){
                return props[prop];
            };


            /** Scroll */
            self.setScrollWatcher = function(widgetId, fn){
                var scrollWatcher = getScrollWatcher(widgetId);

                if(!scrollWatcher) {
                    scrollWatcher = {
                        id: widgetId,
                        fn: fn,
                        preventScroll: false
                    };
                    scrollWatchers.push(scrollWatcher);
                } else {
                    scrollWatcher.fn = fn;
                }
            };

            self.removeScrollWatcher = function(widgetId){
                var scrollWatcher = getScrollWatcher(widgetId);
                if(scrollWatcher) {
                    scrollWatchers.splice(scrollWatchers.indexOf(scrollWatcher), 1);
                }
            };

            function getScrollWatcher (widgetId){
                return scrollWatchers.filter(function(sw){
                    return sw.id === widgetId;
                })[0];
            }

            d3.select(window).on('scroll', function(){
                scrollWatchers.forEach(function(sw){
                    sw.fn();
                });
            });

            /** Resize */
            self.getResizeWatcher = function(block, settings){
                var resizeWatcherEngine = {
                        _sizeTracker: null,
                        _resizeTimer: null,
                        _lastHash: '',
                        _newHash: '',
                        _delay: settings.delay || 50
                    },
                    resizeWatcher = {
                        _engine: resizeWatcherEngine
                    };

                resizeWatcher.runSizeTracker = function(){
                    resizeWatcher.stopSizeTracker();
                    resizeWatcherEngine._sizeTracker = setInterval(resizeWatcherEngine.onResize, resizeWatcherEngine._delay);
                };

                resizeWatcher.stopSizeTracker = function(){
                    if(resizeWatcherEngine._sizeTracker){
                        clearInterval(resizeWatcherEngine._sizeTracker);
                        resizeWatcherEngine._sizeTracker = null;
                    }
                };

                resizeWatcherEngine.onResize = function(){
                    if(settings.stopWhen && settings.stopWhen()) {
                        resizeWatcher.stopSizeTracker();
                    } else {
                        resizeWatcherEngine._newHash = block.clientHeight + '_' + block.clientWidth;
                        resizeWatcherEngine._lastHash = resizeWatcherEngine._lastHash || resizeWatcherEngine._newHash;
                        if(resizeWatcherEngine._newHash !== resizeWatcherEngine._lastHash) {
                            resizeWatcherEngine._lastHash = resizeWatcherEngine._newHash;
                            resizeWatcherEngine.setResizeTimer();
                        }
                    }
                };

                resizeWatcherEngine.setResizeTimer = function(){
                    resizeWatcherEngine.clearResizeTimer();
                    resizeWatcherEngine._resizeTimer = setTimeout(settings.onResize, resizeWatcherEngine._delay * 2);


                };

                resizeWatcherEngine.clearResizeTimer = function(){
                    if(resizeWatcherEngine._resizeTimer){
                        clearTimeout(resizeWatcherEngine._resizeTimer);
                        resizeWatcherEngine._resizeTimer = null;
                    }
                };

                return resizeWatcher;
            };
        }
    ]);

}(angular.module('bw.utilities')));
