(function(module){

    'use strict';

    module.service('BWColorsService', [
        'd3',
        function(d3) {
            var bwColorsService = this;

            Object.defineProperties(bwColorsService, {
                blackWhitePalette: {
                    value: {
                        black: '#000000',
                        grayDarkest: '#262626',
                        grayDarker: '#404040',
                        grayDark: '#606060',
                        gray: '#808080',
                        grayLight: '#A4A4A4',
                        grayLighter: '#C4C8CE',
                        grayLightest: '#FAFAFA',
                        white: '#FFFFFF',
                        transparent: 'rgba(0, 0, 0, 0)'
                    }
                },
                someColors: {
                    value: {
                        brightGold: 'rgba(253, 208, 23, 1)',
                        platinum: 'rgba(229, 228, 226, 1)',
                        blueOrchid: 'rgba(31, 69, 252, 1)',
                        rose: 'rgba(232, 173, 170, 1)',
                        jungleGreen: 'rgba(52, 124, 44, 1)',
                        seaBlue: 'rgba(194, 223, 255, 1)'
                    }
                },
                brandPalette: {
                    value: {
                        bwBlueDarker: '#303f5e',
                        bwBlueDark: '#3a4c72',
                        bwBlue: '#00aeef',
                        bwBlueLight: '#4cc6f3',
                        bwBlueLighter: '#98def7',
                        bwBlueLightest: '#e5f7fd',
                        bwGreen: '#09b061',
                        bwGreenLight: '#60c08e',
                        bwRed: '#e6433b',
                        bwRedLight: '#eb655f',
                        bwGray: '#c4cbd4',
                        bwGrayLight: '#c4cbd4',
                        bwGrayLighter: '#e5e9ef',
                        bwGrayLightest: '#e5f7fd',
                        bwGold: '#fdd017',
                        bwPlatinum: '#e4e4e4'
                    }
                },
                d3Colors: {
                    value: d3.scale.category10().range()
                },
                bwColors: {
                    value: ['#5fc08e', '#b4b4b4', '#ed7c76', '#92c7ec']
                },
                getStdPalette: {
                    value: function(){
                        var palette = [],
                            baseColors = bwColorsService.d3Colors,
                            step = 0.9,
                            i = 0,
                            len = 10;

                        for(; i < len ; i++ ) {
                            baseColors.forEach(function(color){
                                palette.push(d3.hsl(color).darker(step * i).toString());
                            });
                        }
                        return function(n){
                            return palette[n];
                        };
                    }
                },
                getBWPalette: {
                    value: function(fadingOut){
                        var palette = [],
                            bwColors = bwColorsService.bwColors,
                            step = 0.9,
                            i = 0,
                            len = 10;

                        for(; i < len; i++){
                            bwColors.forEach(function(color){
                                palette.push(d3.hsl(color)[fadingOut ? 'brighter' : 'darker'](step * i).toString());
                            });
                        }

                        return function(n){
                            return palette[n];
                        }
                    }
                },
                getDarkerColor: {
                    value: function(color, i, step){
                        var defaultStep = 0.2,
                            hsl = d3.hsl(color).darker((step || defaultStep) * i);
                        return hsl.toString();
                    }
                },
                getBrighterColor: {
                    value: function(color, i, step){
                        var defaultStep = 0.2,
                            hsl = d3.hsl(color).brighter((step || defaultStep) * i);
                        return hsl.toString();
                    }
                },
                getTimelineChartColors: {
                    value: function(){
                        return {
                            mainLayer: {
                                bg: '#f1f1f1',
                                axis: {
                                    line1: '#e3e3e3',
                                    text1: '#9a9a9a',
                                    line2: '#b1b1b1',
                                    text2: '#4a4b4b'
                                },
                                flags: {
                                    bg1: '#ffffff',
                                    bg2: '#e4e4e4',
                                    line: '#d2d2d2',
                                    text1: '#4b4a4a',
                                    text2: '#9a9a9a',
                                    text3: '#969696',
                                    text4: '#b7b7b7',
                                    infoIcon: {
                                        bg1: '#ebebeb',
                                        bg2: '#d2d2d2',
                                        text1: '#b0b0b0',
                                        text2: '989898'
                                    },
                                    red1: '#eb655f',
                                    red2: '#d7b7b6',
                                    green1: '#60c08e',
                                    green2: '#bdd2c8',
                                    gray1: '#b0b1b1',
                                    gray2: '#c6c6c6'
                                }
                            },
                            slider: {
                                rail: '#efefef',
                                caret: '#d0d4d8'
                            },
                            rangeSelector: {
                                bg1: '#d3f2fe',
                                bg2: '#f1f1f1',
                                axis: {
                                    tick: '#dcdcdc',
                                    text: '#a9a9a9'
                                },
                                handlers: {
                                    line: '#d4d4d4',
                                    bg: '#ffffff',
                                    text: '#4a4b4b'
                                }
                            },
                            borders: '#ffffff',
                            transparent: 'rgba(0,0,0,0)'
                        };
                    }
                },
                getRoundChartColors: {
                    value: function(){
                        return {
                            light: '#ffffff',
                            dark: '#606060',
                            transparent: 'rgba(0,0,0,0)'
                        };
                    }
                },
                getBarChartColors: {
                    value: function(){
                        return {
                            dark: '#606060',
                            transparent: 'rgba(0,0,0,0)',
                            axis: {
                                line: '#c0d0e0',
                                text: '#a8a7a7'
                            },
                            bgLine: '#dcdcdc'
                        };
                    }
                },
                getColorScheme: {
                    value: function(colors, range){
                        var firstColorRGB = d3.rgb(colors[0]),
                            secondColorRGB = d3.rgb(colors[1]),
                            scales = {
                                r: d3.scale.linear()
                                    .domain(range)
                                    .range([firstColorRGB.r, secondColorRGB.r]),
                                g: d3.scale.linear()
                                    .domain(range)
                                    .range([firstColorRGB.g, secondColorRGB.g]),
                                b: d3.scale.linear()
                                    .domain(range)
                                    .range([firstColorRGB.b, secondColorRGB.b])
                            };

                        return function(value){
                            return ('rgb(' +
                                parseInt(scales.r(value)) + ', ' +
                                parseInt(scales.g(value)) + ', ' +
                                parseInt(scales.b(value)) + ')' +
                            '').toString();
                        }
                    }
                },
                getContrastyColor: {
                    value: function(color){
                        var lightness = d3.hsl(color).l;
                        return lightness < 0.7 ? '#FFF' : '#000';
                    }
                }
            })
        }
    ]);

}(angular.module('bw.utilities')));

