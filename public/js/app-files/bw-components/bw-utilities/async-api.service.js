(function(module){

    'use strict';

    module.service('AsyncAPI', ['$timeout', function($timeout){
        this.get = function(APIobj){
            return new AsyncAPI(APIobj);
        };

        function AsyncAPI (APIobj){
            var delay = 50,
                ticksToWait = 10;

            var wait = function(methodName, args, ticks){
                ticks -= 1;

                if(ticks) {
                    $timeout(function(){
                        check(methodName) ? run(methodName, args) : wait(methodName, args, ticks);
                    }, delay);
                } else {
                    throw 'API ' + methodName + ' is not ready';
                }
            };

            var run = function(methodName, args){
                return APIobj[methodName].apply(APIobj, args);
            };

            var check = function(fn){
                return APIobj.hasOwnProperty(fn);
            };

            APIobj.applyFn = function(){
                var args = Array.prototype.slice.call(arguments),
                    methodName = args.shift();

                return check(methodName) ? run(methodName, args) : wait(methodName, args, ticksToWait);
            };

            return APIobj;
        }
    }]);

}(angular.module('bw.utilities')));
