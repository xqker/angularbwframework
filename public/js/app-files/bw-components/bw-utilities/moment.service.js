(function(module){

    'use strict';

    module.service('moment', [
        function(){
            var moment = window.moment;
            window.moment = undefined;
            return moment;
        }
    ]);

}(angular.module('bw.utilities')));
