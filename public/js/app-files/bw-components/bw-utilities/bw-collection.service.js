(function(module){

    'use strict';

    module.service('BWCollectionService', [
        '$rootScope',
        function ($rootScope) {
            var bwCollectionService = this,
                collections = [],
                collectionCounter = 0,
                itemCounter = 0;

            /** API */
            Object.defineProperties(bwCollectionService, {
                createNew: {
                    value: createNewCollection
                },
                getAll: {
                    value: getAllCollections
                },
                getCollection: {
                    value: getCollection
                }
            });

            /** Service Methods */
            function createNewCollection (data, settings){
                var newCollection = new BWCollection(data, (settings || {}));
                collections.push(newCollection);
                return newCollection;
            }

            function getCollection (id){
                return getAllCollections().filter(function(collection){
                    return collection._id === id;
                })[0];
            }

            function getAllCollections (){
                return collections;
            }

            function dropCollections (collectionsToDrop){
                collectionsToDrop.forEach(function(collectionToDrop){
                    var index = collections.indexOf(collectionToDrop);
                    if(index !== -1) {
                        collections.splice(index, 1);
                    }
                    collectionToDrop._items.length = 0;
                });

                return collectionsToDrop;
            }

            function dropAll (){
                collections = collections.filter(function(collection){
                    return collection._isProtected;
                });
            }

            function getItem (bwc, bwi){



            }

            /** Abstract Collection Constructor */
            function BWCollection (data, settings){
                var bwCollection = this,
                    _settings = settings || {},
                    hasParent = !!_settings.parentItem,
                    isProtected = !!_settings.protected,
                    isBaseCollection = !hasParent,
                    parentCollection = hasParent ? getCollection(_settings.parentItem._collectionId) : {},
                    collectionId = 'bwc#' + collectionCounter++,
                    breadCrumbs = (hasParent ? _settings.parentItem._breadCrumbs + '_' : '') + collectionId,
                    level = hasParent ? parentCollection._level + 1 : 0,
                    multiselect = _settings.multiselect;

                parentCollection._hasNestedCollections = hasParent;

                Object.defineProperties(bwCollection, {
                    /** Private props */
                    _id: {
                        value: collectionId
                    },
                    _parentCollectionId: {
                        value: hasParent ? parentCollection._id : null
                    },
                    _parentItemId: {
                        value: hasParent ? _settings.parentItem._id : null
                    },
                    _level: {
                        value: level
                    },
                    _baseCollectionId: {
                        value: hasParent ? parentCollection._baseCollectionId : collectionId
                    },
                    _items: {
                        value: data.map(function(item, i){
                            return new BWCollectionItem(item, collectionId, breadCrumbs);
                        }),
                        writable: true
                    },
                    _isProtected: {
                        value: isProtected
                    },
                    _hasNestedCollections: {
                        value: false,
                        writable: true
                    },
                    _isBaseCollection: {
                        value: isBaseCollection
                    },
                    _breadCrumbs: {
                        value: breadCrumbs
                    },

                    /** Public props */
                    selected: {
                        get: function(){
                            return bwCollection.getSelectedItem();
                        }
                    },
                    numberOfItems: {
                        get: function(){
                            return bwCollection._items.length;
                        }
                    },
                    content: {
                        value: {},
                        writable: true
                    },
                    multiselect: {
                        value: multiselect
                    },
                    /** Public meths */
                    getParentItem: {
                        value: function(){
                            return (bwCollection._parentCollectionId && bwCollection._parentItemId && getCollection(bwCollection._parentCollectionId).getItem(bwCollection._parentItemId)) || null;
                        }
                    },
                    getNestedCollectionsByLevel: {
                        value: function(level){
                            return bwCollection.getNestedCollections().filter(function(c){
                                return c._level === level;
                            });
                        }
                    },
                    getNestedCollections: {
                        value: function (fromBaseCollection){
                            var _collections = [],
                                startFromThisOne = fromBaseCollection ? getCollection(bwCollection._baseCollectionId) : bwCollection;

                            var selectAllCollections = function (collection){
                                var items = collection.getItems();
                                items.forEach(function(item){
                                    if(item.nestedCollection) {
                                        selectAllCollections(item.nestedCollection);
                                    }
                                });
                                _collections.push(collection);
                            };

                            selectAllCollections(startFromThisOne);

                            return _collections;
                        }
                    },
                    getSelectedItems: {
                        value: function (){
                            return bwCollection._items.filter(function(item){
                                return item.isSelected;
                            });
                        }
                    },
                    getSelectedItem: {
                        value: function (){
                            return bwCollection._items.filter(function(item){
                                return item.isSelected;
                            })[0];
                        }
                    },
                    getSelectedIndex: {
                        value: function () {
                            var selectedIndex;
                            bwCollection.getItems().forEach(function (item, i, arr) {
                                if (item === bwCollection.getSelectedItem()) {
                                    selectedIndex = i;
                                }
                            });
                            return selectedIndex;
                        }
                    },
                    getAllSelectedItems: {
                        value: function(){
                            return bwCollection.getNestedCollections().map(function(_collection){
                                    return _collection.selected;
                                })
                                .filter(function(item){
                                    if(item) {
                                        var collection = item.getCollection();
                                        item.misc.level = collection && collection._level;
                                    }
                                    return item;
                                })
                                .sort(function(itemA, itemB){
                                    var levelA = itemA.misc.level,
                                        levelB = itemB.misc.level;

                                    if(levelA < levelB) {
                                        return -1;
                                    }
                                    if(levelA > levelB) {
                                        return 1;
                                    }
                                    return 0;
                                });
                        }
                    },
                    getItemsDividedByGroups: {
                        value: function(groupingProperty, preventSorting){
                            var items = bwCollection.getItems(),
                                vals = [],
                                groups = [];

                            if(!groupingProperty) {
                                groups.push({
                                    label: 'All',
                                    items: items
                                });
                            } else {
                                items.forEach(function(item){
                                    var val = item.content[groupingProperty],
                                        index = vals.indexOf(val);

                                    if(index === -1) {
                                        vals.push(val);
                                        groups.push({
                                            label: val,
                                            items: [item]
                                        });
                                    } else {
                                        groups[index].items.push(item);
                                    }
                                });
                            }

                            if(!preventSorting) {
                                groups.sort(function(a, b){
                                    var propA = a.label,
                                        propB = b.label;

                                    if(propA > propB) {
                                        return 1;
                                    } else if(propA < propB) {
                                        return -1;
                                    } else {
                                        return 0;
                                    }
                                });
                            }

                            return groups;
                        }
                    },
                    select: {
                        value: function (item){
                            var affectedCollections = item.getAffectedCollections();
                            bwCollection.getNestedCollections(true).forEach(function(collection){
                                var collectionId = collection._id;
                                collection.getItems().forEach(function(_item){
                                    if(collection.multiselect) {
                                        _item.isSelected = _item.isSelected || affectedCollections[collectionId] === _item._id;
                                    } else {
                                        _item.isSelected = affectedCollections[collectionId] === _item._id;
                                    }
                                });
                            });
                            return affectedCollections;
                        }
                    },
                    selectList: {
                        value: function (prop, indexes){
                            if(bwCollection.multiselect) {
                                indexes.forEach(function(index){
                                    var item = bwCollection.getItemsByProperty(prop, index)[0];
                                    item && item.select();
                                });
                            } else {
                                (list && list[0]) && list[0].select();
                            }
                        }
                    },
                    drop: {
                        value: function(){
                            return dropCollections(bwCollection.getNestedCollections());
                        }
                    },
                    deselectAll: {
                        value: function (item){
                            var nestedCollections = bwCollection.getNestedCollections();
                            nestedCollections.forEach(function(collection){
                                collection.getItems().forEach(function(_item){
                                    _item.isSelected = false;
                                });
                            });
                            return nestedCollections;
                        }
                    },
                    getItems: {
                        value: function (){
                            return bwCollection._items;
                        }
                    },
                    getItem: {
                        value: function (id){
                            return bwCollection.getItems().filter(function(item){
                                return item._id === id;
                            })[0];
                        }
                    },
                    getItemsByProperty: {
                        value: function(prop, val){
                            return bwCollection.getItems().filter(function(item){
                                return item.content[prop] === val;
                            });
                        }
                    },
                    turnToObject: {
                        value: function(prop) {
                            var items = bwCollection.getItems(),
                                _items = {};
                            items.forEach(function(item){
                                _items[item.content[prop]] = item.content;
                            });
                            return _items;
                        }
                    },
                    changeItems: {
                        value: function(newData, newItemsSettings, deep){
                            var _newItemsSettings = newItemsSettings || {},
                                selectedIndex = !isNaN(_newItemsSettings.selectedIndex) ? _newItemsSettings.selectedIndex : null,
                                selectedItems = _newItemsSettings.selectedItems || null,
                                changeDeep = _newItemsSettings.deep,
                                indexBy = _newItemsSettings.indexBy || null;

                            bwCollection.getItems().forEach(function(item){
                                item.dropNestedCollections();
                            });

                            bwCollection._items = newData.map(function(item){
                                return new BWCollectionItem(item, collectionId, breadCrumbs);
                            });

                            if(selectedIndex !== null) {
                                bwCollection.getItems()[newItemsSettings.selectedIndex].select();
                            }

                            if(selectedItems && indexBy){
                                var indexes = selectedItems.map(function(item){
                                    return item.content[indexBy];
                                });

                                bwCollection.getItems().forEach(function(item){
                                    if(indexes.indexOf(item.content[indexBy]) !== -1){
                                        item.select();
                                    }
                                });
                            }

                            if(changeDeep){
                                bwCollection.getItems().forEach(function(layer){
                                    if(layer.content.items || layer.content.bars || layer.content.points || layer.content.subSlices || layer.content.pages) {
                                        layer.createNestedCollection(layer.content.items || layer.content.bars || layer.content.points || layer.content.subSlices || layer.content.pages);
                                    }
                                });
                            }

                            return bwCollection;
                        }
                    },
                    addItems: {
                        value: function(newItems){
                            var _newItems = newItems.map(function(item){
                                return new BWCollectionItem(item, collectionId, breadCrumbs);
                            });
                            bwCollection._items = bwCollection._items.concat(_newItems);
                            return bwCollection;
                        }
                    },
                    getHash: {
                        value: function(prop){
                            return bwCollection.getItems().map(function(item){
                                return item.content[prop];
                            }).join('_');
                        }
                    },
                    resetClasses: {
                        value: function(){
                            bwCollection.getItems().forEach(function(item){
                                item.resetClasses();
                            });
                        }
                    },
                    addClass: {
                        value: function(prop, val, className, opposite){
                            bwCollection.getItems().forEach(function(item){
                                var _prop = item.content[prop];
                                if((opposite && _prop !== val) || (!opposite && _prop === val)) {
                                    item.addClass(className);
                                }
                            });
                        }
                    },
                    removeClass: {
                        value: function(className){
                            bwCollection.getItems().forEach(function(item){
                                item.removeClass(className);
                            });
                        }
                    }
                });

                if(!isNaN(_settings.selectedIndex) && bwCollection._items[_settings.selectedIndex]) {
                    bwCollection._items[_settings.selectedIndex].isSelected = true;
                }
            }

            /** Abstract Item Constructor */
            function BWCollectionItem (data, collectionId, breadCrumbs){
                var bwItem = this,
                    itemId = 'bwi#' + itemCounter++;

                Object.defineProperties(bwItem, {
                    /** Private props */
                    _id: {
                        value: itemId
                    },
                    _collectionId: {
                        value: collectionId
                    },
                    _breadCrumbs: {
                        value: breadCrumbs + ':' + itemId
                    },
                    _classes: {
                        value: [],
                        writable: true
                    },

                    /** Public props */
                    isSelected: {
                        value: false,
                        writable: true
                    },
                    isDisabled: {
                        value: false,
                        writable: true
                    },
                    content: {
                        value: data,
                        writable: true
                    },
                    nestedCollection: {
                        value: null,
                        writable: true
                    },
                    misc: {
                        value: {},
                        writable: true
                    },

                    /** Public meths */
                    select: {
                        value: function(){
                            return bwItem.getCollection().select(bwItem);
                        }
                    },
                    deselect: {
                        value: function(){
                            bwItem.isSelected = false;
                            return bwItem.nestedCollection ? bwItem.nestedCollection.deselectAll() : [];
                        }
                    },
                    createNestedCollection: {
                        value:  function(nestedCollectionData, nestedCollectionSettings){
                            bwItem.nestedCollection = bwCollectionService.createNew(nestedCollectionData,
                                angular.merge(
                                    { parentItem: bwItem },
                                    nestedCollectionSettings || {}
                                )
                            );

                            return bwItem.nestedCollection;
                        }
                    },
                    dropNestedCollections: {
                        value: function(){
                            dropCollections((bwItem.nestedCollection && bwItem.nestedCollection.getNestedCollections()) || []);
                            bwItem.nestedCollection = null;
                            return bwItem;
                        }
                    },
                    getCollection: {
                        value: function(){
                            return getCollection(bwItem._collectionId);
                        }
                    },
                    getAffectedCollections: {
                        value: function(){
                            var breadCrumbs = bwItem._breadCrumbs.split('_'),
                                affectedCollections = {};

                            breadCrumbs.forEach(function(breadCrumb){
                                var _breadCrumb = breadCrumb.split(':');
                                affectedCollections[_breadCrumb[0]] = _breadCrumb[1];
                            });

                            return affectedCollections;
                        }
                    },
                    addClass: {
                        value: function(className) {
                            if(!bwItem.hasClass(className)) {
                                bwItem._classes.push(className);
                            }
                        }
                    },
                    removeClass: {
                        value: function(className) {
                            if(bwItem.hasClass(className)) {
                                bwItem._classes.splice(bwItem._classes.indexOf(className), 1);
                            }
                        }
                    },
                    hasClass: {
                        value: function(className) {
                            return bwItem._classes.indexOf(className) !== -1;
                        }
                    },
                    resetClasses: {
                        value: function(){
                            bwItem._classes.length = 0;
                        }
                    }
                });
            }

            /** Drop unprotected collections when changing state */
            $rootScope.$on('$stateChangeStart', function(){ dropAll();});

        }
    ]);
}(angular.module('bw.utilities')));

