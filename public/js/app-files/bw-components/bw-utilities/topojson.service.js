(function(module){

    'use strict';

    module.service('topojson', [
        function(){
            var topojson = window.topojson;
            window.topojson = undefined;
            return topojson;
        }
    ]);

}(angular.module('bw.utilities')));

