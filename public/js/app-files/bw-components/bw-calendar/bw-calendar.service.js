(function(module){

    'use strict';

    module.service('BWCalendarService', [
        'AsyncAPI', 'BWCollectionService', 'BWColorsService', 'd3', 'moment',
        function(AsyncAPI, BWCollectionService, BWColorsService, d3, moment){
            var bwCalendarService = this,
                calendarCounter = 0;

            Object.defineProperties(bwCalendarService, {
                createNew: {
                    value: function(){
                        return new BWCalendarEntity(calendarCounter++);
                    }
                }
            });

            function BWCalendarEntity (id){
                var bwCalendarEntity = this;
                Object.defineProperties(bwCalendarEntity, {
                    id: {
                        value: id
                    },
                    _api_: {
                        value: AsyncAPI.get({})
                    }
                });
            }

            BWCalendarEntity.prototype.refreshData = function(data){
                this._api_.applyFn('refreshData', data);
            };

            BWCalendarEntity.prototype.refreshView = function(){
                this._api_.applyFn('refreshView');
            };

            BWCalendarEntity.prototype.getRandomData = function(currentDate, range){ // JS Date
                var currentTimestamp = (currentDate && currentDate.getTime()) || new Date().getTime(),
                    fromDate = moment(currentTimestamp).startOf(range || 'month'),
                    toDate = moment(currentTimestamp).endOf(range || 'month'),
                    oneDay = 86400,
                    days = [],
                    i = fromDate.unix(),
                    j = toDate.unix(),
                    events = 'And I saw when the Lamb opened one of the seals, and I heard, as it were the noise of thunder, one of the four beasts saying, Come and see_That`s one small step for a man, one giant leap for mankind_AHHHH! GODZILLA!!!_Banana, please ))))_You are just a marmoset-astronaut_...We shall go on to the end. We shall fight in France, we shall fight on the seas and oceans, we shall fight with growing confidence and growing strength in the air, we shall defend our island, whatever the cost may be. We shall fight on the beaches, we shall fight on the landing grounds, we shall fight in the fields and in the streets, we shall fight in the hills; we shall never surrender...'.split('_');

                for(; i < j ; i += oneDay ){
                    var date = moment(i * 1000).startOf('day'),
                        randomValue = parseInt(Math.random() * 10000000),
                        randomEvent = events[parseInt(Math.random() * events.length)];

                    days.push({
                        date: date,
                        value: randomValue,
                        event: randomEvent
                    });
                }

                return days;
            };


        }
    ]);

}(angular.module('bw.calendar')));

