(function(module){

    'use strict';

    module.directive('bwCalendar', [
        function(){
            return {
                restrict: 'A',
                scope: {
                    model: '=bwCalendar',
                    userSettings: '=bwCalendarSettings'
                },
                template: '' +
                    '<div class="bw-calendar-container">' +
                        '<div class="bw-calendar-controllers" ng-show="settings.showControllers">' +
                            '<i class="fa fa-angle-left" ng-click="stepBwd()"></i>' +
                            '<span class="bw-calendar-label"></span>' +
                            '<i class="fa fa-angle-right" ng-click="stepFwd()"></i>' +
                        '</div>' +
                        '<div class="bw-calendar-weekdays" ng-show="settings.showDays"></div>' +
                        '<div class="bw-calendar-grid" ng-show="settings.showGrid"></div>' +
                    '</div>' +
                '',
                controller: BWCalendarDirectiveCtrl
            }
        }
    ]);

    BWCalendarDirectiveCtrl.$inject = ['$scope', '$element', 'd3', 'moment', 'BWColorsService'];
    function BWCalendarDirectiveCtrl ($scope, $element, d3, moment, BWColorsService){
        var self = this,
            defaultSettings = {
                showGrid: true,
                showDays: true,
                showControllers: true,
                range: 'month',
                fontSize: 10,
                cellWidth: 26,
                cellHeight: 26,
                colors: ['#054424', '#DDDDDD'],
                format: 'MMMM YYYY',

                getCellValue: null,

                onMouseOver: angular.noop,
                onMouseMove: angular.noop,
                onMouseOut: angular.noop,
                onClick: angular.noop,

                onStepFwdClick: angular.noop,
                onStepBwdClick: angular.noop
            };

        self.wrapper = d3.select($element.get(0)).classed('bw-calendar-wrapper', true);
        self.container = self.wrapper.select('.bw-calendar-container');
        self.cotrollersContainer = self.container.select('.bw-calendar-controllers');
        self.weekdaysContainer = self.container.select('.bw-calendar-weekdays');
        self.gridContainer = self.container.select('.bw-calendar-grid');

        self._data_ = null;

        Object.defineProperties($scope, {
            settings: {
                get: function() {
                    return angular.merge(defaultSettings, $scope.userSettings || {});
                }
            },
            stepBwd: {
                value: function(){
                    var newDate =  moment(self._data_.getItems()[0].content.date.unix() * 1000);
                    $scope.settings.onStepBwdClick(newDate.subtract(1, 'day'));
                }
            },
            stepFwd: {
                value: function(){
                    var newDate = moment(self._data_.getItems()[self._data_.getItems().length - 1].content.date.unix() * 1000);
                    $scope.settings.onStepBwdClick(newDate.add(1, 'day'));
                }
            }
        });

        Object.defineProperties($scope.model._api_, {
            refreshData: {
                value: function(data) {
                    self._data_ = data;
                    $scope.model._api_.refreshView();
                }
            },
            refreshView: {
                value: function() {
                    var renderingData = getRenderingData(),
                        widgetWidth = $scope.settings.cellWidth * 7,
                        labelWidth = $scope.settings.cellWidth * 5;

                    self.container.style({
                        width: widgetWidth + 'px'
                    });

                    self.cotrollersContainer
                        .style({
                            width: labelWidth + 'px'
                        })
                        .select('.bw-calendar-label')
                        .html(getLabel);

                    self.gridContainer.style({
                        height: $scope.settings.range === 'month' ? ((renderingData[renderingData.length - 1].weekInMonth + 1) * $scope.settings.cellHeight) + 'px' : 0
                    });

                    var cells = self.gridContainer.selectAll('.calendar-cell').data(renderingData);

                    cells.enter().append('div').classed('calendar-cell', true);

                    cells.exit().remove();

                    cells
                        .attr({
                            class: function(d){
                                return 'calendar-cell ' + (d.isLastDayInWeek || d.isLastDayInMonth ? 'has-border-right ' : ' ') + (d.isLastWeekInMonth ? 'has-border-bottom ' : ' ') + (d.entity.isSelected ? 'selected': '');
                            }
                        })
                        .style({
                            position: 'absolute',
                            left: function(d){
                                return (d.dayInWeek * $scope.settings.cellWidth) + 'px';
                            },
                            top: function(d){
                                return (d.weekInMonth * $scope.settings.cellHeight) + 'px';
                            },
                            width: $scope.settings.cellWidth + 'px',
                            height: $scope.settings.cellHeight + 'px',
                            background: function(d){
                                return d.color;
                            },
                            color: function(d){
                                return BWColorsService.getContrastyColor(d.color);
                            },
                            'font-size': $scope.settings.fontSize + 'px'
                        })
                        .html(function(d){
                            return '' +
                                '<div>' +
                                    '<span>' + d.label + '</span>' +
                                '</div>' +
                            '';
                        })
                        .on('mouseover', onMouseOver)
                        .on('mousemove', onMouseMove)
                        .on('mouseout', onMouseOut)
                        .on('click', onClick);

                    cells.select('div').style({
                        width: ($scope.settings.cellWidth - 2) + 'px',
                        height: ($scope.settings.cellHeight - 2) + 'px'
                    });

                }
            }
        });

        function getRenderingData (){
            if($scope.settings.range !== 'month') {
                return [];
            }

            var items = self._data_.getItems(),
                startFrom = parseInt(items[0].content.date.format('E')) - 1,
                values = items.map(function(item){
                    return item.content.value;
                }),
                colorScheme = BWColorsService.getColorScheme($scope.settings.colors, [d3.min(values), d3.max(values)]),
                totalWeeks = Math.floor((startFrom + (items.length - 1)) / 7);

            return items.map(function(item){
                var content = item.content,
                    dayInWeek = content.date.format('E') - 1,
                    dayInMonth = content.date.format('D') - 1,
                    weekInMonth = Math.floor((startFrom + dayInMonth) / 7);

                return {
                    entity: item,
                    date: content.date,
                    dayInWeek: dayInWeek,
                    dayInMonth: dayInMonth,
                    weekInMonth: weekInMonth,
                    isLastDayInWeek: dayInWeek === 6,
                    isLastWeekInMonth: weekInMonth === totalWeeks,
                    isLastDayInMonth: dayInMonth === (items.length - 1),
                    value: content.value,
                    color: colorScheme(content.value),
                    label: ($scope.settings.getCellValue && (typeof $scope.settings.getCellValue === 'function') && $scope.settings.getCellValue(item)) || content.date.format('Do')
                };
            });
        }

        function getLabel (){
            if($scope.settings.range === 'month') {
                return self._data_.getItems()[0].content.date.format($scope.settings.format);
            } else if($scope.settings.range === 'week') {
                var days = self._data_.getItems(),
                    fromDate = days[0].content.date,
                    toDate = days[6].content.date;
                return fromDate.format('MMM. D') + ' - ' + toDate.format('MMM. D') + ', ' + fromDate.format('YYYY');
            } else if($scope.settings.range === 'year'){
                return self._data_.getItems()[0].content.date.format('YYYY');
            }
        }

        function onMouseOver (d, i){
            var event = d3.event;

            toggleCallback('onMouseOver' , d, {
                x: event.clientX,
                y: event.clientY
            }, event);
        }

        function onMouseMove (d, i){
            var event = d3.event;

            toggleCallback('onMouseMove' , d, {
                x: event.clientX,
                y: event.clientY
            }, event);
        }

        function onMouseOut (d, i){
            var event = d3.event;

            toggleCallback('onMouseOut' , d, {
                x: event.clientX,
                y: event.clientY
            }, event);
        }

        function onClick (d, i){
            var event = d3.event;

            //d.entity.select();

            toggleCallback('onClick' , d, {
                x: event.clientX,
                y: event.clientY
            }, event, toggleCell(d.entity, d3.select(this)));
        }

        function toggleCallback (callback, cell, coords, event, fn){
            $scope.settings[callback](cell.entity, coords, event, fn);
        }

        function toggleCell (d, cell){
            return function(){
                d[d.isSelected ? 'deselect' : 'select' ]();
                self.gridContainer.selectAll('.calendar-cell').classed('selected', function(_d){
                    return _d.entity.isSelected;
                });
            }
        }
    }

}(angular.module('bw.calendar')));

