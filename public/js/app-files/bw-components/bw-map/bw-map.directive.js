(function(module){

    'use strict';

    module.directive('bwMap', [
        function(){
            return {
                restrict: 'A',
                scope: {
                    model: '=bwMap',
                    userSettings: '=bwMapSettings'
                },
                template: '' +
                '<div class="bw-map-container">' +
                    '<svg class="bw-map-canvas">' +
                        '<g class="bw-map-layout">' +
                            '<g class="bw-map-areas-third-layer"></g>' +
                            '<g class="bw-map-areas-second-layer"></g>' +
                            '<g class="bw-map-areas-first-layer"></g>' +
                        '</g>' +
                        '<g class="bw-map-area-details"></g>' +
                    '</svg>' +
                    '<div class="bw-map-overlay is-loading">' +
                        '<div>LOADING DATA...</div>' +
                    '</div>' +
                    '<div class="bw-map-zoom-controllers">' +
                        '<i class="fa fa-plus" ng-click="zoomIn()"></i>' +
                        '<i class="fa fa-minus" ng-click="zoomOut()"></i>' +
                    '</div>' +
                '</div>' +
                '',
                controller: BWMapDirectiveCtrl
            };
        }
    ]);

    BWMapDirectiveCtrl.$inject = ['$scope', '$element', '$q', '$timeout', 'd3', 'topojson', 'BWSharedService', 'BWColorsService'];
    function BWMapDirectiveCtrl ($scope, $element, $q, $timeout, d3, topojson, BWSharedService, BWColorsService) {
        var self = this,
            watchers = [],
            path = null,
            projection = null,
            zoom = null,
            showWorldMap = false,
            showUSAMap = false,
            defaultSettings = {
                width: 500,
                height: 300,
                autoresize: true,
                zoomStep: 0.4,
                watchers: [],

                bgColor: '#e5f7fd',
                areaColor: '#FFF',

                /** Callbacks */
                onMouseOver: angular.noop,
                onMouseOut: angular.noop,
                onMouseMove: angular.noop,
                onClick: angular.noop
            };

        self.containerWidth = 0;
        self.containerHeight = 0;
        self.zoomed = false;
        self.zoomLevel = 0;

        self._data_ = null;
        self._map_ = null;

        self.wrapper = d3.select($element.get(0)).classed('bw-map-wrapper', true);
        self.container = self.wrapper.select('.bw-map-container');
        self.canvas = self.container.select('.bw-map-canvas');
        self.layout = self.canvas.select('.bw-map-layout');
        self.firstLayerAreas = self.layout.select('.bw-map-areas-first-layer');
        self.secondLayerAreas = self.layout.select('.bw-map-areas-second-layer');
        self.thirdLayerAreas = self.layout.select('.bw-map-areas-third-layer');
        self.overlay = self.container.select('.bw-map-overlay');
        self.zoomControllers = self.container.select('.bw-map-zoom-controllers');
        self.loader = self.container.select('.bw-map-loader');

        self.layoutScale = 1;
        self.layoutPosition = [0, 0];

        $scope.userSettings = $scope.userSettings || {};

        Object.defineProperties($scope, {
            settings: {
                get: function(){
                    return angular.merge(defaultSettings, $scope.userSettings);
                }
            },
            zoomIn: {
                value: function(){

                }
            },
            zoomOut: {
                value: function(){

                }
            }
        });

        Object.defineProperties($scope.model._api_, {
            refreshData: {
                value: function(data, territoryName){
                    showWorldMap = territoryName === 'world';
                    showUSAMap = territoryName === 'usa';

                    loadMap(territoryName).then(function(res){
                        self._data_ = data;
                        self._map_ = res;

                        $scope.model._api_.refreshView();
                    });
                }
            },
            refreshView: {
                value: function(){
                    refreshSizes();

                    refreshAreas();

                    refreshCanvasInteractions();
                }
            },
            getZoomState: {
                value: function(){
                    return self.zoomed;
                }
            },
            toggleLoading: {
                value: function(state){
                    self.overlay.classed('is-loading', state);
                }
            }
        });

        /** Watchers */
        $scope.settings.watchers.forEach(function(propertyToWatch){
            watchers.push(
                $scope.$watch(
                    function(){
                        return BWSharedService.getProperty(propertyToWatch);
                    },
                    function(curr, prev){
                        if(curr !== prev) {
                            $scope.model._api_.refreshView(true);
                        }
                    }
                )
            );
        });

        refreshSizes();

        /** Privates */
        function refreshSizes (){
            var parentItem = self.wrapper.node().parentNode,
                settings = $scope.settings;

            self.containerWidth = settings.autoresize ? (parentItem.clientWidth || settings.width) : settings.width;
            self.containerHeight = settings.autoresize ? (parentItem.clientHeight || settings.height) : settings.height;

            [self.wrapper, self.container, self.canvas, self.overlay].forEach(function(node){
                node.style({
                    width: self.containerWidth + 'px',
                    height: self.containerHeight + 'px'
                });
            });

            self.canvas.style({background: settings.bgColor });
        }

        function refreshAreas (){
            var settings = $scope.settings,
                clickingTimestamp,
                mapData;

            if(showWorldMap) {
                self.thirdLayerAreas.selectAll('*').remove();
                self.secondLayerAreas.selectAll('*').remove();

                projection = d3.geo.mercator().translate([self.containerWidth / 2, self.containerHeight / 2]);
                path =  d3.geo.path().projection(projection);

                mapData = $scope.model.getWorldRenderingData(
                    topojson.feature(self._map_, self._map_.objects.countries).features,
                    self._data_,
                    settings
                );

                var countries = drawAreas(self.firstLayerAreas, mapData);

                countries
                    .on('mouseover', function(d){
                        d3.select(this)
                            .transition().duration(150)
                            .attr({
                                fill: function(d){
                                    return BWColorsService.getDarkerColor(d.properties.data.color, 5);
                                }
                            });

                        settings.onMouseOver(d.properties.data, getCoords());
                    })
                    .on('mousemove', function(d){
                        settings.onMouseMove(d.properties.data, getCoords());
                    })
                    .on('mouseout', function(d){
                        d3.select(this)
                            .transition().duration(150)
                            .attr({
                                fill: function(d){
                                    return d.properties.data.color;
                                }
                            });

                        settings.onMouseOut(d.properties.data, getCoords());
                    })
                    .on('mousedown', function(d){
                        clickingTimestamp = d3.event.timeStamp;
                    })
                    .on('mouseup', function(d){
                        if(d3.event.timeStamp - clickingTimestamp < 120) {
                            settings.onClick(d.properties.data, getCoords(), {
                                zoomIn: zoomToArea(d),
                                zoomOut: abortZooming(d),
                                zoomed: self.zoomed
                            });
                        }
                    });

            } else if(showUSAMap) {
                self.thirdLayerAreas.selectAll('*').remove();

                projection = d3.geo.albersUsa().translate([self.containerWidth / 2, self.containerHeight / 2]);
                path =  d3.geo.path().projection(projection);

                mapData = $scope.model.getUSARenderingData(
                    topojson.feature(self._map_, self._map_.objects.states).features,
                    topojson.feature(self._map_, self._map_.objects.counties).features,
                    self._data_,
                    settings
                );

                [
                    {
                        layout: self.firstLayerAreas,
                        data: mapData.states
                    },
                    {
                        layout: self.secondLayerAreas,
                        data: mapData.counties
                    }
                ].forEach(function(datum, i){
                    var areas = drawAreas(datum.layout, datum.data);

                    if(i) {
                        areas
                            .on('mouseover', function(d){
                                d3.select(this)
                                    .transition().duration(150)
                                    .attr({
                                        fill: function(d){
                                            return BWColorsService.getDarkerColor(d.properties.data.color, 5);
                                        }
                                    });

                                settings.onMouseOver(d.properties.data, getCoords());
                            })
                            .on('mousemove', function(d){
                                settings.onMouseMove(d.properties.data, getCoords());
                            })
                            .on('mouseout', function(d){
                                d3.select(this)
                                    .transition().duration(150)
                                    .attr({
                                        fill: function(d){
                                            return d.properties.data.color;
                                        }
                                    });

                                settings.onMouseOut(d.properties.data, getCoords());
                            })
                            .on('mousedown', function(d){
                                clickingTimestamp = d3.event.timeStamp;
                            })
                            .on('mouseup', function(d){
                                if(d3.event.timeStamp - clickingTimestamp < 120) {
                                    settings.onClick(d.properties.data, getCoords(), {
                                        zoomOut: abortZooming(d),
                                        zoomed: self.zoomed
                                    });
                                }
                            });
                    }
                    else {
                        areas
                            .on('mouseover', function(d){
                                d3.select(this)
                                    .transition().duration(150)
                                    .attr({ 'fill-opacity': 0.5 });

                                settings.onMouseOver(d.properties.data, getCoords());
                            })
                            .on('mousemove', function(d){
                                settings.onMouseMove(d.properties.data, getCoords());
                            })
                            .on('mouseout', function(d){
                                d3.select(this)
                                    .transition().duration(150)
                                    .attr({ 'fill-opacity': 1 });

                                settings.onMouseOut(d.properties.data, getCoords());
                            })
                            .on('mousedown', function(d){
                                clickingTimestamp = d3.event.timeStamp;
                            })
                            .on('mouseup', function(d){
                                if(d3.event.timeStamp - clickingTimestamp < 120) {
                                    settings.onClick(d.properties.data, getCoords(),
                                        {
                                            zoomIn: zoomToArea(d, true),
                                            zoomOut: abortZooming(d),
                                            zoomed: self.zoomed
                                        }
                                    );
                                }
                            });
                    }
                });
            }

            $scope.model._api_.toggleLoading(false);
        }

        function refreshCanvasInteractions (){
            zoom = d3.behavior.zoom()
                .translate([0, 0])
                .scale(1)
                .scaleExtent([0, 5])
                .on("zoom", onZoom);

            self.canvas
                .call(zoom)
                .call(zoom.event);

            function onZoom(){
                if(!self.zoomed) {
                    self.layoutPosition = d3.event.translate;
                    self.layoutScale = d3.event.scale;
                    zoom.scale(self.layoutScale);
                    zoom.translate(self.layoutPosition);
                    self.layout.attr({transform : 'translate(' + zoom.translate() + ')scale(' + zoom.scale() + ')'});

                    var strokeWidth = 1.5 / zoom.scale();

                    [self.firstLayerAreas, self.secondLayerAreas, self.thirdLayerAreas].forEach(function(layer){
                        layer.selectAll('path').attr({ 'stroke-width': strokeWidth < 1 ? strokeWidth : 1 });
                    });
                }
            }

            self.container.on('mousewheel', function(){
                if(self.zoomed) {
                    abortZooming()();
                }
            });
        }

        function loadMap (territoryName){
            return $scope.model.getMapCanvas(territoryName);
        }

        function getCoords (){
            var event = d3.event;

            return {
                x: event.clientX,
                y: event.clientY
            };
        }

        function toggleArea (d){
            return function(preventToggling){
                if(!preventToggling) {
                    var entity = d.properties.data.entity;
                    entity[entity.isSelected ? 'deselect' : 'select']();
                }

                self.areas.selectAll('.map-area')
                    .select('.interactive-area')
                    .attr({
                        fill: function(a){
                            return a.properties.data.entity.isSelected ? 'rgba(0,0,0,0.2)' : 'rgba(0,0,0,0)';
                        }
                    });
            }
        }

        function zoomToArea (d, showSublayer){
            return function(){
                self.zoomed = true;

                var b = path.bounds(d),
                    fromX = b[0][0],
                    toX = b[1][0],
                    fromY = b[0][1],
                    toY = b[1][1],
                    wScale = (toX - fromX) / self.containerWidth,
                    hScale = (toY - fromY) / self.containerHeight;

                if(showSublayer) {
                    self.firstLayerAreas.selectAll('.map-area')
                        .attr({
                            transform: function(dd){
                                return 'scale(' + (d.id === dd.id ? 0 : 1) + ')';
                            }
                        });
                }

                return refreshLayout({
                    translate: [-(toX + fromX) / 2, -(toY + fromY) / 2],
                    scale: 0.99 / d3.max([wScale, hScale]),
                    transition: 750
                }, true);
            }
        }

        function abortZooming (d){
            return function(){
                self.zoomed = false;

                self.firstLayerAreas.selectAll('.map-area').attr({transform: 'scale(1)'});

                return refreshLayout({
                    translate: [0, 0],
                    scale: 1,
                    transition: 750
                });
            }
        }

        function drawAreas (layout, renderingData){
            var areas = layout.selectAll('.map-area').data(renderingData);

            areas.enter().append('path');

            areas.exit().remove();

            areas.attr({
                class: function(d){
                    return 'map-area area-id-' + d.id;
                },
                stroke: '#d4d9df',
                'stroke-width': 0.5,
                'stroke-linecap': 'round',
                'stroke-linejoin': 'round',
                fill: function(d){
                    return d.properties.data.color;
                },
                d: path
            });

            return layout.selectAll('.map-area');
        }

        function refreshLayout (params, useProjectionOffset){
            var strokeWidth = 1.5 / params.scale;

            self.layout
                .transition().duration(params.transition || 0)
                .attr({
                    transform: (useProjectionOffset ? 'translate(' + projection.translate() + ')' :  '') +
                        'scale(' + params.scale + ')' +
                        'translate(' + params.translate + ')'
                });

            [self.firstLayerAreas, self.secondLayerAreas, self.thirdLayerAreas].forEach(function(layer){
                layer.selectAll('path')
                    .attr({
                        'stroke-width': strokeWidth < 1 ? strokeWidth : 1
                    });
            });

            if(!useProjectionOffset) {
                self.layoutScale = params.scale;
                self.layoutPosition = params.translate;

                zoom.scale(self.layoutScale);
                zoom.translate(self.layoutPosition);
            }
        }


    }
}(angular.module('bw.map')));
