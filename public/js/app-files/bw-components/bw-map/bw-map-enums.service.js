(function(module){

    'use strict';

    module.service('BWMapEnums', [
        'BWCollectionService',
        function (BWCollectionService){
            var service = this,
                availableMaps = BWCollectionService.createNew([
                    {
                        name: 'world',
                        label: 'World'
                    }
                ]);

            Object.defineProperties(service, {
                getAvailableMaps: {
                    value: function(){
                        return availableMaps;
                    }
                }
            });
        }
    ]);


}(angular.module('bw.map')));

