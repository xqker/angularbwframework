(function(module){

    'use strict';

    module.service('BWMapService', [
        'AsyncAPI', 'BWCollectionService', 'BWColorsService', 'd3', 'moment', 'AccessPointService', '$q',
        function(AsyncAPI, BWCollectionService, BWColorsService, d3, moment, AccessPointService, $q){
            var mapService = this,
                mapCounter = 0;

            Object.defineProperties(this, {
                createNew: {
                    value: function(){
                        return new BWMapEntity(mapCounter++);
                    }
                }
            });

            function BWMapEntity (id){
                var bwChartEntity = this;
                Object.defineProperties(bwChartEntity, {
                    id: {
                        value: '#bw-chart-' + id
                    },
                    _api_: {
                        value: AsyncAPI.get({})
                    }
                });
            }

            BWMapEntity.prototype.refreshData = function(data, territory){
                this._api_.applyFn('refreshData', data, territory);
            };

            BWMapEntity.prototype.refreshView = function(){
                this._api_.applyFn('refreshView');
            };

            BWMapEntity.prototype.getZoomState = function(){
                return this._api_.applyFn('getZoomState');
            };

            BWMapEntity.prototype.toggleLoading = function(state){
                return this._api_.applyFn('toggleLoading', state);
            };

            BWMapEntity.prototype.getMapCanvas = function(territory){
                territory = territory || 'world';

                return AccessPointService.getJSONMap(territory);
            };

            BWMapEntity.prototype.getCountries = function(){
                return AccessPointService.getCountries();
            };

            BWMapEntity.prototype.getRandomWorldData = function(){
                return AccessPointService.getCountries().then(function(res){
                    return res.map(function(country){
                        return {
                            name: country.name,
                            id: country.iso_n3,
                            flag: country.iso_a2.toLowerCase(),
                            isSovereign: country.type !== 'Dependency',
                            sovereignt: country.sovereignt,
                            continent: country.continent,
                            region: country.region_wb,
                            subregion: country.subregion,
                            population: parseInt(country.pop_est),
                            gdp: parseFloat(country.gdp_md_est),
                            economy: country.economy,
                            incomeGrp: country.income_grp,
                            totalPar: Math.random() * 100000000,
                            averageYield: parseInt(Math.random() * 16) - 4,
                            numberOfSecurities: parseInt(Math.random() * 5000)
                        };
                    });
                });
            };

            BWMapEntity.prototype.getRandomUSAData = function(){
                return $q.all([
                    AccessPointService.getStates(),
                    AccessPointService.getCounties()
                ]).then(function(res){
                    var counties = res[1].map(function(county){
                            var countyId = county.id + '',
                                stateId = parseInt((county.id + '').substr(0, countyId.length - 3));

                            return {
                                name: county.name,
                                id: county.id,
                                stateId: stateId,
                                totalPar: Math.random() * 1000000,
                                averageYield: parseInt(Math.random() * 16) - 4,
                                numberOfSecurities: parseInt(Math.random() * 5000)
                            };
                        });

                    return res[0].map(function(state){
                        var totalPar = 0,
                            averageYield = 0,
                            numberOfSecurities = 0,
                            flag = state.code.toLowerCase(),
                            stateCounties = counties.filter(function(county){
                                var isRelated = county.stateId === state.id;
                                if(isRelated) {
                                    totalPar += county.totalPar;
                                    averageYield += averageYield;
                                    numberOfSecurities += numberOfSecurities;
                                    county.flag = flag;
                                    county.state = state.name;
                                }
                                return isRelated;
                            });

                        return {
                            name: state.name,
                            id: state.id,
                            code: state.code,
                            flag: flag,
                            totalPar: totalPar / stateCounties.length,
                            averageYield: averageYield / stateCounties.length,
                            numberOfSecurities: numberOfSecurities / stateCounties.length,
                            counties: stateCounties
                        };
                    });
                });
            };

            BWMapEntity.prototype.getWorldRenderingData = function(topojsonData, areasData, settings){
                var range = [0, 0],
                    rangeKey = settings.indexBy,
                    colors = {
                        totalPar: ['#0000FF', '#FFF44F'],
                        averageYield: ['#808000', '#A52A2A'],
                        numberOfSecurities: ['#DAA520', '#800080']
                    }[rangeKey],
                    colorScheme;

                topojsonData.forEach(function(topojsonItem){
                    var areaItem = areasData.getItemsByProperty('id', topojsonItem.id)[0],
                        rangeValue = areaItem.content[rangeKey];

                    range[0] = d3.min([range[0], rangeValue]);
                    range[1] = d3.max([range[1], rangeValue]);

                    topojsonItem.properties.data = {
                        entity: areaItem,
                        color: '#FFF',
                        name: areaItem.content.name,
                        flag: areaItem.content.flag,
                        value: rangeValue
                    };
                });

                colorScheme = BWColorsService.getColorScheme(colors, range);

                topojsonData.forEach(function(topojsonItem){
                    var itemData = topojsonItem.properties.data;
                    itemData.color = colorScheme(itemData.value);
                });

                return topojsonData;
            };

            BWMapEntity.prototype.getUSARenderingData = function(statesTopojsonData, countiesTopojsonData, areasData, settings){
                var statesData = areasData.getItems(),
                    countiesData = [],
                    range = [0, 0],
                    rangeKey = settings.indexBy,
                    colors = {
                        totalPar: ['#0000FF', '#FFF44F'],
                        averageYield: ['#808000', '#A52A2A'],
                        numberOfSecurities: ['#DAA520', '#800080']
                    }[rangeKey],
                    colorScheme,
                    countiesResponse,
                    statesResponse;

                statesData.forEach(function(state){
                    countiesData = countiesData.concat(state.nestedCollection.getItems());
                });

                countiesTopojsonData.forEach(function(countyTopojson){
                    var areaData = countiesData.filter(function(countyData){
                        return countyTopojson.id === countyData.content.id;
                    })[0];

                    if(areaData) {
                        var rangeValue = areaData.content[rangeKey];
                        range[0] = d3.min([range[0], rangeValue]);
                        range[1] = d3.max([range[1], rangeValue]);

                        countyTopojson.properties.data = {
                            entity: areaData,
                            color: '#FFF',
                            name: areaData.content.name + ', ' + areaData.content.state,
                            flag: areaData.content.flag,
                            value: rangeValue
                        };
                    }
                });

                colorScheme = BWColorsService.getColorScheme(colors, range);

                countiesResponse = countiesTopojsonData.filter(function(countyTopojson){
                    var itemData = countyTopojson.properties.data,
                        isValid = itemData;

                    if(isValid) {
                        itemData.color = colorScheme(itemData.value);
                    }

                    return isValid;
                });

                statesTopojsonData.forEach(function(stateTopojson){
                    var areaData = statesData.filter(function(stateData){
                            return stateTopojson.id === stateData.content.id;
                        })[0],
                        rangeValue = areaData.content[rangeKey];

                    if(areaData) {
                        stateTopojson.properties.data = {
                            entity: areaData,
                            color: colorScheme(rangeValue),
                            name: areaData.content.name,
                            flag: areaData.content.flag,
                            value: rangeValue
                        };
                    }
                });

                statesResponse = statesTopojsonData.filter(function(stateTopojson){
                    return stateTopojson.properties.data;
                });

                return {
                    states: statesResponse,
                    counties: countiesResponse
                }
            }
        }
    ]);

}(angular.module('bw.map')));

