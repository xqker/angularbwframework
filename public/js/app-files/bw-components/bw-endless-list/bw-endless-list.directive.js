(function(module){

    'use strict';

    module.directive('bwEndlessList', [
        function(){
            return {
                restrict: 'A',
                scope: {
                    model: '=bwEndlessList',
                    userSettings: '=bwEndlessListSettings'
                },
                transclude: true,
                template: '' +
                    '<div class="bw-endless-list-container">' +
                        '<div class="bw-endless-list-items" ng-transclude></div>' +
                        '<div class="bw-endless-list-loader"></div>' +
                    '</div>' +
                '',
                controller: BWEndlessListDirectiveCtrl
            };
        }
    ]);

    BWEndlessListDirectiveCtrl.$inject = ['$scope', '$element', '$timeout', 'd3', 'BWSharedService'];
    function BWEndlessListDirectiveCtrl ($scope, $element, $timeout, d3, BWSharedService){
        var self = this,
            defaultSettings = {
                triggerPoint: 300,
                isBusy: true,
                isFinalized: false,

                /** Callbacks */
                onEndOfListReached: angular.noop
            };

        self.body = d3.select(document.querySelector('body'));
        self.wrapper = d3.select($element.get(0)).classed('bw-endless-list-wrapper', true);
        self.container = self.wrapper.select('.bw-endless-list-container');
        self.list = self.container.select('.bw-endless-list-items');

        Object.defineProperties($scope, {
            settings: {
                get: function(){
                    return angular.merge(defaultSettings, ($scope.userSettings || {}));
                }
            }
        });

        Object.defineProperties($scope.model._api_, {
            toggleWaiting: {
                value: function(state){


                }
            }
        });

        var listId = $scope.model.id,
            onScrollWatcher = function() {
                if ($scope.$$destroyed || !self.wrapper.style('display')) {
                    BWSharedService.removeScrollWatcher(listId);
                } else if(!$scope.settings.isBusy && !$scope.settings.isFinalized){
                    var clientRect = self.wrapper.node().getBoundingClientRect(),
                        offsetBottom = clientRect.bottom,
                        windowHeight = window.innerHeight;

                    if(offsetBottom < windowHeight + $scope.settings.triggerPoint) {
                        $scope.settings.onEndOfListReached();
                    }
                }
            };

        BWSharedService.setScrollWatcher(listId, onScrollWatcher);

        onScrollWatcher();
        //
        $timeout(onScrollWatcher, 2000);
    }

}(angular.module('bw.utilities')));

