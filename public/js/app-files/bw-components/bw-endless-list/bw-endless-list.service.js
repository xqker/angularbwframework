(function(module){

    'use strict';

    module.service('BWEndlessListService', [
        'AsyncAPI',
        function(AsyncAPI){
            var bwEndlessListService = this,
                endlessListCounter = 0;

            Object.defineProperties(bwEndlessListService, {
                createNew: {
                    value: function(){
                        return new BWEndlessListEntity(endlessListCounter++);
                    }
                }
            });

            function BWEndlessListEntity (counter){
                var bwGridEntity = this;
                Object.defineProperties(bwGridEntity, {
                    id: {
                        value: '#bw-endless-list-' + counter
                    },
                    _api_: {
                        value: AsyncAPI.get({})
                    }
                });
            }

            BWEndlessListEntity.prototype.toggleWaiting = function(state){
                this._api_.applyFn('toggleWaiting', state);
            };
        }
    ]);

}(angular.module('bw.utilities')));

