(function(){

    /**
     * BW-framework v 0.1.4 (FrontEnd)
     *
     * requires:
     *  - d3.js,
     *  - moment.js
     *
     * modules:
     *  - bw.utilities,
     *  - bw.filters',
     *  - bw.chart',
     *  - bw.grid',
     *  - bw.tooltip'
     *  - bw.calendar
     *  - bw.timeline
     *  - bw.map
     * */

    'use strict';

    angular.module('bw', [
        'utilities',
        'filters',
        'chart',
        'grid',
        'tooltip',
        'calendar',
        'timeline',
        'map'
    ]);

    angular.module('bw.utilities', []);
    angular.module('bw.filters', []);
    angular.module('bw.chart', []);
    angular.module('bw.grid', []);
    angular.module('bw.tooltip', []);
    angular.module('bw.calendar', []);
    angular.module('bw.timeline', []);
    angular.module('bw.map', []);

}());
