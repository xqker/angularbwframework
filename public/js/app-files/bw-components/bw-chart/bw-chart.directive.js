(function(module){

    'use strict';

    module.directive('bwChart', [
        function(){
            return {
                restrict: 'A',
                scope: {
                    model: '=bwChart',
                    userSettings: '=bwChartSettings'
                },
                template: '' +
                    '<div class="bw-chart-container">' +
                        '<svg class="bw-chart-canvas">' +
                            '<g class="bw-chart-layout"></g>' +
                        '</svg>' +
                        '<div class="bw-chart-overlay"></div>' +
                        '<div class="bw-chart-msg"><span></span></div>' +
                    '</div>' +
                '',
                controller: BWChartDirectiveCtrl
            };
        }
    ]);

    BWChartDirectiveCtrl.$inject = ['$scope', '$element', '$filter', '$timeout', 'd3', 'moment', 'BWColorsService', 'BWChartEnums', 'BWSharedService'];
    function BWChartDirectiveCtrl ($scope, $element, $filter, $timeout, d3, moment, BWColorsService, BWChartEnums, BWSharedService){
        var self = this,
            defaultSettings = {
                type: 'roundChart',
                isVisible: true,
                width: 150,
                height: 300,
                animationSpeed: 0,
                autoresize: false
            };

        this.scope = $scope;
        this.d3 = d3;
        this.moment = moment;
        this.colorsService = BWColorsService;
        this.numbFilter = $filter('bigNumber');

        /** DOM-objects */
        this.wrapper = d3.select($element.get(0)).classed('bw-chart-wrapper', true);
        this.container = this.wrapper.select('.bw-chart-container');
        this.canvas = this.wrapper.select('.bw-chart-canvas');
        this.layout = this.wrapper.select('.bw-chart-layout');
        this.overlay = this.wrapper.select('.bw-chart-overlay');
        this.loader = this.wrapper.select('.bw-chart-loader');
        this.msg = this.wrapper.select('.bw-chart-msg');

        /** Misc */
        this.containerWidth = 0;
        this.containerHeight = 0;

        /** Data */
        this._data_ = null;

        /** Model & Settings */
        $scope.userSettings = $scope.userSettings || {};

        Object.defineProperties($scope, {
            selectedChartType: {
                get: function(){
                    return BWChartEnums.getChartType($scope.userSettings.type || defaultSettings.type).content;
                }
            },
            settings: {
                get: function(){
                    defaultSettings[$scope.selectedChartType.name] = $scope.selectedChartType.defaultSettings;
                    return angular.merge(defaultSettings, $scope.userSettings);
                }
            },
            isWaiting: {
                value: false,
                writable: true
            }
        });

        /** API */
        this.clearAPI();
        this.refreshSizes();

        Object.defineProperties($scope.model._api_ , {
            isReady: {
                value: true,
                configurable: true
            },
            refreshData: {
                value: function(data){
                    self._data_ = data;
                    $scope.model._api_.refreshView();
                },
                configurable: true
            },
            refreshView: {
                value: function(){
                    self.refreshSizes();
                    if(self._data_ && self._data_.getItems().length) {

                        self.msg.style({display: 'none'});
                        self[$scope.selectedChartType.drawer]();
                        self.scope.model._api_.toggleWaiting(false);
                    } else {
                        self.showMessage('No Data');
                    }
                },
                configurable: true
            },
            getChartSettings: {
                value: function(){
                    return $scope.settings;
                },
                configurable: true
            },
            toggleWaiting: {
                value: function(state){
                    $scope.isWaiting = state;
                    self.loader.style({display: $scope.isWaiting ? 'block' : 'none'});
                },
                configurable: true
            },
            zoomToPoints: {
                value: function(points){





                },
                configurable: true
            },
            resetZoom: {
                value: function(){
                    if(self.zoomMode){
                        self.zoomMode = false;
                        $scope.model._api_.refreshView();
                    }
                },
                configurable: true
            },
            getZoomState: {
                value: function(){
                    return self.zoomMode;
                },
                configurable: true
            },
            toggleItems: {
                value: function(indexingProperty, indexes, state){
                    if($scope.settings.type === 'scatterPlotChart'){
                        var affectedPoints = [];

                        if(!indexes) {
                            self._data_.getItems().forEach(function(layer){
                                affectedPoints = affectedPoints.concat(layer.nestedCollection.getItems());
                            });
                        } else {
                            indexes.forEach(function(index){
                                self._data_.getItems().forEach(function(layer){
                                    affectedPoints = affectedPoints.concat(layer.nestedCollection.getItemsByProperty(indexingProperty, index));
                                });
                            });
                        }

                        affectedPoints.forEach(function(point){
                            if(state && !point.isSelected) {
                                point.select();
                            } else if(!state && point.isSelected) {
                                point.deselect();
                            }
                        });

                        $scope.model._api_.refreshView();
                    }
                },
                configurable: true
            },
            getRenderingData: {
                value: function(settings){
                    return (self._data_.getRenderingData && self._data_.getRenderingData(settings || $scope.settings[$scope.settings.type])) || [];
                },
                configurable: true
            }
        });

        /** On resize */
        var resizeWatcher = BWSharedService.getResizeWatcher(self.wrapper.node(), {
            onResize: $scope.model._api_.refreshView,
            stopWhen: function(){
                return $scope.$$destroyed;
            }
        });

        resizeWatcher.runSizeTracker();
    }

    BWChartDirectiveCtrl.prototype.clearCanvas = function(current){
        var self = this;
        [
            'bw-scatter-plot-chart',
            'bw-round-chart',
            'bw-bar-chart',
            'bw-timeline-chart',
            'bw-heatmap-calendar',
            'bw-boxplot-chart',
            'bw-sunburst-chart',
            'bw-histogram',
            'bw-treemap',
            'bw-bubble-chart',
            'bw-sankey-chart',
            'bw-activities-chart'
        ].forEach(
            function(className){
                if(current !== className) {
                    self.layout.select('.' + className).remove();
                }
            });
    };

    BWChartDirectiveCtrl.prototype.refreshSizes = function(){
        var self = this,
            parentItem = self.wrapper.node(),
            settings = self.scope.settings;

        self.containerWidth = settings.autoresize ? (parentItem.clientWidth || settings.width) : settings.width;
        self.containerHeight = settings.autoresize ? (parentItem.clientHeight || settings.height) : settings.height;

        self.wrapper.style({
            width: settings.autoresize ? '100%' : self.containerWidth + 'px',
            height: settings.autoresize ? '100%' : self.containerHeight + 'px'
        });

        [self.container, self.canvas].forEach(function(elem){
            elem.style({
                width: self.containerWidth + 'px',
                height: self.containerHeight + 'px'
            });
        });
    };

    BWChartDirectiveCtrl.prototype.clearAPI = function(){
        var self = this,
            api = self.scope.model._api_,
            keys = Object.keys(api);

        keys.forEach(function(key){
            if(key !== 'applyFn' && api.hasOwnProperty(key)) {
                delete api[key];
            }
        });
    };

    BWChartDirectiveCtrl.prototype.drawRoundChart = function(){
        /** Remove exceeded charts */
        this.clearCanvas('bw-round-chart');

        var self = this,
            chartSettings = self.scope.settings.roundChart,
            containerWidth = self.containerWidth,
            containerHeight = self.containerHeight,
            layoutSize = this.d3.min([containerWidth, containerHeight]),
            padding = chartSettings.padding,
            offsetX = (containerWidth - layoutSize) / 2,
            offsetY = (containerHeight - layoutSize) / 2,
            outerRadius = (layoutSize - (padding * 2)) / 2,
            innerRadius = outerRadius * chartSettings.innerRadius * 0.1,
            colors = self.colorsService.getRoundChartColors(),
            arc = this.d3.svg.arc().innerRadius(innerRadius).outerRadius(outerRadius),
            chartData = self._data_.getRenderingData(),

            /** DOM=elements*/
            wrapper, mainLayer, interactiveLayer;

        refreshChartStructure();

        refreshArcs();

        function refreshChartStructure (){

            if(!self.layout.select('.bw-round-chart').node()) {
                self.layout.append('g').classed('bw-round-chart', true);
                self.layout.select('.bw-round-chart').append('g').classed('main-layer', true);
                self.layout.select('.bw-round-chart').append('g').classed('interactive-layer', true);
            }

            wrapper = self.layout.select('.bw-round-chart');
            mainLayer = wrapper.select('.main-layer');
            interactiveLayer = wrapper.select('.interactive-layer');

            self.layout.attr({ transform: 'translate(' + [offsetX, offsetY] + ')' });
            wrapper.attr({ transform: 'translate(' + [(layoutSize * 0.5),(layoutSize * 0.5)] + ')' });
        }

        function refreshArcs () {
            var colorSlices = mainLayer.selectAll('.color-slice').data(chartData),
                interactiveSlices = mainLayer.selectAll('.interactive-slice').data(chartData);

            colorSlices.enter().append('path').classed('color-slice', true);

            colorSlices.exit().remove();

            interactiveSlices.enter().append('path').classed('interactive-slice', true);

            interactiveSlices.exit().remove();

            colorSlices.attr({
                    'stroke-width': layoutSize * 0.005,
                    stroke: colors.light,
                    fill: function (d) {
                        return d.color;
                    },
                    d: arc
                });

            interactiveSlices
                .attr({
                    'stroke-width': layoutSize * 0.005,
                    stroke: function (d) {
                        return d.data.isSelected ? self.colorsService.getDarkerColor(d.color, 1) : colors.transparent;
                    },
                    fill: colors.dark,
                    'fill-opacity': 0,
                    d: arc
                })
                .style({cursor: 'pointer'})
                .on('mouseover', onMouseOver)
                .on('mousemove', onMouseMove)
                .on('mouseout', onMouseOut)
                .on('click', onClick);
        }

        function onMouseOver (d, i){
            var event = self.d3.event;

            self.d3.select(this).attr({ 'fill-opacity': 0.2 });

            toggleCallback('onMouseOver' , d, {
                x: event.clientX,
                y: event.clientY
            }, event);
        }

        function onMouseMove (d, i){
            var event = self.d3.event;

            toggleCallback('onMouseMove' , d, {
                x: event.clientX,
                y: event.clientY
            }, event);
        }

        function onMouseOut (d, i){
            var event = self.d3.event;

            self.d3.select(this).attr({ 'fill-opacity': 0 });

            toggleCallback('onMouseOut' , d, {
                x: event.clientX,
                y: event.clientY
            }, event);
        }

        function onClick (d, i){
            var event = self.d3.event;

            d.data.select();
            refreshArcs();

            toggleCallback('onClick' , d, {
                x: event.clientX,
                y: event.clientY
            }, event);
        }

        function toggleCallback (callback, slice, coords, event){
            self.scope.settings.roundChart[callback](slice.data, coords, event);
        }
    };

    BWChartDirectiveCtrl.prototype.drawScatterPlotChart = function(){
        /** Remove exceeded charts */
        this.clearCanvas('bw-scatter-plot-chart');

        var self = this,
            containerWidth = self.containerWidth,
            containerHeight = self.containerHeight,
            chartSettings = self.scope.settings.scatterPlotChart,
            padding = chartSettings.padding,
            innerWidth = containerWidth - (padding.left + padding.right),
            innerHeight = containerHeight - (padding.bottom + padding.top),
            ranges = self._data_.getRanges(),
            renderingData = chartSettings.clusters ? self._data_.getClustersData({
                x: innerWidth,
                y: innerHeight
            }, chartSettings) : self._data_.getPointsData(),
            scaleX = self.d3.scale.linear()
                .domain(ranges.x)
                .range([0, innerWidth])
                .nice(),
            scaleY = self.d3.scale.linear()
                .domain(ranges.y)
                .range([innerHeight, 0])
                .nice(),
            selectionScaleX = self.d3.scale.linear()
                .domain([0, innerWidth])
                .range(ranges.x),
            selectionScaleY = self.d3.scale.linear()
                .domain([innerHeight, 0])
                .range(ranges.y),
            zoomScaleX = self.d3.scale.linear()
                .range([0, innerWidth])
                .nice(),
            zoomScaleY = self.d3.scale.linear()
                .range([innerHeight, 0])
                .nice(),

            zoomMode = false,

            /** DOM-Elements */
            wrapper, bgLayer, mainLayer, axisXLayer, axisYLayer, zoomLayer, interactiveLayer;

        refreshChartStructure();

        refreshMainLayer();

        refreshAxisX(scaleX);

        refreshAxisY(scaleY);

        refreshBackground();

        refreshInteractiveLayer();

        function refreshChartStructure (){
            if(!self.layout.select('.bw-scatter-plot-chart').node()){
                self.layout.append('g').classed('bw-scatter-plot-chart', true);
                self.layout.select('.bw-scatter-plot-chart').append('g').classed('bw-lc-background-layer', true);
                self.layout.select('.bw-scatter-plot-chart').append('g').classed('bw-lc-axis-x-layer', true);
                self.layout.select('.bw-scatter-plot-chart').append('g').classed('bw-lc-axis-y-layer', true);
                self.layout.select('.bw-scatter-plot-chart').append('g').classed('bw-lc-main-layer', true);
                self.layout.select('.bw-scatter-plot-chart').append('g').classed('bw-lc-zoom-layer', true);
                self.layout.select('.bw-scatter-plot-chart').append('g').classed('bw-lc-interactive-layer', true);
            }

            wrapper = self.layout.select('.bw-scatter-plot-chart');
            bgLayer = wrapper.select('.bw-lc-background-layer');
            mainLayer = wrapper.select('.bw-lc-main-layer');
            axisXLayer = wrapper.select('.bw-lc-axis-x-layer');
            axisYLayer = wrapper.select('.bw-lc-axis-y-layer');
            zoomLayer = wrapper.select('.bw-lc-zoom-layer');
            interactiveLayer = wrapper.select('.bw-lc-interactive-layer');

            self.layout.attr({ transform: 'translate(' + [0, 0] + ')' });
            bgLayer.attr({ transform: 'translate(' + [padding.left, padding.top] + ')' });
            mainLayer.attr({ transform: 'translate(' + [padding.left, padding.top] + ') scale(1)' }).style({ 'display': 'block' });
            axisXLayer.attr({ transform: 'translate(' + [padding.left, padding.top + innerHeight] + ')' });
            axisYLayer.attr({ transform: 'translate(' + [padding.left, padding.top] + ')' });
            zoomLayer.attr({ transform: 'translate(' + [padding.left, padding.top] + ')' });
            interactiveLayer.attr({ transform: 'translate(' + [padding.left, padding.top] + ')' });
            zoomLayer.selectAll('*').remove();
        }

        function refreshMainLayer (){
            self.zoomMode = false;

            if(chartSettings.clusters) {
                var clusters = mainLayer.selectAll('.bw-lc-cluster').data(renderingData.clusters);

                clusters.enter().append('circle').classed('bw-lc-cluster', true);

                clusters.exit().remove();

                clusters
                    .attr({
                        cx: function(c){
                            return scaleX(c.x);
                        },
                        cy: function(c){
                            return scaleY(c.y);
                        },
                        r: function(c){
                            return renderingData.scaleR(c.points.length);
                        },
                        fill: function(c){
                            return c.color;
                        },
                        'fill-opacity': function(c){
                            return c.core.hasClass('skipMe') ? 0 : (c.core.hasClass('transparent') ? 0.5 : 1);
                        }
                    });
            } else {
                var points = mainLayer.selectAll('.bw-lc-point').data(renderingData.points);

                points.enter().append('circle').classed('bw-lc-point', true);

                points.exit().remove();

                points
                    .attr({
                        r: chartSettings.pointSize / 2,
                        fill: function(c){
                            return c.pointEntity.isSelected ? '#FFF' : c.color;
                        },
                        'fill-opacity': function(c){
                            return c.pointEntity.hasClass('transparent') ? 0.5 : 1;
                        },
                        transform: function(c){
                            return 'scale(' + (c.pointEntity.hasClass('skipMe') ? 0 : 1) + ')';
                        },
                        stroke: function(c){
                            return c.color;
                        },
                        'stroke-width': function(c){
                            return c.pointEntity.isSelected ? 2 : 0;
                        }
                    })
                    .transition()
                    .attr({
                        cx: function(c){
                            return scaleX(c.x);
                        },
                        cy: function(c){
                            return scaleY(c.y);
                        }
                    })
                    .duration(self.scope.settings.animationSpeed)

                points
                    .on('mouseover', function(c){
                        var event = self.d3.event;

                        chartSettings.onPointMouseOver(c.pointEntity, {
                            x: scaleX(c.x),
                            y: scaleY(c.y)
                        }, event);
                    })
                    .on('mouseout', function(c){
                        var event = self.d3.event;

                        chartSettings.onPointMouseOut(c.pointEntity, {
                            x: scaleX(c.x),
                            y: scaleY(c.y)
                        }, event);
                    })
                    .on('click', function(c){
                        var event = self.d3.event;

                        chartSettings.onPointClick(c.pointEntity, {
                            x: scaleX(c.x),
                            y: scaleY(c.y)
                        }, event, togglePoint(c, this));
                    });
            }
        }

        function togglePoint (d, point){
            return function() {
                if(!d.pointEntity.isSelected) {
                    d.pointEntity.select();

                    self.d3.select(point).attr({
                        fill: '#FFF',
                        stroke: d.color,
                        'stroke-width': 2
                    });
                } else {
                    d.pointEntity.deselect();

                    self.d3.select(point).attr({
                        fill: d.color,
                        'stroke-width': 0
                    });
                }
            }
        }

        function refreshBackground (){
            if(!bgLayer.select('text').node()) {
                bgLayer.append('text').classed('axis-x-label', true);
                bgLayer.append('text').classed('axis-y-label', true);
            }

            bgLayer.select('.axis-x-label')
                .attr({
                    transform: 'translate(' + [ innerWidth / 2, padding.top + innerHeight + 20] + ')',
                    'font-size': 10,
                    //'font-weight': 'bold',
                    'text-anchor': 'middle',
                    fill: '#000'
                })
                .text(chartSettings.axisXName);

            bgLayer.select('.axis-y-label')
                .attr({
                    transform: 'translate(' + [ -25, innerHeight / 2 ] + ') rotate(-90)',
                    'font-size': 10,
                    //'font-weight': 'bold',
                    'text-anchor': 'middle',
                    fill: '#000'
                })
                .text(chartSettings.axisYName);

            var bgLines = bgLayer.selectAll('.bg-line').data(axisYLayer.selectAll('g').data());

            bgLines.enter().append('line').classed('bg-line', true);

            bgLines.exit().remove();

            bgLines.attr({
                x1: 0,
                x2: innerWidth,
                y1: function(d){
                    return scaleY(d);
                },
                y2: function(d){
                    return scaleY(d);
                },
                stroke: function(d, i){
                    return i ? '#c5c5c7' : '#FFF';
                },
                'stroke-dasharray': '1,1'
            });

        }

        function refreshAxisX (scale){
            var numberOfTicks = Math.floor(containerWidth / 30),
                axisX = self.d3.svg.axis()
                .scale(scale)
                .orient('bottom')
                .ticks(numberOfTicks < 15 ? numberOfTicks : 15);

            axisXLayer.call(axisX);

            axisXLayer.select('path').attr({ stroke: '#c5c5c7', 'stroke-width': 0.5, fill: 'none'});

            axisXLayer.selectAll('g')
                .select('text')
                .attr({
                    fill: '#c5c5c7',
                    'font-size': 10
                });

            axisXLayer.selectAll('g')
                .select('line')
                .attr({
                    x1: 0,
                    x2: 0,
                    y1: 6,
                    y2: 0,
                    stroke: '#e3e3e3',
                    'stroke-width': 1
                });
        }

        function refreshAxisY (scale){
            var numberOfTicks = Math.floor(containerHeight / 30),
                axisY = self.d3.svg.axis()
                .scale(scale)
                .orient('left')
                .ticks(numberOfTicks < 10 ? numberOfTicks : 10);

            axisYLayer.call(axisY);

            axisYLayer.select('path').attr({ stroke: '#c5c5c7', 'stroke-width': 0.5, fill: 'none'});

            axisYLayer.selectAll('g')
                .select('text')
                .attr({
                    fill: '#c5c5c7',
                    'font-size': 10
                });

            axisYLayer.selectAll('g')
                .select('line')
                .attr({
                    x1: -6,
                    x2: innerWidth,
                    y1: 0,
                    y2: 0,
                    stroke: '#e3e3e3',
                    'stroke-width': 0.5
                });
        }

        function refreshInteractiveLayer (){
            if(!interactiveLayer.select('rect').node()) {
                interactiveLayer.append('rect').classed('selection-rect', true);
                interactiveLayer.append('rect').classed('interactive-rect', true);
            }
            var interactiveRect = interactiveLayer.select('.interactive-rect')
                    .attr({
                        x: 0,
                        y: 0,
                        width: chartSettings.clusters ? innerWidth : 0,
                        height: chartSettings.clusters ? innerHeight : 0,
                        fill: 'rgba(255, 255, 255, 0)'
                    })
                    .style({ cursor: 'crosshair' }),
                selectionRect = interactiveLayer.select('.selection-rect')
                    .attr({
                        x: 0,
                        y: 0,
                        width: 0,
                        height: 0,
                        fill: '#00aeef',
                        'fill-opacity': 0.3,
                        stroke: '#00aeef',
                        'stroke-width': 2
                    }),
                    touchStartTimestamp = 0,
                    touchStartPoint = {x: 0, y: 0},
                    selectedFrame = {
                        x: [0, 0],
                        y: [0, 0]
                    };

            interactiveRect.on('mousedown', onMouseDown);

            function onMouseDown (){
                touchStartPoint = self.d3.mouse(this);
                touchStartTimestamp = new Date().getTime();

                self.overlay
                    .style({display: 'block', cursor: 'crosshair'})
                    .on('mousemove', onMouseMove)
                    .on('mouseup', onMouseUp);
            }

            function onMouseMove (){
                var touchMovePoint = self.d3.mouse(interactiveRect.node()),
                    x1 = self.d3.min([touchMovePoint[0], touchStartPoint[0]]),
                    x2 = self.d3.max([touchMovePoint[0], touchStartPoint[0]]),
                    y1 = self.d3.min([touchMovePoint[1], touchStartPoint[1]]),
                    y2 = self.d3.max([touchMovePoint[1], touchStartPoint[1]]);

                if(x1 < 0) { x1 = 0; }
                if(x2 > innerWidth) { x2 = innerWidth; }
                if(y1 < 0) { y1 = 0; }
                if(y2 > innerHeight) { y2 = innerHeight; }

                if(!self.zoomMode) {
                    selectedFrame.x[0] = selectionScaleX(x1);
                    selectedFrame.x[1] = selectionScaleX(x2);
                    selectedFrame.y[0] = selectionScaleY(y2);
                    selectedFrame.y[1] = selectionScaleY(y1);
                } else {
                    selectedFrame.x[0] = selectionScaleX(x1);
                    selectedFrame.x[1] = selectionScaleX(x2);
                    selectedFrame.y[0] = selectionScaleY(y2);
                    selectedFrame.y[1] = selectionScaleY(y1);
                }

                selectionRect.attr({
                    x: x1,
                    y: y1,
                    width: x2 - x1,
                    height: y2 - y1
                });
            }

            function onMouseUp (){
                var touchEndTimestamp = new Date().getTime(),
                    timestampDiff = touchEndTimestamp - touchStartTimestamp,
                    coords = self.d3.mouse(interactiveRect.node()),
                    affectedClusters;

                self.overlay
                    .style({display: 'none', cursor: 'crosshair'})
                    .on('mousemove', null)
                    .on('mouseup', null);

                selectionRect.attr({
                    width: 0,
                    heigth: 0
                });

                if(timestampDiff < 120) {
                    var corrX = ((chartSettings.pointSize) / innerWidth) * ranges.x[1],
                        corrY = ((chartSettings.pointSize) / innerHeight) * ranges.y[1],
                        clickX = selectionScaleX(coords[0]),
                        clickY = selectionScaleY(coords[1]);

                    affectedClusters = renderingData.clusters.filter(function(c){
                        return (c.x >= clickX - corrX && c.x <= clickX + corrX) &&
                            (c.y >= clickY - corrY && c.y <= clickY + corrY) &&
                            (!c.core.hasClass('skipMe'));
                    });

                    if(!self.zoomMode) {
                        chartSettings.onClick( affectedClusters, {
                            x: clickX,
                            y: clickY
                        }, zoomToPoints);
                    }
                } else {
                    affectedClusters = renderingData.clusters.filter(function(c){
                        return (c.x >= selectedFrame.x[0] && c.x <= selectedFrame.x[1]) &&
                            (c.y >= selectedFrame.y[0] && c.y <= selectedFrame.y[1]) &&
                            (!c.core.hasClass('skipMe'));
                    });

                    if(!self.zoomMode) {
                        chartSettings.onRangeSelect( affectedClusters, selectedFrame, zoomToPoints);
                    }
                }
            }

            function zoomToPoints (clusters){
                self.zoomMode = true;

                if(!zoomLayer.select('g').node()) {
                    zoomLayer.append('g').classed('zoom-clusters', true);
                    zoomLayer.append('g').classed('zoom-points', true);
                }

                var zoomRanges = {
                    x: [null, null],
                    y: [null, null]
                };

                clusters.forEach(function(cluster){
                    cluster.points.forEach(function(p){
                        if(zoomRanges.x[0] === null) {
                            zoomRanges.x[0] = p.content.x;
                            zoomRanges.x[1] = p.content.x;
                            zoomRanges.y[0] = p.content.y;
                            zoomRanges.y[1] = p.content.y;
                        } else {
                            zoomRanges.x[0] = self.d3.min([zoomRanges.x[0], p.content.x]);
                            zoomRanges.x[1] = self.d3.max([zoomRanges.x[1], p.content.x]);
                            zoomRanges.y[0] = self.d3.min([zoomRanges.y[0], p.content.y]);
                            zoomRanges.y[1] = self.d3.max([zoomRanges.y[1], p.content.y]);
                        }
                    });
                });

                var zoomScaleR = self.d3.scale.linear()
                        .domain(renderingData.scaleR.domain())
                        .range([5, 20]),
                    zoomedClusters = zoomLayer.select('.zoom-clusters').selectAll('.zoomed-cluster').data(clusters);

                zoomScaleX.domain(zoomRanges.x);

                zoomScaleY.domain(zoomRanges.y);

                zoomedClusters.enter().append('g').classed('zoomed-cluster', true);

                zoomedClusters.exit().remove();

                zoomedClusters.attr({
                    transform: function(c){
                        var cluster = self.d3.select(this),
                            core = cluster.append('circle').classed('zoomed-core', true),
                            points = cluster.selectAll('.zoomed-point').data(c.points);

                        core.attr({
                            cx: scaleX(c.x),
                            cy: scaleY(c.y),
                            r: renderingData.scaleR(c.points.length) - 1,
                            fill: c.color,
                            'fill-opacity': 0.5,
                            stroke: c.color,
                            'stroke-width': 2
                        });

                        core
                            .transition()
                            .attr({
                                r: zoomScaleR(c.points.length),
                                'fill-opacity': 0,
                                'stroke-width': 0
                            })
                            .duration(self.scope.settings.animationSpeed);

                        points.enter().append('circle');

                        points.exit().remove();

                        points.attr({
                            cx: scaleX(c.x),
                            cy: scaleY(c.y),
                            r: 0,
                            fill: c.color
                        });

                        points
                            .transition()
                            .attr({
                                cx: function(p){
                                    return zoomScaleX(p.content.x);
                                },
                                cy: function(p){
                                    return zoomScaleY(p.content.y);
                                },
                                r: chartSettings.pointSize / 2
                            })
                            .duration(self.scope.settings.animationSpeed);
                    }
                });


                mainLayer.attr({ transform: 'scale(0)' });

                refreshAxisX(zoomScaleX);

                refreshAxisY(zoomScaleY);
            }
        }
    };

    BWChartDirectiveCtrl.prototype.drawLinearChart_OldVersion = function(){
        /** Remove exceeded charts */
        this.layout.select('.bw-round-chart').remove();
        this.layout.select('.bw-bar-chart').remove();
        this.layout.select('.bw-timeline-chart').remove();
        this.layout.select('.bw-heatmap-calendar').remove();
        this.layout.select('.bw-boxplot-chart').remove();
        this.layout.select('.bw-sunburst-chart').remove();

        var self = this,
            showCummulative = self.scope.settings.linearChart.cummulative,
            areaTypeIsSelected = self.scope.settings.type === 'area',
            sliderColor = self.scope.settings.linearChart.sliderColor,
            interpolation = self.scope.settings.linearChart.interpolation,
            padding = {
                top: 30,
                right: 10,
                bottom: 5,
                left: 10
            },
            containerWidth = self.containerWidth,
            containerHeight = self.containerHeight,
            innerWidth = containerWidth - (padding.right + padding.left),
            innerHeight = containerHeight - (padding.top + padding.bottom),
            axisYWidth = 50,
            axisXHeight = 30,
            rangeSelectorContainerWidth = innerWidth,
            rangeSelectorContainerHeight = innerHeight * 0.1,
            layoutWidth = innerWidth - axisYWidth,
            layoutHeight = innerHeight - (axisXHeight + rangeSelectorContainerHeight),
            chartRanges = self._data_.getRanges(),
            chartData = self._data_.getRenderingData(),

            /** DOM-elements */
            wrapper, layout, axisX, axisY, rangeSelector,

            /** Range selector settings */
            leftHandlerPosition = 0,
            rightHandlerPosition = 0,
            selectorWidth = 0;

        refreshChartStructure();

        refreshRangeSelector();

        refreshBaseLayer();

        function refreshChartStructure (){
            if(!self.layout.select('.bw-linear-chart').node()){
                self.layout.append('g').classed('bw-linear-chart', true);

                [
                    'bw-linear-chart-background',
                    'bw-linear-chart-axis-x',
                    'bw-linear-chart-axis-y',
                    'bw-linear-chart-range-selector',
                    'bw-linear-chart-layout'
                ].forEach(function(className){
                        self.layout.select('.bw-linear-chart')
                            .append('g').classed(className, true);
                    });

                [
                    'area-layer',
                    'path-layer',
                    'points-layer',
                    'helpers-layer',
                    'points-layer',
                    'interactive-layer'
                ].forEach(function(className){
                        self.layout.select('.bw-linear-chart')
                            .select('.bw-linear-chart-layout').append('g').classed(className, true);
                    });

                [
                    'helper-h',
                    'helper-v'
                ].forEach(function(helperName){
                        self.layout.select('.bw-linear-chart')
                            .select('.bw-linear-chart-layout')
                            .select('.helpers-layer').append('line').classed(helperName, true);
                    });

                [
                    'interactive-overlay'
                ].forEach(function(className){
                        self.layout.select('.bw-linear-chart')
                            .select('.interactive-layer').append('rect').classed(className, true);
                    });

                [
                    {
                        className: 'range-selector-background',
                        tagName: 'g'
                    },
                    {
                        className: 'left-panel',
                        tagName: 'rect'
                    },
                    {
                        className: 'right-panel',
                        tagName: 'rect'
                    },
                    {
                        className: 'range-selector-handler',
                        tagName: 'g'
                    }
                ].forEach(function(rangeSelectorComponent){
                        self.layout.select('.bw-linear-chart')
                            .select('.bw-linear-chart-range-selector')
                            .append(rangeSelectorComponent.tagName)
                            .classed(rangeSelectorComponent.className, true);
                    });

                [
                    'main-handler',
                    'left-handler',
                    'right-handler'
                ].forEach(function(handler){
                        self.layout.select('.bw-linear-chart')
                            .select('.bw-linear-chart-range-selector')
                            .select('.range-selector-handler')
                            .append('rect').classed(handler, true);
                    });
            }

            wrapper = self.layout.select('.bw-linear-chart');
            layout = wrapper.select('.bw-linear-chart-layout');
            axisX = wrapper.select('.bw-linear-chart-axis-x');
            axisY = wrapper.select('.bw-linear-chart-axis-y');
            rangeSelector = wrapper.select('.bw-linear-chart-range-selector');

            self.layout.attr({ transform: 'translate(0, 0)' });
            wrapper.attr({ transform: 'translate(' + padding.left + ', ' + padding.top + ')' });
            layout.attr({ transform: 'translate(' + axisYWidth + ', 0)' });
            axisX.attr({ transform: 'translate(' + axisYWidth + ', ' + layoutHeight + ')' });
            axisY.attr({ transform: 'translate(' + axisYWidth + ', 0)' });
            rangeSelector.attr({ transform: 'translate(0, ' + (layoutHeight + axisXHeight) + ')' });
        }

        function refreshRangeSelector (){
            var background = rangeSelector.select('.range-selector-background'),
                leftPanel = rangeSelector.select('.left-panel'),
                rightPanel = rangeSelector.select('.right-panel'),
                handlers = rangeSelector.select('.range-selector-handler'),
                leftHandler = handlers.select('.left-handler'),
                rightHandler = handlers.select('.right-handler'),
                mainHandler = handlers.select('.main-handler'),
                chartSettings = self.scope.userSettings.linearChart,
                scales = {
                    x: self.d3.scale.linear()
                        .domain(chartRanges.x)
                        .range([0, rangeSelectorContainerWidth]),
                    y: self.d3.scale[self.scope.settings.linearChart.scale]()
                        .domain(chartRanges.y)
                        .range([rangeSelectorContainerHeight, 0])
                        .nice()
                },
                backgroundLine = self.d3.svg.line()
                    .x(function(d) { return scales.x(d.x); })
                    .y(function(d) { return scales.y(showCummulative ? d.yCumm : d.yDiff); })
                    .interpolate(interpolation);

            if(!chartSettings.range) {
                chartSettings.range = [0.9, 1];
            }

            leftHandlerPosition = rangeSelectorContainerWidth * chartSettings.range[0];
            rightHandlerPosition = rangeSelectorContainerWidth * chartSettings.range[1];
            selectorWidth = rightHandlerPosition - leftHandlerPosition;

            leftPanel.attr({
                x: 0,
                y: 0,
                width: Math.abs(leftHandlerPosition),
                height: Math.abs(rangeSelectorContainerHeight),
                fill: self.textColorDark,
                'fill-opacity': 0.3,
                stroke: self.textColorDark,
                'stroke-width': 1
            });

            rightPanel.attr({
                x: rightHandlerPosition,
                y: 0,
                width: Math.abs(rangeSelectorContainerWidth - rightHandlerPosition),
                height: Math.abs(rangeSelectorContainerHeight),
                fill: self.textColorDark,
                'fill-opacity': 0.3,
                stroke: self.textColorDark,
                'stroke-width': 1
            });

            handlers.attr({ transform: 'translate(' + leftHandlerPosition + ', 0)' });

            leftHandler.attr({
                x: 0,
                y: 0,
                width: 6,
                height: Math.abs(rangeSelectorContainerHeight),
                fill: sliderColor.default,
                stroke: self.textColorDark,
                'stroke-width': 1,
                transform: 'translate( -5, 0)'
            }).style({
                cursor: 'col-resize'
            });

            rightHandler.attr({
                x: selectorWidth,
                y: 0,
                width: 6,
                height: Math.abs(rangeSelectorContainerHeight),
                fill: sliderColor.default,
                stroke: self.textColorDark,
                'stroke-width': 1,
                transform: 'translate( -5, 0)'
            }).style({
                cursor: 'col-resize'
            });

            mainHandler.attr({
                x: 0,
                y: 0,
                width: Math.abs(selectorWidth),
                height: Math.abs(rangeSelectorContainerHeight),
                fill: self.transparentColor
            }).style({
                cursor: 'ew-resize'
            });

            var layers = background.selectAll('path').data(chartData);

            layers.enter().append('path');

            layers.exit().remove();

            layers.attr({
                fill: self.transparentColor,
                stroke: function(d){
                    return d.color;
                },
                'stroke-width': 0.5,
                d: function(d) {
                    return backgroundLine(d.items);
                }
            });

            /** Range selector interactions */
            var touchStartPosition = 0,
                touchEndPosition = 0,
                limits = [0, 1];

            var onMainHandlerGrab = function(){
                var event = self.d3.event;

                touchStartPosition = event.clientX;
                touchEndPosition = leftHandlerPosition;
                limits = [0, rangeSelectorContainerWidth - selectorWidth];

                self.overlay
                    .style({display: 'block', cursor: 'ew-resize'})
                    .on('mousemove', onMainHandlerDrag)
                    .on('mouseup', onHandlerRelease);
            };

            var onLeftHandlerGrab = function(){
                var event = self.d3.event;

                touchStartPosition = event.clientX;
                touchEndPosition = leftHandlerPosition;
                limits = [0, rightHandlerPosition - (rangeSelectorContainerWidth * 0.1)];

                self.overlay
                    .style({display: 'block', cursor: 'col-resize'})
                    .on('mousemove', onLeftHandlerDrag)
                    .on('mouseup', onHandlerRelease);

                leftHandler.attr({
                    fill: sliderColor.active
                });
            };

            var onRightHandlerGrab = function(){
                var event = self.d3.event;

                touchStartPosition = event.clientX;
                touchEndPosition = leftHandlerPosition;
                limits = [(rangeSelectorContainerWidth * 0.1), rangeSelectorContainerWidth - leftHandlerPosition];

                self.overlay
                    .style({display: 'block', cursor: 'col-resize'})
                    .on('mousemove', onRightHandlerDrag)
                    .on('mouseup', onHandlerRelease);

                rightHandler.attr({
                    fill: sliderColor.active
                });
            };

            var onMainHandlerDrag = function(){
                var event = self.d3.event,
                    movePosition = event.clientX,
                    offset = movePosition - touchStartPosition,
                    newPosition = leftHandlerPosition + offset;

                if(newPosition < limits[0]) {
                    newPosition = limits[0];
                }
                if(newPosition > limits[1]) {
                    newPosition = limits[1];
                }

                handlers.attr({ transform: 'translate(' + newPosition + ', 0)'});
                leftPanel.attr({ width: newPosition });
                rightPanel.attr({ x: newPosition + selectorWidth, width: rangeSelectorContainerWidth - (newPosition + selectorWidth) });

                touchEndPosition = newPosition;

                chartSettings.range[0] = (touchEndPosition / rangeSelectorContainerWidth);
                chartSettings.range[1] = ((touchEndPosition + selectorWidth) / rangeSelectorContainerWidth);
                refreshBaseLayer();
            };

            var onLeftHandlerDrag = function(){
                var event = self.d3.event,
                    movePosition = event.clientX,
                    offset = movePosition - touchStartPosition,
                    newPosition = leftHandlerPosition + offset;

                if(newPosition < limits[0]) {
                    newPosition = limits[0];
                }
                if(newPosition > limits[1]) {
                    newPosition = limits[1];
                }

                handlers.attr({ transform: 'translate(' + newPosition + ', 0)'});
                mainHandler.attr({ width: rightHandlerPosition - newPosition});
                leftPanel.attr({ width: newPosition });
                rightHandler.attr({x: rightHandlerPosition - newPosition});

                touchEndPosition = newPosition;

                chartSettings.range[0] = (touchEndPosition / rangeSelectorContainerWidth);
                chartSettings.range[1] = (rightHandlerPosition / rangeSelectorContainerWidth);
                refreshBaseLayer();

                leftHandler.attr({
                    fill: sliderColor.active
                });
            };

            var onRightHandlerDrag = function(){
                var event = self.d3.event,
                    movePosition = event.clientX,
                    offset = movePosition - touchStartPosition,
                    newWidth = selectorWidth + offset;

                if(newWidth < limits[0]) {
                    newWidth = limits[0];
                }
                if(newWidth > limits[1]) {
                    newWidth = limits[1];
                }

                mainHandler.attr({ width: newWidth});
                rightPanel.attr({ x: newWidth + leftHandlerPosition, width: rangeSelectorContainerWidth - (newWidth + leftHandlerPosition) });
                rightHandler.attr({ x: newWidth});

                touchEndPosition = leftHandlerPosition;

                chartSettings.range[0] = (leftHandlerPosition / rangeSelectorContainerWidth);
                chartSettings.range[1] = ((leftHandlerPosition + newWidth) / rangeSelectorContainerWidth);
                refreshBaseLayer();

                rightHandler.attr({
                    fill: sliderColor.active
                });
            };

            var onHandlerRelease = function(){
                leftHandlerPosition = touchEndPosition;
                selectorWidth = parseInt(mainHandler.attr('width'));
                rightHandlerPosition = leftHandlerPosition + selectorWidth;

                chartSettings.range[0] = (leftHandlerPosition / rangeSelectorContainerWidth);
                chartSettings.range[1] = (rightHandlerPosition / rangeSelectorContainerWidth);

                self.overlay
                    .style({display: 'none'})
                    .on('mousemove', null)
                    .on('mouseup', null);

                [leftHandler, rightHandler].forEach(function(handler){
                    handler.attr({
                        fill: sliderColor.default
                    });
                });
            };

            mainHandler.on('mousedown', onMainHandlerGrab);
            leftHandler.on('mousedown', onLeftHandlerGrab);
            rightHandler.on('mousedown', onRightHandlerGrab);

            [leftHandler, rightHandler].forEach(function(handler){
                handler
                    .on('mouseover', function(){
                        handler.attr({
                            fill: sliderColor.hover
                        });
                    })
                    .on('mouseout', function(){
                        handler.attr({
                            fill: sliderColor.default
                        });
                    });
            });
        }

        function refreshBaseLayer (){
            var areaLayer = layout.select('.area-layer'),
                pathLayer = layout.select('.path-layer'),
                interactiveLayer = layout.select('.interactive-layer').select('.interactive-overlay'),
                helpersLayer = layout.select('.helpers-layer'),
                helperH = helpersLayer.select('.helper-h'),
                helperV = helpersLayer.select('.helper-v'),
                pointsLayer = layout.select('.points-layer'),
                currentRange = self.scope.userSettings.linearChart.range,
                rangingScales = {
                    straight: self.d3.scale.linear()
                        .domain(chartRanges.x)
                        .range([0, 1]),
                    reversed: self.d3.scale.linear()
                        .domain([0, 1])
                        .range(chartRanges.x)
                },
                baseLayerData = chartData.map(function(layer){
                    return layer.items.filter(function(item){
                        var x = rangingScales.straight(item.x);
                        return x >= currentRange[0] && x <= currentRange[1];
                    });
                }),
                renderingScales = {
                    x: self.d3.scale.linear()
                        .domain([rangingScales.reversed(currentRange[0]), rangingScales.reversed(currentRange[1])])
                        .range([0, layoutWidth]),

                    y: self.d3.scale[self.scope.settings.linearChart.scale]()
                        .domain(chartRanges.y)
                        .range([layoutHeight, 0])
                        .nice(),

                    _x: self.d3.scale.linear()
                        .domain([0, layoutWidth])
                        .range([rangingScales.reversed(currentRange[0]), rangingScales.reversed(currentRange[1])]),

                    _y: self.d3.scale[self.scope.settings.linearChart.scale]()
                        .domain([layoutHeight, 0])
                        .range(chartRanges.y),

                    bgX: self.d3.scale.linear()
                        .domain([0, layoutWidth])
                        .range([axisYWidth + 15, axisYWidth + 5]),

                    bgY: self.d3.scale.linear()
                        .domain([0, layoutHeight])
                        .range([20, 10])
                },
                line = self.d3.svg.line()
                    .x(function(d) { return renderingScales.x(d.x); })
                    .y(function(d) { return renderingScales.y(showCummulative ? d.yCumm : d.yDiff); })
                    .interpolate(interpolation),
                area = self.d3.svg.area()
                    .x(function(d) { return renderingScales.x(d.x); })
                    .y0(function(d){ return renderingScales.y(0); })
                    .y1(function(d) { return renderingScales.y(showCummulative ? d.yCumm : d.yDiff); })
                    .interpolate(interpolation);

            [
                pathLayer.selectAll('path').data(baseLayerData),
                areaLayer.selectAll('path').data(areaTypeIsSelected ? baseLayerData : [])
            ].forEach(function(layers, n){

                    layers.enter().append('path');

                    layers.exit().remove();

                    layers.attr({
                        fill: function(d, i){
                            return [self.transparentColor, chartData[i].color][n];
                        },
                        'fill-opacity': 0.5,
                        'stroke-width': 1.5,
                        stroke: function(d, i){
                            return [chartData[i].color, self.transparentColor][n];
                        },
                        d: function(d){
                            return [line, area][n](d);
                        }
                    });
                });

            interactiveLayer.attr({
                x: 0,
                y: 0,
                width: layoutWidth,
                height: layoutHeight,
                fill: self.transparentColor
            }).style({
                cursor: 'crosshair'
            });

            /** Base layer interactions */
            var onLayoutMouseMove = function(){
                var event = self.d3.event,
                    coords = self.d3.mouse(interactiveLayer.node()),
                    pointSize = 5,
                    detectingZone = [
                        renderingScales._x(coords[0] - pointSize),
                        renderingScales._x(coords[0] + pointSize)
                    ],
                    pointsInRange = baseLayerData.map(function(layer){
                        return layer.filter(function(point){
                            return point.x >= detectingZone[0] && point.x <= detectingZone[1];
                        })[0];
                    }).filter(function(point){
                        return point;
                    }),
                    highlitedPoints = pointsInRange.filter(function(point){
                        return Math.abs(coords[1] - renderingScales.y(showCummulative ? point.yCumm : point.yDiff)) < 10;
                    }),
                    nearestPoints = pointsLayer.selectAll('circle').data(pointsInRange);

                /** Helpers */
                helperH.attr({
                    x1: 0,
                    x2: layoutWidth,
                    y1: coords[1],
                    y2: coords[1],
                    stroke: self.textColorDark,
                    'stroke-width': 0.7
                });
                helperV.attr({
                    x1: coords[0],
                    x2: coords[0],
                    y1: 0,
                    y2: layoutHeight,
                    stroke: self.textColorDark,
                    'stroke-width': 0.7
                });

                /** Points */
                nearestPoints.enter().append('circle');
                nearestPoints.exit().remove();
                nearestPoints.attr({
                    cx: function(d){
                        return renderingScales.x(d.x);
                    },
                    cy: function(d){
                        return renderingScales.y(showCummulative ? d.yCumm : d.yDiff);
                    },
                    r: 4,
                    stroke: function(d){
                        return d.color;
                    },
                    'stroke-width': 2,
                    fill: function(d){
                        return Math.abs(coords[1] - renderingScales.y(showCummulative ? d.yCumm : d.yDiff)) < 10 ? d.color: self.textColorLight;
                    }
                });

                self.scope.settings.linearChart.onMouseOver(highlitedPoints.map(function(p){ return p.pointEntity; }), {
                    x: event.clientX,
                    y: event.clientY
                });
            };

            var onLayoutMouseOut = function(){
                [helperH, helperV].forEach(function(helper){
                    helper.attr({ stroke: self.transparentColor });
                });
                pointsLayer.selectAll('circle').remove();
                self.scope.settings.linearChart.onMouseOut();
            };

            interactiveLayer.on('mousemove', onLayoutMouseMove);

            interactiveLayer.on('mouseout', onLayoutMouseOut);

            refreshAxisX();
            refreshAxisY();
            refreshBackground();
        }

        function refreshAxisY (){
            var axisY = wrapper.select('.bw-linear-chart-axis-y'),
                renderingScale = self.d3.scale[self.scope.settings.linearChart.scale]()
                    .domain(chartRanges.y)
                    .range([layoutHeight, 0]),
                d3Axis = self.d3.svg.axis()
                    .scale(renderingScale)
                    .orient("left")
                    .ticks(7);

            axisY.call(d3Axis);

            axisY.select('path').attr({ fill: self.transparentColor });
            axisY.selectAll('g')
                .select('text')
                .attr({
                    fill: self.textColorDark,
                    'font-size': 12
                })
                .text(function(d){
                    return self.numbFilter(d);
                });

            axisY.selectAll('g')
                .select('line')
                .attr({
                    x1: 0,
                    x2: layoutWidth,
                    y1: 0,
                    y2: 0,
                    stroke: function(d){
                        return !d ? self.textColorDark : self.textColorSemiDark;
                    },
                    'stroke-width': 0.3
                });
        }

        function refreshAxisX (){
            var axisX = wrapper.select('.bw-linear-chart-axis-x'),
                chartSettings = self.scope.userSettings.linearChart,
                currentRange = chartSettings.range,
                rangingScale = self.d3.scale.linear()
                    .domain([0, 1])
                    .range(chartRanges.x),
                renderingScale = self.d3.scale.linear()
                    .domain([rangingScale(currentRange[0]), rangingScale(currentRange[1])])
                    .range([0, layoutWidth]),
                d3Axis = self.d3.svg.axis()
                    .scale(renderingScale)
                    .orient("bottom")
                    .ticks(7);

            axisX.call(d3Axis);

            axisX.select('path').attr({ fill: self.transparentColor });
            axisX.selectAll('g')
                .select('text')
                .attr({
                    fill: self.textColorDark,
                    'font-size': 12
                });

            axisX.selectAll('g')
                .select('line')
                .attr({
                    x1: 0,
                    x2: 0,
                    y1: -(layoutHeight),
                    y2: 0,
                    stroke: self.textColorSemiDark,
                    'stroke-width': 0.3
                });
        }

        function refreshBackground (){
            var bgLayer = wrapper.select('.bw-linear-chart-background'),
                paths = layout.select('.path-layer').selectAll('path')[0],
                helpers = layout.select('.helpers-layer').selectAll('line')[0];

            if(!bgLayer.select('.shadow-layout').node()) {
                bgLayer.append('g').classed('shadow-layout', true);
            }

            bgLayer.attr({
                transform: 'translate(' + [axisYWidth + 10, 10] + ')'
            });

            var shadowsA = bgLayer.select('.shadow-layout').selectAll('path').data(paths);

            shadowsA.enter().append('path');

            shadowsA.exit().remove();

            shadowsA.attr({
                d: function(d){
                    return self.d3.select(d).attr('d');
                },
                stroke: self.textColorDark,
                fill: self.transparentColor,
                'fill-opacity': 0.3,
                filter: 'url(#f1)'
            });

            shadowsA.exit().remove();
        }
    };

    BWChartDirectiveCtrl.prototype.drawBarChart = function(){
        /** Remove exceeded charts */
        this.clearCanvas('bw-bar-chart');

        var self = this,
            chartSettings = self.scope.settings.barChart,
            renderingData = self._data_.getRenderingData(chartSettings),
            currentRanges = self._data_.getRanges(chartSettings),
            showHorizontal = chartSettings.showHorizontal,
            containerWidth = self.containerWidth,
            containerHeight = self.containerHeight,
            axisXHeight = chartSettings.showAxisX ? 20 : 0,
            axisYWidth = chartSettings.showAxisY ? getVerticalAxisWidth() : 0,
            axisXLabelHeight = chartSettings.axisXName ? 20 : 0,
            axisYLabelWidth = chartSettings.axisYName ? 20 : 0,
            paddingTop = chartSettings.fontSize * 0.5,
            paddingRight = chartSettings.fontSize * 0.5,
            paddingLeft = axisYWidth + axisYLabelWidth,
            paddingBottom = axisXHeight + axisXLabelHeight,
            innerWidth = containerWidth - (paddingLeft + paddingRight),
            innerHeight = containerHeight - (paddingTop + paddingBottom),
            clusterSize = (showHorizontal ? innerHeight : innerWidth) / renderingData.length,
            barSize =  clusterSize * (chartSettings.thin * 0.049),
            renderingScale = self.d3.scale.linear()
                .domain(currentRanges)
                .range(showHorizontal ? [0, innerWidth] : [innerHeight, 0])
                .nice(),
            barSizeScale = self.d3.scale.linear()
                .domain([0, renderingScale.domain()[1] - renderingScale.domain()[0]])
                .range(showHorizontal ? [0, innerWidth] : [0, innerHeight]),
            zeroPoint = renderingScale(0),

            /** DOM-elements */
            wrapper, backgroundLayer, mainLayer, axisXLayer, axisYLayer;

        refreshChartStructure();

        refreshClusters();

        refreshAxisX();

        refreshAxisY();

        refreshBackground();

        function refreshChartStructure (){
            if(!self.layout.select('.bw-bar-chart').node()) {
                self.layout.append('g').classed('bw-bar-chart', true);
                self.layout.select('.bw-bar-chart').append('g').classed('axis-x-layer', true);
                self.layout.select('.bw-bar-chart').append('g').classed('axis-y-layer', true);
                self.layout.select('.bw-bar-chart').append('g').classed('background-layer', true);
                self.layout.select('.bw-bar-chart').append('g').classed('main-layer', true);
            }

            wrapper = self.layout.select('.bw-bar-chart');
            backgroundLayer = wrapper.select('.background-layer');
            mainLayer = wrapper.select('.main-layer');
            axisXLayer = wrapper.select('.axis-x-layer');
            axisYLayer = wrapper.select('.axis-y-layer');

            self.layout.attr({
                transform: 'translate(' + [0, 0] + ')'
            });

            wrapper.attr({
                transform: 'translate(' + [0, 0] + ')'
            });

            backgroundLayer.attr({
                transform: 'translate(' + [0, paddingTop] + ')'
            });

            mainLayer.attr({
                transform: 'translate(' + (showHorizontal ? [paddingLeft + zeroPoint, paddingTop] : [paddingLeft, paddingTop + zeroPoint]) + ')'
            });

            axisXLayer.attr({
                transform: 'translate(' + [paddingLeft, paddingTop + innerHeight] + ')'
            });

            axisYLayer.attr({
                transform: 'translate(' + [paddingLeft, paddingTop] + ')'
            });
        }

        function refreshClusters (){
            var clusters = mainLayer.selectAll('.bc-cluster').data(renderingData);

            clusters.enter().append('g').classed('bc-cluster', true);

            clusters.exit().remove();

            clusters.attr({
                transform: function(d, i){
                    refreshBars(d, self.d3.select(this));
                    return 'translate(' + (showHorizontal ? [0, clusterSize * i] : [clusterSize * i, 0]) + ')';
                }
            });
        }

        function refreshBars (d, cluster){
            var bars = cluster.selectAll('.bc-bar').data(d.bars),
                offset = (clusterSize - barSize) / 2;

            bars.enter().append('rect').classed('bc-bar', true);

            bars.exit().remove();

            if(showHorizontal){
                bars.attr({
                    x: function(b){
                        return b.isLowerThenZero ?  -barSizeScale(b.size) : 0;
                    },
                    y: offset,
                    width: function(b){
                        return barSizeScale(b.size);
                    },
                    height: barSize,
                    fill: function(b){
                        return b.color;
                    },
                    'stroke-width': 0.5,
                    stroke: '#FFF'
                });
            } else {
                bars.attr({
                    x: offset,
                    y: function(b){
                        return b.isLowerThenZero ? 0 : -barSizeScale(b.size);
                    },
                    width: barSize,
                    height: function(b){
                        return barSizeScale(b.size);
                    },
                    fill: function(b){
                        return b.color;
                    },
                    'stroke-width': 0.5,
                    stroke: '#FFF'
                });
            }

            bars
                .on('mouseover', onMouseOver)
                .on('mousemove', onMouseMove)
                .on('mouseout', onMouseOut)
                .on('click', onClick);
        }

        function refreshAxisX (){
            if(showHorizontal) {
                var d3Axis = self.d3.svg.axis()
                    .scale(renderingScale)
                    .orient('bottom');

                axisXLayer.call(d3Axis);

                var lastTickIndex = axisXLayer.selectAll('.tick')[0].length - 1;

                axisXLayer.select('path').attr({ fill: 'none' });

                axisXLayer.selectAll('.tick')
                    .select('text')
                    .attr({
                        fill: '#c5c5c7',
                        'font-size': chartSettings.fontSize
                    })
                    .text(function(d, i){
                        return !chartSettings.showAxisX || (chartSettings.showAxisX && i === lastTickIndex) ? '' : chartSettings.getFormattedLabel(d);
                    });

                axisXLayer.selectAll('.tick').select('line')
                    .attr({
                        x1: 0,
                        x2: 0,
                        y1: 0,
                        y2: function(d, i){
                            return i === lastTickIndex || !d || !i ? 0 : -innerHeight;
                        },
                        stroke: '#c5c5c7',
                        'stroke-width': 0.5,
                        'stroke-dasharray': '2,2'
                    });
            } else {
                var labelProperty = chartSettings.keys.label,
                    ticks = axisXLayer.selectAll('.tick').data(renderingData.map(function(d){
                        return d[labelProperty];
                    }));

                ticks.enter().append('text').classed('tick', true);

                ticks.exit().remove();

                ticks
                    .attr({
                        fill: '#c5c5c7',
                        'font-size': chartSettings.fontSize,
                        'text-anchor': 'middle',
                        transform: function(d, i){
                            return 'translate(' + [(clusterSize * i) + (clusterSize * 0.5), chartSettings.fontSize + 2] + ')';
                        }
                    })
                    .text(function(d){ return d; });
            }
        }

        function refreshAxisY (){
            if(showHorizontal) {
                var labelProperty = chartSettings.keys.label,
                    ticks = axisYLayer.selectAll('.tick').data(renderingData.map(function(d){
                        return d[labelProperty];
                    }));

                ticks.enter().append('text').classed('tick', true);

                ticks.exit().remove();

                ticks
                    .attr({
                        fill: '#c5c5c7',
                        'font-size': chartSettings.fontSize,
                        'text-anchor': 'end',
                        transform: function(d, i){
                            return 'translate(' + [ -6, (clusterSize * i)  + (clusterSize * 0.5) + (chartSettings.fontSize * 0.5)] + ')';
                        }
                    })
                    .text(function(d){ return d; });

            } else {
                var d3Axis = self.d3.svg.axis()
                    .scale(renderingScale)
                    .orient('left');

                axisYLayer.call(d3Axis);

                axisYLayer.select('path').attr({ fill: 'none' });

                axisYLayer.selectAll('.tick')
                    .select('text')
                    .attr({
                        fill: '#c5c5c7',
                        'font-size': chartSettings.fontSize
                    })
                    .text(function(d, i){
                        return !chartSettings.showAxisX || (chartSettings.showAxisX && !i) ? '' : chartSettings.getFormattedLabel(d);
                    });

                axisYLayer.selectAll('.tick').select('line')
                    .attr({
                        x1: 0,
                        x2: function(d, i){
                            return !i || !d ? 0 : innerWidth;
                        },
                        y1: 0,
                        y2: 0,
                        stroke: '#c5c5c7',
                        'stroke-width': 0.5,
                        'stroke-dasharray': '2,2'
                    });
            }
        }

        function refreshBackground (){
            if(!backgroundLayer.select('text').node()){
                backgroundLayer.append('text').classed('axis-x-label', true);
                backgroundLayer.append('text').classed('axis-y-label', true);
                backgroundLayer.append('line').classed('base-line', true);
                backgroundLayer.append('line').classed('zero-line', true);
            }

            backgroundLayer.select('.axis-x-label')
                .attr({
                    fill: '#262626',
                    'font-size': chartSettings.fontSize,
                    'font-weight': 'bold',
                    'text-anchor': 'middle',
                    transform: 'translate(' + [paddingLeft + (innerWidth * 0.5), innerHeight + axisXHeight + chartSettings.fontSize] + ')'
                })
                .text(chartSettings.axisXName);

            backgroundLayer.select('.axis-y-label')
                .attr({
                    fill: '#262626',
                    'font-size': chartSettings.fontSize,
                    'font-weight': 'bold',
                    'text-anchor': 'middle',
                    transform: 'translate(' + [chartSettings.fontSize + 2, innerHeight * 0.5] + ') rotate(-90)'
                })
                .text(chartSettings.axisYName);

            backgroundLayer.select('.base-line')
                .attr({
                    transform: 'translate(' + [paddingLeft, 0] + ')',
                    x1: showHorizontal ? 0 : -6,
                    x2: showHorizontal ? 0 : innerWidth,
                    y1: showHorizontal ? 0 : innerHeight,
                    y2: innerHeight,
                    stroke: '#dbe0e6',
                    'stroke-width': 1
                });

            backgroundLayer.select('.zero-line')
                .attr({
                    transform: 'translate(' + [paddingLeft, 0] + ')',
                    x1: showHorizontal ? zeroPoint : 0,
                    x2: showHorizontal ? zeroPoint : innerWidth,
                    y1: showHorizontal ? 0 : zeroPoint,
                    y2: showHorizontal ? innerHeight : zeroPoint,
                    stroke: '#dbe0e6',
                    'stroke-width': 1
                });
        }

        function getVerticalAxisWidth (){
            var ninjaNode = self.layout.append('g').classed('ninja-node', true),
                tickSizes = [];

            if(!showHorizontal) {
                var ninjaScale = self.d3.scale.linear()
                    .domain(currentRanges)
                    .range([0, containerHeight - 32])
                    .nice(),
                    ninjaAxis = self.d3.svg.axis()
                    .scale(ninjaScale)
                    .orient('left');

                ninjaNode.call(ninjaAxis);

                ninjaNode.selectAll('.tick')
                    .select('text')
                    .attr({'font-size': chartSettings.fontSize})
                    .text(function(d){return chartSettings.getFormattedLabel(d);})
                    .attr({fill: function(d){tickSizes.push(this.getBBox().width); return '#FFF';}});
            } else {
                var labelProperty = chartSettings.keys.label,
                    ticks = ninjaNode.selectAll('text').data(renderingData.map(function(d){ return d[labelProperty]; }));

                ticks.enter().append('text');

                ticks
                    .attr({'font-size': chartSettings.fontSize})
                    .text(function(d){return d;})
                    .attr({fill: function(d){
                        tickSizes.push(this.getBBox().width); return '#FFF';}
                    });
            }
            ninjaNode.remove();

            return self.d3.max(tickSizes) + 10;
        }

        /** Callbacks */
        function onMouseOver (b){
            var event = self.d3.event;

            chartSettings.onMouseOver(b, {
                x: event.clientX,
                y: event.clientY
            }, event, getHighlighters(b, this));
        }

        function onMouseMove (b){
            var event = self.d3.event;

            chartSettings.onMouseMove(b, {
                x: event.clientX,
                y: event.clientY
            }, event);
        }

        function onMouseOut (b){
            var event = self.d3.event;

            clearHighlighters(b, this);

            chartSettings.onMouseOut(b, {
                x: event.clientX,
                y: event.clientY
            }, event);
        }

        function onClick (b){
            var event = self.d3.event;

            chartSettings.onClick(b, {
                x: event.clientX,
                y: event.clientY
            }, event);
        }

        function getHighlighters (b, elem){
            return {
                highlightBorders: function(color){
                    var parentNode = self.d3.select(elem.parentNode),
                        ninjaNode = parentNode.append('rect').classed('ninja-node', true)
                            .attr({
                                fill: 'none',
                                stroke: color || '#e6433b',
                                'stroke-width': 2
                            }),
                        nnSize = barSizeScale(Math.abs(b.value)),
                        nnPosition = (clusterSize - barSize) / 2,
                        nodeBBox = elem.getBBox();

                    ninjaNode.attr(
                        showHorizontal ? {
                            x: b.isLowerThenZero ? nodeBBox.x : nodeBBox.width - nnSize,
                            y: nnPosition,
                            width: nnSize,
                            height: barSize
                        } : {
                            x: nnPosition,
                            y: b.isLowerThenZero ? nodeBBox.height - nnSize : nodeBBox.y,
                            width: barSize,
                            height: nnSize
                        }
                    );
                },
                highlightRect: function(){
                    self.d3.select(elem).attr({
                        fill: self.colorsService.getDarkerColor(b.color, 1)
                    });
                }
            }
        }

        function clearHighlighters (b, elem){
            self.d3.select(elem.parentNode).select('.ninja-node').remove();
            self.d3.select(elem).attr({ fill: b.color });
        }




        //console.log(currentRanges, renderingData);
            //padding = getPadding(),
        //    verticalAxisWidth = chartSettings.showAxisY ? padding[showHorizontal ? 'ordinalAxisPadding' : 'linearAxisPadding'] : 0,
        //    horizontalAxisHeight = chartSettings.showAxisX ? padding[showHorizontal ? 'linearAxisPadding' : 'ordinalAxisPadding'] : 0,
        //    innerWidth = containerWidth - verticalAxisWidth,
        //    innerHeight = containerHeight - horizontalAxisHeight,
        //    clusterSize = (showHorizontal ? innerHeight : innerWidth) / renderingData.length,
        //    barSize =  clusterSize * (chartSettings.thin * 0.049),
        //    renderingScale = self.d3.scale.linear()
        //        .domain([0, 1])
        //        .range(showHorizontal ? [0, innerWidth] : [0, innerHeight]),
        //    axisScale = self.d3.scale.linear()
        //        .domain(currentRanges)
        //        .range(showHorizontal ? [0, innerWidth] : [innerHeight, 0]),
        //
        //    /** DOM-elements */
        //    wrapper, backgroundLayer, mainLayer, axisXLayer, axisYLayer;
        //
        //refreshChartStructure();
        //
        //refreshClusters();
        //
        //refreshAxisX();
        //
        //refreshAxisY();
        //
        //function refreshChartStructure (){
        //    if(!self.layout.select('.bw-bar-chart').node()) {
        //        self.layout.append('g').classed('bw-bar-chart', true);
        //        self.layout.select('.bw-bar-chart').append('g').classed('background-layer', true);
        //        self.layout.select('.bw-bar-chart').append('g').classed('axis-x-layer', true);
        //        self.layout.select('.bw-bar-chart').append('g').classed('axis-y-layer', true);
        //        self.layout.select('.bw-bar-chart').append('g').classed('main-layer', true);
        //    }
        //
        //    wrapper = self.layout.select('.bw-bar-chart');
        //    backgroundLayer = wrapper.select('.background-layer');
        //    mainLayer = wrapper.select('.main-layer');
        //    axisXLayer = wrapper.select('.axis-x-layer');
        //    axisYLayer = wrapper.select('.axis-y-layer');
        //
        //    self.layout.attr({
        //        transform: 'translate(' + [0, 0] + ')'
        //    });
        //
        //    wrapper.attr({
        //        transform: 'translate(' + [0, 0] + ')'
        //    });
        //
        //    backgroundLayer.attr({
        //        transform: 'translate(' + [0, 0] + ')'
        //    });
        //
        //    mainLayer.attr({
        //        transform: 'translate(' + [verticalAxisWidth, 0] + ')'
        //    });
        //
        //    axisXLayer.attr({
        //        transform: 'translate(' + [verticalAxisWidth, innerHeight] + ')'
        //    });
        //
        //    axisYLayer.attr({
        //        transform: 'translate(' + [verticalAxisWidth, 0] + ')'
        //
        //    });
        //}
        //
        //function refreshClusters (){
        //    var clusters = mainLayer.selectAll('.bar-chart-cluster').data(renderingData);
        //
        //    clusters.enter().append('g').classed('bar-chart-cluster', true);
        //
        //    clusters.exit().remove();
        //
        //    clusters.attr({
        //        transform: function(d, i){
        //            var cluster = self.d3.select(this),
        //                bars = cluster.selectAll('.bar-chart-bar').data(d.bars);
        //
        //            bars.enter().append('g').classed('bar-chart-bar', true);
        //
        //            bars.exit().remove();
        //
        //            bars.attr({
        //                transform: function(b, ind){
        //                    var bar = self.d3.select(this),
        //                        attributes = getBarAttributes(b, bar),
        //                        coloredBarAttributes = angular.merge({
        //                            fill: b.color
        //                        }, attributes.static),
        //                        interactiveBarAttributes = angular.merge({
        //                            fill: '#000',
        //                            'fill-opacity': 0
        //                        }, attributes.static);
        //
        //                    if(!bar.select('rect').node()) {
        //                        bar.append('rect').classed('colored-bar', true);
        //                        bar.append('rect').classed('interactive-bar', true);
        //                    }
        //
        //                    bar.select('.colored-bar')
        //                        .attr(coloredBarAttributes)
        //                        .transition().duration(self.scope.settings.animationSpeed)
        //                        .attr(attributes.dynamic);
        //
        //                    bar.select('.interactive-bar')
        //                        .attr(interactiveBarAttributes)
        //                        .transition().duration(self.scope.settings.animationSpeed)
        //                        .attr(attributes.dynamic);
        //
        //                    bar.select('.interactive-bar')
        //                        .on('mouseover', function(){
        //                            var event = self.d3.event;
        //
        //                            self.d3.select(this).attr({ 'fill-opacity' : 0.2 });
        //
        //                            chartSettings.onMouseOver(b, {
        //                                x: event.clientX,
        //                                y: event.clientY
        //                            }, event);
        //                        })
        //                        .on('mousemove', function(){
        //                            var event = self.d3.event;
        //
        //                            chartSettings.onMouseMove(b, {
        //                                x: event.clientX,
        //                                y: event.clientY
        //                            }, event);
        //                        })
        //                        .on('mouseout', function(){
        //                            var event = self.d3.event;
        //
        //                            self.d3.select(this).attr({ 'fill-opacity' : 0 })
        //
        //                            chartSettings.onMouseOut(b, {
        //                                x: event.clientX,
        //                                y: event.clientY
        //                            }, event);
        //                        })
        //                        .on('click', function(){
        //                            var event = self.d3.event;
        //
        //                            chartSettings.onClick(b, {
        //                                x: event.clientX,
        //                                y: event.clientY
        //                            }, event);
        //                        });
        //
        //                    return 'translate(' + [0, 0] + ')';
        //                }
        //            });
        //
        //            return 'translate(' + (showHorizontal ? [0, clusterSize * i] : [clusterSize * i, 0] ) + ')';
        //        }
        //    });
        //}
        //
        //function refreshAxisX (){
        //    axisXLayer.selectAll('*').remove();
        //
        //    if(showHorizontal) {
        //        var numberOfTicks = Math.floor(containerWidth / 40),
        //            d3Axis = self.d3.svg.axis()
        //            .scale(axisScale)
        //            .orient('bottom')
        //            .ticks(numberOfTicks < 5 ? numberOfTicks : 5);
        //
        //        axisXLayer.call(d3Axis);
        //
        //        axisXLayer.select('path').attr({ fill: 'none' });
        //
        //        axisXLayer.selectAll('g')
        //            .select('text')
        //            .attr({
        //                fill: '#c5c5c7',
        //                'font-size': 10
        //            })
        //            .text(function(d){
        //                return chartSettings.showAxisX ? chartSettings.getFormattedLabel(d) : '';
        //            });
        //
        //        axisXLayer.selectAll('g')
        //            .select('line')
        //            .attr({
        //                x1: 0,
        //                x2: 0,
        //                y1: -innerHeight,
        //                y2: chartSettings.showAxisX ? 4 : 0,
        //                stroke: '#c5c5c7',
        //                'stroke-width': 0.5
        //            });
        //    } else if(chartSettings.showAxisX){
        //        axisXLayer.append('g').classed('ticks-container', true);
        //        axisXLayer.append('line').classed('axis-x-line', true)
        //            .attr({
        //                x1: 0,
        //                x2: innerWidth,
        //                y1: 0,
        //                y2: 0,
        //                stroke: '#c5c5c7',
        //                'stroke-width': 0.5
        //            });
        //
        //        var ticks = axisXLayer.select('.ticks-container').selectAll('.axis-x-tick').data(renderingData),
        //            baseOffset = clusterSize * 0.5;
        //
        //        ticks.enter().append('g').classed('axis-x-tick', true);
        //
        //        ticks.exit().remove();
        //
        //        ticks.attr({
        //            transform: function(d, i){
        //                var tick = self.d3.select(this);
        //
        //                tick.append('text')
        //                    .attr({
        //                        'font-size': chartSettings.fontSize,
        //                        fill: '#c5c5c7',
        //                        'text-anchor': 'middle',
        //                        transform: 'translate(' + [0 , chartSettings.fontSize + 6] + ')'
        //                    })
        //                    .text(d.label);
        //
        //                tick.append('line')
        //                    .attr({
        //                        x1: 0,
        //                        x2: 0,
        //                        y1: 0,
        //                        y2: 4,
        //                        stroke: '#c5c5c7',
        //                        'stroke-width': 0.5
        //                    });
        //
        //
        //                return 'translate(' + [baseOffset + (clusterSize * i), 0 ] + ')'
        //            }
        //        });
        //    }
        //}
        //
        //function refreshAxisY (){
        //    axisYLayer.selectAll('*').remove();
        //
        //    if(showHorizontal) {
        //        if(chartSettings.showAxisY) {
        //            axisYLayer.append('g').classed('ticks-container', true);
        //            axisYLayer.append('line').classed('axis-y-line', true)
        //                .attr({
        //                    x1: 0,
        //                    x2: 0,
        //                    y1: 0,
        //                    y2: innerHeight,
        //                    stroke: '#b1b1b1',
        //                    'stroke-width': 0.5
        //                });
        //
        //            var ticks = axisYLayer.select('.ticks-container').selectAll('.axis-y-tick').data(renderingData),
        //                baseOffset = clusterSize * 0.5;
        //
        //            ticks.enter().append('g').classed('axis-y-tick', true);
        //
        //            ticks.exit().remove();
        //
        //            ticks.attr({
        //                transform: function(d, i){
        //                    var tick = self.d3.select(this);
        //
        //                    tick.append('text')
        //                        .attr({
        //                            'font-size': chartSettings.fontSize,
        //                            fill: '#b1b1b1',
        //                            'text-anchor': 'end',
        //                            transform: 'translate(' + [-6 , (chartSettings.fontSize - 2) * 0.5] + ')'
        //                        })
        //                        .text(d.label);
        //
        //                    tick.append('line')
        //                        .attr({
        //                            x1: 0,
        //                            x2: -4,
        //                            y1: 0,
        //                            y2: 0,
        //                            stroke: '#b1b1b1',
        //                            'stroke-width': 0.5
        //                        });
        //
        //
        //                    return 'translate(' + [0, baseOffset + (clusterSize * i) ] + ')'
        //                }
        //            });
        //        }
        //    } else {
        //        var numberOfTicks = Math.floor(containerHeight / 40),
        //            d3Axis = self.d3.svg.axis()
        //                .scale(axisScale)
        //                .orient('left')
        //                .ticks(numberOfTicks < 5 ? numberOfTicks : 5);
        //
        //        axisYLayer.call(d3Axis);
        //
        //        axisYLayer.select('path').attr({ fill: 'none' });
        //
        //        axisYLayer.selectAll('g')
        //            .select('text')
        //            .text(function(d){
        //                return '';
        //            });
        //
        //        axisYLayer.selectAll('g')
        //            .select('line')
        //            .attr({
        //                x1: chartSettings.showAxisY ? -4 : 0,
        //                x2: innerWidth,
        //                y1: 0,
        //                y2: 0,
        //                stroke: '#c5c5c7',
        //                'stroke-width': 0.5
        //            });
        //
        //        var ticks = axisYLayer.selectAll('.formatted-label').data(currentRanges.map(function(d){
        //            return chartSettings.getFormattedLabel(d);
        //        }));
        //
        //        ticks.enter().append('text').classed('formatted-label', true);
        //
        //        ticks.exit().remove();
        //
        //        ticks
        //            .attr({
        //                fill: '#c5c5c7',
        //                'font-size': 10,
        //                'text-anchor': function(d, i){
        //                    return i ? 'end' : 'start';
        //                },
        //                transform: function(d, i){
        //                    return 'translate(' + [-6, i ? 0 : innerHeight] + ') rotate(-90)';
        //
        //                }
        //            })
        //            .text(function(d){
        //                return d;
        //            });
        //    }
        //}
        //
        //function getPadding (){
        //    var labels = renderingData.map(function(cluster){
        //            return cluster.label;
        //        }),
        //        ninjaNode = self.layout.append('text')
        //            .classed('bw-bar-chart-ninja-node', true),
        //        maxWidth = 0,
        //        maxHeight = 0;
        //
        //    labels.forEach(function(label){
        //        ninjaNode.text(label);
        //
        //        var bbox = ninjaNode.node().getBBox();
        //
        //        maxWidth = self.d3.max([maxWidth, bbox.width]);
        //        maxHeight = self.d3.max([maxHeight, bbox.height]);
        //    });
        //
        //    ninjaNode.remove();
        //
        //    return {
        //        linearAxisPadding: chartSettings.padding,
        //        ordinalAxisPadding: (showHorizontal ? maxWidth : maxHeight) + 6
        //    };
        //}
        //
        //function getBarAttributes (barData, barNode){
        //    var attrs = {},
        //        calculatedSize = renderingScale(barData.size),
        //        staticSize = (clusterSize - barSize) * 0.5;
        //
        //    if(showHorizontal) {
        //        attrs.static = {
        //            x: 0,
        //            y: staticSize,
        //            height: barSize,
        //            stroke: '#FFF',
        //            'stroke-width': 0.5
        //        };
        //        attrs.dynamic = {
        //            width: calculatedSize
        //        };
        //
        //    } else {
        //        var fromPoint = barNode.select('rect').node() ? parseFloat(barNode.select('.colored-bar').attr('y')) : 0,
        //            y = innerHeight - calculatedSize;
        //
        //        attrs.static = {
        //            x: staticSize,
        //            y: fromPoint || y,
        //            width: barSize,
        //            height: fromPoint ? (innerHeight - fromPoint) : calculatedSize,
        //            stroke: '#FFF',
        //            'stroke-width': 0.5
        //        };
        //        attrs.dynamic = {
        //            y: y,
        //            height: calculatedSize
        //        };
        //    }
        //
        //    return attrs;
        //}
    };

    BWChartDirectiveCtrl.prototype.drawBoxPlotChart = function(){
        /** Remove exceeded charts */
        this.clearCanvas('bw-boxplot-chart');

        var self = this,
            containerWidth = self.containerWidth,
            containerHeight = self.containerHeight,
            chartSettings = self.scope.settings.boxPlotChart,
            padding = chartSettings.padding,
            innerWidth = containerWidth - (padding.left + padding.right),
            innerHeight = containerHeight - (padding.bottom + padding.top),
            ranges = self._data_.getRanges(),
            scaleX = self.d3.time.scale()
                .domain([new Date(ranges.x[0]), new Date(ranges.x[1])])
                .range([0, innerWidth])
                .nice(self.d3.time.year),
            scaleY = self.d3.scale.linear()
                .domain(ranges.y)
                .range([innerHeight, 0])
                .nice(),
            scaleR = self.d3.scale.linear()
                .domain(ranges.r)
                .range([1, chartSettings.barWidth]),

            /** DOM-Elements */
            wrapper, bgLayer, mainLayer, axisXLayer, axisYLayer;

        refreshChartStructure();

        refreshMainLayer();

        refreshAxisX();

        refreshAxisY();

        function refreshChartStructure (){
            if(!self.layout.select('.bw-boxplot-chart').node()){
                self.layout.append('g').classed('bw-boxplot-chart', true);
                self.layout.select('.bw-boxplot-chart').append('g').classed('bg-layer', true);
                self.layout.select('.bw-boxplot-chart').append('g').classed('axis-x-layer', true);
                self.layout.select('.bw-boxplot-chart').append('g').classed('axis-y-layer', true);
                self.layout.select('.bw-boxplot-chart').append('g').classed('main-layer', true);
            }

            wrapper = self.layout.select('.bw-boxplot-chart');
            bgLayer = wrapper.select('.bg-layer');
            mainLayer = wrapper.select('.main-layer');
            axisXLayer = wrapper.select('.axis-x-layer');
            axisYLayer = wrapper.select('.axis-y-layer');

            self.layout.attr({ transform: 'translate(' + [0, 0] + ')' });
            bgLayer.attr({ transform: 'translate(' + [padding.left, padding.top] + ')' });
            mainLayer.attr({ transform: 'translate(' + [padding.left, padding.top] + ')' });
            axisXLayer.attr({ transform: 'translate(' + [padding.left, padding.top + innerHeight] + ')' });
            axisYLayer.attr({ transform: 'translate(' + [padding.left, padding.top] + ')' });
        }

        function refreshMainLayer (){
            var blocks = mainLayer.selectAll('.bw-boxplot').data(self._data_.getRenderingData()),
                barWidth = chartSettings.barWidth;

            blocks.enter().append('g').classed('bw-boxplot', true);

            blocks.exit().remove();

            blocks.attr({
                transform: function(d){
                    var block = self.d3.select(this);

                    if(!block.select('.box-layer').node()) {
                        block.append('g').classed('points-layer', true);
                        block.append('g').classed('box-layer', true);
                        block.append('g').classed('whiskers-layer', true);
                        block.select('.box-layer').append('rect').classed('high-box', true);
                        block.select('.box-layer').append('rect').classed('low-box', true);
                        block.select('.whiskers-layer').append('g').classed('highest-whisker', true);
                        block.select('.whiskers-layer').select('.highest-whisker').append('line').classed('hor-high', true);
                        block.select('.whiskers-layer').select('.highest-whisker').append('line').classed('vertical', true);
                        block.select('.whiskers-layer').select('.highest-whisker').append('line').classed('hor-low', true);
                        block.select('.whiskers-layer').append('g').classed('lowest-whisker', true);
                        block.select('.whiskers-layer').select('.lowest-whisker').append('line').classed('hor-high', true);
                        block.select('.whiskers-layer').select('.lowest-whisker').append('line').classed('vertical', true);
                        block.select('.whiskers-layer').select('.lowest-whisker').append('line').classed('hor-low', true);
                    }

                    var highBox = block.select('.box-layer').select('.high-box'),
                        lowBox = block.select('.box-layer').select('.low-box'),
                        highestWhisker = block.select('.whiskers-layer').select('.highest-whisker'),
                        lowestWhisker = block.select('.whiskers-layer').select('.lowest-whisker'),
                        points = block.select('.points-layer').selectAll('.boxplot-point').data(d.points),
                        halfOfBar = barWidth / 2,
                        animationSpeed = self.scope.settings.animationSpeed;


                    points.enter().append('circle').classed('boxplot-point', true);

                    points.exit().remove();

                    points
                        .attr({
                            cx: 0,
                            r: 3
                        })
                        .transition()
                        .attr({
                            cy: function(c){
                                return scaleY(c.y);
                            },
                            r: function(c){
                                return scaleR(c.r);
                            },
                            fill: function(c){
                                return c.color || chartSettings.pointColor;
                            }
                        })
                        .duration(animationSpeed);

                    highBox
                        .attr({
                            x: -halfOfBar,
                            'fill-opacity': 0.8,
                            width: barWidth
                        })
                        .transition()
                        .attr({
                            y: scaleY(d.h),
                            height: scaleY(d.m) - scaleY(d.h),
                            fill: d.color || chartSettings.highBoxColor
                        })
                        .duration(animationSpeed);

                    lowBox
                        .attr({
                            x: -halfOfBar,
                            'fill-opacity': 0.8,
                            width: barWidth
                        })
                        .transition()
                        .attr({
                            y: scaleY(d.m),
                            height: scaleY(d.l) - scaleY(d.m),
                            fill: d.color || chartSettings.lowBoxColor
                        })
                        .duration(animationSpeed);

                    highestWhisker.select('.hor-high')
                        .attr({
                            x1: -halfOfBar,
                            x2: halfOfBar,
                            stroke: '#5c5b5b',
                            'stroke-width': 1.5
                        })
                        .transition()
                        .attr({
                            y1: scaleY(d.hh),
                            y2: scaleY(d.hh)
                        })
                        .duration(animationSpeed);

                    highestWhisker.select('.hor-low')
                        .attr({
                            x1: -halfOfBar,
                            x2: halfOfBar,
                            stroke: '#5c5b5b',
                            'stroke-width': 1.5
                        })
                        .transition()
                        .attr({
                            y1: scaleY(d.h),
                            y2: scaleY(d.h)
                        })
                        .duration(animationSpeed);

                    highestWhisker.select('.vertical')
                        .attr({
                            x1: 0,
                            x2: 0,
                            stroke: '#5c5b5b',
                            'stroke-width': 1.5
                        })
                        .transition()
                        .attr({
                            y1: scaleY(d.hh),
                            y2: scaleY(d.h)
                        })
                        .duration(animationSpeed);

                    lowestWhisker.select('.hor-high')
                        .attr({
                            x1: -halfOfBar,
                            x2: halfOfBar,
                            stroke: '#5c5b5b',
                            'stroke-width': 1.5
                        })
                        .transition()
                        .attr({
                            y1: scaleY(d.l),
                            y2: scaleY(d.l)
                        })
                        .duration(animationSpeed);

                    lowestWhisker.select('.hor-low')
                        .attr({
                            x1: -halfOfBar,
                            x2: halfOfBar,
                            stroke: '#5c5b5b',
                            'stroke-width': 1.5
                        })
                        .transition()
                        .attr({
                            y1: scaleY(d.ll),
                            y2: scaleY(d.ll)
                        })
                        .duration(animationSpeed);

                    lowestWhisker.select('.vertical')
                        .attr({
                            x1: 0,
                            x2: 0,
                            stroke: '#5c5b5b',
                            'stroke-width': 1.5
                        })
                        .transition()
                        .attr({
                            y1: scaleY(d.l),
                            y2: scaleY(d.ll)
                        })
                        .duration(animationSpeed);

                    return 'translate(' + [scaleX(d.x), 0] + ')';
                }
            });
        }

        function refreshAxisX (){
            var numberOfTicks = Math.floor(containerWidth / 30),
                axisX = self.d3.svg.axis()
                .scale(scaleX)
                .orient('bottom')
                .ticks(numberOfTicks < 15 ? numberOfTicks : 15);

            axisXLayer.call(axisX);

            axisXLayer.select('path').attr({ stroke: '#c5c5c7', 'stroke-width': 0.5, fill: 'none'});

            axisXLayer.selectAll('g')
                .select('text')
                .attr({
                    fill: '#c5c5c7',
                    'font-size': 10
                })
                .text(function(d){
                    return self.moment(d).format('YYYY');
                });

            axisXLayer.selectAll('g')
                .select('line')
                .attr({
                    x1: 0,
                    x2: 0,
                    y1: 6,
                    y2: 0,
                    stroke: '#e3e3e3',
                    'stroke-width': 1
                });
        }

        function refreshAxisY (){
            var numberOfTicks = Math.floor(containerHeight / 30),
                axisY = self.d3.svg.axis()
                .scale(scaleY)
                .orient('left')
                .ticks(numberOfTicks < 10 ? numberOfTicks : 10);

            axisYLayer.call(axisY);

            axisYLayer.select('path').attr({ stroke: '#c5c5c7', 'stroke-width': 0.5, fill: 'none'});

            axisYLayer.selectAll('g')
                .select('text')
                .attr({
                    fill: '#c5c5c7',
                    'font-size': 10
                });

            axisYLayer.selectAll('g')
                .select('line')
                .attr({
                    x1: -6,
                    x2: innerWidth,
                    y1: 0,
                    y2: 0,
                    stroke: '#e3e3e3',
                    'stroke-width': 0.5
                });
        }

    };

    BWChartDirectiveCtrl.prototype.drawSunburstChart = function(){
        /** Remove exceeded charts */
        this.clearCanvas('bw-sunburst-chart');

        var self = this,
            containerWidth = self.containerWidth,
            containerHeight = self.containerHeight,
            chartSettings = self.scope.settings.sunburstChart,
            padding = 1,
            layoutSize = self.d3.min([containerHeight, containerWidth]),
            offsetX = (containerWidth - layoutSize) / 2,
            offsetY = (containerHeight - layoutSize) / 2,
            outerRadius = (layoutSize - (padding * 2)) / 2,
            innerRadius = outerRadius * chartSettings.innerRadius * 0.1,
            middleRadius = innerRadius + ((outerRadius - innerRadius) * 0.5),
            renderingData = self._data_.getRenderingData(chartSettings),
            strokeWidth = getStrokeWidth(),
            /** DOM-Elements */
            wrapper, bgLayer, subLayer, mainLayer, zoomLayer;

        refreshChartStructure();

        refreshMainLayer();

        refreshSublayer();

        refreshBackground();

        function refreshChartStructure (){
            if(!self.layout.select('.bw-sunburst-chart').node()) {
                self.layout.append('g').classed('bw-sunburst-chart', true);
                self.layout.select('.bw-sunburst-chart').append('g').classed('bg-layer', true);
                self.layout.select('.bw-sunburst-chart').append('g').classed('sub-slices-layer', true);
                self.layout.select('.bw-sunburst-chart').append('g').classed('slices-layer', true);
                self.layout.select('.bw-sunburst-chart').append('g').classed('zoom-layer', true);
            }

            wrapper = self.layout.select('.bw-sunburst-chart');
            bgLayer = wrapper.select('.bg-layer');
            subLayer = wrapper.select('.sub-slices-layer');
            mainLayer = wrapper.select('.slices-layer');
            zoomLayer = wrapper.select('.zoom-layer');

            self.layout.attr({ transform: 'translate(' + [offsetX, offsetY] + ')' });
            wrapper.attr({ transform: 'translate(' + [(layoutSize * 0.5),(layoutSize * 0.5)] + ')' });

            cancelZoom();
        }

        function refreshMainLayer (){
            var arc = self.d3.svg.arc().innerRadius(innerRadius).outerRadius(middleRadius),
                slices = mainLayer.selectAll('.slice').data(renderingData.slices);

            slices.enter().append('g');

            slices.exit().remove();

            slices.attr({
                class: function(d) {
                    var slice = self.d3.select(this),
                        sliceId = 'slice-id-' + d.data.id;

                    if(!slice.select('path').node()){
                        slice.append('path').classed('slice-colored', true);
                        slice.append('path').classed('slice-interactive', true);
                    }

                    slice.select('.slice-colored')
                        .attr({
                            stroke: '#FFF',
                            'stroke-width': strokeWidth,
                            fill: d.data.color,
                            d: arc
                        });

                    slice.select('.slice-interactive')
                        .attr({
                            stroke: '#FFF',
                            'stroke-width': strokeWidth,
                            fill: '#000',
                            'fill-opacity': 0,
                            d: arc
                        })
                        .on('mouseover', function(){
                            self.d3.select(this).attr({
                                'fill-opacity': 0.3
                            });

                            subLayer.selectAll('.' + sliceId).select('.sub-slice-interactive').attr({
                                'fill-opacity': 0.2
                            });
                        })
                        .on('mouseout', function(){
                            self.d3.select(this).attr({
                                'fill-opacity': 0
                            });

                            subLayer.selectAll('.' + sliceId).select('.sub-slice-interactive').attr({
                                'fill-opacity': 0
                            });
                        })
                        .on('click', function(){
                            var event = self.d3.event;

                            chartSettings.onClusterClick(d.data.entity, {
                                x: event.clientX,
                                y: event.clientY
                            }, event, zoomSlice(d.data.entity));
                        });

                    return 'slice ' + sliceId;
                }
            });
        }

        function refreshSublayer (){
            var subLayerArc = self.d3.svg.arc().innerRadius(middleRadius).outerRadius(outerRadius),
                subLayerSlices = subLayer.selectAll('.sub-slice').data(renderingData.subSlices);

            subLayerSlices.enter().append('g');

            subLayerSlices.exit().remove();

            subLayerSlices.attr({
                class: function(d){
                    var slice = self.d3.select(this),
                        sliceId = 'sub-slice-id-' + d.data.id,
                        parentSliceId = 'slice-id-' + d.data.sliceId;

                    if(!slice.select('path').node()) {
                        slice.append('path').classed('sub-slice-substrate', true);
                        slice.append('path').classed('sub-slice-colored', true);
                        slice.append('path').classed('sub-slice-interactive', true);
                    }

                    slice.select('.sub-slice-colored').attr({
                        'stroke-width': strokeWidth,
                        stroke: '#FFF',
                        fill: d.data.color,
                        d: subLayerArc,
                        transform: 'scale(' + (chartSettings.simple ? 1 : d.data.y) + ')'
                    });

                    slice.select('.sub-slice-interactive')
                        .attr({
                            'stroke-width': strokeWidth,
                            fill: '#000',
                            stroke: '#FFF',
                            'fill-opacity': 0,
                            d: subLayerArc,
                            transform: 'scale(' + (chartSettings.simple ? 1 : d.data.y) + ')'
                        })
                        .on('mouseover', function(){
                            var event = self.d3.event;

                            self.d3.select(this).attr({
                                'fill-opacity': 0.2
                            });

                            mainLayer.select('.' + parentSliceId).select('.slice-interactive').attr({
                                'fill-opacity': 0.3
                            });

                            chartSettings.onMouseOver(d.data.entity, {
                                x: event.clientX,
                                y: event.clientY
                            }, event);
                        })
                        .on('mousemove', function(){
                            var event = self.d3.event;

                            chartSettings.onMouseMove(d.data.entity, {
                                x: event.clientX,
                                y: event.clientY
                            }, event);
                        })
                        .on('mouseout', function(){
                            var event = self.d3.event;

                            self.d3.select(this).attr({
                                'fill-opacity': 0
                            });

                            mainLayer.select('.' + parentSliceId).select('.slice-interactive').attr({
                                'fill-opacity': 0
                            });

                            chartSettings.onMouseOut(d.data.entity, {
                                x: event.clientX,
                                y: event.clientY
                            }, event);
                        })
                        .on('click', function(){
                            var event = self.d3.event;

                            chartSettings.onClick(d.data.entity, {
                                x: event.clientX,
                                y: event.clientY
                            }, event);
                        });

                    return 'sub-slice ' + sliceId + ' ' + parentSliceId;
                }
            });
        }

        function refreshBackground (){
            var bgArc = self.d3.svg.arc().innerRadius(innerRadius).outerRadius(outerRadius),
                bgSlices = bgLayer.selectAll('.bg-slice').data(renderingData.slices);

            bgSlices.enter().append('path').classed('bg-slice', true);

            bgSlices.exit().remove();

            bgSlices
                .attr({
                    'stroke-width': layoutSize * 0.005,
                    stroke: '#FFF',
                    fill: function(d){
                        return d.data.color;
                    },
                    'fill-opacity': 0.1,
                    d: bgArc
                });
        }

        function zoomSlice (selectedSlice){
            return function(){
                zoomLayer.append('g').classed('zoom-slices-layer', true);
                zoomLayer.append('g').classed('zoom-main-layer', true);
                zoomLayer.append('circle').classed('zoom-cancel-btn', true);

                var zoomedRenderingData = self._data_.getZoomedSliceRenderingData(selectedSlice, chartSettings),
                    mainCircleData = self.d3.layout.pie().value(function(d){
                        return 1;
                    })([selectedSlice.content]),
                    slices = zoomLayer.select('.zoom-slices-layer').selectAll('.zoom-slice').data(zoomedRenderingData),
                    mainCircle = zoomLayer.select('.zoom-main-layer').selectAll('.zoom-slice').data(mainCircleData),
                    cancelBtn = zoomLayer.select('.zoom-cancel-btn'),
                    arcOuter = self.d3.svg.arc().innerRadius(middleRadius).outerRadius(outerRadius),
                    arcInner = self.d3.svg.arc().innerRadius(innerRadius).outerRadius(middleRadius);

                slices.enter().append('g').classed('zoom-slice', true);

                slices.exit().remove();

                slices.attr({
                    stroke: function(d){
                        var slice = self.d3.select(this);

                        slice.append('path').classed('sub-slice-colored', true).attr({
                            'stroke-width': layoutSize * 0.005,
                            fill: d.data.color,
                            'fill-opacity': d.data.opacity,
                            d: arcOuter,
                            transform: 'scale(' + (chartSettings.simple ? 1 : d.data.y) + ')'
                        });

                        slice.append('path').classed('zoom-slice-interactive', true)
                            .attr({
                                'stroke-width': layoutSize * 0.005,
                                fill: '#000',
                                'fill-opacity': 0,
                                d: arcOuter,
                                transform: 'scale(' + (chartSettings.simple ? 1 : d.data.y) + ')'
                            })
                            .on('mouseover', function(){
                                var event = self.d3.event;

                                self.d3.select(this).attr({
                                    'fill-opacity': 0.2
                                });

                                chartSettings.onMouseOver(d.data.entity, {
                                    x: event.clientX,
                                    y: event.clientY
                                }, event);
                            })
                            .on('mousemove', function(){
                                var event = self.d3.event;

                                chartSettings.onMouseMove(d.data.entity, {
                                    x: event.clientX,
                                    y: event.clientY
                                }, event);
                            })
                            .on('mouseout', function(){
                                var event = self.d3.event;

                                self.d3.select(this).attr({
                                    'fill-opacity': 0
                                });

                                chartSettings.onMouseOut(d.data.entity, {
                                    x: event.clientX,
                                    y: event.clientY
                                }, event);
                            })
                            .on('click', function(){
                                var event = self.d3.event;

                                chartSettings.onClick(d.data.entity, {
                                    x: event.clientX,
                                    y: event.clientY
                                }, event);
                            });

                        return '#FFF';
                    }
                });

                mainCircle.enter().append('path');

                mainCircle.exit().remove();

                mainCircle.attr({
                    stroke: '#FFF',
                    'stroke-width': layoutSize * 0.005,
                    fill: function(d){
                        return d.data.color;
                    },
                    d: arcInner
                });

                cancelBtn.attr({
                    cx: 0,
                    cy: 0,
                    r: innerRadius,
                    fill: '#FFF',
                    'fill-opacity': 0.5
                });

                [bgLayer, subLayer, mainLayer].forEach(function(layer){
                    layer
                        .transition().duration(1000)
                        .attr({
                            transform: 'scale(0.2)'
                        });
                });

                zoomLayer
                    .transition().duration(1000)
                    .attr({
                        transform: 'scale(1) rotate(-320)',
                        'fill-opacity': 1
                    })
                    .each('end', function(){
                        cancelBtn.on('click', cancelZoom);
                    });
            }
        }

        function cancelZoom (){
            zoomLayer.select('.zoom-cancel-btn').on('click', null);

            zoomLayer
                .transition().duration(1000)
                .attr({
                    transform: 'scale(2.5) rotate(320)',
                    'fill-opacity': 0
                })
                .each('end', function(){
                    zoomLayer.selectAll('*').remove();
                });

            [bgLayer, subLayer, mainLayer].forEach(function(layer){
                layer
                    .transition().duration(1000)
                    .attr({
                        transform: 'scale(1)'
                    });
            });
        }

        function getStrokeWidth (){
            var defaultNumberOfSlices = 4,
                numberOfSlices = self.d3.max([renderingData.slices.length, renderingData.subSlices.length]),
                calculatedWidth = defaultNumberOfSlices / numberOfSlices;

            return calculatedWidth > 1 ? 1 : (calculatedWidth > 0.05 ? calculatedWidth : 0.05);
        }
    };

    BWChartDirectiveCtrl.prototype.drawHistogram = function(){
        /** Remove exceeded charts */
        this.clearCanvas('bw-histogram');

        var self = this,
            renderingData = self._data_.getRenderingData(),
            chartSettings = self.scope.settings.histogram,

            /** Sizes */
            containerWidth = self.containerWidth,
            containerHeight = self.containerHeight,
            padding = chartSettings.padding,
            innerWidth = containerWidth - (padding.left + padding.right),
            innerHeight = containerHeight - (padding.top + padding.bottom),
            axisXHeight = 20,
            axisYWidth = 35,
            mainLayerHeight = innerHeight - (chartSettings.showAxisX ? axisXHeight : 0),
            mainLayerWidth = innerWidth - (chartSettings.showAxisY ? axisYWidth : 0),
            offsetLeft = padding.left + (chartSettings.showAxisY ? axisYWidth : 0),
            axisLineColor = '#c0d0e0',
            axisTextColor = '#a8a7a7',
            axisLabelColor = '#000',

            /** Scales */
            scaleY = self.d3.scale.linear()
                .domain([0, 1])
                .range([0, mainLayerHeight]),

            /** DOM-elements */
            wrapper, backgroundLayer, mainLayer, interactiveLayer, axisXLayer, axisYLayer;

        refreshChartStructure();

        refreshBars();

        refreshBackground();

        refreshAxisX();

        refreshAxisY();

        function refreshChartStructure (){
            if(!self.layout.select('.bw-histogram').node()){
                self.layout.append('g').classed('bw-histogram', true);
                self.layout.select('.bw-histogram').append('g').classed('background-layer', true);
                self.layout.select('.bw-histogram').append('g').classed('axis-y-layer', true);
                self.layout.select('.bw-histogram').append('g').classed('main-layer', true);
                self.layout.select('.bw-histogram').append('g').classed('interactive-layer', true);
                self.layout.select('.bw-histogram').append('g').classed('axis-x-layer', true);
            }

            wrapper = self.layout.select('.bw-histogram');
            backgroundLayer = wrapper.select('.background-layer');
            mainLayer = wrapper.select('.main-layer');
            interactiveLayer = wrapper.select('.interactive-layer');
            axisXLayer = wrapper.select('.axis-x-layer');
            axisYLayer = wrapper.select('.axis-y-layer');

            self.layout.attr({ transform: 'translate(' + [0, 0] + ')' });
            wrapper.attr({ transform: 'translate(' + [0, 0] + ')' });
            backgroundLayer.attr({ transform: 'translate(' + [padding.left, padding.top] + ')' });
            mainLayer.attr({ transform: 'translate(' + [offsetLeft, padding.top] + ')' });
            interactiveLayer.attr({ transform: 'translate(' + [offsetLeft, padding.top] + ')' });
            axisXLayer.attr({ transform: 'translate(' + [offsetLeft, padding.top + mainLayerHeight] + ')' });
            axisYLayer.attr({ transform: 'translate(' + [offsetLeft, padding.top] + ')' });
        }

        function refreshBars (){
            var colorBars = mainLayer.selectAll('.histogram-bar').data(renderingData),
                interactiveBars = interactiveLayer.selectAll('.interactive-bar').data(renderingData),
                barWidth = mainLayerWidth / (renderingData.length || 1);

            colorBars.enter().append('rect')
                .classed('histogram-bar', true)
                .attr({
                    x: function(d, i){
                        return barWidth * i;
                    },
                    y: function(d){
                        d._randomY = scaleY(Math.random());
                        return mainLayerHeight - d._randomY;
                    },
                    width: function(d){
                        return barWidth;
                    },
                    height: function(d){
                        return d._randomY;
                    },
                    fill: function(d){
                        return d.color;
                    },
                    stroke: '#fff',
                    'stroke-width': 1
                });

            colorBars.exit().remove();

            colorBars
                .transition()
                .attr({
                    x: function(d, i){
                        return barWidth * i;
                    },
                    y: function(d){
                        return mainLayerHeight - scaleY(d.y);
                    },
                    width: function(d){
                        return barWidth;
                    },
                    height: function(d){
                        return scaleY(d.y);
                    },
                    fill: function(d){
                        return d.color;
                    }
                })
                .duration(self.scope.settings.animationSpeed);

            interactiveBars.enter().append('rect').classed('interactive-bar', true);

            interactiveBars.exit().remove();

            interactiveBars
                .transition()
                .attr({
                    x: function(d, i){
                        return barWidth * i;
                    },
                    y: function(d){
                        return mainLayerHeight - scaleY(d.y);
                    },
                    width: function(d){
                        return barWidth;
                    },
                    height: function(d){
                        return scaleY(d.y);
                    },
                    fill: function(d){
                        return '#000';
                    },
                    'fill-opacity': 0
                })
                .duration(self.scope.settings.animationSpeed);

            interactiveBars
                .on('mouseover', function(d){
                    var event = self.d3.event;

                    self.d3.select(this).attr({ 'fill-opacity': 0.1 });

                    toggleCallback('onMouseOver' , d, {
                        x: event.clientX,
                        y: event.clientY
                    }, event);
                })
                .on('mouseout', function(d){
                    var event = self.d3.event;

                    self.d3.select(this).attr({ 'fill-opacity': 0 });

                    toggleCallback('onMouseOut' , d, {
                        x: event.clientX,
                        y: event.clientY
                    }, event);
                })
                .on('mousemove', function(d){
                    var event = self.d3.event;

                    toggleCallback('onMouseMove' , d, {
                        x: event.clientX,
                        y: event.clientY
                    }, event);
                })
                .on('click', function(d){
                    var event = self.d3.event;

                    toggleCallback('onClick' , d, {
                        x: event.clientX,
                        y: event.clientY
                    }, event);
                });
        }

        function refreshAxisX (){
            var axisXData = (chartSettings.showAxisX && self._data_.content.axisXLabels && self._data_.content.axisXLabels.length && self._data_.content.axisXLabels) || [],
                axisXTicks = axisXLayer.selectAll('.axis-x-tick').data(axisXData),
                tickWidth = mainLayerWidth / (renderingData.length || 1),
                fontSize = chartSettings.fontSize;

            axisXTicks.enter().append('g').classed('axis-x-tick', true);

            axisXTicks.exit().remove();

            axisXTicks.attr({
                transform: function(d, i){
                    var tick = self.d3.select(this);

                    if(!tick.select('line').node()) {
                        tick.append('line');
                        tick.append('text');
                    }

                    return 'translate(' + [tickWidth * i, 0] + ')';
                }
            });

            axisXTicks.select('line')
                .attr({
                    x1: 0,
                    x2: 0,
                    y1: 0,
                    y2: 6,
                    stroke: axisLineColor,
                    'stroke-width': 1
                });

            axisXTicks.select('text')
                .attr({
                    x: 0,
                    y: 6 + fontSize,
                    'text-anchor': 'middle',
                    'font-size': fontSize,
                    fill: axisTextColor
                })
                .text(function(d){
                    return d;
                });
        }

        function refreshAxisY (){
                var numberOfTicks = Math.floor(containerHeight / 30),
                    scale = self.d3.scale.linear()
                        .domain(self._data_.getRanges().y)
                        .range([mainLayerHeight, 0])
                        .nice(),
                    axisY = self.d3.svg.axis()
                        .scale(scale)
                        .orient('left')
                        .ticks(numberOfTicks < 15 ? numberOfTicks : 15);

            //var numberOfTicks = Math.floor(containerHeight / 30),
            //    axisX = self.d3.svg.axis()
            //        .scale(renderingData.scaleX)
            //        .orient('bottom')
            //        .ticks(numberOfTicks < 15 ? numberOfTicks : 15

                axisYLayer.call(axisY);

                axisYLayer.select('path').attr({ fill: 'none'});

                axisYLayer.selectAll('g')
                    .select('text')
                    .attr({
                        fill: axisTextColor,
                        'font-size': 10
                    })
                    .text(function(d){
                        return chartSettings.showAxisY ? self.numbFilter(d) : '';
                    });

                axisYLayer.selectAll('g')
                    .select('line')
                    .attr({
                        x1: chartSettings.showAxisY ? -6 : 0,
                        x2: mainLayerWidth,
                        y1: 0,
                        y2: 0,
                        stroke: axisLineColor,
                        'stroke-width': function (d, i){
                            return i ? 0.5 : 1;
                        }
                    });
        }

        function refreshBackground (){
            var fontSize = chartSettings.fontSize;

            if(!backgroundLayer.select('.axis-x-label').node()){
                backgroundLayer.append('text').classed('axis-x-label', true);
                backgroundLayer.append('text').classed('axis-y-label', true);
            }

            backgroundLayer.select('.axis-x-label')
                .attr({
                    transform: 'translate(' + [innerWidth / 2, innerHeight] + ')',
                    'font-size': fontSize,
                    'text-anchor': 'middle',
                    fill: axisLabelColor
                })
                .text(chartSettings.showAxisX ? chartSettings.axisXName : '');

            backgroundLayer.select('.axis-y-label')
                .attr({
                    transform: 'translate(' + [0, innerHeight / 2] + ') rotate(-90)',
                    'font-size': fontSize,
                    'text-anchor': 'middle',
                    fill: axisLabelColor
                })
                .text(chartSettings.showAxisY ? chartSettings.axisYName : '');
        }

        function toggleCallback (callbackName, data, coords, event){
            chartSettings[callbackName](data.entity, coords, event);
        }
    };

    BWChartDirectiveCtrl.prototype.drawTreemap = function(){
        /** Remove exceeded charts */
        this.clearCanvas('bw-treemap');

        var self = this,
            containerWidth = self.containerWidth,
            containerHeight = self.containerHeight,
            chartSettings = self.scope.settings.treemap,
            padding = chartSettings.padding,
            innerWidth = containerWidth - (padding.left + padding.right),
            innerHeight = containerHeight - (padding.top + padding.bottom),
            renderingData = self._data_.getRenderingData(chartSettings, {x: innerWidth, y: innerHeight}),
            wrapper;

        refreshStructure();

        refreshNodes();

        function refreshStructure(){
            if(!self.layout.select('.bw-treemap').node()){
                self.layout.append('g').classed('bw-treemap', true);
            }
            wrapper = self.layout.select('.bw-treemap');
            self.layout.attr({ transform: 'translate(' + [0, 0] + ')'});
            wrapper.attr({ transform: 'translate(' + [padding.left, padding.top] + ')'});
        }

        function refreshNodes(){
            var nodes = wrapper.selectAll('.treemap-node').data(renderingData);

            nodes.enter().append('g').classed('treemap-node', true);

            nodes.exit().remove();

            nodes.attr({
                transform: function(d){
                    var node = self.d3.select(this);
                    if(!node.select('rect').node()){
                        node.append('rect').classed('node-color', true);
                        node.append('text').classed('node-label', true);
                        node.append('rect').classed('node-interactive', true);
                    }
                    return 'translate(' + [d.x, d.y] + ')';
                }
            });

            nodes.select('.node-color')
                .attr({
                    stroke: function(d){
                        return d.entity && d.entity.isSelected ? '#00aeef' : '#FFF';
                    },
                    'stroke-width': 2
                })
                .transition()
                .attr({
                    width: function(d){
                        return d.dx;
                    },
                    height: function(d){
                        return d.dy;
                    },
                    fill: function(d){
                        return d.color;
                    }
                })
                .duration(self.scope.settings.animationSpeed);

            nodes.select('.node-label')
                .attr({
                    'font-size': chartSettings.fontSize,
                    'fill': function (d) {
                        return d.fontColor;
                    },
                    transform: 'translate(' + [10, chartSettings.fontSize * 1.5] + ')'
                })
                .text(function(d){
                    var ninjaLabel = wrapper.append('text')
                        .attr({'font-size': chartSettings.fontSize, fill: '#000'})
                        .text(d.label),
                        ninjaLabelWidth = ninjaLabel.node().getBBox().width,
                        ninjaLabelHeight = chartSettings.fontSize * 2;
                    ninjaLabel.remove();

                    return (d.dx - 20) > ninjaLabelWidth && d.dy > ninjaLabelHeight ? d.label : '';
                });

            nodes.select('.node-interactive')
                .attr({
                    width: function(d){
                        return d.dx;
                    },
                    height: function(d){
                        return d.dy;
                    },
                    fill: '#000',
                    'fill-opacity': 0,
                    stroke: 'rgba(0,0,0,0)'
                })
                .on('mouseover', function(d){
                    var event = self.d3.event,
                        node = self.d3.select(this);

                    node.attr({ 'fill-opacity': 0.2 });

                    chartSettings.onMouseOver(d.entity, {
                        x: event.clientX,
                        y: event.clientY
                    }, event, d.color);
                })
                .on('mousemove', function(d){
                    var event = self.d3.event;

                    chartSettings.onMouseMove(d.entity, {
                        x: event.clientX,
                        y: event.clientY
                    }, event);
                })
                .on('mouseout', function(d){
                    var event = self.d3.event,
                        node = self.d3.select(this);

                    node.attr({ 'fill-opacity': 0 });

                    chartSettings.onMouseOut(d.entity, {
                        x: event.clientX,
                        y: event.clientY
                    }, event);
                })
                .on('click', function(d){
                    var event = self.d3.event;

                    chartSettings.onClick(d.entity, {
                        x: event.clientX,
                        y: event.clientY
                    }, event, toggleNode(d.entity, self.d3.select(this)));
                });
        }

        function toggleNode(d, node){
            return function(){
                d[d.isSelected ? 'deselect' : 'select']();
                refreshNodes();
            }
        }
    };

    BWChartDirectiveCtrl.prototype.drawBubbleChart = function(){
        /** Remove exceeded charts */
        this.clearCanvas('bw-bubble-chart');

        var self = this,
            containerWidth = self.containerWidth,
            containerHeight = self.containerHeight,
            chartSettings = self.scope.settings.bubbleChart,
            padding = chartSettings.padding,
            innerWidth = containerWidth - (padding.left + padding.right),
            innerHeight = containerHeight - (padding.bottom + padding.top),
            renderingData = self._data_.getRenderingData(chartSettings, {x: innerWidth, y: innerHeight}),

            /** DOM-Elements */
            wrapper, bgLayer, mainLayer, axisXLayer, axisYLayer;

        refreshChartStructure();

        refreshMainLayer();

        refreshAxisX();

        refreshAxisY();

        function refreshChartStructure (){
            if(!self.layout.select('.bw-bubble-chart').node()) {
                self.layout.append('g').classed('bw-bubble-chart', true);
                self.layout.select('.bw-bubble-chart').append('g').classed('bg-layer', true);
                self.layout.select('.bw-bubble-chart').append('g').classed('axis-x-layer', true);
                self.layout.select('.bw-bubble-chart').append('g').classed('axis-y-layer', true);
                self.layout.select('.bw-bubble-chart').append('g').classed('main-layer', true);
            }

            wrapper = self.layout.select('.bw-bubble-chart');
            bgLayer = wrapper.select('.bg-layer');
            mainLayer = wrapper.select('.main-layer');
            axisXLayer = wrapper.select('.axis-x-layer');
            axisYLayer = wrapper.select('.axis-y-layer');

            self.layout.attr({ transform: 'translate(' + [0, 0] + ')' });
            bgLayer.attr({ transform: 'translate(' + [padding.left, padding.top] + ')' });
            mainLayer.attr({ transform: 'translate(' + [padding.left, padding.top] + ')' }).style({ 'display': 'block' });
            axisXLayer.attr({ transform: 'translate(' + [padding.left, padding.top + innerHeight] + ')' });
            axisYLayer.attr({ transform: 'translate(' + [padding.left, padding.top] + ')' });
        }

        function refreshMainLayer (){
            var bubbles = mainLayer.selectAll('.bw-bubble').data(renderingData.bubbles);

            bubbles.enter().append('circle').classed('bw-bubble', true);

            bubbles.exit().remove();

            bubbles.attr({
                fill: function (d) {
                    return d.color;
                },
                'fill-opacity': 0.7,
                stroke: function (d) {
                    return d.color;
                },
                'stroke-width': 2
            })
                .transition()
                .attr({
                    cx: function (d) {
                        return d.x;
                    },
                    cy: function (d) {
                        return d.y;
                    },
                    r: function (d) {
                        return d.r;
                    }
                })
                .duration(self.scope.settings.animationSpeed);

            bubbles
                .on('mouseover', onMouseOver)
                .on('mousemove', onMouseMove)
                .on('mouseout', onMouseOut)
                .on('click', onClick);

            function onMouseOver (d){
                var event = self.d3.event;

                chartSettings.onMouseOver(d.entity, {
                    x: event.clientX,
                    y: event.clientY
                }, event, d.color);
            }

            function onMouseMove (d){
                var event = self.d3.event;

                chartSettings.onMouseMove(d.entity, {
                    x: event.clientX,
                    y: event.clientY
                }, event);
            }

            function onMouseOut (d){
                var event = self.d3.event;

                chartSettings.onMouseOut(d.entity, {
                    x: event.clientX,
                    y: event.clientY
                }, event);
            }

            function onClick (d){
                var event = self.d3.event;

                chartSettings.onClick(d.entity, {
                    x: event.clientX,
                    y: event.clientY
                }, event);
            }
        }

        function refreshAxisX (){
            var numberOfTicks = Math.floor(containerWidth / 30),
                axisX = self.d3.svg.axis()
                .scale(renderingData.scaleX)
                .orient('bottom')
                .ticks(numberOfTicks < 15 ? numberOfTicks : 15);

            axisXLayer.call(axisX);

            axisXLayer.select('path').attr({ stroke: '#c5c5c7', 'stroke-width': 0.5, fill: 'none'});

            axisXLayer.selectAll('g')
                .select('text')
                .attr({
                    fill: '#c5c5c7',
                    'font-size': 10
                });

            axisXLayer.selectAll('g')
                .select('line')
                .attr({
                    x1: 0,
                    x2: 0,
                    y1: 6,
                    y2: 0,
                    stroke: '#e3e3e3',
                    'stroke-width': 1
                });
        }

        function refreshAxisY (){
            var numberOfTicks = Math.floor(containerHeight / 30),
                axisY = self.d3.svg.axis()
                .scale(renderingData.scaleY)
                .orient('left')
                .ticks(numberOfTicks < 10 ? numberOfTicks : 10);

            axisYLayer.call(axisY);

            axisYLayer.select('path').attr({ stroke: '#c5c5c7', 'stroke-width': 0.5, fill: 'none'});

            axisYLayer.selectAll('g')
                .select('text')
                .attr({
                    fill: '#c5c5c7',
                    'font-size': 10
                });

            axisYLayer.selectAll('g')
                .select('line')
                .attr({
                    x1: -6,
                    x2: innerWidth,
                    y1: 0,
                    y2: 0,
                    stroke: '#e3e3e3',
                    'stroke-width': 0.5
                });
        }
    };

    BWChartDirectiveCtrl.prototype.drawSankeyChart = function(){
        /** Remove exceeded charts */
        this.clearCanvas('bw-sankey-chart');

        var self = this,
            chartSettings = self.scope.settings.sankeyChart,
            containerWidth = self.containerWidth,
            containerHeight = self.containerHeight,
            renderingData = self._data_.getRenderingData(chartSettings, {
                x: containerWidth,
                y: containerHeight
            }),
            wrapper, linksLayer, nodesLayer;

        refreshStructure();

        refreshNodes();

        refreshLinks();

        function refreshStructure (){
            if(!self.layout.select('.bw-sankey-chart').node()) {
                self.layout.append('g').classed('bw-sankey-chart', true);
                self.layout.select('.bw-sankey-chart').append('g').classed('bw-sankey-nodes-layer', true);
                self.layout.select('.bw-sankey-chart').append('g').classed('bw-sankey-links-layer', true);
            }

            wrapper = self.layout.select('.bw-sankey-chart');
            nodesLayer = wrapper.select('.bw-sankey-nodes-layer');
            linksLayer = wrapper.select('.bw-sankey-links-layer');

            self.layout.attr({ transform: 'translate(' + [0, 0] + ')' });
        }

        function refreshNodes (){
            var nodes = nodesLayer.selectAll('.sankey-node').data(renderingData.nodes);

            nodes.enter().append('rect');

            nodes.exit().remove();

            nodes
                .attr({
                    class: function(d){
                        return 'sankey-node sankey-item-id-' + d.index + ' ' + (d.entity ? 'interactive' : '');
                    },
                    transform: function(d){
                        return 'translate(' + [d.x, d.y] + ')';
                    },
                    fill: function(d){
                        return d.color;
                    },
                    width:  function(d){
                        return d.dx;
                    },
                    height:  function(d){
                        return d.dy;
                    }
                });

            nodesLayer.selectAll('.interactive')
                .on('mouseover', onMouseOver)
                .on('mousemove', onMouseMove)
                .on('mouseout', onMouseOut)
                .on('click', onClick);
        }

        function refreshLinks (){

            var links = linksLayer.selectAll('.sankey-link').data(renderingData.links);

            links.enter().append('path');

            links.exit().remove();

            links
                .attr({
                    class: function(d){
                        return 'sankey-link sankey-item-id-' + d.index;
                    },
                    d: function(d){
                        return d.path;
                    },
                    'stroke-width': function(d){
                        return d.width;
                    },
                    fill: 'none',
                    stroke: chartSettings.linkColor
                });
        }

        function onMouseOver (d){
            var event = self.d3.event;

            chartSettings.onMouseOver(d.entity, {
                x: event.clientX,
                y: event.clientY
            }, event, highlightLink(d));
        }

        function onMouseMove (d){
            var event = self.d3.event;

            chartSettings.onMouseMove(d.entity, {
                x: event.clientX,
                y: event.clientY
            }, event);
        }

        function onMouseOut (d){
            var event = self.d3.event;

            dehighlightLink(d);

            chartSettings.onMouseOut(d.entity, {
                x: event.clientX,
                y: event.clientY
            }, event);
        }

        function onClick (d){
            var event = self.d3.event;

            chartSettings.onClick(d.entity, {
                x: event.clientX,
                y: event.clientY
            }, event);
        }

        function highlightLink (d){
            return function(){
                var linkId = '.sankey-item-id-' + d.index,
                    link = linksLayer.select(linkId),
                    node = nodesLayer.select(linkId);

                link.attr({
                    stroke: self.colorsService.getDarkerColor(chartSettings.linkColor, 2)
                });

                node.attr({
                    fill: self.colorsService.getDarkerColor(d.color, 3)
                });
            }
        }

        function dehighlightLink (d){
            var linkId = '.sankey-item-id-' + d.index,
                link = linksLayer.select(linkId),
                node = nodesLayer.select(linkId);

            link.attr({
                stroke: chartSettings.linkColor
            });

            node.attr({
                fill: d.color
            });
        }
    };

    BWChartDirectiveCtrl.prototype.drawActivitiesChart = function(){
        /** Remove exceeded charts */
        this.clearCanvas('bw-activities-chart');

        var self = this,
            chartSettings = self.scope.settings.activitiesChart,
            ranges = self._data_.getRanges(chartSettings),
            renderingData = self._data_.getRenderingData(chartSettings),
            containerWidth = self.containerWidth,
            containerHeight = self.containerHeight,
            padding = calculatePadding(),
            innerWidth = containerWidth - (padding.left + padding.right),
            innerHeight = containerHeight - (padding.top + padding.bottom),
            clusterWidth = innerWidth / renderingData.axisXLabels.length,
            boxWidth = clusterWidth * 0.66,
            boxOffset = (clusterWidth - boxWidth) / 2,
            scaleY = self.d3.scale.linear()
                .domain(ranges)
                .range([innerHeight, 0])
                .nice(),

            /** DOM-Elements */
            wrapper, bgLayer, mainLayer, axisXLayer, axisYLayer;

        refreshStructure();

        refreshMainLayers();

        refreshAxisX();

        refreshAxisY();

        function refreshStructure (){
            if(!self.layout.select('.bw-activities-chart').node()){
                self.layout.append('g').classed('bw-activities-chart', true);
                self.layout.select('.bw-activities-chart').append('g').classed('bg-layer', true);
                self.layout.select('.bw-activities-chart').append('g').classed('main-layer', true);
                self.layout.select('.bw-activities-chart').append('g').classed('axis-x-layer', true);
                self.layout.select('.bw-activities-chart').append('g').classed('axis-y-layer', true);
            }

            wrapper = self.layout.select('.bw-activities-chart');
            bgLayer = wrapper.select('.bg-layer');
            mainLayer = wrapper.select('.main-layer');
            axisXLayer = wrapper.select('.axis-x-layer');
            axisYLayer = wrapper.select('.axis-y-layer');

            self.layout.attr({ transform: 'translate(' + [0, 0] + ')' });
            wrapper.attr({ transform: 'translate(' + [0, 0] + ')' });
            bgLayer.attr({ transform: 'translate(' + [0, 0] + ')' });
            mainLayer.attr({ transform: 'translate(' + [padding.left, padding.top] + ')' });
            axisXLayer.attr({ transform: 'translate(' + [padding.left, (padding.top + innerHeight)] + ')' });
            axisYLayer.attr({ transform: 'translate(' + [padding.left, padding.top] + ')' });
        }

        function refreshMainLayers (){
            var layers = mainLayer.selectAll('.acitvities-chart-layer').data(renderingData.layers);

            layers.enter().append('g').classed('acitvities-chart-layer', true);

            layers.exit().remove();

            layers.attr({
                fill: function(d){
                    (d.showAsDiagram ? refreshLinearDiagram : refreshClusters)(d, self.d3.select(this), d.color);
                    return d.color;
                }
            });
        }

        function refreshClusters (datum, layer){
            layer.select('polyline').remove();

            var clusters = layer.selectAll('.activities-chart-cluster').data(datum.clusters);

            clusters.enter().append('g').classed('activities-chart-cluster', true);

            clusters.exit().remove();

            clusters.attr({
                transform: function(d, i){
                    var cluster = self.d3.select(this);

                    if(!cluster.select('.activities-chart-box').node()){
                        cluster.append('g').classed('activities-chart-box', true);
                        cluster.append('g').classed('activities-chart-points', true);
                    }
                    refreshBoxLayer(d, cluster.select('.activities-chart-box'));
                    refreshPointsLayer(d, cluster.select('.activities-chart-points'));

                    return 'translate(' + [i * clusterWidth, 0] + ')';
                }
            });
        }

        function refreshBoxLayer (datum, layer){
            var y1 = scaleY(datum.q3),
                y2 = scaleY(datum.q1),
                dy = y2 - y1;

            if(!layer.select('rect').node()){
                layer.append('rect');
                layer.append('line').classed('border-top', true);
                layer.append('line').classed('border-bottom', true);
            }

            layer.select('rect')
                .attr({
                    y: y1,
                    x: boxOffset,
                    width: boxWidth,
                    height: dy,
                    fill: chartSettings.boxColor
                });

            layer.select('.border-top')
                .attr({
                    x1: boxOffset,
                    x2: boxOffset + boxWidth,
                    y1: y1,
                    y2: y1,
                    stroke: '#bbbbbb'
                });

            layer.select('.border-bottom')
                .attr({
                    x1: boxOffset,
                    x2: boxOffset + boxWidth,
                    y1: y2,
                    y2: y2,
                    stroke: '#bbbbbb'
                });
        }

        function refreshPointsLayer (datum, layer){
            var points = layer.selectAll('.activities-chart-point').data(datum.items);

            points.enter().append('circle').classed('activities-chart-point', true);

            points.exit().remove();

            points.attr({
                cx: clusterWidth / 2,
                cy: function(d){
                    return scaleY(d.y);
                },
                r: chartSettings.pointRadius
            });
        }

        function refreshLinearDiagram (datum, layer, color){
            layer.selectAll('*').remove();

            layer.append('polyline')
                .attr({
                    transform: 'translate(' + [clusterWidth / 2, 0] + ')',
                    points: datum.linearDiagramPoints.map(function(point){
                        return (point.x * clusterWidth) + ',' + (scaleY(point.y));
                    }).join(' '),
                    fill: 'none',
                    'stroke-width': chartSettings.strokeWidth,
                    stroke: color
                });
        }

        function refreshAxisX (){
            if(!axisXLayer.select('.ticks').node()){
                axisXLayer.append('g').classed('ticks', true);
                axisXLayer.append('line').classed('border-bottom', true);
            }

            var ticks = axisXLayer.select('.ticks').selectAll('.tick').data(renderingData.axisXLabels);

            ticks.enter().append('g').classed('tick', true);

            ticks.exit().remove();

            ticks.attr({
                transform: function(d, i){
                    var tick = self.d3.select(this);

                    if(!tick.select('line').node()){
                        tick.append('g').classed('text-wrapper', true);
                        tick.select('.text-wrapper').append('text').text(d);
                        tick.append('line');
                    }

                    tick.select('.text-wrapper')
                        .attr({
                            transform: 'translate(' + [(clusterWidth / 2) + (chartSettings.fontSize / 2) , 2] + ')'
                        });

                    tick.select('.text-wrapper')
                        .select('text')
                        .attr({
                            transform: 'rotate(-90)',
                            'text-anchor': 'end',
                            fill: '#c5c5c7',
                            'font-size': chartSettings.fontSize
                        });

                    tick.select('line')
                        .attr({
                            x1: clusterWidth,
                            x2: clusterWidth,
                            y1: 0,
                            y2: 6,
                            stroke: '#c5c5c7',
                            'stroke-width': 0.8

                        });

                    return 'translate(' + [clusterWidth * i, 0] + ')';
                }

            });

            axisXLayer.select('.border-bottom')
                .attr({
                    x1: 0,
                    x2: innerWidth,
                    y1: 0,
                    y2: 0,
                    stroke: '#c5c5c7',
                    'stroke-width': 0.8
                });
        }

        function refreshAxisY (){
            var numberOfTicks = Math.floor(containerHeight / 30),
                axisY = self.d3.svg.axis()
                    .scale(scaleY)
                    .orient('left')
                    .ticks(numberOfTicks < 10 ? numberOfTicks : 10);

            axisYLayer.call(axisY);

            axisYLayer.select('path').attr({ stroke: '#c5c5c7', 'stroke-width': 0.8, fill: 'none'});

            axisYLayer.selectAll('g')
                .select('text')
                .attr({
                    fill: '#c5c5c7',
                    'font-size': chartSettings.fontSize
                });

            axisYLayer.selectAll('g')
                .select('line')
                .attr({
                    x1: -6,
                    x2: 0,
                    y1: 0,
                    y2: 0,
                    stroke: '#e3e3e3',
                    'stroke-width': 0.5
                });

        }

        function calculatePadding (){
            var halfSize = chartSettings.fontSize / 2;

            return {
                top: halfSize,
                right: halfSize,
                bottom: getLongestLabelWidth(renderingData.axisXLabels) + (chartSettings.axisXName ? chartSettings.fontSize + halfSize : 0),
                left: getLongestLabelWidth(ranges.map(function(val){
                    return val.toFixed(2);
                })) + (chartSettings.axisYName ? chartSettings.fontSize + halfSize : 0)
            };
        }

        function getLongestLabelWidth (setOfLabels){
            var ninjaNode = self.layout.append('g').classed('ninja-node', true),
                ninjaLabels = ninjaNode.selectAll('text').data(setOfLabels)
                    .enter().append('text')
                    .attr({ 'font-size': chartSettings.fontSize })
                    .text(function(d){ return d; }),
                maxSize = 0;

            ninjaLabels.attr({
                fill: function(){
                    maxSize = self.d3.max([ maxSize, this.clientWidth ]);
                    return '#ffffff';
                }
            });

            ninjaNode.remove();

            return maxSize + 4;
        }
    };

    BWChartDirectiveCtrl.prototype.showMessage = function(text){
        var self = this;

        self.msg.style({
            width: self.containerWidth + 'px',
            height: self.containerHeight + 'px',
            display: 'block'
        }).select('span').style({
            'line-height': self.containerHeight + 'px'
        }).html(text);
    };

    //BWChartDirectiveCtrl.prototype.drawAxisX = function(axisContainer, axisData, width, height){
    //    var self = this,
    //        numberOfTicks = Math.floor(width / 30),
    //        axis = self.d3.svg.axis()
    //            .scale()
    //
    //    //var numberOfTicks = Math.floor(containerWidth / 30),
    //    //    axisX = self.d3.svg.axis()
    //    //        .scale(scaleX)
    //    //        .orient('bottom')
    //    //        .ticks(numberOfTicks < 15 ? numberOfTicks : 15);
    //    //
    //    //axisXLayer.call(axisX);
    //    //
    //    //axisXLayer.select('path').attr({ stroke: '#c5c5c7', 'stroke-width': 0.5, fill: 'none'});
    //    //
    //    //axisXLayer.selectAll('g')
    //    //    .select('text')
    //    //    .attr({
    //    //        fill: '#c5c5c7',
    //    //        'font-size': 10
    //    //    })
    //    //    .text(function(d){
    //    //        return self.moment(d).format('YYYY');
    //    //    });
    //    //
    //    //axisXLayer.selectAll('g')
    //    //    .select('line')
    //    //    .attr({
    //    //        x1: 0,
    //    //        x2: 0,
    //    //        y1: 6,
    //    //        y2: 0,
    //    //        stroke: '#e3e3e3',
    //    //        'stroke-width': 1
    //    //    });
    //
    //
    //
    //}

    //function getChartPadding(){
    //
    //
    //}

}(angular.module('bw.chart')));
