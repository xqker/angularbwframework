(function(module){

    'use strict';

    module.service('BWChartService', [
        'AsyncAPI', 'BWCollectionService', 'BWChartEnums', 'BWColorsService', 'd3', 'moment', 'AccessPointService',
        function( AsyncAPI, BWCollectionService, BWChartEnums, BWColorsService, d3, moment, AccessPointService){
            var bwChartService = this,
                d3Palette = BWColorsService.getStdPalette(),
                chartCounter = 0;

            Object.defineProperties(bwChartService, {
                createNew: {
                    value: function(){
                        return new BWChartEntity(chartCounter++);
                    }
                },
                getChartTypes: {
                    value: function(){
                        return BWChartEnums.getChartTypes(0);
                    }
                }
            });

            function BWChartEntity (id){
                var bwChartEntity = this;
                Object.defineProperties(bwChartEntity, {
                    id: {
                        value: '#bw-chart-' + id
                    },
                    _api_: {
                        value: AsyncAPI.get({})
                    }
                });
            }

            BWChartEntity.prototype.refreshData = function(data){
                this._api_.applyFn('refreshData', data);
            };

            BWChartEntity.prototype.refreshView = function(){
                this._api_.applyFn('refreshView');
            };

            BWChartEntity.prototype.clearChart = function(){
                this._api_.applyFn('clearChart');
            };

            BWChartEntity.prototype.getChartSettings = function(){
                return this._api_.applyFn('getChartSettings');
            };

            BWChartEntity.prototype.toggleWaiting = function(state){
                this._api_.applyFn('toggleWaiting', state);
            };

            BWChartEntity.prototype.zoomToPoints = function(points){
                this._api_.applyFn('zoomToPoints', points);
            };

            BWChartEntity.prototype.resetZoom = function(){
                this._api_.applyFn('resetZoom');
            };

            BWChartEntity.prototype.getZoomState = function(){
                return this._api_.applyFn('getZoomState');
            };

            BWChartEntity.prototype.toggleItems = function(indexingProperty, indexes, state){
                return this._api_.applyFn('toggleItems', indexingProperty, indexes, state);
            };

            BWChartEntity.prototype.getRenderingData = function(settings){
                return this._api_.applyFn('getRenderingData', settings);
            };

            //
            //BWChartEntity.prototype.getChartRanges = function(){
            //    return this._api_.applyFn('getChartRanges');
            //};

            /** Collection factories */
            BWChartEntity.prototype.getNewRoundChartCollection = function(data, parentItem){
                var newRoundChartCircle = (parentItem ?
                    parentItem.createNestedCollection :
                    BWCollectionService.createNew
                )(data, {type: BWChartEnums.roundChart});

                newRoundChartCircle.getRenderingData = function(){
                    var roundChartRenderingData = d3.layout.pie().value(function(d){
                        return d.content.value;
                    }).sort(null)(newRoundChartCircle.getItems());

                    roundChartRenderingData.forEach(function(datum){
                        datum.color = datum.data.content.color;
                    });

                    return roundChartRenderingData;
                };

                return newRoundChartCircle;
            };

            BWChartEntity.prototype.getNewScatterPlotChartCollection = function(data, parentItem) {
                var self = this,
                    newLayerCollection = (parentItem ?
                        parentItem.createNestedCollection :
                        BWCollectionService.createNew
                    )(data, {type: BWChartEnums.scatterPlotChart, multiselect: true});

                newLayerCollection.getItems().forEach(function(layer){
                    self.getNewScatterPlotChartLayer(layer.content.items, layer);
                });

                newLayerCollection.getRanges = function(){
                    var ranges = {
                            x: [0, 0],
                            y: [0, 0]
                        };

                    newLayerCollection.getItems().forEach(function(layer){
                        var _points = layer.nestedCollection.getItems(),
                            xVals = _points.map(function(point){
                                return parseFloat(point.content.x);
                            }),
                            yVals = _points.map(function(point){
                                return parseFloat(point.content.y);
                            });

                        ranges.x[0] = d3.min([ranges.x[0]].concat(xVals));
                        ranges.x[1] = d3.max([ranges.x[1]].concat(xVals));
                        ranges.y[0] = d3.min([ranges.y[0]].concat(yVals));
                        ranges.y[1] = d3.max([ranges.y[1]].concat(yVals));
                    });

                    return ranges;
                };

                newLayerCollection.getPointsData = function(){
                    var ranges = newLayerCollection.getRanges(),
                        scales = {
                            x: d3.scale.linear()
                                .domain(ranges.x)
                                .range([0, 1]),
                            y: d3.scale.linear()
                                .domain(ranges.y)
                                .range([0, 1])
                        },
                        layers = newLayerCollection.getItems(),
                        points = [];

                    layers.forEach(function(layer){
                        var _points = layer.nestedCollection.getItems().map(function(point){
                            var content = point.content;
                            return {
                                pointEntity: point,
                                x: parseFloat(content.x),
                                y: parseFloat(content.y),
                                color: content.color,
                                label: content.label || ''
                            };
                        });
                        points = points.concat(_points);
                    });

                    return {
                        layers: layers,
                        points: points
                    };
                };

                newLayerCollection.getClusteredData = function(){
                    var totalPoints = 0,
                        minPointRadius = 5,
                        minPointArea = Math.PI * Math.pow(minPointRadius, 2),
                        ranges = newLayerCollection.getRanges(),
                        clusters = [],
                        byX = 7,
                        byY = 7,
                        stepX = (ranges.x[1] - ranges.x[0]) / byX,
                        stepY = (ranges.y[1] - ranges.y[0]) / byY,
                        minR = 0,
                        maxR = 0,
                        i = 0,
                        j = 0;

                    newLayerCollection.getItems().forEach(function(layer){
                        totalPoints += layer.nestedCollection.numberOfItems;
                    });

                    for(; i < byY; i++ ){
                        var valY = stepY * i;

                        for(j = 0; j < byX; j++ ){
                            var valX = stepX * j;
                            clusters.push({
                                minX: valX,
                                maxX: valX + stepX,
                                minY: valY,
                                maxY: valY + stepY,
                                layers: []
                            });
                        }
                    }

                    newLayerCollection.getItems().forEach(function(layer){
                        var nestedItems = layer.nestedCollection.getItems();

                        clusters.forEach(function(cluster){
                            var newLayerItems = nestedItems.filter(function(point){
                                    var valX = parseFloat(point.content.x),
                                        valY = parseFloat(point.content.y);

                                    return (
                                        (cluster.minX <= valX) && (cluster.maxX > valX) &&
                                        (cluster.minY <= valY) && (cluster.maxY > valY)
                                    );
                                }),
                                xVals = [],
                                yVals = [];

                            if(newLayerItems.length) {
                                newLayerItems.forEach(function(point){
                                    xVals.push(parseFloat(point.content.x));
                                    yVals.push(parseFloat(point.content.y));
                                });

                                minR = d3.min([minR, newLayerItems.length]);
                                maxR = d3.max([maxR, newLayerItems.length]);

                                cluster.layers.push({
                                    points: newLayerItems,
                                    color: layer.content.color,
                                    x: xVals.reduce(function(p, c){
                                        return p + c;
                                    }) / (newLayerItems.length || 1),
                                    y: yVals.reduce(function(p, c){
                                        return p + c;
                                    }) / (newLayerItems.length || 1),
                                    r: newLayerItems.length
                                });
                            }
                        });
                    });

                    return {
                        clusters: clusters,
                        rangeR: [minR, maxR]
                    };
                };

                newLayerCollection.getClustersData = function(sizes, settings){
                    var numberOfPoints = 0,
                        ranges = newLayerCollection.getRanges(),
                        layers = newLayerCollection.getItems().map(function(layer){
                            var points = layer.nestedCollection.getItems().map(function(point){
                                    return point;
                                }),
                                layerPointsLen = layer.nestedCollection.numberOfItems,
                                color = layer.content.color;

                            numberOfPoints += layerPointsLen;

                            return {
                                points: points,
                                len: layerPointsLen,
                                color: color
                            };
                        }).filter(function(layer){
                            return layer.points.length;
                        });

                    var clusters = [],
                        minRadius = settings.pointSize / 2,
                        gravityPower = d3.min([sizes.x, sizes.y]) * ((100 - settings.density) * 0.01),
                        gravityX = (gravityPower / sizes.x) * ranges.x[1],
                        gravityY = (gravityPower / sizes.y) * ranges.y[1];

                    layers.forEach(function(layer){
                        var layerClusters = [];

                        layer.points.forEach(function(point){
                            var x = point.content.x,
                                y = point.content.y,
                                closestCores = layerClusters.filter(function(cluster){
                                    return (x >= cluster.gravityZoneX[0] && x <= cluster.gravityZoneX[1]) && (y >= cluster.gravityZoneY[0] && y <= cluster.gravityZoneY[1]);
                                });

                            if(!closestCores.length) {
                                layerClusters.push({
                                    core: point,
                                    x: x,
                                    y: y,
                                    gravityZoneX: [x - gravityX, x + gravityX],
                                    gravityZoneY: [y - gravityY, y + gravityY],
                                    points: [point],
                                    color: point.content.color
                                });

                            } else {
                                closestCores[parseInt( Math.random() * closestCores.length)].points.push(point);
                            }
                        });

                        clusters = clusters.concat(layerClusters);
                    });

                    return {
                        clusters: clusters.sort(function(a, b){
                            var valA = a.points.length,
                                valB = b.points.length;

                            if(valA < valB) {
                                return 1;
                            }
                            if(valA > valB) {
                                return -1;
                            }
                            return 0;
                        }),
                        scaleR: d3.scale.linear()
                            .domain([d3.min(clusters, function(c){ return c.points.length; }), d3.max(clusters, function(c){ return c.points.length; })])
                            .range([minRadius * 0.3, minRadius * 1.3])
                    };
                };

                return newLayerCollection;
            };

            BWChartEntity.prototype.getNewScatterPlotChartLayer = function(data, scatterPlotChartCollection){
                var self = this;
                return scatterPlotChartCollection.createNestedCollection(data, {multiselect: true})
            };

            BWChartEntity.prototype.getNewBarChartCollection = function(data, parentItem) {
                var self = this,
                    newClusterCollection = (parentItem ?
                    parentItem.createNestedCollection :
                    BWCollectionService.createNew
                )(data, {type: BWChartEnums.linearChart});

                newClusterCollection.getItems().forEach(function(cluster){
                    cluster.createNestedCollection(cluster.content.bars);
                });

                newClusterCollection.getRanges = function(settings){
                    var ranges = [0, 0],
                        valueProperty = settings.keys['value'];

                    newClusterCollection.getItems().forEach(function(cluster){
                        var totalLz = 0,
                            totalGz = 0;

                        cluster.nestedCollection.getItems().forEach(function(bar){
                            var value = parseFloat(bar.content[valueProperty]);

                            if(value < 0) {
                                totalLz += value;
                            } else {
                                totalGz += value;
                            }
                        });
                        ranges[0] = d3.min([ranges[0], totalLz]);
                        ranges[1] = d3.max([ranges[1], totalGz]);
                    });

                    return ranges;
                };

                newClusterCollection.getRenderingData = function(settings){
                    return newClusterCollection.getItems().map(function(cluster){
                        var totalLz = 0,
                            totalGz = 0,
                            valueProperty = settings.keys['value'];

                        var bars = cluster.nestedCollection.getItems().map(function(bar){
                            var content = bar.content,
                                value = parseFloat(content[valueProperty]),
                                isLowerThenZero = value < 0;

                            if(isLowerThenZero) {
                                totalLz += value;
                            } else {
                                totalGz += value;
                            }

                            return {
                                entity: bar,
                                value: value,
                                label: content.label,
                                color: content.color,
                                isLowerThenZero: isLowerThenZero,
                                size: 0
                            }
                        });

                        bars.forEach(function(bar){
                            if(bar.isLowerThenZero) {
                                bar.size = Math.abs(totalLz);
                                totalLz -= bar.value;
                            } else {
                                bar.size = totalGz;
                                totalGz -= bar.value;
                            }
                        });

                        return {
                            entity: cluster,
                            label: cluster.content.label,
                            bars: bars
                        };
                    });


                    //var ranges = newClusterCollection.getRanges();

                    //return newClusterCollection.getItems().map();


                    //var ranges = newClusterCollection.getRanges(),
                    //    scale = d3.scale.linear()
                    //        .domain(ranges)
                    //        .range([0, 1]);
                    //
                    //return newClusterCollection.getItems().map(function(cluster){
                    //    var total = 0,
                    //        bars = cluster.nestedCollection.getItems().map(function(bar){
                    //            var content = bar.content,
                    //                value = scale(content.value),
                    //                size = total + value;
                    //
                    //            total += value;
                    //
                    //            return {
                    //                entity: bar,
                    //                value: content.value,
                    //                color: content.color,
                    //                size: size
                    //            };
                    //        });
                    //
                    //    return {
                    //        entity: cluster,
                    //        label: cluster.content.label,
                    //        bgColor: cluster.content.bgColor || 'rgba(0,0,0,0)',
                    //        bars: bars.sort(function(a, b){
                    //            var valA = a.size,
                    //                valB = b.size;
                    //            return valA < valB ? 1 : (valA > valB ? -1 : 0);
                    //        })
                    //    }
                    //});
                };

                return newClusterCollection;
            };

            BWChartEntity.prototype.getNewBoxPlotChartCollection = function(data, parentItem){
                var self = this,
                    newBoxPlotChartCollection = (parentItem ?
                        parentItem.createNestedCollection :
                        BWCollectionService.createNew
                    )(data, {type: BWChartEnums.candleChart});

                newBoxPlotChartCollection.getItems().forEach(function(boxPlot){
                    boxPlot.createNestedCollection(boxPlot.content.points);
                });

                newBoxPlotChartCollection.getRanges = function(){
                    var ranges = {
                        x: [null, null],
                        y: [0, 0],
                        r: [0, 0]
                    };

                    newBoxPlotChartCollection.getItems().forEach(function(block){
                        if(ranges.x[0] === null) {
                            ranges.x[0] = block.content.timestamp;
                            ranges.x[1] = block.content.timestamp;
                        }

                        ranges.x[0] = d3.min([block.content.timestamp, ranges.x[0]]);
                        ranges.x[1] = d3.max([block.content.timestamp, ranges.x[1]]);

                        var valuesY = block.nestedCollection.getItems().map(function(point){
                            return point.content.y;
                        });

                        ranges.y[0] = d3.min(valuesY.concat(block.content.lowest).concat(ranges.y[0]));
                        ranges.y[1] = d3.max(valuesY.concat(block.content.highest).concat(ranges.y[1]));

                        var valuesR = block.nestedCollection.getItems().map(function(point){
                            return point.content.r;
                        });

                        ranges.r[0] = d3.min(valuesR.concat(ranges.r[0]));
                        ranges.r[1] = d3.max(valuesR.concat(ranges.r[1]));
                    });

                    return ranges;
                };

                newBoxPlotChartCollection.getRenderingData = function(){
                    return newBoxPlotChartCollection.getItems().map(function(block){
                        var content = block.content,
                            points = block.nestedCollection.getItems().map(function(point){
                                return {
                                    entity: point,
                                    y: point.content.y,
                                    r: point.content.r
                                };
                            });

                        return {
                            entity: block,
                            x: content.timestamp,
                            hh: content.highest,
                            h: content.high,
                            m: content.median,
                            l: content.low,
                            ll: content.lowest,
                            points: points
                        };
                    });
                };

                return newBoxPlotChartCollection;
            };

            BWChartEntity.prototype.getNewSunburstChartCollection = function(data, parentItem){
                var self = this,
                    newSunburstChartCollection = (parentItem ?
                        parentItem.createNestedCollection :
                        BWCollectionService.createNew
                    )(data, {type: BWChartEnums.sunburstChart});

                newSunburstChartCollection.getItems().forEach(function(slice){
                    slice.createNestedCollection(slice.content.subSlices);
                });

                newSunburstChartCollection.getRanges = function(selectedSlice, keys){
                    var ranges = {
                            x: [0, 0],
                            y: [0, 0]
                        },
                        propX = keys.x,
                        propY = keys.y;

                    (selectedSlice ? [selectedSlice] : newSunburstChartCollection.getItems()).forEach(function(slice){
                        if(slice.nestedCollection && slice.nestedCollection.getItems().length) {
                            slice.nestedCollection.getItems().forEach(function(subSlice){
                                var content = subSlice.content,
                                    x = parseFloat(content[propX]),
                                    y = parseFloat(content[propY]);

                                ranges.x[0] = d3.min([ranges.x[0], x]);
                                ranges.x[1] = d3.max([ranges.x[1], x]);
                                ranges.y[0] = d3.min([ranges.y[0], y]);
                                ranges.y[1] = d3.max([ranges.y[1], y]);
                            });
                        } else {
                            ranges.x[0] = d3.min([ranges.x[0], slice.content[propX]]);
                            ranges.x[1] = d3.max([ranges.x[1], slice.content[propX]]);
                        }
                    });

                    return ranges;
                };

                newSunburstChartCollection.getRenderingData = function(settings){
                    var ranges = newSunburstChartCollection.getRanges(null, settings.keys),
                        slices = [],
                        subSlices = [],
                        propX = settings.keys.x,
                        propY = settings.keys.y,
                        scaleY = d3.scale.linear()
                            .domain(ranges.y)
                            .range([0.75, 1]);

                    newSunburstChartCollection.getItems().forEach(function(slice){
                        var sliceId = slice._id.replace(/#/, ''),
                            color = slice.content.color,
                            hasNestedCollection = slice.nestedCollection && slice.nestedCollection.getItems().length,
                            newSlice = {
                                entity: slice,
                                value: 0,
                                color: color,
                                id: sliceId
                            };


                        if(hasNestedCollection) {
                            newSlice.entity.nestedCollection.getItems().forEach(function(subSlice){
                                var content = subSlice.content,
                                    x = parseFloat(content[propX]),
                                    y = scaleY(parseFloat(content[propY])),
                                    newSubSlice = {
                                        entity: subSlice,
                                        x: x,
                                        y: y,
                                        color: BWColorsService.getBrighterColor(color, (1 - y) * 10, 0.5),
                                        sliceId: sliceId,
                                        id: subSlice._id.replace(/#/, '')
                                    };

                                newSlice.value += x;
                                subSlices.push(newSubSlice);
                            });
                        } else {
                            newSlice.value = parseFloat(slice.content[propX]);
                            subSlices.push({
                                x: newSlice.value,
                                y: 0.6,
                                color: '#000'
                            });
                        }

                        slices.push(newSlice);
                    });

                    return {
                        slices: d3.layout.pie().value(function(d){
                            return d.value;
                        }).sort(null)(slices),
                        subSlices: d3.layout.pie().value(function(d){
                            return d.x;
                        }).sort(null)(subSlices)
                    };
                };

                newSunburstChartCollection.getZoomedSliceRenderingData = function(selectedSlice, settings){
                    var ranges = newSunburstChartCollection.getRanges(selectedSlice, settings.keys),
                        slices = [],
                        color = selectedSlice.content.color,
                        propX = settings.keys.x,
                        propY = settings.keys.y,
                        scaleY = d3.scale.linear()
                            .domain(ranges.y)
                            .range([0.75, 1]),
                        scaleOpacity = d3.scale.linear()
                            .domain([0.75, 1])
                            .range([0.2, 0.8]);

                    selectedSlice.nestedCollection.getItems().forEach(function(slice){
                        var content = slice.content,
                            x = parseFloat(content[propX]),
                            y = scaleY(parseFloat(content[propY])),
                            opacity = scaleOpacity(y);

                        slices.push({
                            entity: slice,
                            x: x,
                            y: y,
                            color: color,
                            opacity: opacity
                        });
                    });

                    return d3.layout.pie().value(function(d){
                        return d.x;
                    }).sort(null)(slices);
                };

                return newSunburstChartCollection;
            };

            BWChartEntity.prototype.getNewHistogramCollection = function(data, parentItem){
                var self = this,
                    newHistogramCollection = (parentItem ?
                        parentItem.createNestedCollection :
                        BWCollectionService.createNew
                    )(data, {type: BWChartEnums.histogram});

                newHistogramCollection.getRanges = function(){
                    var ranges = {
                        x: [0, 0],
                        y: [0, 0]
                    };

                    newHistogramCollection.getItems().forEach(function(bar){
                        ranges.x[0] = d3.min([ranges.x[0], parseFloat(bar.content.x)]);
                        ranges.x[1] = d3.max([ranges.x[1], parseFloat(bar.content.x)]);
                        ranges.y[0] = d3.min([ranges.y[0], parseFloat(bar.content.y)]);
                        ranges.y[1] = d3.max([ranges.y[1], parseFloat(bar.content.y)]);
                    });

                    return {
                        x: ranges.x,
                        y: [ranges.y[0], ranges.y[1] * 1.05]
                    };
                };

                newHistogramCollection.getRenderingData = function(){
                    var ranges = newHistogramCollection.getRanges(),
                        scaleX = d3.scale.linear()
                            .domain(ranges.x)
                            .range([0, 1]),
                        scaleY = d3.scale.linear()
                            .domain(ranges.y)
                            .range([0, 1]);

                    return newHistogramCollection.getItems().map(function(bar){
                        var content = bar.content;

                        return {
                            entity: bar,
                            x: scaleX(parseFloat(content.x)),
                            y: scaleY(parseFloat(content.y)),
                            color: content.color
                        };
                    }).sort(function(a, b){
                        if(a.x > b.x) { return 1; }
                        else if(a.x < b.x) { return -1; }
                        else { return 0; }
                    });
                };

                return newHistogramCollection;
            };

            BWChartEntity.prototype.getNewTreemapCollection = function(data, parentItem){
                var self = this,
                    newTreemapCollection = (parentItem ?
                        parentItem.createNestedCollection :
                        BWCollectionService.createNew
                    )(data, {type: BWChartEnums.treemap});

                newTreemapCollection.getRenderingData = function(chartSettings, sizes){
                    var sizeKey = chartSettings.sizeKey,
                        colorKey = chartSettings.colorKey,
                        labelKey = chartSettings.labelKey,
                        renderingData = {
                            name: '',
                            isRoot: true,
                            size: 0,
                            children: []
                        },
                        colorScheme;

                    if(!chartSettings.usePredefinedColors){
                        var colorValues = newTreemapCollection.getItems().map(function(item){
                                return item.content[colorKey];
                            }),
                            colorRanges = [d3.min(colorValues), d3.max(colorValues)];

                        colorScheme = BWColorsService.getColorScheme(chartSettings.colors, colorRanges);
                    }

                    newTreemapCollection.getItems().forEach(function(item){
                        var content = item.content,
                            color = colorScheme ? colorScheme(content[colorKey]) : content[colorKey];

                        renderingData.size += content[sizeKey];

                        renderingData.children.push({
                            entity: item,
                            size: content[sizeKey],
                            color: color,
                            fontColor: BWColorsService.getContrastyColor(color),
                            label: content[labelKey]
                        });
                    });

                    return d3.layout.treemap()
                        .size(sizes ? [sizes.x, sizes.y] : [100, 100])
                        .value(function(d) { return d.size; })
                        .nodes(renderingData);
                };

                return newTreemapCollection;
            };

            BWChartEntity.prototype.getNewBubbleChartCollection = function(data, parentItem){
                var self = this,
                    newBubbleChartCollection = (parentItem ?
                        parentItem.createNestedCollection :
                        BWCollectionService.createNew
                    )(data, {type: BWChartEnums.bubbleChart});

                newBubbleChartCollection.getItems().forEach(function(layer){
                    self.getNewBubbleChartLayer(layer.content.items, layer);
                });

                newBubbleChartCollection.getRanges = function(){
                    var self = this,
                        ranges = {
                            x: [0, 0],
                            y: [0, 0],
                            r: [null, null]
                        };
                    newBubbleChartCollection.getItems().forEach(function(layer){
                        layer.nestedCollection.getItems().forEach(function(bubble){
                            var content = bubble.content;

                            if(ranges.r[0] === null) {
                                ranges.r[0] = content.r;
                                ranges.r[1] = content.r;
                            }

                            ranges.x[0] = d3.min([content.x, ranges.x[0]]);
                            ranges.x[1] = d3.max([content.x, ranges.x[1]]);
                            ranges.y[0] = d3.min([content.y, ranges.y[0]]);
                            ranges.y[1] = d3.max([content.y, ranges.y[1]]);
                            ranges.r[0] = d3.min([content.r, ranges.r[0]]);
                            ranges.r[1] = d3.max([content.r, ranges.r[1]]);
                        });
                    });

                    var offsetX = (ranges.x[1] - ranges.x[0]) * 0.1,
                        offsetY = (ranges.y[1] - ranges.y[0]) * 0.1;

                    ranges.x[0] -= offsetX;
                    ranges.x[1] += offsetX;
                    ranges.y[0] -= offsetY;
                    ranges.y[1] += offsetY;

                    return ranges;
                };

                newBubbleChartCollection.getRenderingData = function(settings, sizes){
                    var bubbles = [],
                        minRadius = 5,
                        maxRadius = d3.min(sizes ? [sizes.x, sizes.y] : [100, 100]) * 0.1,
                        ranges = newBubbleChartCollection.getRanges(),
                        scales = {
                            x: d3.scale.linear()
                                .domain(ranges.x)
                                .range([0, sizes ? sizes.x : 100])
                                .nice(),
                            y: d3.scale.linear()
                                .domain(ranges.y)
                                .range([sizes ? sizes.y : 100, 0])
                                .nice(),
                            r: d3.scale.linear()
                                .domain(ranges.r)
                                .range([minRadius, maxRadius])
                        };

                    newBubbleChartCollection.getItems().forEach(function(layer, i){
                        var _bubbles = layer.nestedCollection.getItems().map(function(bubble){
                            var content = bubble.content;

                            return {
                                entity: bubble,
                                x: scales.x(content.x),
                                y: scales.y(content.y),
                                r: scales.r(content.r),
                                color: layer.content.color
                            };
                        });

                        bubbles = bubbles.concat(_bubbles);
                    });

                    return {
                        bubbles: bubbles.sort(function(a, b){
                            var radiusA = a.r,
                                radiusB = b.r;
                            if(radiusA < radiusB){
                                return 1;
                            }
                            if(radiusA > radiusB){
                                return -1;
                            }
                            return 0;

                        }),
                        scaleX: scales.x,
                        scaleY: scales.y
                    };
                };


                return newBubbleChartCollection;
            };

            BWChartEntity.prototype.getNewBubbleChartLayer = function(data, bubbleChartCollection){
                bubbleChartCollection.createNestedCollection(data, {multiselect: true});
            };

            BWChartEntity.prototype.getNewSankeyChartCollection = function(data, parentItem){
                var self = this,
                    newSankeyChartCollection = (parentItem ?
                        parentItem.createNestedCollection :
                        BWCollectionService.createNew
                    )(data, {type: BWChartEnums.sankeyChart});

                newSankeyChartCollection.getRenderingData = function(settings, sizes){
                    var keyProperty = settings.keys.value,
                        mainNode = { value: 0, color: settings.baseNodeColor, index: 0 },
                        nodes = [mainNode],
                        sankey = d3.sankey()
                            .nodeWidth(settings.nodeWidth)
                            .nodePadding(settings.nodePadding)
                            .size(sizes ? [sizes.x, sizes.y] : [100, 100]),
                        sankeyPath = sankey.link(),
                        links = [];

                    newSankeyChartCollection.getItems().forEach(function(item, i){
                        var value = parseFloat(item.content[keyProperty]),
                            index = i + 1;

                        mainNode.value += value;
                        nodes.push({
                            entity: item,
                            value: value,
                            color: item.content.color,
                            label: item.content.label,
                            index: index
                        });

                        links.push({
                            entity: item,
                            source: 0,
                            target: index,
                            value: value
                        });
                    });

                    sankey
                        .nodes(nodes)
                        .links(links)
                        .layout(100);

                    return {
                        nodes: nodes,
                        links: links.map(function(link, i){
                            return {
                                entity: link.item,
                                index: i + 1,
                                path: sankeyPath(link),
                                width: d3.max([link.dy, 1])
                            };
                        })
                    };
                };

                return newSankeyChartCollection;
            };

            BWChartEntity.prototype.getNewActivitiesChartCollection = function(data, parentItem){
                var self = this,
                    newActivitiesChartCollection = (parentItem ?
                            parentItem.createNestedCollection :
                            BWCollectionService.createNew
                    )(data, {type: BWChartEnums.activitiesChart});

                newActivitiesChartCollection.getItems().forEach(function(layer){
                    layer.createNestedCollection(layer.content.points);
                });

                newActivitiesChartCollection.getRanges = function(settings){
                    var ranges = [0, 0],
                        propY = settings.keys.y;

                    newActivitiesChartCollection.getItems().forEach(function(layer){
                        if(layer.nestedCollection){
                            layer.nestedCollection.getItems().forEach(function(point){
                                var content = point.content,
                                    valY = parseFloat(content[propY]);

                                ranges[0] = d3.min([valY, ranges[0]]);
                                ranges[1] = d3.max([valY, ranges[1]]);
                            });
                        }
                    });

                    return ranges;
                };

                newActivitiesChartCollection.getRenderingData = function(settings){
                    var propX = settings.keys.x,
                        propY = settings.keys.y,
                        layers = [],
                        axisXLabels = [];

                    newActivitiesChartCollection.getItems().forEach(function(layer){
                        var clusters = getClusters(layer.nestedCollection.getItemsDividedByGroups(propX, true)),
                            points = getLinearDiagramPoints(clusters),
                            color = layer.content.color;

                        clusters.forEach(function(cluster){
                            if(axisXLabels.indexOf(cluster.x) === -1){
                                axisXLabels.push(cluster.x);
                            }
                        });

                        layers.push({
                            entity: layer,
                            clusters: clusters,
                            showAsDiagram: layer.content.showAsDiagram,
                            linearDiagramPoints: points,
                            color: color
                        });
                    });

                    return {
                        axisXLabels: axisXLabels,
                        layers: layers.sort(function(layer){
                            return layer.showAsDiagram ? 1 : -1;
                        })
                    };

                    function getLinearDiagramPoints (clusters){
                        return clusters.map(function(cluster, index){
                            return {
                                x: index,
                                y: cluster.q2
                            };
                        });
                    }

                    function getClusters (clusters){
                        return clusters.map(function(cluster){
                            var items = [],
                                values = cluster.items.map(function(item){
                                    var x = parseInt(item.content[propX]),
                                        y = parseFloat(item.content[propY]);
                                    items.push({
                                        entity: item,
                                        x: x,
                                        y: y
                                    });
                                    return y;
                                }).sort(function(a, b){
                                    if(a < b) { return -1; }
                                    if(a > b) { return 1; }
                                    return 0;
                                }),
                                q2 = getMedian(values),
                                q1 = getMedian(q2.arrA),
                                q3 = getMedian(q2.arrB);

                            return {
                                items: items,
                                q1: q1.median,
                                q2: q2.median,
                                q3: q3.median,
                                x: (cluster.label).toString()
                            };
                        });
                    }

                    function getMedian (arr){
                        var median = 0,
                            len = arr.length,
                            isOdd = !!(len % 2),
                            mid = len / 2,
                            arrA = 0,
                            arrB = 0;

                        if(isOdd) {
                            median = arr[Math.floor(mid)];
                            arrA = arr.slice(0, Math.floor(mid));
                            arrB = arr.slice(Math.ceil(mid), len);
                        } else {
                            median = len > 2 ? (arr[mid] + arr[mid + 1]) / 2 : (arr[0] + arr[1]) / 2;
                            arrA = arr.slice(0, mid);
                            arrB = arr.slice(mid, len);
                        }

                        return {
                            median: median,
                            arrA: arrA,
                            arrB: arrB,
                            len: len
                        };
                    }
                };

                return newActivitiesChartCollection;
            };

            /** Randomizers */
            BWChartEntity.prototype.getRandomRoundChartCollection = function(n){
                var self = this,
                    roundChartData = [],
                    i = 0;

                for(; i < (n || 5); i++){
                    roundChartData.push(self.getRandomRoundChartSlice(i));
                }

                return roundChartData;
            };

            BWChartEntity.prototype.getRandomRoundChartSlice = function(i){
                return {
                    value: parseInt(Math.random() * 1000000),
                    label: 'SLICE: ' + i,
                    index: i,
                    color: d3Palette(i)
                }
            };

            BWChartEntity.prototype.getRandomScatterPlotChartCollection = function(n){
                var self = this,
                    scatterPlotChartData = [],
                    i = 0;

                for(; i < (n || 4); i++){
                    scatterPlotChartData.push(self.getRandomScatterPlotChartLayer(i));
                }

                return scatterPlotChartData;
            };

            BWChartEntity.prototype.getRandomScatterPlotChartLayer = function(i){
                var self = this,
                    randomNumberOfPoints = parseInt(Math.random() * 50) + 20,
                    j = 0,
                    newLayer = {
                        items: [],
                        label: 'LAYER: ' + i,
                        index: i,
                        color: d3Palette(i),
                        isVisible: true
                    };

                for(; j < randomNumberOfPoints; j++) {
                    newLayer.items.push(self.getRandomScatterPlotChartPoint(j, newLayer.color));
                }
                return newLayer;
            };

            BWChartEntity.prototype.getRandomScatterPlotChartPoint = function(i, color){
                var maxX = 20,
                    maxY = 50;

                return {
                    x: parseFloat(Math.random() * maxX),
                    y: parseFloat(Math.random() * maxY),
                    //y: (Math.pow(parseInt(Math.random() * 10), pow) * m * [-1, 1][parseInt(Math.random() * 2)]) + (100000 * parseInt(Math.random() * 10)),
                    label: 'POINT: ' + i,
                    color: color
                }
            };

            BWChartEntity.prototype.getRandomBarChartCollection = function(n, numberOfItems){
                var self = this,
                    barChartData = [],
                    i = 0;

                for(; i < (n || 7); i++){
                    barChartData.push(self.getRandomBarChartCluster(i, numberOfItems || 3));
                }

                return barChartData;
            };

            BWChartEntity.prototype.getRandomBarChartCluster = function(i, numberOfItems){
                var self = this,
                    color = d3Palette(i),
                    newCluster = {
                        bars: [],
                        label: 'C#' + i,
                        index: i
                    },
                    j = 0;

                for(; j < numberOfItems; j++) {
                    newCluster.bars.push(self.getRandomBarChartBar(color, j));
                }

                return newCluster;
            };

            BWChartEntity.prototype.getRandomBarChartBar = function(color, i){
                return {
                    //value: (parseInt(Math.random() * 1000000) * [-1, 1][parseInt(Math.random() * 2)]) + 250000,
                    value: (parseInt(Math.random() * 100000000)),
                    label: 'BAR: ' + i,
                    index: i,
                    color: d3Palette(i)
                }
            };

            BWChartEntity.prototype.getRandomBoxPlotChartCollection = function(_fromYear){
                var self = this,
                    candles = [],
                    startFrom = new Date(_fromYear || 2014, 0, 1).getTime(),
                    step = 86400000 * 100, /// step 100 days
                    i = 1,
                    len = 40,
                    minYield = -4,
                    maxYield = 8,
                    maxCandleSize = 12,
                    minCandleSize = 1.3,
                    valuesRail = maxCandleSize - minCandleSize;

                for(; i < len ; i++ ){
                    var randoms = [],
                        points = [],
                        j = 0,
                        randomsQuantity = 5,
                        numberOfPoints = parseInt(Math.random() * 10),
                        newCandle = {
                            timestamp: startFrom + (step * i),
                            highest: 0,
                            high: 0,
                            median: 0,
                            low: 0,
                            lowest: 0,
                            date: 0,
                            points: []
                        };

                    for(; j < randomsQuantity ; j++ ) {
                        randoms.push(minYield + parseFloat(Math.random() * valuesRail));
                    }

                    for( j = 0; j < numberOfPoints; j++ ) {
                        points.push({
                            y: minYield + parseFloat(Math.random() * maxCandleSize),
                            r: parseFloat(Math.random() * 100)
                        });
                    }

                    randoms.sort(function(a, b){
                        if(a < b) { return 1; }
                        if(a > b) { return -1; }
                        return 0;
                    });

                    points.sort(function(a, b){
                        if(a.y < b.y) { return 1; }
                        if(a.y > b.y) { return -1; }
                        return 0;
                    });

                    newCandle.highest = randoms[0];
                    newCandle.high = randoms[1];
                    newCandle.median = randoms[2];
                    newCandle.low = randoms[3];
                    newCandle.lowest = randoms[4];
                    newCandle.points = points;

                    candles.push(newCandle);
                }

                return candles;
            };

            BWChartEntity.prototype.getRandomSunburstChartCollection = function(){
                var self = this,
                    slices = ['FIRST SLICE','SECOND SLICE','THIRD SLICE', 'FOURTH SLICE'].map(function(slice, i){
                        return {
                            name: 'slice#' + i,
                            label: slice,
                            color: d3Palette(i + 2),
                            x: parseFloat(Math.random() * 990) + 10,
                            subSlices: []
                        };
                    });

                slices.forEach(function(slice){
                    slice.subSlices = self.getRandomSunburstChartSliceCollection(slice.x);
                });

                return slices;
            };

            BWChartEntity.prototype.getRandomSunburstChartSliceCollection = function(value){
                var slices = [],
                    total = value,
                    numberOfSlices = parseInt(Math.random() * 8),
                    labels = ['FIRST SUBSLICE', 'SECOND SUBSLICE', 'THIRD SUBSLICE', 'FOURTH SUBSLICE', 'FIFTH SUBSLICE', 'SIXTH SUBSLICE', 'SEVENTH SUBSLICE', 'EIGHTH SUBSLICE'],
                    i = 0;

                for(; i < numberOfSlices ; i++ ){
                    var randomX = parseFloat(Math.random() * (total * 0.75)),
                        randomY = parseInt(Math.random() * 1000),
                        x = i === numberOfSlices ? total : randomX;

                    total -= x;

                    slices.push({
                        name: 'subSlice#' + i,
                        label: labels[i],
                        x: x,
                        y: randomY
                    });
                }

                return slices;
            };

            BWChartEntity.prototype.getRandomHistogramCollection = function(value){
                var bars = [],
                    i = 0,
                    len = parseInt(Math.random() * 10) + 20,
                    randomMax = parseInt(Math.random() * 5000000) + 1000000;

                for(; i < len ; i++ ){
                    bars.push({
                        x: i,
                        y: parseInt(Math.random() * randomMax),
                        color: d3Palette(0),
                        label: i
                    });
                }

                return bars;
            };

            BWChartEntity.prototype.getRandomTreemapCollection = function(){
                return ['FIRST', 'SECOND', 'THIRD', 'FOURTH', 'FIFTH', 'SIXTH', 'SEVENTH'].map(function(name){
                    return {
                        name: name,
                        color: parseInt(Math.random() * 5000),
                        value: parseInt(Math.random() * 9) + 1
                    };
                });
            };

            BWChartEntity.prototype.getRandomBubbleChartCollection = function(n){
                var self = this,
                    layers = [],
                    i = 0;

                for(; i < (n || 4); i++ ){
                    layers.push(self.getRandomBubbleChartLayer(i));
                }

                return layers;
            };

            BWChartEntity.prototype.getRandomBubbleChartLayer = function(i){
                var self = this,
                    randomNumberOfBubbles = 10,
                    j = 0,
                    newLayer = {
                        items: [],
                        label: 'LAYER: ' + i,
                        index: i,
                        color: d3Palette(i),
                        isVisible: true
                    };

                for(; j < randomNumberOfBubbles; j++) {
                    newLayer.items.push(self.getRandomBubbleChartPoint(j, newLayer.color));
                }
                return newLayer;
            };

            BWChartEntity.prototype.getRandomBubbleChartPoint = function(i, color){
                var maxX = 50,
                    maxY = 100,
                    maxR = 999999999;

                return {
                    x: parseFloat(Math.random() * maxX),
                    y: parseFloat(Math.random() * maxY),
                    r: parseFloat(Math.random() * maxR),
                    label: 'BUBBLE#' + i,
                    color: color
                }
            };

            BWChartEntity.prototype.getRandomSankeyChartCollection = function(n){
                return ['BLACK CAT', 'WHITE CAT', 'VERY FAT CAT', 'FUNNY CAT'].map(function(label, i){
                    return {
                        label: label,
                        color: d3Palette(i + 1),
                        value: parseFloat(Math.random() * 250)
                    };
                });
            };

            BWChartEntity.prototype.getRandomActivitiesChartCollection = function(_fromYear, n){
                var fromYear = _fromYear || new Date().getFullYear(),
                    len = parseInt(Math.random() * 10) + 20,
                    minValue = -4,
                    maxValue = 8,
                    valuesRail = Math.abs(maxValue - minValue);

                return ['BLUE LAYER', 'GREEN LAYER'].map(function(layerName, index){
                    var layer = {
                            name: layerName,
                            color: ['#00aeef', '#09b061'][index],
                            showAsDiagram: index === 0,
                            //showAsDiagram: true,
                            points: []
                        },
                        i = 0;

                    for(; i < len ; i++){
                        var _points = [],
                            j = 0,
                            _len = parseInt(Math.random() * 10) + 5;

                        for(; j < _len; j++){
                            _points.push({
                                x: fromYear + i,
                                y: minValue + parseFloat(Math.random() * valuesRail)
                            });
                        }
                        layer.points = layer.points.concat(_points);
                    }

                    return layer;
                });
            }
        }
    ]);

}(angular.module('bw.chart')));


