(function(module){

    'use strict';

    module.service('BWChartEnums', [
        'BWCollectionService',
        function(BWCollectionService){
            var bwChartEnums = this,
                chartTypes = BWCollectionService.createNew([
                    {
                        label: 'ROUND CHART',
                        name: 'roundChart',
                        randomizer: 'getRandomRoundChartCollection',
                        collectionCreator: 'getNewRoundChartCollection',
                        drawer: 'drawRoundChart',
                        icon: 'fa-pie-chart',
                        defaultSettings: {
                            innerRadius: 8,
                            padding: 1,
                            onMouseOver: angular.noop,
                            onMouseMove: angular.noop,
                            onMouseOut: angular.noop,
                            onClick: angular.noop
                        }
                    },
                    {
                        label: 'SCATTER PLOT CHART',
                        name: 'scatterPlotChart',
                        randomizer: 'getRandomScatterPlotChartCollection',
                        collectionCreator: 'getNewScatterPlotChartCollection',
                        drawer: 'drawScatterPlotChart',
                        icon: 'fa-area-chart',
                        defaultSettings: {
                            axisXName: 'Axis X',
                            axisYName: 'Axis Y',
                            pointSize: 10,
                            density: 93, //[1, 99]
                            padding: {
                                top: 5,
                                right: 5,
                                bottom: 35,
                                left: 35
                            },
                            clusters: false,
                            onMouseOver: angular.noop,
                            onMouseOut: angular.noop,
                            onClick: angular.noop,
                            onRangeSelect: angular.noop,
                            onPointClick: angular.noop,
                            onPointMouseOver: angular.noop,
                            onPointMouseOut: angular.noop
                        }
                    },
                    {
                        label: 'BAR CHART',
                        name: 'barChart',
                        randomizer: 'getRandomBarChartCollection',
                        collectionCreator: 'getNewBarChartCollection',
                        drawer: 'drawBarChart',
                        icon: 'fa-align-left',
                        defaultSettings: {
                            showHorizontal: false,
                            thin: 5,
                            fontSize: 12,
                            showAxisX: true,
                            showAxisY: false,
                            axisXName: '',
                            axisYName: '',
                            padding: 40,
                            keys: {
                                value: 'value',
                                label: 'label'
                            },

                            /** Callbacks */
                            onMouseOver: angular.noop,
                            onMouseMove: angular.noop,
                            onMouseOut: angular.noop,
                            onClick: angular.noop,
                            getFormattedLabel: angular.noop
                        }
                    },
                    {
                        label: 'BOX PLOT CHART',
                        name: 'boxPlotChart',
                        randomizer: 'getRandomBoxPlotChartCollection',
                        collectionCreator: 'getNewBoxPlotChartCollection',
                        drawer: 'drawBoxPlotChart',
                        icon: 'fa-candle-chart',
                        defaultSettings: {
                            barWidth: 10,
                            highBoxColor: '#c7c6c5',
                            lowBoxColor: '#909090',
                            pointColor: '#1e77b4',
                            padding: {
                                top: 5,
                                right: 5,
                                bottom: 35,
                                left: 35
                            },
                            onMouseOver: angular.noop,
                            onMouseMove: angular.noop,
                            onMouseOut: angular.noop,
                            onClick: angular.noop
                        }
                    },
                    {
                        label: 'SUNBURST CHART',
                        name: 'sunburstChart',
                        randomizer: 'getRandomSunburstChartCollection',
                        collectionCreator: 'getNewSunburstChartCollection',
                        drawer: 'drawSunburstChart',
                        icon: 'fa-sun-o',
                        defaultSettings: {
                            innerRadius: 4,
                            simple: false,
                            keys: {
                                x: 'x',
                                y: 'y'
                            },
                            onMouseOver: angular.noop,
                            onMouseMove: angular.noop,
                            onMouseOut: angular.noop,
                            onClick: angular.noop,
                            onClusterClick: angular.noop
                        }
                    },
                    {
                        label: 'HISTOGRAM',
                        name: 'histogram',
                        randomizer: 'getRandomHistogramCollection',
                        collectionCreator: 'getNewHistogramCollection',
                        drawer: 'drawHistogram',
                        icon: 'fa-bar-chart',
                        defaultSettings: {
                            showAxisX: true,
                            showAxisY: true,
                            axisXName: '',
                            axisYName: '',
                            fontSize: 10,

                            padding: {
                                top: 5,
                                right: 10,
                                bottom: 1,
                                left: 1
                            },

                            /** Callbacks */
                            onMouseOver: angular.noop,
                            onMouseMove: angular.noop,
                            onMouseOut: angular.noop,
                            onClick: angular.noop
                        }
                    },
                    {
                        label: 'TREEMAP',
                        name: 'treemap',
                        randomizer: 'getRandomTreemapCollection',
                        collectionCreator: 'getNewTreemapCollection',
                        drawer: 'drawTreemap',
                        icon: 'fa-th-large',
                        defaultSettings: {
                            sizeKey: 'value',
                            colorKey: 'color',
                            labelKey: 'name',
                            colors: ['#F00', '#00F'],
                            usePredefinedColors: false,
                            fontSize: 10,
                            padding: {
                                top: 1,
                                right: 1,
                                bottom: 1,
                                left: 1
                            },
                            onMouseOver: angular.noop,
                            onMouseMove: angular.noop,
                            onMouseOut: angular.noop,
                            onClick: angular.noop,
                            onClusterClick: angular.noop
                        }
                    },
                    {
                        label: 'BUBBLE CHART',
                        name: 'bubbleChart',
                        randomizer: 'getRandomBubbleChartCollection',
                        collectionCreator: 'getNewBubbleChartCollection',
                        drawer: 'drawBubbleChart',
                        icon: 'fa-circle-o',
                        defaultSettings: {
                            opacity: 5,
                            padding: {
                                top: 5,
                                right: 5,
                                bottom: 35,
                                left: 35
                            },
                            onMouseOver: angular.noop,
                            onMouseMove: angular.noop,
                            onMouseOut: angular.noop,
                            onClick: angular.noop
                        }
                    },
                    {
                        label: 'SANKEY CHART',
                        name: 'sankeyChart',
                        randomizer: 'getRandomSankeyChartCollection',
                        collectionCreator: 'getNewSankeyChartCollection',
                        drawer: 'drawSankeyChart',
                        icon: 'fa-share',
                        defaultSettings: {
                            linkColor: '#c4c8ce',
                            baseNodeColor: '#1f77b4',
                            nodeWidth: 36,
                            nodePadding: 20,
                            keys: {
                                value: 'value'
                            },
                            /** Callbacks */
                            onMouseOver: angular.noop,
                            onMouseMove: angular.noop,
                            onMouseOut: angular.noop,
                            onClick: angular.noop
                        }
                    },
                    {
                        label: 'ACTIVITIES CHART',
                        name: 'activitiesChart',
                        randomizer: 'getRandomActivitiesChartCollection',
                        collectionCreator: 'getNewActivitiesChartCollection',
                        drawer: 'drawActivitiesChart',
                        icon: 'fa-area-chart',
                        defaultSettings: {
                            fontSize: 10,
                            pointRadius: 2,
                            axisXName: '',
                            axisYName: '',
                            boxColor: '#f6f6f6',
                            strokeWidth: 2,
                            keys: {
                                x: 'x',
                                y: 'y'
                            }

                            //axisXName: 'Axis X',
                            //axisYName: 'Axis Y',
                            //pointSize: 10,
                            //density: 93, //[1, 99]
                            //padding: {
                            //    top: 5,
                            //    right: 5,
                            //    bottom: 35,
                            //    left: 35
                            //},
                            //clusters: false,
                            //onMouseOver: angular.noop,
                            //onMouseOut: angular.noop,
                            //onClick: angular.noop,
                            //onRangeSelect: angular.noop,
                            //onPointClick: angular.noop,
                            //onPointMouseOver: angular.noop,
                            //onPointMouseOut: angular.noop
                        }
                    }
                ]);

            Object.defineProperties(bwChartEnums, {
                roundChart: {
                    value: 'roundChart'
                },
                scatterPlotChart: {
                    value: 'scatterPlotChart'
                },
                barChart: {
                    value: 'barChart'
                },
                boxPlotChart: {
                    value: 'boxPlotChart'
                },
                sunburstChart: {
                    value: 'sunburstChart'
                },
                histogram: {
                    value: 'histogram'
                },
                treemap: {
                    value: 'treemap'
                },
                bubbleChart: {
                    value: 'bubbleChart'
                },
                sankeyChart: {
                    value: 'sankeyChart'
                },
                activitiesChart: {
                    value: 'activitiesChart'
                },

                getChartTypes: {
                    value: function(){
                        return chartTypes;
                    }
                },
                getChartType: {
                    value: function(selectedType){
                        return chartTypes.getItemsByProperty('name', selectedType)[0];
                    }
                }
            });
        }
    ]);

}(angular.module('bw.chart')));
