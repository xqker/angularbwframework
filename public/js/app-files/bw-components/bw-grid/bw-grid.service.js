(function(module){

    'use strict';

    module.service('BWGridService', [
        'AsyncAPI', 'BWCollectionService', 'BWColorsService', 'd3',
        function (AsyncAPI, BWCollectionService, BWColorsService, d3){
            var bwGridService = this,
                d3Palette = BWColorsService.getStdPalette(),
                gridCounter = 0;

            Object.defineProperties(bwGridService, {
                createNew: {
                    value: function(){
                        return new BWGridEntity(gridCounter++);
                    }
                }
            });

            function BWGridEntity (counter){
                var bwGridEntity = this;
                Object.defineProperties(bwGridEntity, {
                    id: {
                        value: '#bw-grid-' + counter
                    },
                    _api_: {
                        value: AsyncAPI.get({})
                    }
                });
            }

            BWGridEntity.prototype.refreshData = function(data, scrollTo){
                this._api_.applyFn('refreshData', data, scrollTo);
            };

            BWGridEntity.prototype.refreshView = function(resetSliders){
                this._api_.applyFn('refreshView', resetSliders);
            };

            BWGridEntity.prototype.clearGrid = function(){
                this._api_.applyFn('clearGrid');
            };

            BWGridEntity.prototype.toggleWaiting = function(state){
                this._api_.applyFn('toggleWaiting', state);
            };

            BWGridEntity.prototype.addRows = function(rows, scrollTo){
                this._api_.applyFn('addRows', rows, scrollTo);
            };

            BWGridEntity.prototype.scrollTo = function(index){
                this._api_.applyFn('scrollTo', index);
            };

            BWGridEntity.prototype.selectAll = function(){
                this._api_.applyFn('selectAll');
            };

            BWGridEntity.prototype.deselectAll = function(){
                this._api_.applyFn('deselectAll');
            };

            BWGridEntity.prototype.getAffectedIndexes = function(){
                return this._api_.applyFn('getAffectedIndexes');
            };

            BWGridEntity.prototype.clearGroups = function(){
                this._api_.applyFn('clearGroups');
            };

            BWGridEntity.prototype.toggleRow = function(index){
                this._api_.applyFn('toggleRow', index);
            };

            BWGridEntity.prototype.toggleRows = function(indexes, state, preventSelection){
                this._api_.applyFn('toggleRows', indexes, state, preventSelection);
            };

            /** Random data */
            BWGridEntity.prototype.getRandomData = function(numberOfRows){
                var defaultNumberOfRows = 100,
                    randomIdCounter = parseInt(Math.random() * 100000),
                    _cols = [
                        {
                            name: 'someId',
                            label: 'Some ID#',
                            before: '#'
                        },
                        {
                            name: 'someName',
                            label: 'Some Random Cat',
                            link: true
                        },
                        {
                            name: 'preferences',
                            label: 'This Cat Prefers To Eat...'
                        },
                        {
                            name: 'someValue',
                            label: 'Some Value',
                            align: 'right',
                            type: 'comaSeparated'
                        },
                        {
                            name: 'someDate',
                            label: 'Some Date',
                            align: 'right',
                            type: 'date'
                        },
                        {
                            name: 'columnToHide',
                            label: 'This one will be hidden',
                            hidden: true
                        },
                        {
                            name: 'oneMoreRow',
                            label: 'One More Row',
                            hidden: false
                        },
                        {
                            name: 'andOneMore',
                            label: 'And One More',
                            hidden: false
                        },
                        {
                            name: 'andOneAgain',
                            label: 'And One Again',
                            hidden: false
                        }
                    ],
                    _rows = [],
                    names = ('Black Cat,White Cat,Very Fat Cat,Funny Cat').split(','),
                    i = 0,
                    len = numberOfRows || defaultNumberOfRows;

                for(; i < len; i++){
                    var someName = names[parseInt(Math.random() * names.length)];

                    _rows.push({
                        someId: i,
                        someName: someName,
                        preferences: someName === 'Black Cat' || someName === 'White Cat' ? 'Milk & Sour Cream' : (someName === 'Very Fat Cat' ? 'Fresh Meat' : 'Rats'),
                        someValue: parseInt(Math.random() * 1000000000000),
                        someDate: new Date(parseInt( Math.random() * (new Date().getTime()))),
                        columnToHide: 'Hidden Cell',
                        oneMoreRow: '',
                        andOneMore: 'Some looooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooong text',
                        andOneAgain: '$%!@^$#%^$^!%@$#^*^%*!@%$*'
                    });
                }

                return {
                    cols: _cols,
                    //rows: []
                    rows: _rows
                };
            };
        }
    ]);


}(angular.module('bw.grid')));
