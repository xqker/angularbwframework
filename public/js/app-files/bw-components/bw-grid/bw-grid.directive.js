(function(module){

    'use strict';

    /**
     * ToDo:
     *  - add scroll watchers
     *
     * */

    module.directive('bwGrid', [
        function(){
            return {
                restrict: 'A',
                scope: {
                    model: '=bwGrid',
                    userSettings: '=bwGridSettings'
                },
                template: '' +
                '<div class="bw-grid-container view-type-{{settings.viewType}}">' +
                    '<div class="bw-grid-table-wrapper">' +
                        '<div class="bw-grid-static-container">' +
                            '<table>' +
                                '<thead>' +
                                    '<tr class="header-columns"></tr>' +
                                '</thead>' +
                                '<tbody></tbody>' +
                            '</table>' +
                        '</div>' +
                    '</div>' +
                    '<div class="bw-grid-groups-wrapper">' +
                        '<table>' +
                            '<thead>' +
                                '<tr class="groups-header-wrapper"><td class="header-cell"></td></tr>' +
                            '</thead>' +
                            '<tbody></tbody>' +
                        '</table>' +
                    '</div>' +
                    '<div class="bw-grid-checkboxes-wrapper">' +
                        '<table>' +
                            '<thead>' +
                                '<tr>' +
                                    '<td class="select-all-checkbox-cell" ng-click="toggleGlobalSelection()">' +
                                        '<div class="select-all-checkbox">' +
                                            '<i class="icon-check" ng-show="allRowsAreSelected"></i>' +
                                        '</div>' +
                                    '</td>' +
                                '</tr>' +
                            '</thead>' +
                            '<tbody></tbody>' +
                        '</table>' +
                    '</div>' +
                    '<div class="bw-grid-info-wrapper">' +
                        '<table>' +
                            '<thead>' +
                                '<tr><td class="just-empty-cell"></td></tr>' +
                            '</thead>' +
                            '<tbody></tbody>' +
                        '</table>' +
                    '</div>' +
                    '<div class="bw-grid-floating-header-wrapper">' +
                        '<div class="bw-grid-floating-header-container">' +
                            '<div class="main-table-header">' +
                                '<div class="bw-grid-static-container">' +
                                    '<table><thead><tr class="header-columns"></tr></tbody></table>' +
                                '</div>' +
                            '</div>' +
                            '<div class="checkboxes-table-header">' +
                                '<table>' +
                                    '<thead>' +
                                        '<tr>' +
                                            '<td class="select-all-checkbox-cell" ng-click="toggleGlobalSelection()">' +
                                                '<div class="select-all-checkbox">' +
                                                    '<i class="icon-check" ng-show="allRowsAreSelected"></i>' +
                                                '</div>' +
                                            '</td>' +
                                        '</tr>' +
                                    '</thead>' +
                                '</table>' +
                            '</div>' +
                            '<div class="groups-table-header">' +
                                '<table>' +
                                    '<thead>' +
                                        '<tr class="groups-header-wrapper"><td class="header-cell"></td></tr>' +
                                    '</thead>' +
                                '</table>' +
                            '</div>' +
                            '<div class="info-table-header">' +
                                '<table>' +
                                    '<thead>' +
                                        '<tr><td class="just-empty-cell"></td></tr>' +
                                    '</thead>' +
                                '</table>' +
                            '</div>' +
                        '</div>' +
                    '</div>' +
                    '<div class="bw-grid-slider-h">' +
                        '<div class="bw-grid-slider-caret"></div>' +
                    '</div>' +
                    '<div class="bw-grid-slider-v">' +
                        '<div class="bw-grid-slider-caret"></div>' +
                    '</div>' +
                    '<div class="bw-page-overlay"></div>' +
                    '<div class="bw-grid-overlay">' +
                        '<div>LOADING...</div>' +
                    '</div>' +
                '</div>' +
                '',
                controller: BWGridDirectiveCtrl
            };
        }
    ]);

    BWGridDirectiveCtrl.$inject = ['$scope', '$element', '$filter', 'd3', 'moment', 'BWColorsService', 'BWSharedService'];
    function BWGridDirectiveCtrl ($scope, $element, $filter, d3, moment, BWColorsService, BWSharedService){
        var self = this,
            selectNewItems = false,
            watchers = [],
            selectedIndexes = [],
            deselectedIndexes = [],
            collapsedGroups = [],
            defaultSettings = {
                viewType: 'A',
                showFirstColumn: true,
                showLastColumn: true,

                visibleRows: 20,

                headerFontSize: 12,
                fontSize: 12,
                rowHeight: 26,
                firstColumnWidth: 24,
                lastColumnWidth: 38,
                groupsColumnWidth: 170,
                allRowsAreChecked: false,
                preventLoading: false,

                colors: {
                    even: BWColorsService.blackWhitePalette.white,
                    odd: BWColorsService.blackWhitePalette.grayLightest,
                    hover: BWColorsService.brandPalette.bwBlueLightest,
                    selected: BWColorsService.brandPalette.bwBlueLightest // NEED TO CHANGE!!!
                },
                callbacks: {
                    onFirstColHeaderClick: angular.noop,
                    onColumnHeaderClick: angular.noop,

                    onRowSelect: angular.noop,
                    onLinkClicked: angular.noop,

                    onInfoIconEnter: angular.noop,
                    onInfoIconOut: angular.noop,
                    onInfoIconClick: angular.noop,

                    onFirstColumnClick: angular.noop,

                    endOfListReached: angular.noop
                },
                watchers: []
            };

        self.wrapper = d3.select($element.get(0)).classed('bw-grid-wrapper', true);
        self.container = self.wrapper.select('.bw-grid-container');
        self.mainTable = self.container.select('.bw-grid-table-wrapper').select('.bw-grid-static-container');
        self.groupsColumn = self.container.select('.bw-grid-groups-wrapper');
        self.checkboxesColumn = self.container.select('.bw-grid-checkboxes-wrapper');
        self.infoColumn = self.container.select('.bw-grid-info-wrapper');
        self.horizontalSlider = self.container.select('.bw-grid-slider-h');
        self.verticalSlider = self.container.select('.bw-grid-slider-v');
        self.pageOverlay = self.container.select('.bw-page-overlay');
        self.gridOverlay = self.container.select('.bw-grid-overlay');
        self.floatingHeader = self.container.select('.bw-grid-floating-header-wrapper');

        self._data_ = null;
        self._cols_ = null;
        self._rows_ = null;
        self._groups_ = null;

        Object.defineProperties($scope, {
            settings: {
                get: function(){
                    return angular.merge(defaultSettings, ($scope.userSettings || {}));
                }
            },
            tableWidth: {
                get: function(){
                    return self.mainTable.select('table').node().clientWidth;
                }
            },
            wrapperWidth: {
                get: function(){
                    return self.mainTable.node().clientWidth;
                }
            },
            wrapperHeight: {
                get: function(){
                    return $scope.settings.rowHeight * $scope.settings.visibleRows;
                }
            },
            showFloatingHeader: {
                get: function(){
                    return $scope.settings.groupBy || (!$scope.settings.groupBy && $scope.settings.visibleRows === null);
                }
            },
            firstRowIndex: {
                value: 0,
                writable: true
            },
            isWaiting: {
                value: false,
                writable: true
            },
            allRowsAreSelected: {
                value: false,
                writable: true
            },
            toggleGlobalSelection: {
                value: function (){
                    $scope.model._api_[$scope.allRowsAreSelected ? 'deselectAll' : 'selectAll']();
                    $scope.settings.callbacks.onFirstColHeaderClick($scope.allRowsAreSelected);
                }
            }
        });

        Object.defineProperties($scope.model._api_, {
            refreshData: {
                value: function(data, scrollTo){
                    self._data_ = data || self._data_;

                    self._data_.cols.getItems().forEach(function(col){
                        col.gridOptions = col.gridOptions || {};
                        col.gridOptions.isReversed = col.gridOptions.isReversed || false;
                    });

                    $scope.firstRowIndex = isNaN(scrollTo) ? $scope.firstRowIndex : scrollTo;

                    $scope.model._api_.refreshView();
                }
            },
            refreshView: {
                value: function(scrollTop){
                    /** ViewType 'A' */
                    if($scope.settings.viewType === 'A') {
                        if(scrollTop) {
                            resetSliderPosition();
                        }

                        self._cols_ = getColsRenderingData();
                        self._groups_ = getRowsRenderingData();
                        self._rows_ = {};

                        self.floatingHeader.style({
                            display: $scope.settings.groupBy ? 'block' : ($scope.settings.visibleRows ? 'none' : 'block')
                        });

                        refreshSelections();

                        $scope.settings.groupBy ? refreshGroups() : refreshRows();

                        refreshCols();

                        refreshSliderH();

                        refreshSliderV();

                        addInteractions();
                    } else {
                        /** ViewType 'B' */
                        self._cols_ = getColsRenderingData();
                        self._groups_ = getRowsRenderingData_typeB();
                        self._rows_ = {};

                        refreshCols_typeB();

                        refreshRows_typeB();
                    }
                }
            },
            toggleWaiting: {
                value: function(state){
                    $scope.isWaiting = state;

                    if($scope.isWaiting) {
                        self.pageOverlay.style({display: 'none'});
                        self.gridOverlay.style({display: 'block'});

                        self.pageOverlay.on('mousemove', null);
                        self.pageOverlay.on('mouseup', null);
                    } else {
                        self.gridOverlay.style({display: 'none'});
                    }
                }
            },
            addRows: {
                value: function(rows, scrollTo){
                    $scope.firstRowIndex = isNaN(scrollTo) ? $scope.firstRowIndex : scrollTo;

                    self._data_.rows.addItems(rows);
                    $scope.model._api_.refreshData();
                }
            },
            selectAll: {
                value: function(){
                    if(self._data_.rows.multiselect && !$scope.allRowsAreSelected) {
                        $scope.allRowsAreSelected = true;

                        self._data_.rows.getItems().forEach(function(row){
                            row.select();
                        });

                        selectedIndexes.length = 0;
                        deselectedIndexes.length = 0;
                        selectNewItems = true;
                        $scope.model._api_.refreshView();
                    }
                }
            },
            deselectAll: {
                value: function(){
                    if(self._data_.rows.multiselect && $scope.allRowsAreSelected) {
                        $scope.allRowsAreSelected = false;

                        self._data_.rows.getItems().forEach(function(row){
                            row.deselect();
                        });

                        deselectedIndexes.length = 0;
                        selectedIndexes.length = 0;
                        selectNewItems = false;
                        $scope.model._api_.refreshView();
                    }
                }
            },
            getAffectedIndexes: {
                value: function(){
                    return {
                        selected: selectedIndexes,
                        deselected: deselectedIndexes,
                        allRowsAreSelected: $scope.allRowsAreSelected
                    };
                }
            },
            clearGroups: {
                value: function(){
                    collapsedGroups.length = 0;
                }
            },
            toggleRow: { // Do we need to keep this API ?
                value: function(index){
                    //toggleRow(self._data_.rows.getItemsByProperty($scope.settings.indexBy, index)[0]);
                    //$scope.model._api_.refreshView();
                }
            },
            toggleRows: {
                value: function(indexes, state, preventSelection){
                    (angular.isArray(indexes) ? indexes : [indexes]).forEach(function(index){
                        toggleRow(index, state, preventSelection);
                    });
                    refreshSelections();
                    if(collapsedGroups.length) {
                        $scope.model._api_.refreshView();
                    }
                }
            }
        });

        $scope.settings.watchers.forEach(function(propertyToWatch){
            watchers.push(
                $scope.$watch(
                    function(){
                        return BWSharedService.getProperty(propertyToWatch);
                    },
                    function(curr, prev){
                        if(curr !== prev) {
                            $scope.model._api_.refreshView();
                        }
                    }
                )
            );
        });

        function refreshSelections (){
            var indexBy = $scope.settings.indexBy;

            if(selectNewItems) {
                self._data_.rows.getItems().forEach(function(row){
                    row[deselectedIndexes.indexOf(row.content[indexBy]) === -1 ? 'select' : 'deselect']();
                });

            } else {
                self._data_.rows.getItems().forEach(function(row){
                    row[selectedIndexes.indexOf(row.content[indexBy]) === -1 ? 'deselect' : 'select']();
                });
            }
        }

        function refreshCols (){
            var cols = self.mainTable.select('table')
                .select('thead')
                .select('.header-columns')
                .selectAll('.grid-header-cell')
                .data(self._cols_),
                sizes = [];

            cols.enter().append('td');

            cols.exit().remove();

            cols
                .attr({
                    class: function(d){
                        return 'grid-header-cell ' + (d.entity.isSelected ? 'selected' : '');
                    }
                })
                .style({
                    'text-align': function(d){
                        return d.align;
                    },
                    'font-size': $scope.settings.headerFontSize + 'px'
                })
                .html(function(d){
                    return '' +
                        '<div class="grid-cell-wrapper">' +
                            '<span>' + d.label + '</span>' +
                            '<i class="fa ' + d.arrow + '"></i>' +
                        '</div>' +
                    '';
                })
                .style({
                    width: function(d, i){
                        var width = d3.select(this).select('.grid-cell-wrapper').style('width');
                        sizes.push(width);
                    }
                })
                .on('click', function(d){
                    if(d.entity.isSelected) {
                        d.entity.gridOptions.isReversed = !d.entity.gridOptions.isReversed;
                    } else {
                        d.entity.gridOptions.isReversed = false;
                    }

                    $scope.settings.callbacks.onColumnHeaderClick(d.entity, d.entity.gridOptions.isReversed);
                });

            var floatingHeaderCols = self.floatingHeader
                .select('.bw-grid-static-container')
                .select('thead')
                .select('.header-columns')
                .selectAll('.grid-header-cell')
                .data(self._cols_);

            floatingHeaderCols.enter().append('td');

            floatingHeaderCols.exit().remove();

            floatingHeaderCols
                .attr({
                    class: function(d){
                        return 'grid-header-cell ' + (d.entity.isSelected ? 'selected' : '');
                    }
                })
                .style({
                    'text-align': function(d){
                        return d.align;
                    },
                    'font-size': $scope.settings.headerFontSize + 'px'
                })
                .html(function(d){
                    return '' +
                        '<div class="grid-cell-wrapper">' +
                            '<span>' + d.label + '</span>' +
                            '<i class="fa ' + d.arrow + '"></i>' +
                        '</div>' +
                    '';
                })
                .style({
                    width: function(d, i){
                        d3.select(this).select('.grid-cell-wrapper').style({'width' : sizes[i]});
                    }
                })
                .on('click', function(d){
                    if(d.entity.isSelected) {
                        d.entity.gridOptions.isReversed = !d.entity.gridOptions.isReversed;
                    } else {
                        d.entity.gridOptions.isReversed = false;
                    }

                    $scope.settings.callbacks.onColumnHeaderClick(d.entity, d.entity.gridOptions.isReversed);
                });

            /** Watch window scroll event */
            var gridId = $scope.model.id,
                onScrollWatcher = function() {
                    if ($scope.$$destroyed || !self.wrapper.style('display')) {
                        BWSharedService.removeScrollWatcher(gridId);
                    } else {
                        var clientRect = self.wrapper.node().getBoundingClientRect(),
                            windowHeight = window.innerHeight,
                            offsetTop = clientRect.top,
                            offsetBottom = clientRect.bottom,
                            hideFloatingBlocks = (offsetTop > windowHeight) || (offsetBottom < $scope.settings.rowHeight * 2),
                            minHeight = $scope.settings.rowHeight * 2,
                            containerWidth = self.wrapper.node().clientWidth;

                        if (!$scope.showFloatingHeader || hideFloatingBlocks) {
                            self.floatingHeader.style({display: 'none'});
                            self.horizontalSlider.style({
                                position: 'static',
                                left: clientRect.left + 'px',
                                width: containerWidth + 'px'
                            });
                        } else {
                            self.floatingHeader.style({
                                display: (offsetTop < 0) && (offsetBottom > minHeight) ? 'block' : 'none',
                                width: containerWidth + 'px',
                                left: clientRect.left + 'px'
                            });
                            self.horizontalSlider.style({
                                position: offsetBottom < windowHeight ? 'static' : 'fixed',
                                width: containerWidth + 'px',
                                left: clientRect.left + 'px'
                            });

                            self.floatingHeader
                                .select('.main-table-header')
                                .select('.bw-grid-static-container')
                                .node().scrollLeft = self.mainTable.node().scrollLeft;

                            if (!$scope.settings.preventLoading  && ((offsetBottom - windowHeight) < $scope.settings.rowHeight * 2)) {
                                $scope.settings.callbacks.endOfListReached(self._data_.rows.getItems().length);
                            }
                        }
                    }
                };

            BWSharedService.setScrollWatcher(gridId, onScrollWatcher);
            onScrollWatcher();
        }

        function refreshCols_typeB (){
            var cols = self.mainTable.select('table')
                .select('thead')
                .select('.header-columns')
                .selectAll('.grid-header-cell')
                .data(self._cols_);

            cols.enter().append('td').classed('grid-header-cell', true);

            cols.exit().remove();

            cols
                .style({
                    'text-align': function(d){
                        return d.align;
                    },
                    'font-size': $scope.settings.headerFontSize + 'px'
                })
                .html(function(d){
                    return '' +
                        '<div class="grid-cell-wrapper">' +
                            '<span>' + d.label + '</span>' +
                        '</div>' +
                    '';
                })

        }

        function refreshRows_typeB (){
            var rowsData = [],
                totals = {};

            self._groups_.forEach(function(group){
                if(group.label !== '%GRAND_TOTALS%') {
                    rowsData = rowsData.concat(group.rows);
                } else {
                    totals = group;
                }
            });

            var gridRows = self.mainTable.select('table').select('tbody').selectAll('.grid-row').data(rowsData);

            gridRows.enter().append('tr');

            gridRows.exit().remove();

            gridRows
                .attr({
                    class: function(d){
                        return 'grid-row ' + d.rowClass;
                    }
                })
                .style({
                    background: function(d){
                        var row = d3.select(this),
                            cells = row.selectAll('.row-cell').data(d.cells);

                        cells.enter().append('td');

                        cells.exit().remove();

                        cells
                            .attr({
                                class: 'row-cell',
                                colspan: function(c){
                                    return c.colspan;
                                }
                            })
                            .style({
                                'text-align': function(c){
                                    return c.align;
                                }
                            })
                            .html(function(c){
                                return '' +
                                    '<div class="grid-cell-wrapper">' +
                                        '<span>' + c.label + '</span>' +
                                    '</div>' +
                                '';
                            });

                        return d.rowClass === 'odd-row' ? '#fafafa' : '#fff';
                    }
                });

            var emptyRow = self.mainTable.select('table').select('tbody').append('tr').classed('grid-row empty-row', true),
                grandTotalRow = self.mainTable.select('table').select('tbody').append('tr').classed('grid-row grand-total', true),
                grandTotalCells = grandTotalRow.selectAll('.row-cell').data(totals.cells);

            emptyRow.append('td').attr({ colspan: self._cols_.length });

            grandTotalCells.enter().append('td').classed('row-cell', true);

            grandTotalCells.exit().remove();

            grandTotalCells
                .style({
                    'text-align': function(d){
                        return d.align;
                    }
                })
                .html(function(d, i){
                    return '' +
                        '<div class="grid-cell-wrapper">' +
                            '<span>' + d.label + '</span>' +
                        '</div>' +
                    '';
                });
        }

        function refreshGroups (){
            var groups = self.groupsColumn.select('table').select('tbody').selectAll('tr').data(self._groups_),
                rowsData = [],
                groupBorder = '1px solid #cdcdcd',
                rowBorder = '1px solid #e5f7fd',
                groupingLabel = self._data_.cols.getItemsByProperty('name', $scope.settings.groupBy)[0].content.label,
                evenColor = $scope.settings.colors.even,
                oddColor = $scope.settings.colors.odd,
                selectedColor = $scope.settings.colors.selected,
                paddingLeft = $scope.settings.showFirstColumn ? $scope.settings.firstColumnWidth + ($scope.settings.groupBy ? $scope.settings.groupsColumnWidth : 0 ): 0,
                paddingRight = $scope.settings.showLastColumn ? $scope.settings.lastColumnWidth : 0,
                indexingProperty = $scope.settings.indexBy,
                indexingClass = 'entity-index-';

            self.container.select('.bw-grid-table-wrapper').style({
                padding: '0 ' + paddingRight + 'px 0 ' + paddingLeft + 'px'
            });

            self.floatingHeader.select('.bw-grid-floating-header-container').style({
                padding: '0 ' + paddingRight + 'px 0 ' + paddingLeft + 'px'
            });

            self.horizontalSlider.style({
                padding: '1px ' + paddingRight + 'px 1px ' + paddingLeft + 'px'
            });

            self.floatingHeader.select('.bw-grid-floating-header-container').select('.checkboxes-table-header').style({ display: $scope.settings.showFirstColumn ? 'block' : 'none', left: $scope.settings.groupsColumnWidth + 'px' });
            self.floatingHeader.select('.bw-grid-floating-header-container').select('.info-table-header').style({ display: $scope.settings.showLastColumn ? 'block' : 'none' });
            self.floatingHeader.select('.bw-grid-floating-header-container').select('.groups-table-header').style({ display: 'block' });
            self.checkboxesColumn.style({ display: $scope.settings.showFirstColumn ? 'block' : 'none', left: $scope.settings.groupsColumnWidth + 'px' });
            self.infoColumn.style({ display: $scope.settings.showLastColumn ? 'block' : 'none' });
            self.groupsColumn.style({ display: 'block' });

            self.groupsColumn
                .select('table')
                .select('thead')
                .select('.groups-header-wrapper')
                .select('td')
                .style({
                    'font-size': $scope.settings.headerFontSize + 'px'
                })
                .html('' +
                    '<div>' + groupingLabel + '</div>' +
                '');

            self.floatingHeader.select('.bw-grid-floating-header-container').select('.groups-table-header')
                .select('table')
                .select('thead')
                .select('.groups-header-wrapper')
                .select('td')
                .style({
                    'font-size': $scope.settings.headerFontSize + 'px'
                })
                .html('' +
                    '<div>' + groupingLabel + '</div>' +
                '');

            groups.enter().append('tr');

            groups.exit().remove();

            groups.style({
                'border-bottom': function(d){
                    var row = d3.select(this),
                        visibleRows = [],
                        selectedRowsInGroup = d.rows.filter(function(row){
                            return row.entity.isSelected;
                        }),
                        numberOfRowsIfThisGroupIsCollapsed = selectedRowsInGroup.length || 1,
                        rowHeight = $scope.settings.rowHeight * (d.isCollapsed ? numberOfRowsIfThisGroupIsCollapsed : d.rows.length);

                    if(!row.select('td').node()) {
                        row.html('' +
                            '<td class="row-cell">' +
                                '<div>' +
                                    '<i></i>' +
                                    '<span class="group-label"></span>' +
                                    '<span class="number-of-rows"></span>' +
                                '</div>' +
                            '</td>' +
                        '');
                    }
                    if(d.isCollapsed){
                        if(!selectedRowsInGroup.length) {
                            visibleRows[0] = d.rows[0];
                        } else {
                            visibleRows = visibleRows.concat(selectedRowsInGroup);
                        }
                    } else {
                        visibleRows = visibleRows.concat(d.rows);
                    }

                    row.select('td').style({
                        'vertical-align': d.isCollapsed && numberOfRowsIfThisGroupIsCollapsed === 1 ? 'middle' : 'top'
                    });

                    row.select('td').select('div').style({
                        'padding-top': (d.isCollapsed && numberOfRowsIfThisGroupIsCollapsed === 1 ? 0 : 5) + 'px'
                    });

                    row.select('td').select('div').select('i').attr({
                        class: 'fa ' + (d.isCollapsed ? 'fa-caret-right' : 'fa-caret-down')
                    });

                    row.select('td').select('div').select('.group-label').html(d.label);

                    row.select('td').select('div').select('.number-of-rows').html('(' + d.rows.length + ')');

                    row.select('td').style({
                        height: rowHeight + 'px'
                    });

                    visibleRows.forEach(function(visibleRow, i){
                        visibleRow.lastInGroup = i === (visibleRows.length - 1);
                    });

                    rowsData = rowsData.concat(visibleRows);

                    return groupBorder;
                }
            });

            if(!rowsData.length) {
                return;
            }

            rowsData[rowsData.length - 1].lastInList = true;

            var rows = self.mainTable.select('table')
                    .select('tbody').selectAll('.grid-row').data(rowsData),
                firstColumnRows = self.checkboxesColumn.select('table')
                    .select('tbody').selectAll('.checkboxes-column-row').data($scope.settings.showFirstColumn ? rowsData : []),
                lastColumnRows = self.infoColumn.select('table')
                    .select('tbody').selectAll('.info-column-row').data($scope.settings.showLastColumn ? rowsData : []);

            [rows, firstColumnRows, lastColumnRows].forEach(function(r){
                r.enter().append('tr');
                r.exit().remove();
            });

            rows
                .attr({
                    class: function(d, i){
                        self._rows_[d.entity.content[indexingProperty]] = d;
                        return 'grid-row ' + (d.entity.isSelected ? 'selected ' : ' ') + (indexingClass + d.entity.content[indexingProperty]);
                    }
                })
                .style({
                    background: function(d, i){
                        var row = d3.select(this),
                            cells = row.selectAll('.grid-cell').data(d.cells);

                        cells.enter().append('td').classed('grid-cell', true);

                        cells.exit().remove();

                        cells
                            .style({
                                'text-align': function(c){
                                    return c.align;
                                },
                                color: '#4b4a4a',
                                'font-size': $scope.settings.fontSize + 'px'
                            })
                            .html(function(c){
                                return '' +
                                    '<div class="grid-cell-wrapper ' + (c.link ? 'link-cell' : '') + '">' +
                                        '<span>' + c.label + '</span>' +
                                    '</div>' +
                                '';
                            }).select('.link-cell').datum(d.entity);

                        return d.entity.isSelected ? selectedColor : ([oddColor, evenColor][(i + $scope.firstRowIndex) % 2]);
                    },
                    opacity: function(d){
                        return d.entity.hasClass('transparent') ? 0.5 : 1;
                    },
                    'border-bottom': function(d){
                        return d.lastInGroup ? (d.lastInList ? 'none' : groupBorder) : rowBorder;
                    }
                });

            firstColumnRows
                .attr({
                    class: function(d, i){
                        return 'checkboxes-column-row ' + (d.entity.isSelected ? 'selected ' : ' ') + (indexingClass + d.entity.content[indexingProperty]);
                    }
                })
                .style({
                    background: function(d, i){
                        return d.entity.isSelected ? selectedColor : ([oddColor, evenColor][(i + $scope.firstRowIndex) % 2]);
                    },
                    opacity: function(d){
                        return d.entity.hasClass('transparent') ? 0.5 : 1;
                    },
                    'border-bottom': function(d){
                        return d.lastInGroup ? (d.lastInList ? 'none' : groupBorder) : rowBorder;
                    }
                })
                .html(function(d){
                    return '' +
                        '<td>' +
                            '<div class="grid-cell-wrapper">' +
                                '<i class="icon-check"></i>' +
                            '</div>' +
                        '</td>' +
                    ''});

            lastColumnRows
                .attr({
                    class: function(d, i){
                        return 'info-column-row ' + (d.entity.isSelected ? 'selected ' : ' ') + (indexingClass + d.entity.content[indexingProperty]);
                    }
                })
                .style({
                    background: function(d, i){
                        return d.entity.isSelected ? selectedColor : ([oddColor, evenColor][(i + $scope.firstRowIndex) % 2]);
                    },
                    opacity: function(d){
                        return d.entity.hasClass('transparent') ? 0.5 : 1;
                    },
                    'border-bottom': function(d){
                        return d.lastInGroup ? (d.lastInList ? 'none' : groupBorder) : rowBorder;
                    },
                    color: '#4b4a4a'
                })
                .html(function(d){
                    return '' +
                        '<td>' +
                            '<div class="grid-cell-wrapper">' +
                                '<i class="fa fa-info-circle"></i>' +
                            '</div>' +
                        '</td>' +
                    ''});
        }

        function refreshRows (){
            var rowsData = self._groups_.length ? self._groups_[0].rows : [],
                evenColor = $scope.settings.colors.even,
                oddColor = $scope.settings.colors.odd,
                selectedColor = $scope.settings.colors.selected,
                rowBorder = '1px solid #e5f7fd',
                fromRow = $scope.settings.visibleRows === null ? 0 : $scope.firstRowIndex,
                toRow = $scope.settings.visibleRows === null ? rowsData.length : fromRow + $scope.settings.visibleRows,
                paddingLeft = $scope.settings.showFirstColumn ? $scope.settings.firstColumnWidth : 0,
                paddingRight = $scope.settings.showLastColumn ? $scope.settings.lastColumnWidth : 0,
                indexingProperty = $scope.settings.indexBy,
                indexingClass = 'entity-index-';

            self.container.select('.bw-grid-table-wrapper').style({
                padding: '0 ' + paddingRight + 'px 0 ' + paddingLeft + 'px'
            });

            self.floatingHeader.select('.bw-grid-floating-header-container').style({
                padding: '0 ' + paddingRight + 'px 0 ' + paddingLeft + 'px'
            });

            self.horizontalSlider.style({
                padding: '1px ' + paddingRight + 'px 1px ' + paddingLeft + 'px'
            });

            if(rowsData.length <= $scope.settings.visibleRows) {
                $scope.firstRowIndex = fromRow = 0;
                toRow = rowsData.length;
            } else if(rowsData.length < toRow) {
                $scope.firstRowIndex = fromRow = rowsData.length - $scope.settings.visibleRows - 1;
                toRow = fromRow + $scope.settings.visibleRows;
            }

            self.floatingHeader.select('.bw-grid-floating-header-container').select('.checkboxes-table-header').style({ display: $scope.settings.showFirstColumn ? 'block' : 'none', left: 0});
            self.floatingHeader.select('.bw-grid-floating-header-container').select('.info-table-header').style({ display: $scope.settings.showLastColumn ? 'block' : 'none' });
            self.floatingHeader.select('.bw-grid-floating-header-container').select('.groups-table-header').style({ display: 'none' });

            self.checkboxesColumn.style({ display: $scope.settings.showFirstColumn ? 'block' : 'none', left: 0});
            self.infoColumn.style({ display: $scope.settings.showLastColumn ? 'block' : 'none' });
            self.groupsColumn.style({ display: 'none' });

            var rowsRenderingData = rowsData.slice(fromRow, toRow),
                rows = self.mainTable.select('table')
                    .select('tbody').selectAll('.grid-row').data(rowsRenderingData),
                firstColumnRows = self.checkboxesColumn.select('table')
                    .select('tbody').selectAll('.checkboxes-column-row').data($scope.settings.showFirstColumn ? rowsRenderingData : []),
                lastColumnRows = self.infoColumn.select('table')
                    .select('tbody').selectAll('.info-column-row').data($scope.settings.showLastColumn ? rowsRenderingData : []);

            [rows, firstColumnRows, lastColumnRows].forEach(function(r){
                r.enter().append('tr');
                r.exit().remove();
            });

            rows
                .attr({
                    class: function(d, i){
                        self._rows_[d.entity.content[indexingProperty]] = d;
                        return 'grid-row ' + (d.entity.isSelected ? 'selected ' : ' ') + (indexingClass + d.entity.content[indexingProperty]);
                    }
                })
                .style({
                    background: function(d, i){
                        var cells = d3.select(this).selectAll('.grid-cell').data(d.cells);

                        cells.enter().append('td').classed('grid-cell', true);

                        cells.exit().remove();

                        cells
                            .style({
                                'text-align': function(c){
                                    return c.align;
                                },
                                color: '#4b4a4a',
                                'font-size': $scope.settings.fontSize + 'px'
                            })
                            .html(function(c){
                                return '' +
                                    '<div class="grid-cell-wrapper ' + (c.link ? 'link-cell' : '') + '">' +
                                        '<span>' + c.label + '</span>' +
                                    '</div>' +
                                '';
                            }).select('.link-cell').datum(d.entity);

                        return d.entity.isSelected ? selectedColor : ([oddColor, evenColor][(i + $scope.firstRowIndex) % 2]);
                    },
                    opacity: function(d){
                        return d.entity.hasClass('transparent') ? 0.5 : 1;
                    },
                    'border-bottom': rowBorder
                });

            firstColumnRows
                .attr({
                    class: function(d, i){
                        return 'checkboxes-column-row ' + (d.entity.isSelected ? 'selected ' : ' ') + (indexingClass + d.entity.content[indexingProperty]);
                    }
                })
                .style({
                    background: function(d, i){
                        return d.entity.isSelected ? selectedColor : ([oddColor, evenColor][(i + $scope.firstRowIndex) % 2]);
                    },
                    opacity: function(d){
                        return d.entity.hasClass('transparent') ? 0.5 : 1;
                    },
                    'border-bottom': rowBorder
                })
                .html(function(d){
                    return '' +
                        '<td>' +
                            '<div class="grid-cell-wrapper">' +
                                '<i class="icon-check"></i>' +
                            '</div>' +
                        '</td>' +
                ''});

            lastColumnRows
                .attr({
                    class: function(d, i){
                        return 'info-column-row ' + (d.entity.isSelected ? 'selected ' : ' ') + (indexingClass + d.entity.content[indexingProperty]);
                    }
                })
                .style({
                    background: function(d, i){
                        return d.entity.isSelected ? selectedColor : ([oddColor, evenColor][(i + $scope.firstRowIndex) % 2]);
                    },
                    opacity: function(d){
                        return d.entity.hasClass('transparent') ? 0.5 : 1;
                    },
                    color: '#4b4a4a',
                    'border-bottom': rowBorder
                })
                .html(function(d){
                    return '' +
                        '<td>' +
                            '<div class="grid-cell-wrapper">' +
                                '<i class="fa fa-info-circle"></i>' +
                            '</div>' +
                        '</td>' +
                ''});
        }

        function getColsRenderingData (){
            return self._data_.cols.getItems()
                .map(function(col, i){
                    var content = col.content;
                    return content.hidden || content.name === $scope.settings.groupBy ?
                        null :
                        {
                            entity: col,
                            label: content.label,
                            arrow: 'fa-caret-' + (col.gridOptions.isReversed ? 'down' : 'up'),
                            align: content.align,
                            link: content.link
                        };
                })
                .filter(function(col){
                    return col;
                });
        }

        function getRowsRenderingData (){
            var cols = self._data_.cols,
                groups = self._data_.rows.getItemsDividedByGroups($scope.settings.groupBy),
                defaultAlign = 'left',
                defaultType = 'string',
                defaultFormat = 'MMM Do YYYY',
                settings = angular.copy(cols.turnToObject('name')),
                numbRegExp = /^float#(\d)$/,
                key;

            for(key in settings) {
                var colSettings = settings[key];
                colSettings.align = colSettings.align || defaultAlign;
                colSettings.type = colSettings.type || defaultType;
                colSettings.format = colSettings.format || defaultFormat;
                colSettings.before = colSettings.before || '';
                colSettings.after = colSettings.after || '';
                colSettings.link = colSettings.link || false;
                colSettings.getSum = colSettings.getSum || false;

                if(colSettings.name === $scope.settings.groupBy) {
                    colSettings.hidden = true;
                }
            }

            groups.forEach(function(group){
                group.isCollapsed = collapsedGroups.indexOf(group.label) !== -1;
                group.rows = [];

                group.items.forEach(function(row){
                    var content = row.content,
                        cells = [],
                        kkey;

                    for(kkey in settings){
                        if(settings.hasOwnProperty(kkey) && !settings[kkey].hidden){
                            var cellSettings = settings[kkey],
                                value = content[kkey],
                                cellType = cellSettings.type,
                                link = cellSettings.link,
                                isNegative = false,
                                str = '';

                            if(cellType === 'string') {
                                str = value;
                            } else if(cellType === 'bigNumber'){
                                isNegative = parseFloat(value) < 0;
                                str = $filter('bigNumber')(value);
                            } else if(cellType === 'date') {
                                str = moment(value).format(cellSettings.format);
                            } else if(cellType === 'comaSeparated') {
                                isNegative = parseFloat(value) < 0;
                                str = $filter('comaSeparated')(value);
                            } else if(numbRegExp.test(cellType)){
                                isNegative = parseFloat(value) < 0;
                                str = Math.abs(parseFloat(value)).toFixed(numbRegExp.exec(cellType)[1])
                            }

                            cells.push({
                                align: cellSettings.align,
                                label: (isNegative ? '-' : '') + cellSettings.before + str + cellSettings.after,
                                link: link
                            });
                        }
                    }

                    group.rows.push({
                        entity: row,
                        cells: cells
                    });
                });
            });
            return groups.filter(function(group){
                return group.rows.length;
            });
        }

        function getRowsRenderingData_typeB (){
            var cols = self._data_.cols,
                groups = self._data_.rows.getItemsDividedByGroups($scope.settings.groupBy, true),
                defaultAlign = 'left',
                defaultType = 'string',
                defaultFormat = 'MMM Do YYYY',
                settings = angular.copy(cols.turnToObject('name')),
                propertiesForSumCalculation = [],
                grandTotals = {},
                numbRegExp = /^float#(\d)$/,
                key;

            for(key in settings) {
                if(settings.hasOwnProperty(key)) {
                    var colSettings = settings[key];
                    colSettings.align = colSettings.align || defaultAlign;
                    colSettings.type = colSettings.type || defaultType;
                    colSettings.format = colSettings.format || defaultFormat;
                    colSettings.before = colSettings.before || '';
                    colSettings.after = colSettings.after || '';
                    colSettings.link = colSettings.link || false;
                    colSettings.showTotal = colSettings.showTotal || false;
                    colSettings.getSum = colSettings.getSum || false;

                    if(colSettings.showTotal && propertiesForSumCalculation.indexOf(key) === -1){
                        propertiesForSumCalculation.push(key);
                        grandTotals[key] = 0;
                    }
                    if(colSettings.name === $scope.settings.groupBy) {
                        colSettings.hidden = true;
                    }
                }
            }

            groups.forEach(function(group){
                group.totals = {};
                group.rows = [];

                propertiesForSumCalculation.forEach(function(prop){
                    group.totals[prop] = 0;
                });

                group.items.forEach(function(row, i){
                    var content = row.content,
                        cells = [],
                        kkey;

                    for(kkey in settings){
                        if(settings.hasOwnProperty(kkey) && !settings[kkey].hidden){
                            var cellSettings = settings[kkey],
                                value = content[kkey],
                                cellType = cellSettings.type,
                                isNegative = false,
                                str = '';

                            if(cellSettings.getSum) {
                                value = 0;
                                cellSettings.getSum.forEach(function(prop){
                                    value += parseFloat(content[prop]);
                                });
                            }

                            if(group.totals.hasOwnProperty(kkey)) {
                                var floatingValue = parseFloat(value);
                                group.totals[kkey] += floatingValue;
                                grandTotals[kkey] += floatingValue;
                            }

                            if(cellType === 'string') {
                                str = value;
                            } else if(cellType === 'bigNumber'){
                                isNegative = parseFloat(value) < 0;
                                str = $filter('bigNumber')(value);
                            } else if(cellType === 'date') {
                                str = moment(value).format(cellSettings.format);
                            } else if(cellType === 'comaSeparated') {
                                isNegative = parseFloat(value) < 0;
                                str = $filter('comaSeparated')(value);
                            } else if(numbRegExp.test(cellType)){
                                isNegative = parseFloat(value) < 0;
                                str = Math.abs(parseFloat(value)).toFixed(numbRegExp.exec(cellType)[1])
                            }

                            cells.push({
                                align: cellSettings.align,
                                label: (isNegative ? '-' : '') + cellSettings.before + str + cellSettings.after,
                                colspan: 1
                            });

                            /**
                             * ToDo: add new types 'dateRange', 'floatingNumber', ...
                             * */
                        }
                    }

                    group.rows.push({
                        rowClass: i % 2 ? 'odd-row' : 'even-row',
                        entity: row,
                        cells: cells
                    });
                });

                group.rows = group.rows.filter(function(row){
                    return !row.entity.hasClass('skipMe');
                });
            });

            groups = groups.filter(function(group){
                var isEmpty = group.rows.length === 0;

                if(!isEmpty) {
                    var groupHeaderRow = {
                            rowClass: 'group-header',
                            cells: [{
                                align: 'left',
                                label: group.label,
                                colspan: Object.keys(settings).length
                            }]
                        },
                        groupFooterRow = {
                            rowClass: 'group-footer',
                            cells: Object.keys(settings).map(function(prop){
                                var colSettings = settings[prop],
                                    isNegative = parseFloat(group.totals[prop]) < 0,
                                    str = group.totals[prop] || null;

                                if(numbRegExp.test(colSettings.type)){
                                    str = parseFloat(str).toFixed(numbRegExp.exec(colSettings.type)[1]);
                                } else if(colSettings.type === 'comaSeparated') {
                                    str = $filter('comaSeparated')(parseFloat(str));
                                }

                                return {
                                    align: colSettings.align,
                                    hidden: colSettings.hidden,
                                    label: colSettings.showTotal ? (isNegative ? '-' : '') + (colSettings.before + str + colSettings.after) : '',
                                    colspan: 1
                                };
                            }).filter(function(cell){
                                return !cell.hidden;
                            })
                        };

                    group.rows = [groupHeaderRow]
                        .concat(group.rows)
                        .concat(groupFooterRow);
                }

                return !isEmpty;
            });

            if(Object.keys(grandTotals).length){
                groups.push({
                    label: '%GRAND_TOTALS%',
                    cells: Object.keys(settings).map(function(prop, i){
                        var colSettings = settings[prop],
                            isNegative = parseFloat(grandTotals[prop]) < 0,
                            str = grandTotals.hasOwnProperty(prop) ? grandTotals[prop] : null;

                        if(numbRegExp.test(colSettings.type)){
                            str = parseFloat(str).toFixed(numbRegExp.exec(colSettings.type)[1]);
                        } else if(colSettings.type === 'comaSeparated') {
                            str = $filter('comaSeparated')(parseFloat(str));
                        }

                        return {
                            align: !i ? 'left' : colSettings.align,
                            hidden: colSettings.hidden,
                            label: colSettings.showTotal ? (isNegative ? '-' : '') + (colSettings.before + str + colSettings.after) : (!i ? 'Totals:' : ''),
                            colspan: 1
                        };
                    }).filter(function(cell){
                        return !cell.hidden;
                    })
                });
            }

            return groups;
        }

        function getGridSettings (){
            var cols = self._data_.cols,
                defaultAlign = 'left',
                defaultType = 'string',
                defaultFormat = 'MMM Do YYYY',
                settings = angular.copy(cols.turnToObject('name')),
                propertiesForSumCalculation = [],
                grandTotals = {},
                key;

            for(key in settings) {
                if(settings.hasOwnProperty(key)) {
                    var colSettings = settings[key];
                    colSettings.align = colSettings.align || defaultAlign;
                    colSettings.type = colSettings.type || defaultType;
                    colSettings.format = colSettings.format || defaultFormat;
                    colSettings.before = colSettings.before || '';
                    colSettings.after = colSettings.after || '';
                    colSettings.link = colSettings.link || false;
                    colSettings.showTotal = colSettings.showTotal || false;

                    if(colSettings.showTotal && propertiesForSumCalculation.indexOf(key) === -1){
                        propertiesForSumCalculation.push(key);
                        grandTotals[key] = 0;
                    }
                    if(colSettings.name === $scope.settings.groupBy) {
                        colSettings.hidden = true;
                    }
                }
            }

            return settings;
        }

        function addInteractions (){
            self.mainTable.select('table').select('tbody').selectAll('.grid-row')
                .on('click', function(d, i){
                    var event = d3.event;

                    toggleRow(d.entity.content[$scope.settings.indexBy]);

                    $scope.settings.callbacks.onRowSelect(d.entity, {
                        x: event.clientX,
                        y: event.clientY
                    }, event);
                })
                .on('mouseover', onMouseOver)
                .on('mouseout', onMouseOut);

            self.mainTable.select('table').select('tbody').selectAll('.grid-row').selectAll('.grid-cell').selectAll('.link-cell')
                .on('click', function(d, i){
                    var event = d3.event;
                    event.preventDefault();
                    event.stopPropagation();

                    $scope.settings.callbacks.onLinkClicked(d, {
                        x: event.clientX,
                        y: event.clientY
                    }, event);
                });

            self.checkboxesColumn.select('table').select('tbody').selectAll('.checkboxes-column-row')
                .on('click', function(d, i){
                    var event = d3.event;

                    toggleRow(d.entity.content[$scope.settings.indexBy]);

                    $scope.settings.callbacks.onFirstColumnClick(d.entity, {
                        x: event.clientX,
                        y: event.clientY
                    }, event);
                })
                .on('mouseover', onMouseOver)
                .on('mouseout', onMouseOut);

            self.infoColumn.select('table').select('tbody').selectAll('.info-column-row')
                .on('mouseover', onMouseOver)
                .on('mouseout', onMouseOut);

            self.infoColumn.select('table').select('tbody').selectAll('.info-column-row').select('i')
                .on('mouseover', function(d){
                    var clientRect = this.parentNode.getBoundingClientRect();

                    $scope.settings.callbacks.onInfoIconEnter(d.entity, {
                        x: clientRect.left + (clientRect.width / 2),
                        y: clientRect.top
                    });
                })
                .on('mouseout', function(d){
                    $scope.settings.callbacks.onInfoIconOut(d.entity);
                })
                .on('click', function(d){
                    var event = d3.event;

                    $scope.settings.callbacks.onInfoIconClick(d.entity, {
                        x: event.clientX,
                        y: event.clientY
                    }, event);
                });

            self.groupsColumn.select('table').select('tbody').selectAll('tr').select('td').on('click', toggleGroupCollapsing);

            function onMouseOver (d, i){
                var indexingClass = '.entity-index-' + d.entity.content[$scope.settings.indexBy];

                [
                    self.mainTable,
                    self.checkboxesColumn,
                    self.infoColumn
                ].forEach(function(node){
                    node.select('table').select('tbody').select(indexingClass)
                        .classed('hover', true);
                });
            }

            function onMouseOut (d, i){
                var indexingClass = '.entity-index-' + d.entity.content[$scope.settings.indexBy];

                [
                    self.mainTable,
                    self.checkboxesColumn,
                    self.infoColumn
                ].forEach(function(node){
                    node.select('table').select('tbody').select(indexingClass)
                        .classed('hover', false);
                });
            }

            function toggleGroupCollapsing (group){
                var index = collapsedGroups.indexOf(group.label);

                group.isCollapsed = !group.isCollapsed;

                if(group.isCollapsed) {
                    if(index === -1){
                        collapsedGroups.push(group.label);
                    }
                } else {
                    if(index !== -1){
                        collapsedGroups.splice(index, 1);
                    }
                }

                refreshGroups();

                refreshSliderH();

                addInteractions();

                var boundingClientRect = self.wrapper.node().getBoundingClientRect(),
                    sliderPosition = boundingClientRect.bottom - window.innerHeight;

                self.horizontalSlider.style({
                    bottom: (!$scope.settings.visibleRows || sliderPosition > 0 ? sliderPosition -5 : -5) + 'px'
                });
            }
        }

        function refreshSliderH (){
            var sliderCaret = self.horizontalSlider.select('.bw-grid-slider-caret'),
                caretWidth = ($scope.wrapperWidth / $scope.tableWidth) * $scope.wrapperWidth,
                railWidth = $scope.wrapperWidth - caretWidth,
                touchStartPosition = 0,
                caretPosition = (self.mainTable.node().scrollLeft / ($scope.tableWidth - $scope.wrapperWidth)) * railWidth;

            var onMouseDown = function(){
                var event = d3.event;
                touchStartPosition = event.clientX;
                caretWidth = ($scope.wrapperWidth / $scope.tableWidth) * $scope.wrapperWidth;
                railWidth = $scope.wrapperWidth - caretWidth;
                caretPosition = (self.mainTable.node().scrollLeft / ($scope.tableWidth - $scope.wrapperWidth)) * railWidth;

                self.pageOverlay.style({display: 'block', cursor: 'ew-resize'});

                self.pageOverlay
                    .on('mousemove', onMouseMove)
                    .on('mouseup', onMouseUp);
            };

            var onMouseMove = function(){
                var event = d3.event,
                    offset = event.clientX - touchStartPosition,
                    newPosition = caretPosition + offset,
                    scrollPosition;

                event.preventDefault();

                if(newPosition > railWidth) {
                    newPosition = railWidth;
                }
                if(newPosition < 0) {
                    newPosition = 0;
                }

                sliderCaret.style({ 'margin-left': newPosition + 'px' }).classed('hover', true);

                scrollPosition = (newPosition / railWidth) * ($scope.tableWidth - $scope.wrapperWidth);

                self.mainTable.node().scrollLeft = scrollPosition;

                self.floatingHeader
                    .select('.main-table-header')
                    .select('.bw-grid-static-container')
                    .node().scrollLeft = scrollPosition;

                caretPosition = newPosition;
                touchStartPosition = event.clientX;
            };

            var onMouseUp = function(){
                self.pageOverlay.style({display: 'none'});

                self.pageOverlay.on('mousemove', null);
                self.pageOverlay.on('mouseup', null);

                sliderCaret.classed('hover', false);
            };

            self.horizontalSlider.style({ display: ($scope.wrapperWidth < $scope.tableWidth) ? 'block' : 'none' });

            self.floatingHeader
                .select('.main-table-header')
                .select('.bw-grid-static-container')
                .node().scrollLeft = self.mainTable.node().scrollLeft;

            sliderCaret
                .style({
                    width: caretWidth + 'px',
                    'margin-left': caretPosition + 'px'
                })
                .on('mousedown', onMouseDown);
        }

        function refreshSliderV (){
            if($scope.settings.groupBy || !self._groups_.length) {
                self.verticalSlider.style({display: 'none'});
            } else {
                var sliderRail = self.verticalSlider,
                    sliderCaret = sliderRail.select('.bw-grid-slider-caret'),
                    numberOfRows = self._groups_[0].rows.length,
                    tableHeight = $scope.settings.rowHeight * numberOfRows,
                    caretHeight = ($scope.wrapperHeight / tableHeight) * $scope.wrapperHeight,
                    lastRowIndex = numberOfRows - $scope.settings.visibleRows,
                    railHeight = $scope.wrapperHeight - caretHeight,
                    touchStartPosition = 0,
                    caretPosition = $scope.firstRowIndex ? ($scope.firstRowIndex / lastRowIndex) * railHeight : 0;

                var onMouseDown = function(){
                    var event = d3.event;
                    touchStartPosition = event.clientY;
                    caretHeight = ($scope.wrapperHeight / tableHeight) * $scope.wrapperHeight;
                    railHeight = $scope.wrapperHeight - caretHeight;
                    self.pageOverlay.style({display: 'block', cursor: 'ns-resize'});

                    self.pageOverlay
                        .on('mousemove', onMouseMove)
                        .on('mouseup', onMouseUp);

                };

                var onMouseMove = function(){
                    var event = d3.event,
                        offset = event.clientY - touchStartPosition,
                        newPosition = caretPosition + offset;

                    if(newPosition >= railHeight) {
                        newPosition = railHeight;
                    }
                    if(newPosition <= 0) {
                        newPosition = 0;
                    }

                    sliderCaret.style({ 'margin-top' : newPosition + 'px' }).classed('hover', true);

                    caretPosition = newPosition;
                    touchStartPosition = event.clientY;

                    $scope.firstRowIndex = Math.floor((caretPosition / railHeight) * lastRowIndex);

                    if(!$scope.settings.preventLoading && $scope.firstRowIndex === lastRowIndex) {
                        $scope.settings.callbacks.endOfListReached(numberOfRows);
                    }

                    refreshRows();
                    refreshSliderH();
                };

                var onMouseUp = function(){
                    self.pageOverlay.style({display: 'none'});

                    self.pageOverlay.on('mousemove', null);
                    self.pageOverlay.on('mouseup', null);

                    sliderCaret.classed('hover', false);
                };

                sliderRail.style({
                    height: $scope.wrapperHeight + 'px',
                    display: ($scope.wrapperHeight < tableHeight) ? 'block' : 'none'
                });

                sliderCaret
                    .style({
                        height: caretHeight + 'px',
                        'margin-top': caretPosition + 'px'
                    })
                    .on('mousedown', onMouseDown);
            }
        }

        function resetSliderPosition (){
            $scope.firstRowIndex = 0;
            self.verticalSlider.select('.bw-grid-slider-caret').style({'margin-top' : 0});
        }

        function toggleRow (index, state, preventSelection){
            var evenColor = $scope.settings.colors.even,
                oddColor = $scope.settings.colors.odd,
                selectedColor = $scope.settings.colors.selected,
                multiselect = self._data_.rows.multiselect,
                inSelectedIndexes = selectedIndexes.indexOf(index),
                inDeselectedIndexes = deselectedIndexes.indexOf(index),
                inLoadedList = self._rows_[index],
                indexingClass = '.entity-index-' + index;

            if(inLoadedList && !preventSelection) {
                inLoadedList.entity[inLoadedList.entity.isSelected ? 'deselect' : 'select' ]();
                state = inLoadedList.entity.isSelected;
            }

            if(state) {
                if(multiselect) {
                    if($scope.allRowsAreSelected) {
                        deselectedIndexes.splice(inDeselectedIndexes, 1);
                    } else {
                        inSelectedIndexes === -1 ? selectedIndexes.push(index) : '';
                    }
                } else {
                    selectedIndexes[0] = index;
                }
            } else {
                if(multiselect) {
                    if($scope.allRowsAreSelected) {
                        inDeselectedIndexes === -1 ? deselectedIndexes.push(index) : '';
                    } else {
                        selectedIndexes.splice(inSelectedIndexes, 1);
                    }
                } else {
                    selectedIndexes.length = 0;
                }
            }

            [
                self.mainTable, self.checkboxesColumn, self.infoColumn
            ].forEach(function(node){
                node.select('table').select('tbody').select(indexingClass)
                    .classed('selected', state)
                    .style({
                        background: function(d, i){
                            return state ? selectedColor : ([oddColor, evenColor][(i + $scope.firstRowIndex) % 2]);
                        }
                    });
            });
        }
    }
}(angular.module('bw.grid')));
