(function(module){

    'use strict';

    module.service('BWTooltipService', [
        'AsyncAPI',
        function (AsyncAPI){
            var bwTooltipService = this,
                idCounter = 0;

            bwTooltipService.createNew = function(){
                return new BWTooltipEntity(idCounter++);
            };

            /** Tooltip Entity */
            function BWTooltipEntity (counter){
                var bwTooltipEntity = this;

                Object.defineProperties(bwTooltipEntity, {
                    id : {
                        value: '#bw-tooltip-' + counter
                    },
                    _api_ : {
                        value: AsyncAPI.get({})
                    },
                    content: {
                        value: null,
                        writable: true
                    },
                    header: {
                        value: 'HEADER',
                        writable: true
                    }
                });
            }

            BWTooltipEntity.prototype.show = function(){
                return this._api_.applyFn('show');
            };

            BWTooltipEntity.prototype.hide = function(){
                return this._api_.applyFn('hide');
            };

            BWTooltipEntity.prototype.setHeader = function(header){
                this.header = header;
                return this;
            };

            BWTooltipEntity.prototype.setContent = function(content){
                this.content = content;
                return this;
            };

            BWTooltipEntity.prototype.setPosition = function(coords){
                return this._api_.applyFn('setPosition', coords);
            };
        }
    ]);


}(angular.module('bw.tooltip')));

