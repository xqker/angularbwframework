(function(module){

    'use strict';

    module.directive('bwTooltip', [
        function(){
            return {
                restrict: 'A',
                scope: {
                    model: '=bwTooltip',
                    userSettings: '=bwTooltipSettings'
                },
                transclude: true,
                template: '' +
                    '<div class="bw-tooltip-container">' +
                        '<div class="bw-tooltip-inner">' +
                            '<div class="bw-tooltip-header" ng-bind="model.header" ng-show="showHeader"></div>' +
                            '<div class="bw-tooltip-body" ng-transclude></div>' +
                        '</div>' +
                    '</div>' +
                '',
                controller: BWTooltipDirectiveCtrl
            };
        }
    ]);

    BWTooltipDirectiveCtrl.$inject = ['$scope', '$element', '$attrs', '$timeout', 'd3', 'BWColorsService'];
    function BWTooltipDirectiveCtrl ($scope, $element, $attrs, $timeout, d3, BWColorsService){
        var self = this,
            defaultSettings = {
                interactive: false,
                showHeader: false,
                showArrow: false,
                sizes: {
                    width: 200,
                    height: 30
                },
                coords: {
                    x: 0,
                    y: 0
                },
                offsetX: 20,
                offsetY: 15,
                headerColors: {
                    background: BWColorsService.blackWhitePalette.grayDark,
                    font: BWColorsService.blackWhitePalette.white
                },
                bodyColors: {
                    background: BWColorsService.blackWhitePalette.white,
                    font: BWColorsService.blackWhitePalette.grayDark
                },

                /** Callbacks */
                onShow: angular.noop,
                onHide: angular.noop
            };

        self.wrapper = d3.select($element.get(0)).classed('bw-tooltip-wrapper', true);
        self.container = self.wrapper.select('.bw-tooltip-container');
        self.tooltipInner = self.container.select('.bw-tooltip-inner');
        self.tooltipHeader = self.tooltipInner.select('.bw-tooltip-header');
        self.tooltipBody = self.tooltipInner.select('.bw-tooltip-body');
        self.tooltipArrow = self.wrapper.append('div').classed('bw-tooltip-arrow', true);
        self.pageOverlay = self.wrapper.append('div').classed('bw-tooltip-page-overlay', true);

        $scope.userSettings = $scope.userSettings || {};

        Object.defineProperties($scope, {
            settings: {
                get: function(){
                    return angular.merge(defaultSettings, $scope.userSettings);
                }
            }
        });

        self.wrapper.classed('with-arrow', $scope.settings.showArrow);

        Object.defineProperties($scope.model._api_, {
            show: {
                value: function(){
                    var settings = $scope.settings,
                        left = settings.coords.x,
                        top = settings.coords.y,
                        width = settings.sizes.width,
                        height = settings.sizes.height,
                        headerBg = settings.headerColors.background,
                        headerFont = settings.headerColors.font,
                        bodyBg = settings.bodyColors.background,
                        bodyFont = settings.bodyColors.font;

                    self.wrapper
                        .style({
                            left: left + 'px',
                            top: top + 'px',
                            display: 'block',
                            opacity: 0
                        })
                        .transition()
                        .style({  opacity: 1 })
                        .delay(50)
                        .duration(200);

                    self.container.style({
                        width: width + 'px',
                        'min-height': height  + 'px'
                    });

                    self.tooltipHeader.style({
                        'background-color': headerBg,
                        'color': headerFont
                    });

                    self.tooltipBody.style({
                        'background-color': bodyBg,
                        'color': bodyFont
                    });

                    self.pageOverlay.on('click', $scope.model._api_.hide);

                    if(settings.interactive) {
                        self.pageOverlay.style({ display: 'block' });
                    } else {
                        self.container.on('mouseover', $scope.model._api_.hide);
                    }

                    $timeout(function(){
                        refreshView();
                        $scope.settings.onShow();
                    });

                    return $scope.model;
                }
            },
            hide: {
                value: function(){
                    self.wrapper.style({
                        display: 'none'
                    });

                    self.pageOverlay.style({
                        display: 'none'
                    });

                    $scope.settings.onHide();

                    return $scope.model;
                }
            },
            setPosition: {
                value: function(coords){
                    $scope.userSettings.coords = coords;

                    self.wrapper.style({
                        left: coords.x + 'px',
                        top: coords.y + 'px'
                    });

                    refreshView();
                    return $scope.model;
                }
            }
        });

        function refreshView (){
            var tooltipWidth = self.container.node().clientWidth,
                tooltipHeight = self.container.node().clientHeight,
                tooltipHeaderHeight = self.tooltipHeader.node().clientHeight,
                halfWidth = tooltipWidth / 2,
                windowWidth = window.innerWidth,
                windowHeight = window.innerHeight,
                positionX = $scope.settings.coords.x,
                positionY = $scope.settings.coords.y,
                offsetX = $scope.settings.offsetX,
                offsetY = $scope.settings.offsetY,
                spaceLeft = positionX - halfWidth,
                spaceRight = positionX + halfWidth,
                spaceTop = positionY + (tooltipHeight - tooltipHeaderHeight),
                canPlaceUpside = ((positionY - (offsetY + tooltipHeight)) > 0) && ((positionX - halfWidth) > -offsetX) && ((positionX + halfWidth) < (windowWidth + offsetX)),
                canPlaceLeft = (positionX - (tooltipWidth + offsetX)) > 0,
                canPlaceRight = (positionX + (tooltipWidth + offsetX)) < windowWidth,
                marginLeft,
                marginTop;

            if(canPlaceUpside){
                marginLeft = -halfWidth;
                marginTop = -(tooltipHeight + offsetY);

                if(spaceLeft < 0) {
                    marginLeft = -offsetX;
                } else if(spaceRight > windowWidth){
                    marginLeft = -(tooltipWidth - offsetX);
                }

                self.wrapper.classed('placed-upside', true);
                self.wrapper.classed('placed-left', false);
                self.wrapper.classed('placed-right', false);

            } else if(canPlaceLeft){
                marginLeft = -(tooltipWidth + offsetX);
                marginTop = -(tooltipHeaderHeight + (offsetY / 2));

                if(spaceTop > windowHeight){
                    marginTop = -(tooltipHeight - offsetY);
                }

                self.wrapper.classed('placed-upside', false);
                self.wrapper.classed('placed-left', true);
                self.wrapper.classed('placed-right', false);

            } else if(canPlaceRight){
                marginLeft = offsetX;
                marginTop = -(tooltipHeaderHeight + (offsetY / 2));

                if(spaceTop > windowHeight){
                    marginTop = -(tooltipHeight - offsetY);
                }

                self.wrapper.classed('placed-upside', false);
                self.wrapper.classed('placed-left', false);
                self.wrapper.classed('placed-right', true);
            } else {
                marginLeft = -halfWidth;
                marginTop = -(tooltipHeight + offsetY);

                self.wrapper.classed('placed-upside', true);
                self.wrapper.classed('placed-left', false);
                self.wrapper.classed('placed-right', false);
            }

            self.container
                .transition()
                .style({
                    'margin-left':  marginLeft + 'px',
                    'margin-top':  marginTop + 'px'
                })
                .duration(50);
        }
    }


}(angular.module('bw.tooltip')));
