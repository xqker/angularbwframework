(function(module){

    'use strict';

    module.controller('CalendarDemoPageCtrl',[
        '$scope', '$filter', 'd3', 'moment', 'BWCollectionService', 'BWCalendarService', 'BWColorsService', 'BWTooltipService',
        function($scope, $filter, d3, moment, BWCollectionService, BWCalendarService, BWColorsService, BWTooltipService){

            $scope.rangeSelectorDropdown = BWCollectionService.createNew([
                {
                    name: 'month',
                    label: 'Month'
                },
                {
                    name: 'week',
                    label: 'Week'
                },
                {
                    name: 'year',
                    label: 'Year'
                }
            ], {selectedIndex: 0});

            $scope.rangeSelectorDropdownSettings = {
                onSelect: function(d){
                    $scope.BWCalendarSettings.range = d.content.name;
                    d.select();
                    refreshData();
                }
            };

            $scope.BWCalendar = BWCalendarService.createNew();

            $scope.BWCalendarSettings = {
                showGrid: true,
                showDays: true,
                showControllers: true,
                range: $scope.rangeSelectorDropdown.selected.content.name,
                fontSize: 12,
                cellWidth: 75,
                cellHeight: 75,
                colors: ['#054424', '#FFFFFF'],
                format: 'MMMM YYYY',

                //callbacks
                getCellValue: function(d){
                    return $filter('bigNumber')(d.content.value);
                },
                onMouseOver: function(item, coords, index){
                    $scope.BWChartTooltipSettings.sizes = {
                        width: 350
                    };

                    $scope.BWChartTooltipSettings.headerColors = {
                        background: item.content.color,
                        font: BWColorsService.blackWhitePalette.white
                    };

                    $scope.BWChartTooltip.setHeader(item.content.date.format('Do MMMM, YYYY'));
                    $scope.BWChartTooltip.setContent([{label: 'EVENT', value: item.content.event}]);
                    $scope.BWChartTooltip.setPosition(coords);
                    $scope.BWChartTooltip.show();
                },
                onMouseMove: function(item, coords){
                    $scope.BWChartTooltip.setPosition(coords);
                },
                onMouseOut: function(){
                    $scope.BWChartTooltip.hide();
                },
                onClick: function(item, coords, event, toggle){
                    toggle();
                },
                onStepFwdClick: function(momentDate){

                    console.log(momentDate._d);
                    //refreshData(momentDate.toDate());
                },
                onStepBwdClick: function(momentDate){

                    console.log(momentDate._d);
                    //refreshData(momentDate.toDate());
                }};

            $scope.BWChartTooltip = BWTooltipService.createNew();

            $scope.BWChartTooltipSettings = { width: 350 };

            var randomData = BWCollectionService.createNew([]);

            refreshData();

            function refreshData (startDate){
                var newDays = $scope.BWCalendar.getRandomData(startDate, $scope.BWCalendarSettings.range);

                randomData.changeItems(newDays);
                $scope.BWCalendar.refreshData(randomData);
            }
        }
    ]);

}(myApp.angServices));
