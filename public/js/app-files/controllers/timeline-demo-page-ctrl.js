(function(module){

    'use strict';

    module.controller('TimelineDemoPageCtrl', [
        '$scope', 'BWTimelineService', 'BWGridService', 'BWCollectionService',
        function($scope, BWTimelineService, BWGridService, BWCollectionService){
            var gridData = {
                cols: BWCollectionService.createNew([
                    {
                        name: 'id',
                        label: 'ID#',
                        before: '#'
                    },
                    {
                        name: 'timestamp',
                        label: 'Timestamp',
                        type: 'date',
                        format: 'MMM YYYY'
                    },
                    {
                        name: 'event',
                        label: 'Event'
                    },
                    {
                        name: 'description',
                        label: 'Description'
                    },
                    {
                        name: 'eventType',
                        label: 'Event Type'
                    },
                    {
                        name: 'color',
                        label: 'Color'
                    }
                ], {selectedIndex: 1}),
                rows: BWCollectionService.createNew([],  {multiselect: true})
            };

            $scope.BWTimeline = BWTimelineService.createNew();

            $scope.BWGrid = BWGridService.createNew();

            $scope.BWGridSettings = {
                showFirstColumn: true,
                showLastColumn: true,
                watchers: ['windowSize'],
                sortBy: 'timestamp',
                indexBy: 'id',
                visibleRows: 14
            };

            $scope.BWTimelineSettings = {
                watchers: ['windowSize'],
                onRangeChanged: refreshGrid,
                onReady: refreshGrid
            };

            $scope.BWTimeline.refreshData(BWCollectionService.createNew($scope.BWTimeline.getRandomData()));

            function refreshGrid (range, data){
                var highlightedEvents = data(range).map(function(item){
                    return item.content;
                }).sort(function(a, b){
                    var valA = a[$scope.BWGridSettings.sortBy],
                        valB = b[$scope.BWGridSettings.sortBy];

                    if(valA < valB) {
                        return -1;
                    }
                    if(valA > valB) {
                        return 1;
                    }
                    return 0;
                });
                gridData.rows.changeItems(highlightedEvents);
                $scope.BWGrid.refreshData(gridData);
            }

        }
    ]);

}(myApp.angControllers));

