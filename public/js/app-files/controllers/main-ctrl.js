(function(){

    MainCtrl.$inject = ['$scope', '$state', 'BWCollectionService', 'BWSharedService'];
    function MainCtrl ($scope, $state, BWCollectionService, BWSharedService){

        $scope.states = BWCollectionService.createNew(
            [
                {
                    name: 'chart_demo',
                    label: 'Charts',
                    icon: 'fa-line-chart'
                },
                {
                    name: 'grid_demo',
                    label: 'Grid',
                    icon: 'fa-table'
                }/*,
                {
                    name: 'export_demo',
                    label: 'EXPORT',
                    icon: 'fa-file-pdf-o'
                },
                {
                    name: 'annotation_demo',
                    label: 'ANNOTATION',
                    icon: 'fa-pencil'
                }*/,
                {
                    name: 'collection_demo',
                    label: 'Collections',
                    icon: 'fa-sitemap'
                },
                {
                    name: 'tooltip_demo',
                    label: 'Tooltip',
                    icon: 'fa-comment-o'
                },
                {
                    name: 'progress_bar_demo',
                    label: 'Progress Bar',
                    icon: 'fa-tasks'
                },
                {
                    name: 'calendar_demo',
                    label: 'Calendar',
                    icon: 'fa-calendar-check-o'
                },
                {
                    name: 'timeline_demo',
                    label: 'Timeline',
                    icon: 'fa-clock-o'
                },
                {
                    name: 'map_demo',
                    label: 'Maps',
                    icon: 'fa-map-marker'
                },
                {
                    name: 'report_demo',
                    label: 'Report',
                    icon: 'fa-floppy-o'
                },
                {
                    name: 'endless_list_demo',
                    label: 'Endless List',
                    icon: 'fa-sort-amount-desc'
                },
                {
                    name: 'bugs',
                    label: 'Bugs',
                    icon: 'fa-bug'
                }
            ],
            {
                protected: true
            }
        );

        $scope.BWComboboxSettings = {
            indexingProperty: 'label',
            onSelect: function(item){
                item.select();
            }
        };

        $scope.$watch(
            function(){
                return $scope.states.selected && $scope.states.selected.content.name;
            },
            function(curr, prev){
                if(curr && prev) {
                    $state.go($scope.states.selected.content.name);
                }
            }
        );

        var initializer = $scope.$watch(
            function(){
                return $state.current.name;
            },
            function(curr, prev){
                if(curr) {
                    var currentState = $scope.states.getItemsByProperty('name', curr)[0];
                    if(currentState){
                        currentState.select();
                        $scope.states.content.label = $scope.states.selected.content.label;
                        initializer();
                    }
                }
            }
        );

        /** Resize */
        window.addEventListener('resize', function(){
            $scope.$apply(function(){
                BWSharedService.setProperty('windowSize' , window.innerWidth + '_' + window.innerHeight);
            });
        });
    }

    myApp.angControllers.controller("mainCtrl", MainCtrl);
}());