(function(module){

    'use strict';

    module.controller('EndlessListDemoPageCtrl', [
        '$scope', '$q', '$filter', '$timeout', 'd3', 'BWCollectionService', 'BWGridService', 'BWChartService', 'BWMapService', 'BWChartEnums', 'BWEndlessListService',
        function($scope, $q, $filter, $timeout, d3, BWCollectionService, BWGridService, BWChartService, BWMapService, BWChartEnums, BWEndlessListService){

            $scope.endlessListBlocks = BWCollectionService.createNew([]);

            $scope.endlessList = BWEndlessListService.createNew();

            $scope.endlessListSettings = {
                onEndOfListReached: function(){
                    console.log('asdasdasdasd');
                }
            };

            $scope.endlessListBlocks.addItems([generateNewBlock()]);

            refreshVisibleBlocks();

            function generateNewBlock (){
                var chartTypes = BWChartEnums.getChartTypes().getItems(),
                    label = getRandomLabel(),
                    charts = [6, 6, 6, 6, 12].map(function(size){
                        var chartType = chartTypes[parseInt(Math.random() * chartTypes.length)],
                            newChart = BWChartService.createNew(),
                            data = newChart[chartType.content.collectionCreator](newChart[chartType.content.randomizer]());

                        return {
                            model: newChart,
                            data: data,
                            settings: {
                                type: chartType.content.name,
                                autoresize: true
                            },
                            size: size
                        };
                    });

                return {
                    label: label,
                    charts: charts,
                    index: $scope.endlessListBlocks.getItems().length
                };
            }

            function getRandomLabel (){
                var symbols = '0123456789ZXCVBNMASDFGHJKLL0123456789'.split(''),
                    str = '',
                    i = 0;
                for(; i < 12 ; i++ ){
                    str += symbols[parseInt(Math.random() * symbols.length)];
                }
                return str;
            }

            function refreshVisibleBlocks (){
                $timeout(function(){
                    $scope.endlessListBlocks.getItems().forEach(function(block){
                        block.content.charts.forEach(function(chart){
                            chart.model.refreshData(chart.data);
                        });
                    });
                }, 10);
            }


            //var chartTypes = BWChartEnums.getChartTypes(),
            //    firstThreeItems = [],
            //    index = 0;
            //
            //$scope.endlessListItems = BWCollectionService.createNew([]);
            //
            //$scope.endlessListSettings = {
            //    isBusy: true,
            //    onEndOfListReached: function(){
            //        $scope.endlessListSettings.isBusy = true;
            //        generateNewBlock().then(function(res){
            //            $scope.endlessListSettings.isBusy = false;
            //            $scope.endlessListItems.addItems([res]);
            //        });
            //    }
            //};
            //
            //$scope.$watch(
            //    function(){
            //        return $scope.endlessListItems.getHash('index');
            //    },
            //    function(curr, prev){
            //        if(curr !== prev){
            //            $timeout(refreshVisibleBlocks, 10);
            //        }
            //    }
            //);
            //
            //initPage();
            //
            //function initPage (){
            //    if(firstThreeItems.length < 3) {
            //        generateNewBlock(10).then(function(res){
            //            firstThreeItems.push(res);
            //            initPage();
            //        });
            //    } else {
            //        $scope.endlessListItems.addItems(firstThreeItems);
            //        $scope.endlessListSettings.isBusy = false;
            //    }
            //}
            //
            //function refreshVisibleBlocks (){
            //    $scope.endlessListItems.getItems().forEach(function(item){
            //        var content = item.content;
            //        content.model.refreshData(item.content.data);
            //    });
            //}
            //
            //function generateNewBlock (_delay){
            //    var delay = _delay || 1000,
            //        types = chartTypes.getItems(),
            //        newChart = BWChartService.createNew(),
            //        chartType = types[parseInt(Math.random() * types.length)];
            //
            //    return $timeout(function(){
            //        return {
            //            model: newChart,
            //            settings: {
            //                type: chartType.content.name,
            //                autoresize: true
            //            },
            //            index: index++,
            //            data: newChart[chartType.content.collectionCreator](newChart[chartType.content.randomizer]())
            //        };
            //    }, delay);
            //}
        }
    ]);

}(myApp.angControllers));

