(function(module){

    'use strict';

    module.controller('ChartDemoPageCtrl', [
        '$scope', '$state', '$timeout', '$filter', 'd3', 'moment', 'BWCollectionService', 'BWChartService', 'BWChartEnums', 'BWColorsService', 'BWTooltipService', 'AccessPointService',
        function($scope, $state, $timeout, $filter, d3, moment, BWCollectionService, BWChartService, BWChartEnums, BWColorsService, BWTooltipService, AccessPointService){

            /** Chart */
            $scope.BWCharts = BWCollectionService.createNew(
                BWChartService.getChartTypes().getItems().map(function(item, i){
                    var api = BWChartService.createNew(),
                        entity = item.content,
                        settings = {
                            type: entity.name,
                            autoresize: true
                        },
                        dataCollection = api[item.content.collectionCreator]([]);

                    settings[entity.name] = entity.defaultSettings;

                    return {
                        entity: entity,
                        name: entity.name,
                        label: entity.label,
                        settings: settings,
                        isDancing: false,
                        dancingTimer: null,
                        api: api,
                        dataCollection: dataCollection
                    };
                })
            );

            $scope.refreshRandomData = function (block){
                if(!block) {
                    $scope.BWCharts.getItems().forEach($scope.refreshRandomData);
                } else {
                    var randomData = block.content.api[block.content.entity.randomizer]();
                    block.content.dataCollection.changeItems(randomData, {deep: true});
                    block.content.api.refreshData(block.content.dataCollection);
                }
            };

            $scope.getRenderingData = function(block){
                console.log(block.content.api.getRenderingData(block.content.settings[block.content.name]));
            };

            $scope.makeItDance = function (block){
                if(!block){
                    $scope.BWCharts.getItems();
                }
            };

            $scope.BWCharts.getItems().forEach(function(block){
                $scope.refreshRandomData(block);
            });
        }
    ]);

}(myApp.angControllers));

