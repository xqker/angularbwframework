(function(module){

    'use strict';

    module.controller('ProgressBarDemoPageCtrl', [
        '$scope', '$timeout', '$filter', '$state', 'd3', 'moment', 'BWCollectionService', 'BWChartService', 'BWColorsService', 'BWTooltipService', 'BWProgressBarService',
        function($scope, $timeout, $filter, $state, d3, moment, BWCollectionService, BWChartService, BWColorsService, BWTooltipService, BWProgressBarService){

            $scope.progressBars = BWCollectionService.createNew([
                {
                    label: 'RED PROGRESS-BAR',
                    isDancing: false,
                    model: null,
                    timeout: null,
                    settings: {
                        barColor: '#e6433b',
                        singlePoint: true,
                        animationSpeed: 500,
                        barHeight: 7,
                        railHeight: 7,
                        showDescription: true,
                        showFlags: false,
                        showLabel: true,
                        showHelpers: false,
                        autoresize: true,
                        watchers: ['windowSize'],
                        onClick: function(d){
                            if(!d.isSelected) {
                                d.select();
                                $scope.progressBars.getItems()[0].content.model.refreshView();
                            }
                        }
                    }
                },
                {
                    label: 'BLUE PROGRESS-BAR',
                    isDancing: false,
                    model: null,
                    timeout: null,
                    useThisRange: [0, 30],
                    useThisFormat: 'float#2',
                    settings: {
                        barColor: '#00aeef',
                        singlePoint: false,
                        animationSpeed: 1000,
                        barHeight: 7,
                        railHeight: 3,
                        showDescription: false,
                        showFlags: true,
                        showLabel: false,
                        showHelpers: true,
                        range: 'auto',
                        autoresize: true,
                        watchers: ['windowSize']
                    }
                },
                {
                    label: 'GREEN PROGRESS-BAR',
                    isDancing: false,
                    model: null,
                    timeout: null,
                    useThisRange: [0, 750],
                    useThisFormat: 'bigNumber',
                    settings: {
                        barColor: '#09b061',
                        railColor: '#41eb9a',
                        singlePoint: true,
                        animationSpeed: 1300,
                        barHeight: 15,
                        railHeight: 17,
                        showDescription: false,
                        showFlags: false,
                        showLabel: true,
                        showHelpers: false,
                        range: 'auto',
                        autoresize: true,
                        watchers: ['windowSize']
                    }
                }
            ]);

            /** Controls */
            $scope.refreshBars = function(pb){
                if(!pb.content.isDancing) {
                    var randomData = pb.content.model.getRandomProgressBarCollectionData(pb.content.useThisRange, pb.content.useThisFormat),
                        nestedCollection = (pb.nestedCollection ? pb.nestedCollection.changeItems : pb.content.model.getNewProgressBarCollection)(randomData, pb);
                    pb.content.model.refreshData(nestedCollection);
                }
            };

            $scope.makeItDance = function(pb){
                if($state.current.name === 'progress_bar_demo') {
                    var newData = pb.content.model.getRandomProgressBarCollectionData(pb.content.useThisRange, pb.content.useThisFormat);

                    pb.nestedCollection.changeItems(newData);
                    pb.content.isDancing = true;
                    pb.content.model.refreshData(pb.nestedCollection);
                    pb.content.timeout = setTimeout(function(){
                        $scope.makeItDance(pb);
                    }, pb.content.settings.animationSpeed);
                }
            };

            $scope.stopDancing = function(pb){
                pb.content.isDancing = false;
                if(pb.content.timeout) {
                    clearTimeout(pb.content.timeout);
                    pb.content.timeout = null;
                }
            };

            /** Draw first Time */
            $scope.progressBars.getItems().forEach(function(item){
                item.content.model = BWProgressBarService.createNew();
            });

            $timeout(function(){
                $scope.progressBars.getItems().forEach($scope.refreshBars);
            });
        }
    ]);

}(myApp.angControllers));
