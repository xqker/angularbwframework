(function(module){

    'use strict';

    module.controller('GridDemoPageCtrl', [
        '$scope', '$timeout', 'moment', 'BWCollectionService', 'BWGridService', 'BWTooltipService', 'BWChartService', 'BWColorsService', 'BWChartEnums',
        function($scope, $timeout, moment, BWCollectionService, BWGridService, BWTooltipService, BWChartService, BWColorsService, BWChartEnums){
            var d3Palette = BWColorsService.getBWPalette();

            /** Grid */
            var randomData,
                fromRow = 0, toRow = 10,
                portion = 20,
                gridCols = BWCollectionService.createNew([]),
                gridRows = BWCollectionService.createNew([], { multiselect: true});

            $scope.BWGrid = BWGridService.createNew();

            $scope.BWGridSettings = {
                visibleRows: null,
                showFirstColumn: true,
                showLastColumn: true,
                watchers: ['windowSize'],

                sortBy: 'someId',
                //groupBy: 'someName',
                indexBy: 'someId',

                allRowsAreChecked: false,
                callbacks: {
                    onFirstColHeaderClick: function(state){
                        $scope.BWChart.toggleItems($scope.BWChartSettings.indexBy, null, state);
                        $scope.BWGrid.refreshView();
                    },
                    onRowSelect: function(d, coords){
                        var index = d.content[$scope.BWGridSettings.indexBy];

                        $scope.BWChart.toggleItems($scope.BWChartSettings.indexBy, [index], d.isSelected);
                        $scope.BWGrid.refreshView();
                    },
                    onFirstColumnClick: function(d, coords){
                        var index = d.content[$scope.BWGridSettings.indexBy];

                        $scope.BWChart.toggleItems($scope.BWChartSettings.indexBy, [index], d.isSelected);
                        $scope.BWGrid.refreshView();
                    },
                    onInfoIconEnter: function(d, coords){
                        var content = d.content;

                        $scope.BWGridTooltip
                            .setHeader('#' + content.someId)
                            .setContent([
                                {
                                    label: 'NAME',
                                    value: content.someName
                                },
                                {
                                    label: 'VALUE',
                                    value: content.someValue
                                },
                                {
                                    label: 'DATE',
                                    value: moment(content.someValue).format('MMM Do YYYY')
                                }
                            ])
                            .setPosition(coords)
                            .show();

                        $scope.$digest();
                    },
                    onInfoIconOut: function(d){
                        $scope.BWGridTooltip.hide();
                    },
                    onInfoIconClick: function(d, coords){
                        console.log(d, coords);
                    },
                    endOfListReached: function(lastRowIndex){
                        if($scope.BWGridSettings.visibleRows !== null && gridRows.getItems().length < 100){
                            $scope.BWGrid.toggleWaiting(true);

                            var scrollToRow = toRow - ($scope.BWGridSettings.visibleRows / 2);

                            toRow = (lastRowIndex - 1) + portion;

                            $timeout(function(){
                                gridRows.changeItems(getVisibleRows());

                                $scope.BWGrid.refreshData({
                                    cols: gridCols,
                                    rows: gridRows
                                }, scrollToRow);
                                $scope.BWGrid.toggleWaiting(false);
                            }, 2000);
                        } else if($scope.BWGridSettings.visibleRows === null && gridRows.getItems().length < 100){
                            $scope.BWGrid.toggleWaiting(true);

                            toRow += portion;

                            $timeout(function(){
                                gridRows.changeItems(getVisibleRows());

                                $scope.BWGrid.refreshData({
                                    cols: gridCols,
                                    rows: gridRows
                                }, scrollToRow);
                                $scope.BWGrid.toggleWaiting(false);
                            }, 2000);
                        }
                    },
                    onColumnHeaderClick: function(col, isReversed){
                        $scope.BWGridSettings.sortBy = col.content.name;
                        fromRow = 0;
                        toRow = 50;

                        sortRows(isReversed);

                        if(!col.isSelected) {
                            col.select();
                        }

                        gridRows.changeItems(getVisibleRows());

                        $scope.BWGrid.toggleWaiting(true);

                        $timeout(function(){
                            $scope.BWGrid.toggleWaiting(false);
                            $scope.BWGrid.refreshData({
                                cols: gridCols,
                                rows: gridRows
                            }, fromRow);
                        }, 2000);
                    },
                    onLinkClicked: function(link){
                        console.log(link);
                        location.href = '#testlink';
                    }
                }
            };

            /** ScatterPlo Chart */
            $scope.BWChart = BWChartService.createNew();

            $scope.BWChartSettings = {
                type: BWChartEnums.scatterPlotChart,
                autoresize: true,
                indexBy: 'relatedGridId',
                watchers: ['windowSize'],
                linearChart: {
                    axisXName: 'THIS IS AXIS X',
                    axisYName: 'THIS IS AXIS Y',
                    onPointClick: function(point, coords, event, togglePoint){
                        togglePoint();

                        $scope.BWGrid.toggleRows(point.content[$scope.BWChartSettings.indexBy], point.isSelected);
                        //$scope.BWGrid.toggleRow(point.content[$scope.BWChartSettings.indexBy]);
                    }
                }
            };

            /** Tooltip */
            $scope.BWGridTooltip = BWTooltipService.createNew();

            $scope.BWGridTooltipSettings = {
                sizes: {
                    width: 300,
                    height: 50
                }
            };

            /** Grid Data */
            loadData();

            function sortRows (isReversed){
                var sortingProp = $scope.BWGridSettings.sortBy;

                randomData.rows.sort(function(a, b) {
                    var valA = isReversed ? b[sortingProp] : a[sortingProp],
                        valB = isReversed ? a[sortingProp] : b[sortingProp];

                    if(valA > valB) {
                        return 1;
                    }
                    if(valA < valB) {
                        return -1;
                    }
                    return 0;
                });
            }

            function getVisibleRows (){
                if($scope.BWGridSettings.groupBy){
                    return randomData.rows;
                } else {
                    return randomData.rows.slice(fromRow, toRow);
                }
            }

            function loadData (){
                randomData = $scope.BWGrid.getRandomData();

                sortRows();

                gridCols.changeItems(randomData.cols, {selectedIndex: 0});
                gridRows.changeItems(getVisibleRows());

                $scope.BWGrid.refreshData({
                    cols: gridCols,
                    rows: gridRows
                });

                var groupNames = [],
                    groups = [];

                randomData.rows.forEach(function(item){
                    var index = groupNames.indexOf(item.someName);

                    if(index === -1) {
                        groupNames.push(item.someName);
                        groups.push([item]);
                    } else {
                        groups[index].push(item);
                    }
                });

                var scatterplotChartData = $scope.BWChart.getNewScatterPlotChartCollection(
                        groups.map(function(group, i){
                            var items = [],
                                index = i,
                                color = d3Palette(i),
                                isVisible = true;

                            group.forEach(function(item){
                                items.push({
                                    relatedGridId: item[$scope.BWGridSettings.indexBy],
                                    x: parseFloat(Math.random() * 20),
                                    y: parseFloat(Math.random() * 50),
                                    color: color
                                });
                            });

                            return {
                                items: items,
                                index: i,
                                color: color,
                                isVisible: true
                            }
                        })
                    );

                $scope.BWChart.refreshData(scatterplotChartData);
            }

            /** External Controllers */
            $scope.onBigRedButtonClick = function(){
                console.log($scope.BWGrid.getAffectedIndexes());
            };

            $scope.groupingDropdown = BWCollectionService.createNew([
                {
                    name: 'empty',
                    label: 'No Grouping'
                },
                {
                    name: 'someName',
                    label: 'Group By Cat'
                },
                {
                    name: 'preferences',
                    label: 'Group By Cat`s Preferences'
                }
            ], {selectedIndex: 0});

            $scope.groupingDropdownSettings = {
                onSelect: function(item){
                    item.select();
                    fromRow = 0;
                    toRow = 50;

                    if(item.content.name === 'empty') {
                        $scope.BWGridSettings.groupBy = '';
                    } else {
                        $scope.BWGridSettings.groupBy = item.content.name;
                    }

                    gridRows.changeItems(getVisibleRows());
                    $scope.BWGrid.clearGroups();
                    $scope.BWGrid.refreshData({
                        cols: gridCols,
                        rows: gridRows
                    });
                }
            };
        }
    ]);

}(myApp.angServices));

