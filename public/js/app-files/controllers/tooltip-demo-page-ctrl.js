(function(module){

    'use strict';

    module.controller('TooltipDemoPageCtrl', [
        '$scope', '$timeout', 'BWCollectionService', 'BWTooltipService', 'BWColorsService', 'BWChartService', 'd3',
        function($scope, $timeout, BWCollectionService, BWTooltipService, BWColorsService, BWChartService, d3){

            $scope.BWTooltip = BWTooltipService.createNew();
            $scope.BWTooltipSettings = {};

            $scope.onMouseOver = function(label, event) {
                $scope.BWTooltip.setContent(label);
                $scope.BWTooltip.setPosition(getCoords(event));
                $scope.BWTooltip.show();
            };

            $scope.onMouseMove = function(event) {
                $scope.BWTooltip.setPosition(getCoords(event));
            };
            $scope.onMouseOut = function(event) {
                $scope.BWTooltip.hide();
            };

            $scope.labels = [
                'Oh, what a day... what a lovely day!',
                'I live, I die. I LIVE AGAIN!',
                'You know, hope is a mistake. If you can`t fix what`s broken, you`ll, uh... you`ll go insane.',
                'Witness me!!!!!',
                'My name is Max. My world is fire and blood. Once, I was a cop. A road warrior searching for a righteous cause. As the world fell, each of us in our own way was broken. It was hard to know who was more crazy... me... or everyone else.',
                'If I`m gonna die, I`m gonna die historic on the Fury Road!',
                'Do not, my friends, become addicted to water. It will take hold of you, and you will resent its absence!',
                'I am the scales of justice! Con-DUCT-or of the choir of death!'
            ];



            //var d3Palette = BWColorsService.getStdPalette(),
            //    coloredItems = [];
            //
            //for(var i = 0; i < 8; i++ ) {
            //    var color = d3Palette(i);
            //    coloredItems.push({
            //        label: color,
            //        bgColor: color
            //    });
            //}
            //
            ///** Tooltips */
            //$scope.BWTooltip = BWTooltipService.createNew();
            //$scope.BWTooltipSettings = {};
            //
            //$scope.BWTrickyTooltip = BWTooltipService.createNew();
            //$scope.BWTrickyTooltipSettings = {
            //    sizes: {
            //        width: 300,
            //        height: 100
            //    },
            //    interactive: true
            //};
            //
            //$scope.BWTrickyTooltip.setHeader('INTERACTIVE TOOLTIP');
            //
            ///** Chart */
            //$scope.TooltipDemoChart = BWChartService.createNew();
            //$scope.TooltipDemoChartSettings = {
            //    width: 300,
            //    height: 250
            //};
            //
            //var chartData = $scope.TooltipDemoChart.getNewRoundChartCollection(
            //    $scope.TooltipDemoChart.getRandomRoundChartCollection(5)
            //);
            //
            //
            ///** Tiles for Demo */
            //$scope.tiles = BWCollectionService.createNew([
            //    {
            //        name: 'default',
            //        label: 'HOVER TO SEE DEFAULT TOOLTIP',
            //        subItems: [
            //            {
            //                label: 'DEFAULT SETTINGS'
            //            }
            //        ],
            //        onMouseOver: function(tile, item, $event){
            //            tile.select();
            //
            //            $scope.BWTooltipSettings.headerColors = {
            //                background: BWColorsService.blackWhitePalette.grayDark,
            //                font: BWColorsService.blackWhitePalette.white
            //            };
            //
            //            $scope.BWTooltipSettings.sizes = {
            //                width: 200,
            //                height: 110
            //            };
            //
            //            $scope.BWTooltip.setHeader('DEFAULT');
            //            $scope.BWTooltip.setContent('SOME CONTENT');
            //            $scope.BWTooltip.setPosition(getCoords($event));
            //            $scope.BWTooltip.show();
            //        },
            //        onMouseOut: function(){
            //            $scope.BWTooltip.hide();
            //        },
            //        onMouseMove: function(tile, item, $event){
            //            $scope.BWTooltip.setPosition(getCoords($event));
            //        }
            //    },
            //    {
            //        name: 'colors',
            //        label: 'TOOLTIP COLORS CAN BE CUSTOMIZED',
            //        subItems: coloredItems,
            //        onMouseOver: function(tile, item, $event){
            //            tile.select();
            //
            //            $scope.BWTooltipSettings.headerColors = {
            //                background: item.bgColor,
            //                font: BWColorsService.blackWhitePalette.white
            //            };
            //
            //            $scope.BWTooltipSettings.sizes = {
            //                width: 230,
            //                height: 110
            //            };
            //
            //            var hex = item.bgColor,
            //                rgb = d3.rgb(hex),
            //                hsl = d3.hsl(hex);
            //
            //            $scope.BWTooltip.setHeader('COLORED');
            //            $scope.BWTooltip.setContent({
            //                hex: item.bgColor,
            //                rgb: 'rgb ( ' + rgb.r + ', ' + rgb.g + ', ' + rgb.b + ' )',
            //                hsl: 'hsl ( ' + hsl.h.toFixed(0) + ', ' + hsl.s.toFixed(2) + ', ' + hsl.l.toFixed(2) + ' )'
            //
            //            });
            //            $scope.BWTooltip.setPosition(getCoords($event));
            //            $scope.BWTooltip.show();
            //        },
            //        onMouseOut: function(){
            //            $scope.BWTooltip.hide();
            //        },
            //        onMouseMove: function(tile, item, $event){
            //            $scope.BWTooltip.setPosition(getCoords($event));
            //        }
            //    },
            //    {
            //        name: 'withChartInside',
            //        label: 'ANY WIDGET CAN BE PLACED INSIDE',
            //        subItems: [
            //            {
            //                label: 'PIE CHART INSIDE'
            //            }
            //        ],
            //        onMouseOver: function(tile, item, $event){
            //            tile.select();
            //
            //            $scope.BWTooltipSettings.headerColors = {
            //                background: BWColorsService.blackWhitePalette.grayDark,
            //                font: BWColorsService.blackWhitePalette.white
            //            };
            //
            //            $scope.BWTooltipSettings.sizes = {
            //                width: 320,
            //                height: 250
            //            };
            //
            //            $scope.BWTooltip.setHeader('WITH CHART INSIDE');
            //            $scope.BWTooltip.setPosition(getCoords($event));
            //            $scope.BWTooltip.show();
            //
            //            $scope.TooltipDemoChart.refreshData(chartData);
            //        },
            //        onMouseOut: function(){
            //            $scope.BWTooltip.hide();
            //        },
            //        onMouseMove: function(tile, item, $event){
            //            $scope.BWTooltip.setPosition(getCoords($event));
            //        }
            //    }
            //]);
            //
            //$scope.clickableTile = {
            //    label: 'CLICK ME!',
            //    icons: BWCollectionService.createNew([
            //        {
            //            name: 'BOMB',
            //            icon: 'fa-bomb',
            //            color: BWColorsService.blackWhitePalette.black
            //        },
            //        {
            //            name: 'MAGIC',
            //            icon: 'fa-magic',
            //            color: BWColorsService.brandPalette.bwBlueDark
            //        },
            //        {
            //            name: 'COFFEE',
            //            icon: 'fa-coffee',
            //            color: BWColorsService.brandPalette.bwRed
            //        },
            //        {
            //            name: 'BELL',
            //            icon: 'fa-bell',
            //            color: BWColorsService.brandPalette.bwGold
            //        }
            //    ],{
            //        selectedIndex: 0
            //    }),
            //    onClick: function(event){
            //        var boundingClientRect = event.currentTarget.getBoundingClientRect(),
            //            coords = {
            //                x: boundingClientRect.left + (boundingClientRect.width / 2),
            //                y: boundingClientRect.top + (boundingClientRect.height / 2)
            //            };
            //
            //        $scope.BWTrickyTooltip.setPosition(coords);
            //        $scope.BWTrickyTooltip.show();
            //    },
            //    onKeyPress: function(event){
            //        var submitKeyCode = 13;
            //        if(event.keyCode === submitKeyCode) {
            //            $scope.BWTrickyTooltip.hide();
            //        }
            //    }
            //};
            //
            //$scope.showTile = function(tile){
            //    console.log(tile);
            //};
            //
            //$scope.getItemStyle = function(tile, item){
            //    if(tile.content.subItems.length > 1) {
            //        var height = 300 / (tile.content.subItems.length / 4),
            //            width = 25,
            //            bgColor = item.bgColor || '#FFF',
            //            fontColor = item.bgColor ? '#FFF' : '#666',
            //            paddingTop = (height / 2) - 10;
            //
            //        return {
            //            height: height + 'px',
            //            width: width + '%',
            //            'background-color': bgColor,
            //            color: fontColor,
            //            'padding-top': paddingTop + 'px',
            //            'text-align' : 'center',
            //            'font-size': '20px',
            //            display: 'inline-block',
            //            cursor: 'default'
            //        };
            //    } else {
            //        return {
            //            height: '300px',
            //            width: '100%',
            //            'padding-top': '140px',
            //            'text-align' : 'center',
            //            'font-size': '20px',
            //            display: 'inline-block',
            //            cursor: 'default'
            //        };
            //    }
            //};
            //


            function getCoords (event){
                var _event = event.originalEvent,
                    clientX = _event.clientX,
                    clientY = _event.clientY;

                return {
                    x: clientX,
                    y: clientY
                }
            }
        }
    ]);

}(myApp.angControllers));
