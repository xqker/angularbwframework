(function(module){

    'use strict';

    module.controller('BugsFixingPageCtrl', [
        '$scope', '$q', '$filter', '$timeout', 'd3', 'BWCollectionService', 'BWGridService', 'BWChartService', 'BWTooltipService', 'BWMapService', 'AccessPointService',
        function ($scope, $q, $filter, $timeout, d3, BWCollectionService, BWGridService, BWChartService, BWTooltipService, BWMapService, AccessPointService){

            var newChart = BWChartService.createNew();

            $scope.BWTooltip = BWTooltipService.createNew();

            $scope.BWTooltipSettings = {};

            $scope.resizableChart = {
                model: newChart,
                settings: {
                    type: 'activitiesChart',
                    isVisible: true,
                    autoresize: true,

                    sankeyChart: {
                        /** Callbacks */
                        onMouseOver: function(d, coords, event, highlightNode){
                            $scope.BWTooltip.setContent(d.content);
                            $scope.BWTooltip.setPosition(coords);
                            $scope.BWTooltip.show();
                            highlightNode();
                        },
                        onMouseMove: function(d, coords){
                            $scope.BWTooltip.setPosition(coords);
                        },
                        onMouseOut: function(){
                            $scope.BWTooltip.hide();
                        },
                        onClick: function(d){
                            console.log(d);
                        }
                    }

                    //sunburstChart: {
                    //    innerRadius: 4,
                    //    simple: false,
                    //    keys: {
                    //        x: 'x',
                    //        y: 'y'
                    //    }
                    //}
                    //barChart: {
                    //    thin: 15,
                    //    fontSize: 12,
                    //    showAxisX: true,
                    //    showAxisY: true,
                    //    showHorizontal: true,
                    //    axisXName: 'This is axis X',
                    //    axisYName: 'This is axis Y',
                    //    keys: {
                    //        label: 'label'
                    //    },
                    //
                    //    /** Callbacks */
                    //    onMouseOver: function(d, coords, event, callbacks){
                    //        callbacks.highlightBorders();
                    //    },
                    //    onMouseMove: angular.noop,
                    //    onMouseOut: angular.noop,
                    //    onClick: angular.noop,
                    //    getFormattedLabel: $filter('bigNumber')
                    //}
                },
                data: newChart.getNewActivitiesChartCollection(newChart.getRandomActivitiesChartCollection())
                //data: newChart.getNewSunburstChartCollection([])
                //data: newChart.getNewSunburstChartCollection(newChart.getRandomSunburstChartCollection())
                //data: newChart.getNewBarChartCollection(newChart.getRandomBarChartCollection())
            };

            $scope.sizes = BWCollectionService.createNew([
                {
                    label: 'XS-0',
                    value: null
                },
                {
                    label: 'XS-3',
                    value: 3
                },
                {
                    label: 'XS-4',
                    value: 4
                },
                {
                    label: 'XS-6',
                    value: 6
                },
                {
                    label: 'XS-12',
                    value: 12
                }
            ], {selectedIndex: 4});

            $scope.onBigGreenButtonClick = function(){
                $scope.resizableChart.data.changeItems($scope.resizableChart.model.getRandomActivitiesChartCollection(), {deep: true});
                $scope.resizableChart.model.refreshData($scope.resizableChart.data);
            };

            $scope.onBigRedButtonClick = function(){
                $scope.resizableChart.data.changeItems([], {deep: true});
                $scope.resizableChart.model.refreshData($scope.resizableChart.data);
            };

            $scope.resizableChart.model.refreshData($scope.resizableChart.data);

            $scope.$watch(
                function(){
                    return $scope.sizes.selected && $scope.sizes.selected.content.value;
                },
                function(curr, prev){
                    if(prev === null && curr !== prev){
                        $timeout(function(){
                            $scope.resizableChart.model.refreshData($scope.resizableChart.data);
                        }, 10);
                    }
                }
            );
        }
    ]);

}(myApp.angControllers));

