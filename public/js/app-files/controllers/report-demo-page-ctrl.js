(function(module){

    'use strict';

    module.controller('ReportDemoPageCtrl', [
        '$scope', '$q', '$filter', '$timeout', 'd3', 'BWCollectionService', 'BWGridService', 'BWChartService', 'BWMapService', 'AccessPointService',
        function($scope, $q, $filter, $timeout, d3, BWCollectionService, BWGridService, BWChartService, BWMapService, AccessPointService){

            var reportData = null,
                chartSettings = {
                    type: 'barChart',
                    isVisible: true,
                    autoresize: true,
                    watchers: ['windowSize'],
                    barChart: {
                        thin: 15,
                        fontSize: 12,
                        showAxisX: true,
                        //showAxisY: true,
                        //showHorizontal: true,
                        axisXName: 'This is axis X',
                        axisYName: 'This is axis Y',
                        keys: {
                            value: 'someValueWhichIsNotDisplayedOnGrid',
                            label: 'label'
                        },

                        /** Callbacks */
                        onMouseOver: function(d, coords, event, callbacks){
                            callbacks.highlightBorders();
                        },
                        onMouseMove: angular.noop,
                        onMouseOut: angular.noop,
                        onClick: angular.noop,

                        getFormattedLabel: function(n){
                            return n;
                        }
                    }
                },
                gridSettings = {
                    viewType: 'B',
                    visibleRows: null,
                    showFirstColumn: true,
                    showLastColumn: true,
                    fontSize: 12,
                    watchers: ['windowSize'],
                    sortBy: 'someId',
                    groupBy: 'month',
                    indexBy: 'someId'
                };

            $scope.previewData = null;

            $scope.report = ['Some Guy`s Account', 'Some Another Guy`s Account'].map(function(accountName){
                return {
                    title: accountName,
                    chart: {
                        model: BWChartService.createNew(),
                        settings: angular.copy(chartSettings),
                        data: null
                    },
                    grid: {
                        model: BWGridService.createNew(),
                        settings: angular.copy(gridSettings),
                        data: {
                            cols: BWCollectionService.createNew(getGridColumns()),
                            rows: BWCollectionService.createNew([], {multiselect: true})
                        }
                    }
                };
            });

            $scope.reportPreview = BWCollectionService.createNew([]);

            $scope.reportPreviewSettings = { isOpened: false };

            $scope.reportControllers = {
                dataIsGenerated: false,
                previewIsReady: false,
                isLoadingOrGeneratingData: false,
                buttons: [
                    {
                        color: 'red-btn',
                        isActive: true,
                        getLabel: function(){
                            return !$scope.reportControllers.dataIsGenerated ? 'GENERATE RANDOM DATA' : 'DATA IS GENERATED';
                        },
                        onClick: function(){
                            if($scope.reportControllers.buttons[0].isActive && !$scope.reportControllers.isLoadingOrGeneratingData) {
                                $scope.reportControllers.isLoadingOrGeneratingData = true;

                                $timeout(function(){
                                    generateData();
                                    $scope.reportControllers.buttons[0].isActive = false;
                                    $scope.reportControllers.buttons[1].isActive = true;
                                    $scope.reportControllers.buttons[2].isActive = false;
                                    $scope.reportControllers.isLoadingOrGeneratingData = false;
                                    $timeout($scope.refreshData);
                                }, 100);
                            }
                        }
                    },
                    {
                        color: 'red-btn',
                        isActive: false,
                        getLabel: function(){
                            return !$scope.reportControllers.previewIsReady ? 'PUBLISH REPORT' : 'REPORT IS PUBLISHED';
                        },
                        onClick: function(){
                            if($scope.reportControllers.buttons[1].isActive && !$scope.reportControllers.isLoadingOrGeneratingData){
                                $scope.reportControllers.buttons[0].isActive = false;
                                $scope.reportControllers.isLoadingOrGeneratingData = true;

                                createReport().then(function(res){
                                    console.log(res);
                                    $scope.previewData = res;
                                    //$scope.reportPreviewSettings.title = 'REPORT ' + res.documentName;
                                    $scope.reportControllers.previewIsReady = true;
                                    $scope.reportControllers.buttons[0].isActive = false;
                                    $scope.reportControllers.buttons[1].isActive = false;
                                    $scope.reportControllers.buttons[2].isActive = true;
                                    $scope.reportControllers.isLoadingOrGeneratingData = false;
                                });
                            }
                        }
                    },
                    {
                        color: 'green-btn',
                        isActive: false,
                        getLabel: function(){
                            return 'CLICK TO SEE REPORT';
                        },
                        onClick: function(){
                            if($scope.reportControllers.buttons[2].isActive && !$scope.reportControllers.isLoadingOrGeneratingData){
                                var dataA = $scope.previewData,
                                    dataB = angular.copy($scope.previewData);

                                $scope.reportPreview.changeItems([dataA, dataB], {selectedIndex: 0, deep: true});
                                $scope.reportPreviewSettings.isOpened = true;
                            }
                        }
                    }
                ]
            };

            $scope.refreshData = function(){
                $scope.report.forEach(function(account, i){
                    if(!account.chart.data) {
                        account.chart.data = account.chart.model.getNewBarChartCollection([]);
                    }

                    account.chart.data.changeItems(reportData[i].chart, {deep: true});
                    account.grid.data.rows.changeItems(reportData[i].grid);
                    account.chart.model.refreshData(account.chart.data);
                    account.grid.model.refreshData(account.grid.data);
                });
            };

            function createReport (){
                var dataToCreateReport = reportData.map(function(datum, i){
                    var account = $scope.report[i];

                    return {
                        grid: {
                            header: account.title,
                            cols: getGridColumns(),
                            rows: datum.grid,
                            settings: account.grid.settings
                        },
                        chart: {
                            data: datum.chart,
                            settings: account.chart.model.getChartSettings().barChart
                        }
                    }
                });

                return AccessPointService.generateReport(dataToCreateReport);
            }

            function generateData (){
                reportData = $scope.report.map(function(){
                    var randomData = generateBarChartData();
                    return {
                        chart: randomData,
                        grid: getGridRows(randomData)
                    };
                });
                $scope.reportControllers.dataIsGenerated = true;
            }

            function generateBarChartData (){
                return [ 'December','January','February','March','April','May','June','July','August','September','October','November'].map(function(month){
                    var barsA = [],
                        barsB = [],
                        numberOfBars = parseInt(Math.random() * 3) + 2,
                        i = 0;

                    [barsA, barsB].forEach(function(setOfBars, n){
                        var color = ['#5490b9', '#fcab41'][n],
                            useNegativeValues = !!n;

                        for(i = 0; i < numberOfBars; i++){
                            setOfBars.push({
                                someId: getRandomLabel(),
                                description: getRandomText(),
                                someValue: parseInt(Math.random() * 1999) + 1,
                                someAbbreviation: ['QW', 'ER', 'SW', 'TG', 'PH', 'CC', 'BV'][parseInt(Math.random() * 7)],
                                someDate: new Date(parseInt( Math.random() * (new Date().getTime()))),
                                someSmallValue: parseFloat(Math.random() * 9),
                                someBigValue: parseFloat(Math.random() * 999999) * -1,
                                someAnotherBigValue: parseFloat(Math.random() * 99999) * -1,
                                someValueWhichIsNotDisplayedOnGrid: getRandomValue(useNegativeValues),
                                color: color,
                                month: month
                            });
                        }
                    });

                    return {
                        bars: barsA.concat(barsB),
                        label: month.substr(0, 3)
                    };
                });
            }

            function getRandomLabel (n){
                var numberOfSymbols = n || 9,
                    i = 0,
                    str = '',
                    symbols = '0123456789ABSFIRGJM'.split(''),
                    symbLen = symbols.length;

                for(; i < numberOfSymbols ; i++){
                    str += symbols[parseInt(Math.random() * symbLen)];
                }

                return str;
            }

            function getRandomText (){
                var numberOfWords = parseInt(Math.random() * 3) + 3,
                    randomWords = 'intervocal iso tonic palstave bundle inscriptively decoke non culpab saransk demonetising exstipulate westport unhinted similarity suberize hasten lineal lifo fleecer hullabaloo ice'.split(' '),
                    i = 0,
                    str = '';

                for(; i < numberOfWords; i++){
                    str += ' ';
                    str += (randomWords[parseInt(Math.random() * randomWords.length)]);
                }

                return str.toUpperCase();
            }

            function getRandomValue (useNegativeValues){
                return (Math.random() * 1500) * (useNegativeValues ? [-1, -1, 1][parseInt(Math.random() * 3)] : 1);
            }

            function getGridColumns (){
                return [
                    {
                        name: 'someId',
                        label: 'Some ID#',
                        width: 12
                    },
                    {
                        name: 'description',
                        label: 'Description',
                        width: 18
                    },
                    {
                        name: 'someValue',
                        label: 'Some Value',
                        align: 'right',
                        width: 7
                    },
                    {
                        name: 'someAbbreviation',
                        label: 'Some Letters',
                        width: 7
                    },
                    {
                        name: 'someDate',
                        label: 'Some Date',
                        type: 'date',
                        format: 'MM/DD/YYYY',
                        width: 10
                    },
                    {
                        name: 'someSmallValue',
                        label: 'Some Small Value',
                        align: 'right',
                        type: 'float#3',
                        width: 10
                    },
                    {
                        name: 'someBigValue',
                        label: 'Some Big Value',
                        before: '$',
                        align: 'right',
                        type: 'comaSeparated',
                        showTotal: true,
                        width: 12
                    },
                    {
                        name: 'someAnotherBigValue',
                        label: 'Some Another Big Value',
                        before: '$',
                        align: 'right',
                        type: 'comaSeparated',
                        showTotal: true,
                        width: 12
                    },
                    {
                        name: 'total',
                        label: 'Total',
                        before: '$',
                        align: 'right',
                        getSum: ['someBigValue', 'someAnotherBigValue'],
                        type: 'comaSeparated',
                        showTotal: true,
                        width: 12
                    }
                ];
            }

            function getGridRows (data){
                var gridData = [];

                data.forEach(function(cluster){
                    gridData = gridData.concat(cluster.bars);
                });

                return gridData;
            }
        }
    ]);

}(myApp.angControllers));

