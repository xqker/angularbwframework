(function(module){

    'use strict';

    module.controller('MapDemoPageCtrl', [
        '$scope', 'BWMapService', 'BWCollectionService', 'BWTooltipService', 'BWGridService', 'BWColorsService', 'AccessPointService',
        function($scope, BWMapService, BWCollectionService, BWTooltipService, BWGridService, BWColorsService, AccessPointService){

            var mapData = BWCollectionService.createNew([]);

            $scope.selectedArea = null;

            $scope.availableMaps = BWCollectionService.createNew([
                {
                    name: 'world',
                    label: 'WORLD',
                    dataLoader: 'getRandomWorldData'
                },
                {
                    name: 'usa',
                    label: 'USA',
                    dataLoader: 'getRandomUSAData'
                }
            ], {selectedIndex: 0});

            $scope.availableMapsSettings = {
                onSelect: function(d){
                    d.select();
                    $scope.refreshData();
                }
            };

            $scope.indexingProperties = BWCollectionService.createNew([
                {
                    name: 'totalPar',
                    label: 'Total Par'
                },
                {
                    name: 'averageYield',
                    label: 'Average Yield'
                },
                {
                    name: 'numberOfSecurities',
                    label: 'Number Of Securities'
                }
            ], {selectedIndex: 0});

            $scope.indexingPropertiesSettings = {
                onSelect: function(d){
                    d.select();
                    $scope.BWMap.toggleLoading(true);

                    setTimeout(function(){
                        $scope.BWMapSettings.indexBy = $scope.indexingProperties.selected.content.name;
                        $scope.BWMap.refreshView();
                    }, 300);
                }
            };

            Object.defineProperties($scope, {
                worldMapIsSelected: {
                    get: function(){
                        return $scope.availableMaps.selected.content.name === 'world';
                    }
                }
            });

            $scope.BWMap = BWMapService.createNew();

            $scope.BWMapSettings = {
                watchers: ['windowSize'],
                indexBy: $scope.indexingProperties.selected.content.name,
                onMouseOver: function(d, coords){
                    $scope.BWMapDemoTooltip.setContent({
                        label: d.name,
                        flag: d.flag
                    });
                    $scope.BWMapDemoTooltip.setPosition(coords);
                    $scope.BWMapDemoTooltip.show();
                },
                onMouseMove: function(d, coords){
                    $scope.BWMapDemoTooltip.setPosition(coords);
                },
                onMouseOut: function(d, coords){
                    $scope.BWMapDemoTooltip.hide();
                },
                onClick:  function(d, coords, callbacks){
                    if(callbacks.zoomIn){
                        callbacks.zoomIn();
                    }

                    $scope.selectedArea = d.entity.content;
                    $scope.$digest();
                }
            };

            $scope.BWMapDemoTooltip = BWTooltipService.createNew();

            $scope.BWMapDemoTooltipSettings = {};

            $scope.refreshData = function(){
                $scope.BWMap.toggleLoading(true);
                $scope.selectedArea = null;

                if($scope.worldMapIsSelected) {
                    $scope.BWMap.getRandomWorldData().then(function(data){
                        mapData.changeItems(data);
                        $scope.BWMap.refreshData(mapData, 'world');
                    });
                } else {
                    $scope.BWMap.getRandomUSAData().then(function(data){
                        mapData.changeItems(data);
                        mapData.getItems().forEach(function(item){
                            item.createNestedCollection(item.content.counties);
                        });
                        $scope.BWMap.refreshData(mapData, 'usa');
                    });
                }
            };

            $scope.onBigRedButtonClick = function(){
                if($scope.selectedArea.flag === 'us') {
                    $scope.availableMaps.getItems()[1].select();
                    $scope.refreshData();
                }
            };

            $scope.refreshData();
        }
    ]);

}(myApp.angControllers));