(function(module){

    'use strict';

    module.controller('ExportDemoPageCtrl', [
        '$scope', '$timeout', 'BondChartService', 'BondGridService', 'BondModalService', 'BondChartCollection', 'AccessPointService', 'd3',
        function($scope, $timeout, BondChartService, BondGridService, BondModalService, BondChartCollection, AccessPointService, d3){

            /** Export */

            $scope.sizeOptions = getSizeOptions();

            $scope.chartTiles = [
                {
                    widthOptions: getSizeOptions(),
                    icon: 'fa-line-chart',
                    name: 'Linear Chart Example',
                    chart: BondChartService.get(),
                    settings: {
                        type: 'linear',
                        isVisible: true,
                        linearChart: {
                            cummulative: false,
                            interpolation: 'linear',
                            scale: 'linear',
                            pointFormat: 'circle',
                            pointSize: 10,
                            legend: 'onHover',
                            vertical: true,
                            reversed: false,
                            showHelper: true
                        }
                    },
                    data: []
                },
                {
                    widthOptions: getSizeOptions(),
                    icon: 'fa-pie-chart',
                    name: 'Pie Chart Example',
                    chart: BondChartService.get(),
                    settings: {
                        type: 'pie',
                        isVisible: true,
                        roundChart: {
                            displayInset: false,
                            appearance: 'always',
                            fontSize: 12
                        }
                    },
                    data: []
                },
                {
                    widthOptions: getSizeOptions(),
                    icon: 'fa-tasks',
                    name: 'Composite Bar Chart Example',
                    chart: BondChartService.get(),
                    settings: {
                        type: 'bar',
                        isVisible: true,
                        barChart: {
                            showHorizontal: false,
                            composite: true,
                            margin: 1
                        }
                    },
                    data: []
                },
                {
                    widthOptions: getSizeOptions(),
                    name: 'Area Chart Example',
                    icon: 'fa-area-chart',
                    chart: BondChartService.get(),
                    settings: {
                        type: 'area',
                        isVisible: false,
                        linearChart: {
                            cummulative: false,
                            interpolation: 'linear',
                            scale: 'linear',
                            pointFormat: 'circle',
                            pointSize: 10,
                            legend: 'onHover',
                            vertical: true,
                            reversed: false,
                            showHelper: true
                        }
                    },
                    data: []
                },
                {
                    widthOptions: getSizeOptions(),
                    name: 'Donut Chart Example',
                    icon: 'fa-circle-o',
                    chart: BondChartService.get(),
                    settings: {
                        type: 'donut',
                        isVisible: false,
                        roundChart: {
                            innerRadius: 5,
                            displayInset: false,
                            appearance: 'always',
                            fontSize: 12
                        }
                    },
                    data: []
                },
                {
                    widthOptions: getSizeOptions(),
                    name: 'Bar Chart Example',
                    icon: 'fa-bar-chart',
                    chart: BondChartService.get(),
                    settings: {
                        type: 'bar',
                        isVisible: false,
                        barChart: {
                            showHorizontal: false,
                            composite: false,
                            margin: 1
                        }
                    },
                    data: []
                },
                {
                    widthOptions: getSizeOptions(),
                    name: 'Cummulative Area Chart Example',
                    icon: 'fa-area-chart',
                    chart: BondChartService.get(),
                    settings: {
                        type: 'area',
                        isVisible: false,
                        linearChart: {
                            cummulative: true,
                            interpolation: 'linear',
                            scale: 'linear',
                            pointFormat: 'circle',
                            pointSize: 10,
                            legend: 'onHover',
                            vertical: true,
                            reversed: false,
                            showHelper: true
                        }
                    },
                    data: []
                },
                {
                    widthOptions: getSizeOptions(),
                    name: 'Thin Donut Chart Example',
                    icon: 'fa-circle-o',
                    chart: BondChartService.get(),
                    settings: {
                        type: 'donut',
                        isVisible: false,
                        roundChart: {
                            innerRadius: 9,
                            displayInset: false,
                            appearance: 'onHover',
                            fontSize: 12
                        }
                    },
                    data: []
                },
                {
                    widthOptions: getSizeOptions(),
                    name: 'Composite Bar Chart Example',
                    icon: 'fa-tasks',
                    chart: BondChartService.get(),
                    settings: {
                        type: 'bar',
                        isVisible: false,
                        barChart: {
                            showHorizontal: false,
                            composite: true,
                            margin: 1
                        }
                    },
                    data: []
                },
                {
                    widthOptions: getSizeOptions(),
                    name: 'Pie Chart Example',
                    icon: 'fa-pie-chart',
                    chart: BondChartService.get(),
                    settings: {
                        type: 'pie',
                        isVisible: false,
                        roundChart: {
                            displayInset: false,
                            appearance: 'always',
                            fontSize: 12
                        }
                    },
                    data: []
                },
                {
                    widthOptions: getSizeOptions(),
                    name: 'Bar Chart Example',
                    icon: 'fa-bar-chart',
                    chart: BondChartService.get(),
                    settings: {
                        type: 'bar',
                        isVisible: false,
                        barChart: {
                            showHorizontal: false,
                            composite: false,
                            margin: 1
                        }
                    },
                    data: []
                }
            ];

            $scope.gridTiles = [
                {
                    name: 'Grid Example',
                    icon: 'fa-th-list',
                    grid: BondGridService.get(),
                    settings: {
                        type: 'grid',
                        isVisible: true
                    },
                    data: []
                }
            ];

            $scope.BondModal = BondModalService.get();

            $scope.formats = BondChartCollection.createNew(BondModalService.getFormats());

            $scope.pageAppearance = BondChartCollection.createNew(BondModalService.getOrientations());

            $scope.rowsPerPageOptions = BondChartCollection.createNew(BondModalService.getRowsPerPageOptions());

            $scope.formats.getItems()[0].select();

            $scope.pageAppearance.getItems()[0].select();

            $scope.rowsPerPageOptions.getItems()[2].select();

            $scope.BondModalSettings = {
                documentTitle: '',
                pageTitle: '',
                tableTitle: ''
            };

            $scope.saveAsPDF = function(){
                var rowsPerPage = $scope.rowsPerPageOptions.selected.n,
                    colsInRow = 12,
                    visibleTiles = $scope.chartTiles.filter(function(tile){
                        return tile.settings.isVisible;
                    }),
                    rows = visibleTiles.map(function(){
                        return {
                            width: 0
                        };
                    }),
                    i = 0,
                    len = rows.length,
                    currentRow = 0;

                for(; i < len; ){
                    var row = rows[currentRow],
                        tile = visibleTiles[i],
                        rowWidth = row.width,
                        tileWidth = tile.widthOptions.selected.width;

                    if(colsInRow < (rowWidth + tileWidth)){
                        currentRow++;
                    } else {
                        tile.row = currentRow;
                        row.width += tileWidth;
                        i++;
                    }
                }

                var data = {
                        grids: $scope.gridTiles.map(function(tile){
                            return {
                                renderingData: tile.grid.getData(),
                                settings: tile.settings
                            };
                        }),
                        charts: visibleTiles.map(function(tile){
                            return {
                                renderingData: tile.chart.getRenderingData(),
                                settings: angular.merge(tile.settings, {
                                    width: tile.widthOptions.selected.width,
                                    ranges: tile.chart.getChartRanges(),
                                    name: tile.name,
                                    row: tile.row
                                })
                            };
                        })
                    };

                $scope.overlayIsShown = true;
                $scope.BondModal.show();
                $scope.BondModal.setLoading();


                AccessPointService.saveAsPDF({
                    items: data,
                    docSettings: {
                        name: $scope.BondModalSettings.documentTitle,
                        pageTitle: $scope.BondModalSettings.pageTitle,
                        tableTitle: $scope.BondModalSettings.tableTitle,
                        format: $scope.BondModalSettings.format + '_' + $scope.BondModalSettings.ppi,
                        orientation: $scope.BondModalSettings.orientation,
                        rowsPerPage: rowsPerPage
                    }
                }).then(function(res){
                    console.log(res);
                    $scope.BondModal.setContent(res);
                });
            };

            $scope.refreshTiles = function(){
                $scope.chartTiles.forEach(function(tile){
                    var type = tile.settings.type;

                    if(type === 'pie' || type === 'donut'){
                        tile.data = tile.chart.getRandomRoundChartCircle(5);
                    } else if(type === 'linear' || type === 'area'){
                        tile.data = tile.chart.getRandomLinearChartCollection(5);
                    } else if(type === 'bar'){
                        tile.data = tile.chart.getRandomBarChartCollection(5);
                    }
                    tile.chart.refreshData(tile.data.getItems());
                });

                $scope.gridTiles.forEach(function(tile){
                    tile.data = tile.grid.getRandomData(150, 4);
                    tile.grid.refreshData(tile.data);
                });
            };

            $scope.changeAppearance = function(){
                $scope.pageAppearance.getItems().filter(function(type){
                    return !type.isSelected;
                })[0].select();
                $scope.BondModalSettings.orientation = $scope.pageAppearance.selected.type;
            };

            $scope.changeFormat = function(format){
                format.select();

                $scope.BondModalSettings.format = format.format;
                $scope.BondModalSettings.ppi = format.ppi;
            };

            $scope.removeTile = function(tile){
                tile.settings.isVisible = !tile.settings.isVisible;
            };

            $scope.refreshTiles();

            function getSizeOptions (n){
                var sizeOptions = BondChartCollection.createNew([
                    {
                        width: 3,
                        label: '25%',
                        icon: 'fa-th'
                    },
                    {
                        width: 6,
                        label: '50%',
                        icon: 'fa-th-large'
                    },
                    {
                        width: 12,
                        label: '100%',
                        icon: 'fa-bars'
                    }
                ]);
                sizeOptions.getItems()[1].select();

                return sizeOptions;
            }

            d3.select(window).on('resize', function(){
                $scope.chartTiles.concat($scope.gridTiles).forEach(function(tile){
                    (tile.chart || tile.grid).refreshView();
                });
            });

            $scope.$watch(
                function(){
                    return $scope.chartTiles.map(function(tile){
                        return tile.widthOptions.selected.label + '_' + tile.settings.isVisible;
                    }).join('_');
                },
                function(curr, prev){
                    if(curr !== prev) {
                        $timeout(
                            function(){
                                $scope.chartTiles.forEach(function(tile){
                                    tile.chart.refreshView();
                                });
                            }
                        );

                    }
                }
            );

            $scope.$watch(
                function(){
                    return [
                            $scope.pageAppearance.selected.type,
                            $scope.formats.selected.name
                        ].join('_');
                },
                function(curr, prev){
                    $scope.BondModalSettings.format = $scope.formats.selected.format;
                    $scope.BondModalSettings.ppi = $scope.formats.selected.ppi;
                    $scope.BondModalSettings.orientation = $scope.pageAppearance.selected.type;
                }
            );

            $scope.$watch(
                function(){
                    return $scope.sizeOptions.selected.label;
                },
                function(curr){
                    var i = $scope.sizeOptions.getItems()
                        .indexOf($scope.sizeOptions.getItemByProperty('label', curr).select());

                    $scope.sizeOptions.getItems()[i].select();

                    $scope.chartTiles.forEach(function(tile){
                        if(tile.settings.isVisible){
                            tile.widthOptions.getItems()[i].select();
                        }
                    });
                }
            );
        }
    ]);

}(myApp.angControllers));

