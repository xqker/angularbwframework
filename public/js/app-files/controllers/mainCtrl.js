(function(){

    MainCtrl.$inject = ['$scope', '$timeout', 'BondChartService', 'BondChartCollection'];
    function MainCtrl ($scope, $timeout, BondChartService, BondChartCollection){
        //this.model = $scope;
        //this.model.title = "HELLO WORLD!";

        /** Chart */
        $scope.BondChart = BondChartService.get();

        $scope.collection = $scope.BondChart.getRandomData();

        $scope.chartTypes = BondChartCollection.createNew([
            {
                label: 'PIE',
                name: 'pie'
            },
            {
                label: 'DONUT',
                name: 'donut'
            },
            {
                label: 'LINEAR',
                name: 'linear'
            },
            {
                label: 'AREA',
                name: 'area'
            },
            {
                label: 'BAR',
                name: 'bar'
            }
        ]);

        $scope.legendAppearanceTypes = BondChartCollection.createNew([
            {
                label: 'ALWAYS',
                name: 'always'
            },
            {
                label: 'NEVER',
                name: 'never'
            },
            {
                label: 'ON HOVER',
                name: 'onHover'
            },
            {
                label: 'ON CLICK',
                name: 'onClick'
            }
        ]);

        $scope.linearChartScales = BondChartCollection.createNew([
            {
                label: 'LINEAR',
                name: 'linear'
            },
            {
                label: 'POW',
                name: 'pow'
            },
            {
                label: 'SQRT',
                name: 'sqrt'
            }
        ]);

        $scope.linearChartInterpolations = BondChartCollection.createNew([
            {
                label: 'LINEAR',
                name: 'linear'
            },
            {
                label: 'MONOTONE',
                name: 'monotone'
            },
            {
                label: 'CARDINAL',
                name: 'cardinal'
            },
            {
                label: 'STEP BEF.',
                name: 'step-before'
            }
        ]);

        $scope.legendAppearanceTypes.getItems()[0].select();

        $scope.chartTypes.getItems()[0].select();

        $scope.linearChartScales.getItems()[0].select();

        $scope.linearChartInterpolations.getItems()[0].select();

        $scope.BondChartSettings = {
            type: $scope.chartTypes.selected.name,
            roundChart: {
                /** Inner radius */
                innerRadius: 5, // means 50%, default value

                /** Legend */
                displayInset: false,
                appearance: $scope.legendAppearanceTypes.selected.name,
                fontSize: 12
            },
            linearChart: {
                /** Data type */
                cummulative: false,
                scale: 'linear',

                /** Points */
                pointFormat: 'circle',
                pointSize: 10,

                /** Appearance */
                legend: 'onHover',
                vertical: true,

                /** Helper */
                showHelper: true
            },
            barChart: {
                showHorizontal: false,
                composite: true,
                margin: 1,
                interpolation: $scope.linearChartInterpolations.selected.name

            }
        };

        $scope.refreshRandomData = function(){
            var type = $scope.collection.selected.type;

            $scope.collection.selected.nestedCollection.getItems().forEach(function(item){
                var newContent, points, bars;

                if(type === 'roundChartData'){
                    newContent = $scope.BondChart.getRandomRoundChartSlice();
                    item.value = newContent.value;
                } else if(type === 'linearChartData') {
                    newContent = $scope.BondChart.getRandomLinearChartLayer();
                    points = item.nestedCollection.getItems();
                    points.forEach(function(point, i){
                        point.y = newContent.items[i].y;
                    });
                } else if(type === 'barChartData') {
                    newContent = $scope.BondChart.getRandomBarChartCluster();
                    bars = item.nestedCollection.getItems();
                    bars.forEach(function(bar, i){
                        bar.value = newContent.items[i].value;
                    });
                }
            });

            $scope.BondChart.refreshData($scope.collection.selected.nestedCollection.getItems());
        };

        $scope.addItem = function(){
            var type = $scope.collection.selected.type,
                itemsLimit = 12,
                collection = $scope.collection.selected.nestedCollection;

            if(collection.itemsLength < itemsLimit) {
                var indexes = [],
                    existingIndexes = collection.getItems().map(function(item){
                        return item.index;
                    }),
                    nextIndex, newItem,
                    i, len;

                for(i = 0, len = itemsLimit; i < len; i++) {
                    indexes.push(i);
                }

                for(i = 0, len = indexes.length; i < len; i++) {
                    if(existingIndexes.indexOf(i) === -1) {
                        nextIndex = indexes[i];
                        break;
                    }
                }

                if(type === 'roundChartData'){
                    newItem = $scope.BondChart.getRandomRoundChartSlice(nextIndex);
                } else if(type === 'linearChartData') {
                    newItem = $scope.BondChart.getRandomLinearChartLayer(nextIndex);
                }

                collection.addItem(newItem, type);

                $scope.BondChart.refreshData(collection.getItems());
            }
        };

        $scope.removeItem = function(item){
            var collection = $scope.collection.selected.nestedCollection;

            if(collection.itemsLength > 2) {
                item.remove();
                $scope.BondChart.refreshData(collection.getItems());
            }
        };

        /** Panels */
        $scope.leftPanelExpanded = true;

        $scope.rightPanelExpanded = true;

        /** Round charts */
        $scope.changeLegendAppearance = function(type){
            if(!type.isSelected) {
                type.select();
                $scope.BondChartSettings.roundChart.appearance = $scope.legendAppearanceTypes.selected.name;
            }
        };

        $scope.changeRadius = function(n){
            var newRadius = $scope.BondChartSettings.roundChart.innerRadius + n;

            if(newRadius >= 1 && newRadius <= 9) {
                $scope.BondChartSettings.roundChart.innerRadius = newRadius;
                $scope.BondChart.refreshView();
            }
        };

        /** Linear charts */
        $scope.changeLinearChartScale = function(type){
            if(!type.isSelected) {
                type.select();
                $scope.BondChartSettings.linearChart.scale = $scope.linearChartScales.selected.name;
            }
        };

        $scope.changeLinearChartInterpolation = function(type){
            if(!type.isSelected) {
                type.select();
                $scope.BondChartSettings.linearChart.interpolation = $scope.linearChartInterpolations.selected.name;
            }
        };

        /** Bar charts */
        $scope.changeBarChartMargin = function(n){
            var newMargin = $scope.BondChartSettings.barChart.margin + n;

            if(newMargin >= 0 && newMargin <= 9) {
                $scope.BondChartSettings.barChart.margin = newMargin;
                $scope.BondChart.refreshView();
            }
        };

        /** Watchers */
        $scope.$watch(
            function(){
                return [$scope.leftPanelExpanded, $scope.rightPanelExpanded].join('_');
            },
            function(curr, prev){
                if(curr !== prev) {
                    $timeout(function(){
                        $scope.BondChart.refreshView();
                    });
                }
            }
        );

        $scope.$watch(
            function(){
                var roundChartSettings = [],
                    linearChartSettings = [],
                    barChartSettings = [],
                    roundChart = $scope.BondChartSettings.roundChart,
                    linearChart = $scope.BondChartSettings.linearChart,
                    barChart = $scope.BondChartSettings.barChart,
                    key;

                for(key in roundChart) {
                    if(roundChart.hasOwnProperty(key)){
                        roundChartSettings.push(roundChart[key]);
                    }
                }

                for(key in linearChart) {
                    if(linearChart.hasOwnProperty(key)){
                        linearChartSettings.push(linearChart[key]);
                    }
                }

                for(key in barChart) {
                    if(barChart.hasOwnProperty(key)){
                        barChartSettings.push(barChart[key]);
                    }
                }

                return  roundChartSettings.concat(linearChartSettings).concat(barChartSettings).join('_');
            },
            function(curr, prev){
                if(curr !== prev) {
                    $scope.BondChart.refreshView();
                }
            }
        );

        $scope.$watch(
            function(){
                return $scope.chartTypes.selected.name;
            },
            function(curr, prev){
                if(!curr) {
                    return;
                }

                if(curr && curr !== prev){
                    if(curr === 'pie' || curr === 'donut'){
                        $scope.collection.getItems()[0].select();
                        $scope.BondChartSettings.type = $scope.chartTypes.selected.name;
                        $scope.BondChart.refreshData($scope.collection.selected.nestedCollection.getItems());
                    } else if(curr === 'linear' || curr === 'area') {
                        $scope.collection.getItems()[1].select();
                        $scope.BondChartSettings.type = $scope.chartTypes.selected.name;
                        $scope.BondChart.refreshData($scope.collection.selected.nestedCollection.getItems());
                    } else if(curr === 'bar') {
                        $scope.collection.getItems()[2].select();
                        $scope.BondChartSettings.type = $scope.chartTypes.selected.name;
                        $scope.BondChart.refreshData($scope.collection.selected.nestedCollection.getItems());
                    }
                }
            }
        );

        /** Draw first time */
        $scope.BondChart.refreshData($scope.collection.selected.nestedCollection.getItems());
    }

    myApp.angControllers.controller("mainCtrl", MainCtrl);
}());