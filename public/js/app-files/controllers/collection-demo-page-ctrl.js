(function(module){

    'use strict';

    module.controller('CollectionDemoPageCtrl', [
        '$scope', 'BWCollectionService', 'BWColorsService', 'BWTooltipService',
        function($scope, BWCollectionService, BWColorsService, BWTooltipService){
            var colors = BWColorsService.d3Colors;

            /** Tooltip */
            $scope.BWCollectionDemoTooltip = BWTooltipService.createNew();
            $scope.BWCollectionDemoTooltipSettings = {
                sizes: {
                    width: 300,
                    height: 100
                },
                interactive: true
            };
            $scope.activeTile = null;
            $scope.tooltipActions = BWCollectionService.createNew([
                {
                    name: 'createCollection',
                    icon: 'fa-sitemap',
                    label: 'CREATE COLLECTION',
                    isDisabled: false,
                    onClick: function(){
                        if(!$scope.activeTile.nestedCollection) {
                            $scope.activeTile.createNestedCollection(getSomeData());
                            $scope.activeTile.select();
                            $scope.BWCollectionDemoTooltip.hide();
                        }
                    }
                },
                {
                    name: 'dropCollection',
                    icon: 'fa-thumbs-o-down',
                    label: 'DROP COLLECTION',
                    isDisabled: true,
                    onClick: function(){
                        if($scope.activeTile.nestedCollection) {
                            $scope.activeTile.dropNestedCollections();
                            $scope.BWCollectionDemoTooltip.hide();
                        }
                    }
                },
                {
                    name: 'selectItem',
                    icon: 'fa-check-circle-o',
                    label: 'SELECT ITEM',
                    isDisabled: false,
                    onClick: function(){
                        if(!$scope.activeTile.isSelected) {
                            $scope.activeTile.select();
                            $scope.BWCollectionDemoTooltip.hide();
                        }
                    }
                },
                {
                    name: 'deselectItem',
                    icon: 'fa-circle-o',
                    label: 'DESELECT ITEM',
                    isDisabled: true,
                    onClick: function(){
                        if($scope.activeTile.isSelected) {
                            $scope.activeTile.deselect();
                            $scope.BWCollectionDemoTooltip.hide();
                        }
                    }
                }
            ]);

            /** Collection */
            $scope.collection = null;

            $scope.createBaseCollection = function(){
                $scope.collection = BWCollectionService.createNew(getSomeData().map(function(item, i){
                    return angular.merge(item, {
                        baseColor: colors[i]
                    });
                }));
            };

            $scope.createNestedCollection = function(item){
                if(!item.nestedCollection) {
                    item.createNestedCollection(getSomeData());
                } else {
                    item.select();
                }
            };

            $scope.getNestedCollections = function(){
                return $scope.collection.getNestedCollections();
            };

            $scope.getAllItems = function(){
                var items = [];
                $scope.getNestedCollections().forEach(function(c){
                    items = items.concat(c.getItems());
                });
                return items;
            };

            $scope.getColor = function(color, i){
                return BWColorsService.getDarkerColor(color, i);
            };

            $scope.onItemClick = function(item, event) {
                event.stopPropagation();

                var boundingClientRect = event.currentTarget.getBoundingClientRect(),
                    coords = {
                        x: boundingClientRect.left + (boundingClientRect.width / 2),
                        y: boundingClientRect.top + (boundingClientRect.height / 2)
                    },
                    level = item.getCollection()._level,
                    actions = $scope.tooltipActions.getItems();

                $scope.activeTile = item;

                actions[0].content.isDisabled = $scope.activeTile.nestedCollection || level > 2;
                actions[1].content.isDisabled = !$scope.activeTile.nestedCollection;
                actions[2].content.isDisabled = $scope.activeTile.isSelected;
                actions[3].content.isDisabled = !$scope.activeTile.isSelected;

                $scope.BWCollectionDemoTooltip.setPosition(coords);
                $scope.BWCollectionDemoTooltip.setHeader(item._id);
                $scope.BWCollectionDemoTooltip.show();
            };

            $scope.dropAllCollections = function(){
                $scope.collection.drop();
                $scope.collection = null;
            };

            function getSomeData (){
                var items = [
                    {
                        name: 'FirstOne',
                        label: 'First One',
                        value: 0
                    },
                    {
                        name: 'SecondOne',
                        label: 'Second One',
                        value: 1
                    },
                    {
                        name: 'ThirdOne',
                        label: 'Third One',
                        value: 2
                    },
                    {
                        name: 'FourthOne',
                        label: 'Fourth One',
                        value: 3
                    },
                    {
                        name: 'FourthOne',
                        label: 'Fourth One',
                        value: 3
                    }
                ];

                return items.slice(0, 3);
            }

            ///** Combobox */
            //$scope.BWCombobox = BWCollectionService.createNew([
            //    {
            //        label: 2
            //    },
            //    {
            //        label: '2'
            //    },
            //    {
            //        label: '3'
            //    },
            //    {
            //        label: 34.54
            //    },
            //    {
            //        label: '55.555'
            //    }
            //]);
            //
            //$scope.BWComboboxSettings = {};
        }
    ]);

}(myApp.angControllers));
