var d3 = require('d3'),
    moment = require('moment'),
    document = require('jsdom').jsdom(),
    fs = require('fs'),
    defaultSettings = {
        pageWidth: 928,
        pageHeight: 1313,
        pageHeaderHeight: 50,
        widthToHeightRelation: 0.5,
        padding: {
            top: 10,
            right: 10,
            bottom: 10,
            left: 10
        }
    },
    width = defaultSettings.pageWidth - (defaultSettings.padding.left + defaultSettings.padding.right),
    height = defaultSettings.pageHeight - (defaultSettings.padding.top + defaultSettings.padding.bottom);

exports.generateReport = function(data, settings){
    var pages = [],
        reportId = generateReportId();

    pages.push(getTitlePage(reportId));

    data.forEach(function(datum){
        var chartBlock = getChart(datum.chart),
            gridBlock = getGrid(datum.grid, chartBlock);

        pages = pages.concat(gridBlock);
    });

    pages.forEach(function(page, i){
        page.index = i;
    });

    return {
        documentName: reportId,
        numberOfPages: pages.length,
        pages: pages,
        format: 'A4',
        orientation: 'Portrait',
        width: defaultSettings.pageWidth,
        height: defaultSettings.pageHeight
    };
};

function getChart (chart){
    var chartHeader = getChartHeader(),
        canvas = d3.select(document.createElement('svg')),
        ranges = getRanges(),
        axisXHeight = 20,
        axisYWidth = getVerticalAxisWidth(),
        axisXLabelHeight = chart.settings.axisXName ? 20 : 0,
        axisYLabelWidth = chart.settings.axisYName ? 20 : 0,
        paddingTop = chart.settings.fontSize * 0.5,
        paddingRight = chart.settings.fontSize * 0.5,
        paddingLeft = axisYWidth + axisYLabelWidth,
        paddingBottom = axisXHeight + axisXLabelHeight,
        canvasWidth = width,
        canvasHeight = 300,
        innerWidth = canvasWidth - (paddingLeft + paddingRight),
        innerHeight = canvasHeight - (paddingTop + paddingBottom),
        clusterSize = innerWidth / chart.data.length,
        barSize =  clusterSize * (chart.settings.thin * 0.049),

        /** Scales */
        renderingScale = d3.scale.linear()
            .domain(ranges)
            .range([innerHeight, 0])
            .nice(),
        barSizeScale = d3.scale.linear()
            .domain([0, renderingScale.domain()[1] - renderingScale.domain()[0]])
            .range([0, innerHeight]),
        zeroPoint = renderingScale(0),

        /** DOM-elements */
        wrapper = canvas.append('g').classed('bw-bar-chart', true),
        axisXLayer = wrapper.append('g').classed('axis-x-layer', true),
        axisYLayer = wrapper.append('g').classed('axis-y-layer', true),
        backgroundLayer = wrapper.append('g').classed('background-layer', true),
        mainLayer = wrapper.append('g').classed('main-layer', true),

        /** Rendering Data*/
        renderingData = getChartRenderingData();

    refreshStructure();

    refreshClusters();

    refreshAxisX();

    refreshAxisY();

    refreshBackground();

    return refreshChartBlock();

    function getChartHeader (){
        return d3.select(document.createElement('div'))
            .style({
                'font-size': '14px',
                'font-weight': 600,
                'text-align': 'left',
                'line-height': defaultSettings.pageHeaderHeight + 'px',
                'padding': '0 10px'
            })
            .html('Report Visualization');
    }

    function getRanges (){
        var ranges = [0, 0],
            valueProperty = chart.settings.keys['value'];

        chart.data.forEach(function(cluster){
            var totalLz = 0,
                totalGz = 0;

            cluster.bars.forEach(function(bar){
                var value = parseFloat(bar[valueProperty]);

                if(value < 0) {
                    totalLz += value;
                } else {
                    totalGz += value;
                }
            });
            ranges[0] = d3.min([ranges[0], totalLz]);
            ranges[1] = d3.max([ranges[1], totalGz]);
        });

        return ranges;
    }

    function getVerticalAxisWidth (){
        var ninjaNode = canvas.append('g').classed('ninja-node', true),
            tickSizes = [],
            ninjaScale = d3.scale.linear()
                .domain(ranges)
                .range([0, canvasHeight - 40])
                .nice(),
            ninjaAxis = d3.svg.axis()
                .scale(ninjaScale)
                .orient('left');

        ninjaNode.call(ninjaAxis);

        ninjaNode.selectAll('.tick')
            .select('text')
            .attr({'font-size': chart.settings.fontSize})
            .text(function(d){
                tickSizes.push(d.toString().length * (chart.settings.fontSize * defaultSettings.widthToHeightRelation));
                return d;
            });

        ninjaNode.remove();

        return Math.ceil(d3.max(tickSizes)) + 10;
    }

    function getChartRenderingData (){
        return chart.data.map(function(cluster){
            var totalLz = 0,
                totalGz = 0,
                valueProperty = chart.settings.keys['value'],
                bars = cluster.bars.map(function(bar){
                    var value = parseFloat(bar[valueProperty]),
                        isLowerThenZero = value < 0;

                    if(isLowerThenZero) {
                        totalLz += value;
                    } else {
                        totalGz += value;
                    }

                    return {
                        entity: bar,
                        value: value,
                        label: bar.label,
                        color: bar.color,
                        isLowerThenZero: isLowerThenZero,
                        size: 0
                    }
                });

            bars.forEach(function(bar){
                if(bar.isLowerThenZero) {
                    bar.size = Math.abs(totalLz);
                    totalLz -= bar.value;
                } else {
                    bar.size = totalGz;
                    totalGz -= bar.value;
                }
            });

            return {
                entity: cluster,
                label: cluster.label,
                bars: bars
            };
        });
    }

    function refreshStructure (){
        canvas.style({ width: canvasWidth + 'px', height: canvasHeight + 'px' });
        wrapper.attr({ transform: 'translate(' + [0, 0] + ')' });
        backgroundLayer.attr({ transform: 'translate(' + [0, paddingTop] + ')' });
        mainLayer.attr({ transform: 'translate(' + [paddingLeft, paddingTop + zeroPoint] + ')' });
        axisXLayer.attr({ transform: 'translate(' + [paddingLeft, paddingTop + innerHeight] + ')' });
        axisYLayer.attr({ transform: 'translate(' + [paddingLeft, paddingTop] + ')' });
    }

    function refreshClusters (){
        var clusters = mainLayer.selectAll('.bc-cluster').data(renderingData);

        clusters.enter().append('g').classed('bc-cluster', true);

        clusters.exit().remove();

        clusters.attr({
            transform: function(d, i){
                refreshBars(d, d3.select(this));
                return 'translate(' + ([clusterSize * i, 0]) + ')';
            }
        });
    }

    function refreshBars (d, cluster){
        var bars = cluster.selectAll('.bc-bar').data(d.bars),
            offset = (clusterSize - barSize) / 2;

        bars.enter().append('rect').classed('bc-bar', true);

        bars.exit().remove();

        bars.attr({
            x: offset,
            y: function(b){
                return b.isLowerThenZero ? 0 : -barSizeScale(b.size);
            },
            width: barSize,
            height: function(b){
                return barSizeScale(b.size);
            },
            fill: function(b){
                return b.color;
            },
            'stroke-width': 0.5,
            stroke: '#FFF'
        });
    }

    function refreshAxisX (){
        var labelProperty = chart.settings.keys.label,
            ticks = axisXLayer.selectAll('.tick').data(renderingData.map(function(d){
                return d[labelProperty];
            }));

        ticks.enter().append('text').classed('tick', true);

        ticks.exit().remove();

        ticks
            .attr({
                fill: '#c5c5c7',
                'font-size': chart.settings.fontSize,
                'text-anchor': 'middle',
                transform: function(d, i){
                    return 'translate(' + [(clusterSize * i) + (clusterSize * 0.5), chart.settings.fontSize + 2] + ')';
                }
            })
            .text(function(d){ return d; });
    }

    function refreshAxisY (){
        var d3Axis = d3.svg.axis()
            .scale(renderingScale)
            .orient('left');

        axisYLayer.call(d3Axis);

        axisYLayer.select('path').attr({ fill: 'none' });

        axisYLayer.selectAll('.tick')
            .select('text')
            .attr({
                fill: '#c5c5c7',
                'font-size': chart.settings.fontSize
            })
            .text(function(d, i){
                return !chart.settings.showAxisX || (chart.settings.showAxisX && !i) ? '' : d;
            });

        axisYLayer.selectAll('.tick').select('line')
            .attr({
                x1: 0,
                x2: function(d, i){
                    return !i || !d ? 0 : innerWidth;
                },
                y1: 0,
                y2: 0,
                stroke: '#c5c5c7',
                'stroke-width': 0.5,
                'stroke-dasharray': '2,2'
            });
    }

    function refreshBackground (){
        if(!backgroundLayer.select('text').node()){
            backgroundLayer.append('text').classed('axis-x-label', true);
            backgroundLayer.append('text').classed('axis-y-label', true);
            backgroundLayer.append('line').classed('base-line', true);
            backgroundLayer.append('line').classed('zero-line', true);
        }

        backgroundLayer.select('.axis-x-label')
            .attr({
                fill: '#262626',
                'font-size': chart.settings.fontSize,
                'font-weight': 'bold',
                'text-anchor': 'middle',
                transform: 'translate(' + [paddingLeft + (innerWidth * 0.5), innerHeight + axisXHeight + chart.settings.fontSize] + ')'
            })
            .text(chart.settings.axisXName);

        backgroundLayer.select('.axis-y-label')
            .attr({
                fill: '#262626',
                'font-size': chart.settings.fontSize,
                'font-weight': 'bold',
                'text-anchor': 'middle',
                transform: 'translate(' + [chart.settings.fontSize + 2, innerHeight * 0.5] + ') rotate(-90)'
            })
            .text(chart.settings.axisYName);

        backgroundLayer.select('.base-line')
            .attr({
                transform: 'translate(' + [paddingLeft, 0] + ')',
                x1: -6,
                x2: innerWidth,
                y1: innerHeight,
                y2: innerHeight,
                stroke: '#dbe0e6',
                'stroke-width': 1
            });

        backgroundLayer.select('.zero-line')
            .attr({
                transform: 'translate(' + [paddingLeft, 0] + ')',
                x1: 0,
                x2: innerWidth,
                y1: zeroPoint,
                y2: zeroPoint,
                stroke: '#dbe0e6',
                'stroke-width': 1
            });
    }

    function refreshChartBlock (){
        return chartHeader.node().outerHTML + canvas.node().outerHTML;
    }
}

function getGrid (grid, chartBlock){
    var lineHeight = grid.settings.fontSize + 2,
        rowPadding = {
            top: 5,
            right: 10,
            bottom: 5,
            left: 10
        },
        columnWidth = 0,
        colsData = getColsRenderingData(),
        rowsData = getRowsRenderingData();

    return getPagesRenderingData();

    function getColsRenderingData (){
        return grid.cols.map(function(col, i){
                return col.hidden || col.name === grid.settings.groupBy ?
                    null :
                {
                    entity: col,
                    label: col.label,
                    width: (col.width / 100) * width,
                    align: col.align || 'left',
                    html: '' +
                    '<div class="grid-cell-wrapper">' +
                        '<span>' + col.label + '</span>' +
                    '</div>' +
                    ''
                };
            })
            .filter(function(col){
                return col;
        });
    }

    function getRowsRenderingData (){
        var settings = turnToObject(grid.cols, 'name'),
            groups = getItemsDividedByGroups(),
            rows = [],
            totalsRow = {},
            defaultAlign = 'left',
            defaultType = 'string',
            defaultFormat = 'MMM Do YYYY',
            propertiesForSumCalculation = [],
            grandTotals = {},
            numbRegExp = /^float#(\d)$/,
            key;

        for(key in settings) {
            if(settings.hasOwnProperty(key)) {
                var colSettings = settings[key];
                colSettings.align = colSettings.align || defaultAlign;
                colSettings.type = colSettings.type || defaultType;
                colSettings.format = colSettings.format || defaultFormat;
                colSettings.before = colSettings.before || '';
                colSettings.after = colSettings.after || '';
                colSettings.showTotal = colSettings.showTotal || false;

                if(colSettings.showTotal && propertiesForSumCalculation.indexOf(key) === -1){
                    propertiesForSumCalculation.push(key);
                    grandTotals[key] = 0;
                }
                if(colSettings.name === grid.settings.groupBy) {
                    colSettings.hidden = true;
                }
            }
        }

        groups.forEach(function(group){
            group.totals = {};
            group.rows = [];

            propertiesForSumCalculation.forEach(function(prop){
                group.totals[prop] = 0;
            });

            group.items.forEach(function(row, i){
                var cells = [],
                    rowHeight = 0,
                    kkey;

                for(kkey in settings){
                    if(settings.hasOwnProperty(kkey) && !settings[kkey].hidden){
                        var cellSettings = settings[kkey],
                            value = row[kkey],
                            cellType = cellSettings.type,
                            cellWidth = (cellSettings.width / 100) * width,
                            str = '',
                            label = '';

                        if(cellSettings.getSum) {
                            value = 0;
                            cellSettings.getSum.forEach(function(prop){
                                value += parseFloat(row[prop]);
                            });
                        }

                        if(group.totals.hasOwnProperty(kkey)) {
                            var floatingValue = parseFloat(value);
                            group.totals[kkey] += floatingValue;
                            grandTotals[kkey] += floatingValue;
                        }

                        if(cellType === 'string') {
                            str = value;
                        } else if(cellType === 'bigNumber'){
                            str = parseInt(value);
                        } else if(cellType === 'date') {
                            str = moment(value).format(cellSettings.format);
                        } else if(numbRegExp.test(cellType)){
                            str = parseFloat(value).toFixed(numbRegExp.exec(cellType)[1])
                        }

                        label = cellSettings.before + str + cellSettings.after;

                        var labelWidth = ((label.length * (grid.settings.fontSize * defaultSettings.widthToHeightRelation)) + (rowPadding.left + rowPadding.right)),
                            cellHeight = Math.ceil(labelWidth / cellWidth) * lineHeight * 1.5;

                        rowHeight = d3.max([rowHeight, cellHeight]);

                        cells.push({
                            align: cellSettings.align,
                            label: label,
                            colspan: 1,
                            html: '' +
                            '<div class="grid-cell-wrapper">' +
                                '<span>' + label + '</span>' +
                            '</div>' +
                            ''
                        });
                    }
                }

                group.rows.push({
                    rowClass: i % 2 ? 'odd-row' : 'even-row',
                    entity: row,
                    height: rowHeight,
                    cells: cells
                });
            });
        });

        groups = groups.filter(function(group){
            var isEmpty = group.rows.length === 0;

            if(!isEmpty) {
                var groupHeaderRow = {
                        rowClass: 'group-header',
                        height: lineHeight + (rowPadding.top + rowPadding.bottom),
                        cells: [{
                            align: 'left',
                            label: group.label,
                            colspan: Object.keys(settings).length,
                            html: '' +
                            '<div class="grid-cell-wrapper">' +
                                '<span>' + group.label + '</span>' +
                            '</div>' +
                            ''
                        }]
                    },
                    groupFooterRow = {
                        rowClass: 'group-footer',
                        height: lineHeight + (rowPadding.top + rowPadding.bottom),
                        cells: Object.keys(settings).map(function(prop){
                            var colSettings = settings[prop],
                                str = group.totals[prop] || null;

                            if(numbRegExp.test(colSettings.type)){
                                str = parseFloat(str).toFixed(numbRegExp.exec(colSettings.type)[1]);
                            }

                            return {
                                align: colSettings.align,
                                hidden: colSettings.hidden,
                                label: colSettings.showTotal ? (colSettings.before + str + colSettings.after) : '',
                                colspan: 1,
                                html: '' +
                                '<div class="grid-cell-wrapper">' +
                                    '<span>' + (colSettings.showTotal ? (colSettings.before + str + colSettings.after) : '') + '</span>' +
                                '</div>' +
                                ''
                            };
                        }).filter(function(cell){
                            return !cell.hidden;
                        })
                    };

                group.rows = [groupHeaderRow]
                    .concat(group.rows)
                    .concat(groupFooterRow);
            }

            return !isEmpty;
        });

        if(Object.keys(grandTotals).length){
            groups.push({
                label: '%GRAND_TOTALS%',
                height: lineHeight + (rowPadding.top + rowPadding.bottom),
                cells: Object.keys(settings).map(function(prop, i){
                    var colSettings = settings[prop],
                        str = grandTotals.hasOwnProperty(prop) ? grandTotals[prop] : null;

                    if(numbRegExp.test(colSettings.type)){
                        str = parseFloat(str).toFixed(numbRegExp.exec(colSettings.type)[1]);
                    }

                    return {
                        align: !i ? 'left' : colSettings.align,
                        hidden: colSettings.hidden,
                        label: colSettings.showTotal ? (colSettings.before + str + colSettings.after) : (!i ? 'Totals:' : ''),
                        colspan: 1
                    };
                }).filter(function(cell){
                    return !cell.hidden;
                })
            });
        }

        groups.forEach(function(group){
            if(group.label !== '%GRAND_TOTALS%') {
                rows = rows.concat(group.rows);
            }/* else {
                totalsRow = group;
            }*/
        });

        return rows;
    }

    function getPagesRenderingData (){
        var tablePages = [],
            pageHeader = getPageHeader(),
            tableHeader = getTableHeader(),
            maxPageHeight = height - getRowHeight() - defaultSettings.pageHeaderHeight,
            numberOfPage = 0,
            currentHeight = 0,
            placeChartOnSeparatePage = true,
            i = 0, j = 0;

        for(; i < rowsData.length ; i++ ){
            var row = rowsData[i],
                rowHeight = getRowHeight(row.rowClass) + (rowPadding.top + rowPadding.bottom);

            if(currentHeight + rowHeight > maxPageHeight) {
                currentHeight = rowHeight;
                numberOfPage++;
            } else {
                currentHeight += rowHeight;
            }
            row.page = numberOfPage;
        }

        placeChartOnSeparatePage = maxPageHeight - currentHeight < 350;

        for( j = 0; j <= numberOfPage ; j++ ){
            tablePages.push(rowsData.filter(function(row){
                return row.page === j;
            }));
        }

        var renderingData = tablePages.map(function(pageRows, n){
            var pageCanvas = d3.select(document.createElement('div'))
                .style({
                    width: defaultSettings.pageWidth + 'px',
                    height: defaultSettings.pageHeight + 'px',
                    padding: defaultSettings.padding.top + 'px ' + defaultSettings.padding.right + 'px ' + defaultSettings.padding.bottom + 'px ' + defaultSettings.padding.left + 'px',
                    background: '#FFF'
                }),
                pageInner = pageCanvas.append('div')
                    .style({
                        width: width + 'px'
                    }),
                table = pageInner.append('table')
                    .style({
                        width: '100%',
                        'table-layout': 'fixed'
                    })
                    .html(pageHeader + tableHeader + getTableInner(pageRows));

            if(!placeChartOnSeparatePage && n === numberOfPage) {
                pageInner.append('div').html(chartBlock);
            }

            return {
                index: n,
                html: pageCanvas.node().outerHTML
            };
        });

        if(placeChartOnSeparatePage) {
            var pageCanvas = d3.select(document.createElement('div'))
                .style({
                    width: defaultSettings.pageWidth + 'px',
                    height: defaultSettings.pageHeight + 'px',
                    padding: defaultSettings.padding.top + 'px ' + defaultSettings.padding.right + 'px ' + defaultSettings.padding.bottom + 'px ' + defaultSettings.padding.left + 'px',
                    background: '#FFF'
                }),
                pageInner = pageCanvas.append('div')
                    .style({
                        width: width + 'px'
                    });

            pageInner.html(pageHeader + chartBlock);

            renderingData.push({
                index: numberOfPage + 1,
                html: pageCanvas.node().outerHTML
            });
        }

        return renderingData;
    }

    function getTableInner (pageRows){
        var tableBody = d3.select(document.createElement('tbody')),
            tableRows = tableBody.selectAll('tr').data(pageRows),
            calculatedPadding = rowPadding.top + 'px ' + rowPadding.right + 'px ' + rowPadding.bottom + 'px ' + rowPadding.left + 'px';

        tableRows.enter().append('tr');

        tableRows
            .attr({
                class: function(d){
                    return 'grid-row ' + d.rowClass;
                }
            })
            .style({
                width: columnWidth + 'px',
                background: function(d){
                    var row = d3.select(this),
                        cells = row.selectAll('.row-cell').data(d.cells);

                    cells.enter().append('td');

                    cells
                        .attr({
                            class: 'row-cell',
                            colspan: function(c){
                                return c.colspan;
                            }
                        })
                        .style({
                            'vertical-align': 'top',
                            height: getRowHeight(d.rowClass) + 'px',
                            color: d.rowClass === 'group-header' ? '#01539d' : '#757676',
                            padding: calculatedPadding,
                            'font-size': grid.settings.fontSize + 'px',
                            'font-weight': d.rowClass === 'group-header' || d.rowClass === 'group-footer' ? 600 : 400,
                            'border-top': d.rowClass === 'group-footer' ? '1px dashed #e5e9ef' : 'none',
                            'line-height': lineHeight + 'px',
                            overflow: 'hidden',
                            'text-overflow': 'ellipsis',
                            'text-align': function(c){
                                return c.align;
                            }
                        })
                        .html(function(c){
                            return c.html;
                        });

                    cells.select('.grid-cell-wrapper')
                        .style({
                            height: getRowHeight(d.rowClass) + 'px',
                            overflow: 'hidden',
                            'text-overflow': 'ellipsis'
                        });

                    return d.rowClass === 'odd-row' ? '#fafafa' : '#fff';
                }
            });

        return tableBody.node().outerHTML;
    }

    function getTableHeader (){
        var tableHeader = d3.select(document.createElement('thead')).append('tr'),
            headerCells = tableHeader.selectAll('td').data(colsData),
            calculatedPadding = rowPadding.top + 'px ' + rowPadding.right + 'px ' + rowPadding.bottom + 'px ' + rowPadding.left + 'px';

        headerCells.enter().append('td');

        headerCells
            .style({
                height: getRowHeight() + 'px',
                width: function(d){
                    return d.width + 'px';
                },
                padding: calculatedPadding,
                'font-size': grid.settings.fontSize + 'px',
                'font-weight': 600,
                'line-height': lineHeight + 'px',
                'text-align': function(d){
                    return d.align;
                },
                'border-bottom': '1px dashed #e5e9ef'
            })
            .html(function(d){
                return d.html;
            });

        return tableHeader.node().outerHTML;
    }

    function getPageHeader (){
        return d3.select(document.createElement('div'))
            .style({
                'font-size': '16px',
                'font-weight': 600,
                'text-align': 'left',
                'line-height': defaultSettings.pageHeaderHeight + 'px',
                padding: '0 10px',
                background: '#e5e9ef'
            })
            .html(grid.header).node().outerHTML;
    }

    function getRowHeight (className){
        var n = {
            'odd-row': 3,
            'even-row': 3,
            'group-header': 1,
            'group-footer': 1
        }[className];

        return (lineHeight * (className ? n : 2)) + (className ? 0 : (rowPadding.top + rowPadding.bottom));
    }

    function getItemsDividedByGroups (){
        var items = grid.rows,
            groupingProperty = grid.settings.groupBy,
            vals = [],
            groups = [];

        if(!groupingProperty) {
            groups.push({
                label: 'All',
                items: items
            });
        } else {
            items.forEach(function(item){
                var val = item[groupingProperty],
                    index = vals.indexOf(val);

                if(index === -1) {
                    vals.push(val);
                    groups.push({
                        label: val,
                        items: [item]
                    });
                } else {
                    groups[index].items.push(item);
                }
            });
        }

        return groups;
    }

    function turnToObject (items, prop){
        var _items = {};
        items.forEach(function(item){
            _items[item[prop]] = item;
        });
        return _items;
    }
}

function getTitlePage (reportId){
    var titlePageCanvas = d3.select(document.createElement('div')),
        titlePageInner = titlePageCanvas.append('div'),
        reportDate = moment(new Date().getTime()).format('Do of MMMM YYYY, HH:mm:ss'),
        reportAuthor = 'T`chirasak Rackponmyang';


    return buildTitlePage();

    function buildTitlePage (){
        titlePageCanvas.style({
            width: defaultSettings.pageWidth + 'px',
            height: defaultSettings.pageHeight + 'px',
            padding: defaultSettings.padding.top + 'px ' + defaultSettings.padding.right + 'px ' + defaultSettings.padding.bottom + 'px ' + defaultSettings.padding.left + 'px',
            background: '#FFF'
        });

        titlePageInner
            .style({
                width: width + 'px',
                height: height + 'px',
                padding: '250px 150px',
                'text-align': 'left',
                'border-left': '40px solid #999'
            })
            .html('' +
                '<h1>REPORT ' + reportId + '</h1>' +
                '<h3>' +
                    '<i>Created: ' + reportDate + '</i></br>' +
                    '<i>Author:  ' + reportAuthor + '</i></br>' +
                '</h3>' +
            '');

        titlePageInner.select('h1').style({
            'text-decoration': 'underline'
        });

        return {
            index: 0,
            html: titlePageCanvas.node().outerHTML
        };
    }
}


function generateReportId (){
    var numberOfSymbols = 12,
        i = 0,
        str = '',
        symbols = '0123456789ABSFIRGJMQYRGJASDHR'.split(''),
        symbLen = symbols.length;

    for(; i < numberOfSymbols ; i++){
        str += symbols[parseInt(Math.random() * symbLen)];
    }
    return '#' + str;
}

