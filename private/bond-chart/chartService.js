var d3 = require('d3'),
    document = require('jsdom').jsdom(),
    fs = require('fs'),
    htmlToPdf = require('html-to-pdf'),
    formats = {
        'A4_72': [793, 1122],
        'A4_200': [1654, 2339],
        'A4_300': [2480,3508],
        'A4_400': [3307, 4677],
        'A4_600': [4961, 7016],
        'A4_1200': [9921, 14031],
        'Letter_72': [612, 792],
        'Letter_200': [1700, 2200],
        'Letter_300': [2550, 3300],
        'Letter_400': [3400, 4400],
        'Letter_600': [5100, 6600],
        'Letter_1200': [10200, 13200]
    },
    selectedFormat = [0, 0],
    colors = {
        black: 'rgba(0, 0, 0, 1)',
        grayDarkest: 'rgba(32, 32, 32, 1)',
        grayDarker: 'rgba(64, 64, 64, 1)',
        grayDark: 'rgba(96, 96, 96, 1)',
        gray: 'rgba(128, 128, 128, 1)',
        grayLight: 'rgba(164, 164, 164, 1)',
        grayLighter: 'rgba(196, 196, 196, 1)',
        grayLightest: 'rgba(224, 224, 224, 1)',
        white: 'rgba(255, 255, 255, 1)',
        transparent: 'rgba(0, 0, 0, 0)'
    };

function getNewHtmlPage (clientData, numberOfPage, pageTitle, rowsPerPage){
    var padding = {
            top: 20,
            right: 50,
            bottom: 20,
            left: 50
        },
        headerHeight = 30,
        footerHeight = 20,
        headerMargin = 10,
        footerMargin = 10,
        pageInnerSize = [
            selectedFormat[0] - (padding.right + padding.left),
            selectedFormat[1] - (padding.top + padding.bottom) - (headerHeight + footerHeight) - (headerMargin + footerMargin)
        ],
        textColorLight = colors.white,
        textColorDark = colors.grayDark,
        pageWrapper = getPageWrapper(),
        pageInner = getPageInner(),
        blocks = getBlocks(),
        header = getHeader(),
        footer = getFooter();

    function getPageWrapper (){
        return d3.select(document.createElement('div')).style({
            'padding-top': padding.top + 'px',
            'padding-right': padding.right + 'px',
            'padding-bottom': padding.bottom + 'px',
            'padding-left': padding.left + 'px',
            width: selectedFormat[0],
            height: selectedFormat[1]
        });
    }

    function getPageInner (){
        return d3.select(document.createElement('div')).style({
            width: pageInnerSize[0] + 'px',
            height: pageInnerSize[1] + 'px'
        });
    }

    function getBlocks (){
        return clientData.map(function(datum){
            var dataToRender = datum.renderingData,
                settings = datum.settings,
                blockWidth = pageInnerSize[0] / 12 * settings.width,
                blockHeight = pageInnerSize[1] / rowsPerPage,
                blockPadding = 1,
                blockHeaderHeight = 27,
                blockCanvasWidth = blockWidth - (blockPadding * 2),
                blockCanvasHeight = blockHeight - (blockPadding * 2) - blockHeaderHeight,
                block = d3.select(document.createElement('div')).classed('widget-block', true),
                blockInner = block.append('div').classed('widget-block-inner', true),
                blockHeader = blockInner.append('div').classed('widget-block-header', true),
                blockCanvas = blockInner.append('svg').classed('widget-block-canvas', true);

            block.style({
                width: blockWidth + 'px',
                height: blockHeight + 'px',
                padding: blockPadding + 'px',
                float: 'left'
            });

            blockInner.style({
                width: (blockWidth - (blockPadding * 2)) + 'px',
                height: (blockHeight - (blockPadding * 2)) + 'px',
                'border-width': '1px',
                'border-style': 'solid',
                'border-color': textColorDark
            });

            blockHeader
                .style({
                    background: textColorDark,
                    color: textColorLight,
                    height: blockHeaderHeight + 'px',
                    width: blockWidth - (padding * 2) + 'px',
                    padding: '0px 5px',
                    'font-size': '18px'
                })
                .html('<span>' + settings.name + '</span>');

            blockCanvas
                .attr({
                    width: blockCanvasWidth,
                    height: blockCanvasHeight
                });

            if(settings.type === 'pie' || settings.type === 'donut') {
                drawRoundChart(blockCanvas, dataToRender, settings, [blockCanvasWidth, blockCanvasHeight]);
            } else if(settings.type === 'linear' || settings.type === 'area'){
                drawLinearChart(blockCanvas, dataToRender, settings, [blockCanvasWidth, blockCanvasHeight]);
            } else if(settings.type === 'bar'){
                drawBarChart(blockCanvas, dataToRender, settings, [blockCanvasWidth, blockCanvasHeight]);
            }

            return block.node().outerHTML;
        }).join('');
    }

    function getHeader (){
        return d3.select(document.createElement('div')).style({
            height: headerHeight,
            'font-size': '18px',
            'border-bottom-width': '2px',
            'border-bottom-style': 'solid',
            'border-bottom-color': textColorDark,
            'margin-bottom': headerMargin + 'px'
        }).html('<b>' + (pageTitle + 'THIS IS THE PAGE HEADER') + '</b>');
    }

    function getFooter (){
        return d3.select(document.createElement('div')).style({
            height: footerHeight,
            'font-size': '12px',
            'border-top-width': '2px',
            'border-top-style': 'solid',
            'border-top-color': textColorDark,
            'margin-top': footerMargin + 'px',
            'text-align': 'center'
        }).html('<b>PAGE: ' + (numberOfPage + 1) + '</b>');
    }

    function drawRoundChart (blockCanvas, dataToRender, settings, sizes){
        var layout = blockCanvas.append('g').classed('round-chart-layout', true),
            wrapper = layout.append('g').classed('round-chart-slices-wrapper', true),
            donutChartTypeSelected = settings.type === 'donut',
            showInsetLegend = settings.roundChart.displayInset,
            layoutSize = d3.min(sizes),
            pieChartPadding = layoutSize * 0.2,
            outerRadius = (layoutSize - (pieChartPadding * 2)) / 2,
            innerRadius = outerRadius * settings.roundChart.innerRadius * 0.1,
            legendFontSize = layoutSize * 0.04,
            arc = d3.svg.arc().innerRadius(donutChartTypeSelected ? innerRadius : 0).outerRadius(outerRadius),
            legendArc = d3.svg.arc().innerRadius(outerRadius).outerRadius(layoutSize / 2),
            slices = wrapper.selectAll('.round-chart-slice').data(dataToRender);

        layout.attr({
            transform: 'translate(' + (sizes[0] / 2) + ', ' + (sizes[1] / 2) + ')'
        });

        slices.enter().append('g').classed('round-chart-slice');

        slices.append('path').classed('round-chart-slice-color', true)
            .attr({
                stroke: textColorLight,
                'stroke-width': layoutSize * 0.005,
                fill: function(d){
                    return d.color;
                },
                d: arc
            });

        if(!showInsetLegend){
            slices.append('polyline')
                .attr({
                    stroke: textColorDark,
                    'stroke-width': 1,
                    'fill-opacity': 0,
                    points: function(d){
                        var legendSize = (sizes[0] - layoutSize) * 0.5,
                            startPoint = arc.centroid(d),
                            middlePoint = legendArc.centroid(d),
                            turnLeft = startPoint[0] < 0,
                            endPoint = [middlePoint[0] + (legendSize * (turnLeft ? -1 : 1)), middlePoint[1]];

                        return [startPoint, middlePoint, endPoint].join(' ');
                    }
                });

            slices.append('text')
                .attr({
                    'transform': function(d){
                        var legendSize = (sizes[0] - layoutSize) * 0.5,
                            anchorPoint = legendArc.centroid(d),
                            turnLeft = anchorPoint[0] < 0,
                            turnBottom = anchorPoint[1] < 0;

                        return 'translate(' + (anchorPoint[0] + (legendSize * (turnLeft ? -1 : 1))) + ', ' + (anchorPoint[1] + (turnBottom ? -5 : (legendFontSize + 2))) + ')';
                    },
                    'font-size': legendFontSize,
                    fill: textColorDark,
                    'text-anchor': function(d){
                        return legendArc.centroid(d)[0] < 0 ? 'start' : 'end';
                    }
                })
                .text(function(d){
                    return d.value;
                });

        } else {
            slices.append('text')
                .attr({
                    'transform': function(d){
                        return 'translate(' + arc.centroid(d) + ')';
                    },
                    'font-size': legendFontSize,
                    fill: textColorLight,
                    'text-anchor': 'middle'
                })
                .text(function(d){
                    return d.data.value;
                });
        }
    }

    function drawLinearChart (blockCanvas, dataToRender, settings, sizes){
        var linearChartLayout = blockCanvas.append('g').classed('linear-chart-layout', true),
            chartRanges = getChartRanges(),
            areaTypeIsSelected = settings.type === 'area',
            interpolation = settings.linearChart.interpolation,
            axisYWidth = 0,
            linearChartPadding = 10,
            layoutWidth = sizes[0] - (linearChartPadding * 2) - axisYWidth,
            layoutHeight = sizes[1] - (linearChartPadding * 2),
            renderingScales = {
                x: d3.scale.linear()
                    .domain(chartRanges.x)
                    .range([0, layoutWidth]),

                y: d3.scale[settings.linearChart.scale]()
                    .domain(chartRanges.y)
                    .range([layoutHeight, 0])
                    .nice(),

                testX: d3.scale.linear()
                    .domain([0, 100])
                    .range([0, layoutWidth]),

                testY: d3.scale.linear()
                    .domain([0, 100])
                    .range([0, layoutWidth])
            },
            line = d3.svg.line()
                .x(function(d) { return renderingScales.x(d.x); })
                .y(function(d) { return renderingScales.y(d.y); }),
            area = d3.svg.area()
                .x(function(d) { return renderingScales.x(d.x); })
                .y0(function(d){ return renderingScales.y(0); })
                .y1(function(d) { return renderingScales.y(d.y); })
                .interpolate(interpolation);

        linearChartLayout.attr({
            transform: 'translate(' + linearChartPadding + ', ' + linearChartPadding +')'
        });

        var layers = linearChartLayout.selectAll('.linear-chart-layer').data(dataToRender);

        layers.enter().append('g').classed('linear-chart-layer', true);

        if(areaTypeIsSelected){
            layers.append('path')
                .attr({
                    fill: function(d){
                        return d[0].color;
                    },
                    'fill-opacity': function(d){
                        return 0.5;
                    },
                    stroke: function(d){
                        return 'none';
                    },
                    d: function(d){
                        return area(d);
                    }
                });
        }

        layers.append('path').classed('linear-chart-layer', true)
            .attr({
                fill: 'none',
                stroke: function(d){
                    return d[0].color;
                },
                'stroke-width': 1.5,
                d: function(d){
                    return line(d);
                }
            });

        function getChartRanges (){
            var _ranges = {
                x: [],
                y: []
            };
            dataToRender.forEach(function(layer){
                var layerRanges = {
                    x: [d3.min(layer, function(d){ return d.x; }), d3.max(layer, function(d){ return d.x; })],
                    y: [d3.min(layer, function(d){ return d.y; }), d3.max(layer, function(d){ return d.y; })]
                };
                _ranges.x[0] = d3.min([_ranges.x[0],  layerRanges.x[0]]);
                _ranges.x[1] = d3.max([_ranges.x[1],  layerRanges.x[1]]);
                _ranges.y[0] = d3.min([_ranges.y[0],  layerRanges.y[0]]);
                _ranges.y[1] = d3.max([_ranges.y[1],  layerRanges.y[1]]);
            });
            _ranges.y[0] = _ranges.y[0] > 0 ? 0 : _ranges.y[0];

            return _ranges;
        }
    }

    function drawBarChart (blockCanvas, dataToRender, settings, sizes){
        var layout = blockCanvas.append('g').classed('bar-chart-layout', true),
            showComposite = settings.barChart.composite,
            barChartPadding = 10,
            fontSize = 12,
            clusterWidth = (sizes[0] - (barChartPadding * 2)) / dataToRender.length,
            clusterHeight = (sizes[1] - (barChartPadding * 2)),
            layoutWidth = clusterWidth * dataToRender.length,
            offsetX = ((sizes[0] - layoutWidth) / 2) + barChartPadding,
            scales = {
                height: d3.scale.linear()
                    .domain([0, 100])
                    .range([0, clusterHeight])
            },
            clusters = layout.selectAll('.bar-chart-cluster').data(dataToRender),
            barWidth = clusterWidth / (showComposite ? 1 : 4),
            margin = (barWidth * (settings.barChart.margin * 0.05));

        layout.attr({transform: 'translate(' + barChartPadding + ', ' + barChartPadding + ')'});

        clusters.enter().append('g').classed('bar-chart-cluster', true)
            .attr({
                transform: function(d, i){
                    return 'translate(' + (i * clusterWidth) + ', 0)';
                }
            });

        clusters[0].forEach(function(cluster){
            var _cluster = d3.select(cluster),
                datum = cluster.__data__,
                _bars = _cluster.selectAll('.bar-chart-cluster').data(datum);

            _bars.enter().append('g').classed('bar-chart-bars', true);

            _bars.attr({
                transform: function(d, i){
                    return 'translate(' + (showComposite ? 0 : (barWidth * i)) + ', 0)';
                }
            });

            _bars.append('rect')
                .attr({
                    x: margin,
                    y: function(d) {
                        return scales.height(d.offsetTop);
                    },
                    width: barWidth - (margin * 2),
                    height: function (d, i){
                        return scales.height(d.height);
                    },
                    fill: function (d, i){
                        return d.originalColor;
                    },
                    'fill-opacity': 0.8,
                    stroke: function(d){
                        return d.originalColor;
                    }
                });
        });
    }

    pageInner.html(blocks);

    pageWrapper.html(header.node().outerHTML + pageInner.node().outerHTML + footer.node().outerHTML);

    return {
        content: pageWrapper.node().outerHTML
    };
}

function getNewTable (clientData, numberOfPage, pageTitle, tableTitle){
    var tablePages = [],
        padding = {
            top: 20,
            right: 50,
            bottom: 20,
            left: 50
        },
        headerHeight = 30,
        footerHeight = 20,
        headerMargin = 10,
        footerMargin = 10,
        pageInnerSize = [
            selectedFormat[0] - (padding.right + padding.left),
            selectedFormat[1] - (padding.top + padding.bottom) - (headerHeight + footerHeight) - (headerMargin + footerMargin)
        ],
        textColorLight = colors.white,
        textColorDark = colors.grayDark,

        tableHeaderHeight = 52,
        tableRowHeight = 28,
        tableBodyHeight = pageInnerSize[1] - tableHeaderHeight,
        rowsPerPage = Math.floor(tableBodyHeight / tableRowHeight),
        numberOfPages = Math.ceil(clientData.body.length / rowsPerPage);

    for(var i = 0; i < numberOfPages ; i++ ){
        var fromIndex = rowsPerPage * i,
            toIndex = rowsPerPage * (i + 1);
        tablePages.push(getTablePage(clientData.body.slice(fromIndex, toIndex)));

        numberOfPage += 1;
    }

    function getPageWrapper (){
        return d3.select(document.createElement('div')).style({
            'padding-top': padding.top + 'px',
            'padding-right': padding.right + 'px',
            'padding-bottom': padding.bottom + 'px',
            'padding-left': padding.left + 'px',
            width: selectedFormat[0],
            height: selectedFormat[1]
        });
    }

    function getPageInner (){
        return d3.select(document.createElement('div')).style({
            width: pageInnerSize[0] + 'px',
            height: pageInnerSize[1] + 'px',
            'font-size': '12px!important'
        });
    }

    function getHeader (){
        return d3.select(document.createElement('div')).style({
            height: headerHeight,
            'font-size': '18px',
            'border-bottom-width': '2px',
            'border-bottom-style': 'solid',
            'border-bottom-color': textColorDark,
            'margin-bottom': headerMargin + 'px'
        }).html('<b>' + (pageTitle) +'</b>');
    }

    function getFooter (){
        return d3.select(document.createElement('div')).style({
            height: footerHeight,
            'font-size': '12px',
            'border-top-width': '2px',
            'border-top-style': 'solid',
            'border-top-color': textColorDark,
            'margin-top': footerMargin + 'px',
            'text-align': 'center'
        }).html('<b>PAGE: ' + (numberOfPage + 1) + '</b>');
    }

    function getTablePage (rows){
        var pageWrapper = getPageWrapper(),
            pageInner = getPageInner(),
            header = getHeader(),
            footer = getFooter(),
            table = d3.select(document.createElement('table')).style({'font-size': '12px!important'}),
            tableHeader = table.append('thead').append('tr').selectAll('td').data(clientData.header),
            tableBody = table.append('tbody').selectAll('tr').data(rows);

        table.style({ width: '100%'});

        tableHeader.enter()
            .append('td')
            .style({
                'white-space': 'nowrap',
                padding: '5px',
                'border-width': '1px',
                'border-style': 'solid',
                'border-color': textColorDark
            })
            .html(function(d){
                return d.htmlContent;
            });

        table.select('thead')
            .append('tr')
            .append('td')
            .attr({
                colspan: 1000
            })
            .html('<i>' + (tableTitle || '...Some text here...') + '</i>');

        tableBody.enter()
            .append('tr')
            .style({
                background: function(d, i){
                    return [textColorDark, textColorLight][i % 2];
                },
                color: function(d, i){
                    return [textColorLight, textColorDark][i % 2];
                }
            })
            .html(function(d){
                return d.htmlContent;
            });

        table.select('tbody').selectAll('tr').selectAll('td')
            .style({
                'white-space': 'nowrap',
                padding: '5px',
                'border-width': '1px',
                'border-style': 'solid',
                'border-color': '#000'
            });

        pageInner.html(table.node().outerHTML);
        pageWrapper.html(header.node().outerHTML + pageInner.node().outerHTML + footer.node().outerHTML);

        return {
            content: pageWrapper.node().outerHTML
        };
    }

    return tablePages;
}

exports._getNewHtmlDocument = function (clientData, docSettings){

    /**
     * Create title page - DONE
     * Add pages numeration - DONE
     * Define algorithm to divide blocks by pages - DONE
     * Add linearChart drawer - DONE
     * Add grid drawer - DONE
     * Add portrait/landscape switcher - DONE
     * Add format switcher - TODO
     *
     *
     * */
    var format = formats[docSettings.format];

    selectedFormat = docSettings.orientation === 'portrait' ? format : [format[1], format[0]];

    var pages = [buildTitlePage()],
        blocks = clientData.charts.map(function(block){
            block.width = block.settings.width || 12;
            return block;
        }),
        numberOfRows = blocks[blocks.length - 1].settings.row,
        rowsPerPage = docSettings.rowsPerPage || 3,
        numberOfPages = !numberOfRows ? 1 : Math.ceil(numberOfRows / rowsPerPage),
        i = 0;

    for(; i < numberOfPages; i++){
        var fromRow = i * rowsPerPage,
            toRow = fromRow + rowsPerPage;
        var _blocks = blocks.filter(function(block){
            return fromRow <= block.settings.row && block.settings.row < toRow;
        });
        pages.push(getNewHtmlPage(_blocks, i, docSettings.pageTitle || 'THIS IS THE PAGE HEADER', rowsPerPage  ));
    }

    pages = pages.concat(getNewTable(clientData.grids[0].renderingData, (pages.length - 1), docSettings.pageTitle, docSettings.tableTitle));

    function buildTitlePage (){
        var _titlePage = d3.select(document.createElement('div')).classed('title-page', true),
            titlePagePadding = 30;

        _titlePage
            .style({
                width: selectedFormat[0] - (titlePagePadding * 2),
                height: selectedFormat[1] - (titlePagePadding * 2),
                padding: titlePagePadding + 'px'
            })
            .html('' +
                '<b>' + (docSettings.name || 'THIS IS THE TITLE PAGE') + '</b>' +
                '<br><br><br>' +
                '<div><i>Created: ' + new Date() + '</i></div>' +
                '<div><i>Author: SomeAuthor</i></div>' +
            '');

        _titlePage.select('b')
            .style({
                'font-size': (selectedFormat[0] * 0.1) + 'px',
                'border-bottom-width': '5px',
                'border-bottom-style': 'solid',
                'border-bottom-color': colors.grayDark
            });

        _titlePage.selectAll('div')
            .style({
                'font-size': (selectedFormat[0] * 0.02) + 'px'
            });

        return {
            content: _titlePage.node().outerHTML,
            name: 'TITLE PAGE'
        };
    }

    return {
        documentName: docSettings.name || 'TEST DOCUMENT NAME',
        numberOfPages: pages.length,
        pages: pages,
        format: docSettings.format,
        orientation: docSettings.orientation,
        width: selectedFormat[0],
        height: selectedFormat[1]
    };
};

