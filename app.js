
/**
 * Module dependencies.
 */

var express = require('express');
var http = require('http');
var path = require('path');
var reportGenerator = require('./private/bw-report-generator/bw-report-generator.js').generateReport;
var app = express();
var gridData = require('./data/grid.json');
var chartData = require('./data/chart.json');

// all environments
app.set('port', process.env.PORT || 8081);
app.set('views', path.join(__dirname, ''));
app.set('indexPage', ('index.html'));
//app.use(favicon('favicon.ico'));
app.use(express.favicon(__dirname + '/public/img/favicon.ico'));
app.use(express.logger('dev'));
app.use(express.json());
app.use(express.urlencoded());
app.use(express.methodOverride());
app.use(app.router);
app.use(require('less-middleware')({ src: path.join(__dirname, '') }));
app.use(express.static(path.join(__dirname, '')));

// development only
if ('development' == app.get('env')) {
    app.use(express.errorHandler());
}

app.get('/', function(req, res){
    res.sendfile(app.get("indexPage"));
});
app.get('/api/gridData', function(req, res){
    res.send(gridData);
});
app.get('/api/chartData', function(req, res){
    res.send(chartData);
});

app.post('*', function(req, res){
    res.send(reportGenerator(req.body.data));

    //res.send(chartService._getNewHtmlDocument(req.body.data.items, (req.body.data.docSettings || {})));
});

http.createServer(app).listen(8080, function(){
    console.log("LET'S ROCK! DUCK TNE SUSTEM!!!");
});


